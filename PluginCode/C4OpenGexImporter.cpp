//=============================================================
//
// C4 Engine version 3.5.1
// Copyright 1999-2013, by Terathon Software LLC
//
// This file is part of the C4 Engine and is provided under the
// terms of the license agreement entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#include "C4OpenGexImporter.h"
#include "C4EditorSupport.h"
#include "C4ModelViewer.h"


using namespace C4;


/*
	struct $metric (id = "Metric", top_level = true)
	{
		property (id = "key")
		{
			type (string}
		}

		data
		{
			type {float, string}
			instances {int32 {1, 1}}
			elements {int32 {1, 1}}
		}
	}

	struct $name (id = "Name")
	{
		data
		{
			type {string}
			instances {int32 {1, 1}}
			elements {int32 {1, 1}}
		}
	}

	struct $object_ref (id = "ObjectRef")
	{
		data
		{
			type {ref}
			instances {int32 {1, 1}}
			elements {int32 {1, 1}}
		}
	}

	struct $material_ref (id = "MaterialRef")
	{
		property (id = "index")
		{
			type {int32}
			default {int32 {0}}
		}

		data
		{
			type {ref}
			instances {int32 {1, 1}}
			elements {int32 {1, 1}}
		}
	}

	struct $transform (id = "Transform")
	{
		property (id = "object")
		{
			type {bool}
			default {bool {false}}
		}

		data
		{
			type {float}
			instances {int32 {1, 1}}
			elements {int32 {1}}
			array_size {int32 {16, 16}}
		}
	}

	struct $translation (id = "Translation")
	{
		property (id = "object")
		{
			type {bool}
			default {bool {false}}
		}

		property (id = "kind")
		{
			type {string}
		}

		data
		{
			type {float}
			instances {int32 {1, 1}}
			elements {int32 {1}}
			array_size {int32 {0, 3}}
		}
	}

	struct $rotation (id = "Rotation")
	{ 
		property (id = "object")
		{
			type {bool} 
			default {bool {false}}
		} 
 
		property (id = "kind")
		{
			type {string}
		} 

		data
		{
			type {float}
			instances {int32 {1, 1}} 
			elements {int32 {1}}
			array_size {int32 {0, 4}}
		}
	}

	struct $scale (id = "Scale")
	{
		property (id = "object")
		{
			type {bool}
			default {bool {false}}
		}

		property (id = "kind")
		{
			type {string}
		}

		data
		{
			type {float}
			instances {int32 {1, 1}}
			elements {int32 {1}}
			array_size {int32 {0, 3}}
		}
	}

	struct $node (id = "Node", top_level = true)
	{
		sub
		{
			ref {$name}
			instances {int32 {0, 1}}
		}

		sub
		{
			ref {$transform, $translation, $rotation, $scale}
		}

		sub
		{
			ref {$animation}
		}

		sub
		{
			ref {$node, $bone_node, $geometry_node, $light_node, $camera_node}
		}
	}

	struct $bone_node (id = "BoneNode")
	{
		base {ref {$node}}
	}

	struct $geometry_node (id = "GeometryNode")
	{
		base {ref {$node}}

		property (id = "visible")
		{
			type {bool}
			default {bool {true}}
		}

		property (id = "shadow")
		{
			type {bool}
			default {bool {true}}
		}

		property (id = "motion_blur")
		{
			type {bool}
			default {bool {true}}
		}

		sub
		{
			ref ($object_ref}
			instances {int32 {1, 1}}
		}

		sub
		{
			ref ($material_ref}
		}
	}

	struct $light_node (id = "LightNode")
	{
		base {ref {$node}}

		sub
		{
			ref ($object_ref}
			instances {int32 {1, 1}}
		}
	}

	struct $camera_node (id = "CameraNode")
	{
		base {ref {$node}}

		sub
		{
			ref ($object_ref}
			instances {int32 {1, 1}}
		}
	}

	struct $vertex_array (id = "VertexArray")
	{
		property (id = "attrib")
		{
			type {string}
		}

		property (id = "morph")
		{
			type {unsigned_int32}
			default {unsigned_int32 {0}}
		}

		data
		{
			type {int8, int16, int32, int64, unsigned_int8, unsigned_int16, unsigned_int32, unsigned_int64, float, double}
			instances {int32 {1, 1}}
			array_size {int32 {1, 4}}
		}
	}

	struct $index_array (id = "IndexArray")
	{
		property (id = "material")
		{
			type {int32}
			default {int32 {0}}
		}

		data
		{
			type {unsigned_int8, unsigned_int16, unsigned_int32, unsigned_int64}
			instances {int32 {1, 1}}
			array_size {int32 {1, 3}}
		}
	}

	struct $bone_ref_array (id = "BoneRefArray")
	{
		data
		{
			type {ref}
			instances {int32 {1, 1}}
		}
	}

	struct $bone_count_array (id = "BoneCountArray")
	{
		data
		{
			type {unsigned_int8, unsigned_int16, unsigned_int32, unsigned_int64}
			instances {int32 {1, 1}}
		}
	}

	struct $bone_index_array (id = "BoneIndexArray")
	{
		data
		{
			type {unsigned_int8, unsigned_int16, unsigned_int32, unsigned_int64}
			instances {int32 {1, 1}}
		}
	}

	struct $bone_weight_array (id = "BoneWeightArray")
	{
		data
		{
			type {float, double}
			instances {int32 {1, 1}}
		}
	}

	struct $skeleton (id = "Skeleton")
	{
		sub
		{
			ref {$bone_ref_array}
			instances {int32 {1, 1}}
		}

		sub
		{
			ref {$transform}
			instances {int32 {1, 1}}
		}
	}

	struct $skin (id = "Skin")
	{
		sub
		{
			ref {$transform}
			instances {int32 {0, 1}}
		}

		sub
		{
			ref {$skeleton}
			instances {int32 {1, 1}}
		}

		sub
		{
			ref {$bone_count_array, $bone_index_array, $bone_weight_array}
			instances {int32 {1, 1}}
		}
	}

	struct $mesh (id = "Mesh")
	{
		property (id = "lod")
		{
			type {int32}
			default {int32 {0}}
		}

		property (id = "primitive")
		{
			type {string}
			default {string {"triangles"}}
		}

		sub
		{
			ref {$vertex_array, $index_array}
		}

		sub
		{
			ref {$skin}
			instances {int32 {0, 1}}
		}
	}

	struct $geometry_object (id = "GeometryObject", top_level = true)
	{
		sub
		{
			ref {$mesh}
			instances {int32 {1}}
		}
	}

	struct $param (id = "Param")
	{
		property (id = "attrib")
		{
			type {string}
		}

		data
		{
			type {float}
			instances {int32 {1, 1}}
			elements {int32 {1, 1}}
		}
	}

	struct $color (id = "Color")
	{
		property (id = "attrib")
		{
			type {string}
		}

		data
		{
			type {float}
			instances {int32 {1, 1}}
			elements {int32 {1, 1}}
			array_size {int32 {3, 4}}
		}
	}

	struct $texture (id = "Texture")
	{
		property (id = "attrib")
		{
			type {string}
		}

		property (id = "texcoord")
		{
			type {int32}
			default {int32 {0}}
		}

		data
		{
			type {string}
			instances {int32 {1, 1}}
			elements {int32 {1, 1}}
		}

		sub
		{
			ref {$transform}
			instances {int32 {0, 1}}
		}
	}

	struct $material (id = "Material", top_level = true)
	{
		property (id = "two_sided")
		{
			type {bool}
			default {bool {false}}
		}

		sub
		{
			ref {$name}
			instances {int32 {0, 1}}
		}

		sub
		{
			ref {$param, $color, $texture}
		}
	}

	struct $atten (id = "Atten")
	{
		property (id = "kind")
		{
			type {string}
			default {string {"distance"}}
		}

		property (id = "curve")
		{
			type {string}
			default {string {"linear"}}
		}

		sub
		{
			ref {$param}
			instances {int32 {1, 2}}
		}
	}

	struct $light_object (id = "LightObject", top_level = true)
	{
		sub
		{
			ref {$param, $atten}
		}

		sub
		{
			ref {$color, $texture}
			instances {int32 {0, 1}}
		}
	}

	struct $camera_object (id = "CameraObject", top_level = true)
	{
		sub
		{
			ref {$param}
		}
	}

	struct $key (id = "Key")
	{
		property (id = "kind")
		{
			type {string}
			default {string {"value"}}
		}

		data
		{
			type {float}
			instances {int32 {1, 1}}
			array_size {int32 {0, 7}}
		}
	}

	struct $time (id = "Time")
	{
		property (id = "curve")
		{
			type {string}
			default {string {"linear"}}
		}

		sub
		{
			ref {$key}
			instances {int32 {1}}
		}
	}

	struct $value (id = "Value")
	{
		property (id = "curve")
		{
			type {string}
			default {string {"linear"}}
		}

		sub
		{
			ref {$key}
			instances {int32 {1}}
		}
	}

	struct $track (id = "Track")
	{
		property (id = "target")
		{
			type {ref}
		}

		sub
		{
			ref {$time}
			instances {int32 {1, 1}}
		}

		sub
		{
			ref {$value}
			instances {int32 {1, 1}}
		}
	}

	struct $animation (id = "Animation")
	{
		property (id = "clip")
		{
			type {int32}
			default {int32 {0}}
		}

		sub
		{
			ref {$track}
			instances {int32 {1}}
		}
	}
*/


namespace
{
	enum
	{
		kMaxAnimationBucketCount = 32
	};

	struct AnimationNode
	{
		const Node			*modelNode;
		NodeStructure		*nodeStructure;

		AnimationNode(const Node *node, NodeStructure *structure)
		{
			modelNode = node;
			nodeStructure = structure;
		}
	};

	struct AnimationHash
	{
		unsigned_int32		hashValue;
		int32				nodeIndex;

		AnimationHash(unsigned_int32 hash, int32 index)
		{
			hashValue = hash;
			nodeIndex = index;
		}
	};
}


OpenGexImporter *C4::TheOpenGexImporter = nullptr;


ResourceDescriptor OpenGexResource::descriptor("ogex", kResourceTerminatorByte);


C4::Plugin *ConstructPlugin(void)
{
	return (new OpenGexImporter);
}


OpenGexResource::OpenGexResource(const char *name, ResourceCatalog *catalog) : Resource<OpenGexResource>(name, catalog)
{
}

OpenGexResource::~OpenGexResource()
{
}


MetricStructure::MetricStructure() : Structure(kStructureMetric)
{
}

MetricStructure::~MetricStructure()
{
}

bool MetricStructure::ValidateProperty(const DataDescription *dataDescription, const String<>& identifier, DataType *type, void **value)
{
	if (identifier == "key")
	{
		*type = kDataString;
		*value = &metricKey;
		return (true);
	}

	return (false);
}

bool MetricStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	return (structure->GetBaseStructureType() == kStructurePrimitive);
}

DataResult MetricStructure::ProcessData(DataDescription *dataDescription)
{
	const Structure *structure = GetFirstSubnode();
	if (!structure)
	{
		return (kDataMissingSubstructure);
	}

	if (GetLastSubnode() != structure)
	{
		return (kDataExtraneousSubstructure);
	}

	if (metricKey == "distance")
	{
		if (structure->GetStructureType() != kDataFloat)
		{
			return (kDataInvalidDataFormat);
		}

		const DataStructure<FloatDataType> *dataStructure = static_cast<const DataStructure<FloatDataType> *>(structure);
		if (dataStructure->GetDataElementCount() != 1)
		{
			return (kDataInvalidDataFormat);
		}

		static_cast<OpenGexDataDescription *>(dataDescription)->SetDistanceScale(dataStructure->GetDataElement(0));
	}
	else if (metricKey == "angle")
	{
		if (structure->GetStructureType() != kDataFloat)
		{
			return (kDataInvalidDataFormat);
		}

		const DataStructure<FloatDataType> *dataStructure = static_cast<const DataStructure<FloatDataType> *>(structure);
		if (dataStructure->GetDataElementCount() != 1)
		{
			return (kDataInvalidDataFormat);
		}

		static_cast<OpenGexDataDescription *>(dataDescription)->SetAngleScale(dataStructure->GetDataElement(0));
	}
	else if (metricKey == "time")
	{
		if (structure->GetStructureType() != kDataFloat)
		{
			return (kDataInvalidDataFormat);
		}

		const DataStructure<FloatDataType> *dataStructure = static_cast<const DataStructure<FloatDataType> *>(structure);
		if (dataStructure->GetDataElementCount() != 1)
		{
			return (kDataInvalidDataFormat);
		}

		static_cast<OpenGexDataDescription *>(dataDescription)->SetTimeScale(dataStructure->GetDataElement(0));
	}
	else if (metricKey == "up")
	{
		int32	direction;

		if (structure->GetStructureType() != kDataString)
		{
			return (kDataInvalidDataFormat);
		}

		const DataStructure<StringDataType> *dataStructure = static_cast<const DataStructure<StringDataType> *>(structure);
		if (dataStructure->GetDataElementCount() != 1)
		{
			return (kDataInvalidDataFormat);
		}

		const String<>& string = dataStructure->GetDataElement(0);
		if (string == "z")
		{
			direction = 2;
		}
		else if (string == "y")
		{
			direction = 1;
		}
		else
		{
			return (kDataOpenGexInvalidUpDirection);
		}

		static_cast<OpenGexDataDescription *>(dataDescription)->SetUpDirection(direction);
	}

	return (kDataOkay);
}


VertexArrayStructure::VertexArrayStructure() : Structure(kStructureVertexArray)
{
	morphIndex = 0;

	arrayIndex = -1;
	arrayStorage = nullptr;
}

VertexArrayStructure::~VertexArrayStructure()
{
	delete[] arrayStorage;
}

bool VertexArrayStructure::ValidateProperty(const DataDescription *dataDescription, const String<>& identifier, DataType *type, void **value)
{
	if (identifier == "attrib")
	{
		*type = kDataString;
		*value = &arrayAttrib;
		return (true);
	}

	if (identifier == "morph")
	{
		*type = kDataUnsignedInt32;
		*value = &morphIndex;
		return (true);
	}

	return (false);
}

bool VertexArrayStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	return (structure->GetStructureType() == kDataFloat);
}

DataResult VertexArrayStructure::ValidateAttrib(Range<int32> *componentRange)
{
	enum
	{
		kArrayAttribCount = 6
	};

	static const char *const attribName[kArrayAttribCount] =
	{
		"position", "normal", "color", "tangent", "bitangent", "texcoord"
	};

	static const int8 attribIndex[kArrayAttribCount] =
	{
		kArrayPosition, kArrayNormal, kArrayColor, -1, -1, kArrayTexcoord
	};

	static const int8 minComponentCount[kArrayAttribCount] =
	{
		3, 3, 3, 3, 3, 2
	};

	static const int8 maxComponentCount[kArrayAttribCount] =
	{
		3, 3, 4, 3, 3, 2
	};

	const char *text = arrayAttrib;

	for (machine a = 0; a < kArrayAttribCount; a++)
	{
		const char *name = attribName[a];
		int32 length = Text::GetTextLength(name);

		if (Text::CompareText(arrayAttrib, name, length))
		{
			text += length;
			int32 c = text[0];
			if (c == 0)
			{
				arrayIndex = attribIndex[a];
				componentRange->Set(minComponentCount[a], maxComponentCount[a]);
				return (kDataOkay);
			}

			if (c == '[')
			{
				unsigned_int64		value;

				text++;
				if (Data::ReadUnsignedLiteral(text, &length, &value) == kDataOkay)
				{
					text += length;
					if ((text[0] == ']') && (text[1] == 0))
					{
						int32 index = attribIndex[a];
						if (value != 0)
						{
							if ((index == kArrayTexcoord) && (value < 2))
							{
								index += (int32) value;
							}
							else
							{
								index = -1;
							}
						}

						arrayIndex = index;
						componentRange->Set(minComponentCount[a], maxComponentCount[a]);
						return (kDataOkay);
					}
				}
			}

			break;
		}
	}

	return (kDataOpenGexUndefinedAttrib);
}

DataResult VertexArrayStructure::ProcessData(DataDescription *dataDescription)
{
	Range<int32>	componentRange;

	DataResult result = ValidateAttrib(&componentRange);
	if (result != kDataOkay)
	{
		return (result);
	}

	const Structure *structure = GetFirstSubnode();
	if (!structure)
	{
		return (kDataMissingSubstructure);
	}

	if (GetLastSubnode() != structure)
	{
		return (kDataExtraneousSubstructure);
	}

	const DataStructure<FloatDataType> *dataStructure = static_cast<const DataStructure<FloatDataType> *>(structure);

	int32 arraySize = dataStructure->GetArraySize();
	if ((arraySize < componentRange.min) || (arraySize > componentRange.max))
	{
		return (kDataInvalidDataFormat);
	}

	int32 elementCount = dataStructure->GetDataElementCount();
	const float *data = &dataStructure->GetDataElement(0);

	vertexCount = elementCount / arraySize;
	componentCount = arraySize;
	vertexArrayData = data;

	if (arrayIndex == kArrayPosition)
	{
		const OpenGexDataDescription *openGexDataDescription = static_cast<OpenGexDataDescription *>(dataDescription);
		float scale = openGexDataDescription->GetDistanceScale();
		int32 up = openGexDataDescription->GetUpDirection();

		if ((scale != 1.0F) || (up != 2))
		{
			Transform4D		transform;

			if (up == 2)
			{
				transform.Set(scale, 0.0F, 0.0F, 0.0F,
								0.0F, scale, 0.0F, 0.0F,
								0.0F, 0.0F, scale, 0.0F);
			}
			else
			{
				transform.Set(scale, 0.0F, 0.0F, 0.0F,
								0.0F, 0.0F, -scale, 0.0F,
								0.0F, scale, 0.0F, 0.0F);
			}

			arrayStorage = new char[vertexCount * sizeof(Point3D)];
			vertexArrayData = arrayStorage;

			const Point3D *inputPosition = reinterpret_cast<const Point3D *>(data);
			Point3D *outputPosition = reinterpret_cast<Point3D *>(arrayStorage);

			for (machine a = 0; a < vertexCount; a++)
			{
				outputPosition[a] = transform * inputPosition[a];
			}
		}
	}
	else if (arrayIndex == kArrayNormal)
	{
		const OpenGexDataDescription *openGexDataDescription = static_cast<OpenGexDataDescription *>(dataDescription);
		if (openGexDataDescription->GetUpDirection() != 2)
		{
			arrayStorage = new char[vertexCount * sizeof(Vector3D)];
			vertexArrayData = arrayStorage;

			const Vector3D *inputNormal = reinterpret_cast<const Vector3D *>(data);
			Vector3D *outputNormal = reinterpret_cast<Vector3D *>(arrayStorage);

			for (machine a = 0; a < vertexCount; a++)
			{
				const Vector3D& normal = inputNormal[a];
				outputNormal[a].Set(normal.x, -normal.z, normal.y);
			}
		}
	}
	else if (arrayIndex == kArrayColor)
	{
		if (arraySize == 3)
		{
			componentCount = 4;
			arrayStorage = new char[vertexCount * sizeof(ColorRGBA)];
			vertexArrayData = arrayStorage;

			const ColorRGB *inputColor = reinterpret_cast<const ColorRGB *>(data);
			ColorRGBA *outputColor = reinterpret_cast<ColorRGBA *>(arrayStorage);

			for (machine a = 0; a < vertexCount; a++)
			{
				outputColor[a] = inputColor[a];
			}
		}
	}

	return (kDataOkay);
}


IndexArrayStructure::IndexArrayStructure() : Structure(kStructureIndexArray)
{
	materialIndex = 0;
	restartIndex = 0;
	frontFace = "ccw";

	arrayStorage = nullptr;
}

IndexArrayStructure::~IndexArrayStructure()
{
	delete[] arrayStorage;
}

bool IndexArrayStructure::ValidateProperty(const DataDescription *dataDescription, const String<>& identifier, DataType *type, void **value)
{
	if (identifier == "material")
	{
		*type = kDataUnsignedInt32;
		*value = &materialIndex;
		return (true);
	}

	if (identifier == "restart")
	{
		*type = kDataUnsignedInt64;
		*value = &restartIndex;
		return (true);
	}

	if (identifier == "front")
	{
		*type = kDataString;
		*value = &frontFace;
		return (true);
	}

	return (false);
}

bool IndexArrayStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	StructureType type = structure->GetStructureType();
	return ((type == kDataUnsignedInt8) || (type == kDataUnsignedInt16) || (type == kDataUnsignedInt32) || (type == kDataUnsignedInt64));
}

DataResult IndexArrayStructure::ProcessData(DataDescription *dataDescription)
{
	const Structure *structure = GetFirstSubnode();
	if (!structure)
	{
		return (kDataMissingSubstructure);
	}

	if (GetLastSubnode() != structure)
	{
		return (kDataExtraneousSubstructure);
	}

	const PrimitiveStructure *primitiveStructure = static_cast<const PrimitiveStructure *>(structure);
	if (primitiveStructure->GetArraySize() != 3)
	{
		return (kDataInvalidDataFormat);
	}

	StructureType type = primitiveStructure->GetStructureType();
	if (type == kDataUnsignedInt16)
	{
		const DataStructure<UnsignedInt16DataType> *dataStructure = static_cast<const DataStructure<UnsignedInt16DataType> *>(primitiveStructure);
		triangleCount = dataStructure->GetDataElementCount() / 3;
		triangleArray = reinterpret_cast<const Triangle *>(&dataStructure->GetDataElement(0));
	}
	else if (type == kDataUnsignedInt8)
	{
		const DataStructure<UnsignedInt8DataType> *dataStructure = static_cast<const DataStructure<UnsignedInt8DataType> *>(primitiveStructure);
		int32 elementCount = dataStructure->GetDataElementCount();
		triangleCount = elementCount / 3;

		const unsigned_int8 *data = &dataStructure->GetDataElement(0);
		arrayStorage = new unsigned_int16[elementCount];
		triangleArray = reinterpret_cast<const Triangle *>(arrayStorage);

		for (machine a = 0; a < elementCount; a++)
		{
			arrayStorage[a] = data[a];
		}
	}
	else if (type == kDataUnsignedInt32)
	{
		const DataStructure<UnsignedInt32DataType> *dataStructure = static_cast<const DataStructure<UnsignedInt32DataType> *>(primitiveStructure);
		int32 elementCount = dataStructure->GetDataElementCount();
		triangleCount = elementCount / 3;

		const unsigned_int32 *data = &dataStructure->GetDataElement(0);
		arrayStorage = new unsigned_int16[elementCount];
		triangleArray = reinterpret_cast<const Triangle *>(arrayStorage);

		for (machine a = 0; a < elementCount; a++)
		{
			unsigned_int32 index = data[a];
			if (index > 65535)
			{
				return (kDataOpenGexIndexValueUnsupported);
			}

			arrayStorage[a] = (unsigned_int16) index;
		}
	}
	else
	{
		const DataStructure<UnsignedInt64DataType> *dataStructure = static_cast<const DataStructure<UnsignedInt64DataType> *>(primitiveStructure);
		int32 elementCount = dataStructure->GetDataElementCount();
		triangleCount = elementCount / 3;

		const unsigned_int64 *data = &dataStructure->GetDataElement(0);
		arrayStorage = new unsigned_int16[elementCount];
		triangleArray = reinterpret_cast<const Triangle *>(arrayStorage);

		for (machine a = 0; a < elementCount; a++)
		{
			unsigned_int64 index = data[a];
			if (index > 65535)
			{
				return (kDataOpenGexIndexValueUnsupported);
			}

			arrayStorage[a] = (unsigned_int16) index;
		}
	}

	return (kDataOkay);
}


MeshStructure::MeshStructure() : Structure(kStructureMesh)
{
	meshLevel = 0;

	skinStructure = nullptr;
}

MeshStructure::~MeshStructure()
{
	indexArrayList.RemoveAll();
}

bool MeshStructure::ValidateProperty(const DataDescription *dataDescription, const String<>& identifier, DataType *type, void **value)
{
	if (identifier == "lod")
	{
		*type = kDataUnsignedInt32;
		*value = &meshLevel;
		return (true);
	}

	if (identifier == "primitive")
	{
		*type = kDataString;
		*value = &meshPrimitive;
		return (true);
	}

	return (false);
}

bool MeshStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	StructureType type = structure->GetStructureType();
	return ((type == kStructureVertexArray) || (type == kStructureIndexArray) || (type == kStructureSkin));
}

DataResult MeshStructure::ProcessData(DataDescription *dataDescription)
{
	DataResult result = Structure::ProcessData(dataDescription);
	if (result != kDataOkay)
	{
		return (result);
	}

	const Point2D	*texcoordArray[kMaxGeometryTexcoordCount];

	const void *arrayData[kMaxAttributeArrayCount] = {nullptr};
	int32 vertexCount = 0;

	Structure *structure = GetFirstSubnode();
	while (structure)
	{
		StructureType type = structure->GetStructureType();
		if (type == kStructureVertexArray)
		{
			const VertexArrayStructure *vertexArrayStructure = static_cast<const VertexArrayStructure *>(structure);

			int32 arrayIndex = vertexArrayStructure->GetArrayIndex();
			if (arrayIndex != -1)
			{
				if (arrayData[arrayIndex])
				{
					return (kDataOpenGexDuplicateVertexArray);
				}

				int32 count = vertexArrayStructure->GetVertexCount();
				if (vertexCount == 0)
				{
					vertexCount = count;
				}
				else if (vertexCount != count)
				{
					return (kDataOpenGexVertexCountMismatch);
				}

				arrayData[arrayIndex] = vertexArrayStructure->GetVertexArrayData();
			}
		}
		else if (type == kStructureIndexArray)
		{
			IndexArrayStructure *indexArrayStructure = static_cast<IndexArrayStructure *>(structure);
			indexArrayList.Append(indexArrayStructure);
		}
		else if (type == kStructureSkin)
		{
			if (skinStructure)
			{
				return (kDataExtraneousSubstructure);
			}

			skinStructure = static_cast<SkinStructure *>(structure);
		}

		structure = structure->Next();
	}

	if ((vertexCount < 3) || (vertexCount > 65535))
	{
		return (kDataOpenGexVertexCountUnsupported);
	}

	int32 surfaceCount = indexArrayList.GetElementCount();
	if (surfaceCount == 0)
	{
		return (kDataOpenGexIndexArrayRequired);
	}

	const Point3D *positionArray = static_cast<const Point3D *>(arrayData[kArrayPosition]);
	if (!positionArray)
	{
		return (kDataOpenGexPositionArrayRequired);
	}

	unsigned_int32 surfaceFlags = kSurfaceValidTangents;

	const Vector3D *normalArray = static_cast<const Vector3D *>(arrayData[kArrayNormal]);
	if (normalArray)
	{
		surfaceFlags |= kSurfaceValidNormals;
	}

	const ColorRGBA *colorArray = static_cast<const ColorRGBA *>(arrayData[kArrayColor]);
	if (colorArray)
	{
		surfaceFlags |= kSurfaceValidColors;
	}

	int32 texcoordCount = 1;
	for (machine a = 0; a < kMaxGeometryTexcoordCount; a++)
	{
		texcoordArray[a] = static_cast<const Point2D *>(arrayData[kArrayTexcoord + a]);
		if (texcoordArray[a])
		{
			texcoordCount = (int32) (a + 1);
		}
	}

	const IndexArrayStructure *indexArrayStructure = indexArrayList.First();
	do
	{
		static const unsigned_int8 ccwIndex[3] = {0, 1, 2};
		static const unsigned_int8 cwIndex[3] = {0, 2, 1};

		const String<>& front = indexArrayStructure->GetFrontFace();
		const unsigned_int8 *windingRemap = (front == "ccw") ? ccwIndex : cwIndex;

		GeometrySurface *surface = new GeometrySurface(surfaceFlags, indexArrayStructure->GetMaterialIndex(), texcoordCount);
		surfaceList.Append(surface);

		int32 triangleCount = indexArrayStructure->GetTriangleCount();
		const Triangle *triangle = indexArrayStructure->GetTriangleArray();

		for (machine a = 0; a < triangleCount; a++)
		{
			GeometryVertex		*vertex[3];
			Vector4D			tangent;

			GeometryPolygon *polygon = new GeometryPolygon;
			surface->polygonList.Append(polygon);

			for (machine b = 0; b < 3; b++)
			{
				GeometryVertex *gv = new GeometryVertex;
				polygon->vertexList.Append(gv);
				vertex[b] = gv;

				unsigned_int32 index = triangle->index[windingRemap[b]];
				gv->skinIndex = index;

				gv->position = positionArray[index];

				if (normalArray)
				{
					gv->normal = Normalize(normalArray[index]);
				}

				if (colorArray)
				{
					gv->color = colorArray[index];
				}

				for (machine c = 0; c < kMaxGeometryTexcoordCount; c++)
				{
					if (texcoordArray[c])
					{
						gv->texcoord[c] = texcoordArray[c][index];
					}
				}
			}

			if (Math::CalculateTangent(vertex[0]->position, vertex[1]->position, vertex[2]->position, vertex[0]->texcoord[0], vertex[1]->texcoord[0], vertex[2]->texcoord[0], &tangent))
			{
				vertex[0]->tangent = tangent;
				vertex[1]->tangent = tangent;
				vertex[2]->tangent = tangent;
			}

			triangle++;
		}

		indexArrayStructure = indexArrayStructure->Next();
	} while (indexArrayStructure);

	return (kDataOkay);
}


NodeStructure::NodeStructure() : Structure(kStructureNode)
{
	SetBaseStructureType(kStructureNode);
}

NodeStructure::NodeStructure(StructureType type) : Structure(type)
{
	SetBaseStructureType(kStructureNode);
}

NodeStructure::~NodeStructure()
{
}

bool NodeStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	StructureType type = structure->GetBaseStructureType();
	if ((type == kStructureNode) || (type == kStructureMatrix))
	{
		return (true);
	}

	type = structure->GetStructureType();
	return ((type == kStructureName) || (type == kStructureAnimation));
}

DataResult NodeStructure::ProcessData(DataDescription *dataDescription)
{
	DataResult result = Structure::ProcessData(dataDescription);
	if (result != kDataOkay)
	{
		return (result);
	}

	const Structure *structure = GetFirstSubstructure(kStructureName);
	if (structure)
	{
		if (GetLastSubstructure(kStructureName) != structure)
		{
			return (kDataExtraneousSubstructure);
		}

		nodeName = static_cast<const NameStructure *>(structure)->GetName();
		nodeHash = Text::GetTextHash(nodeName);
	}
	else
	{
		nodeName = nullptr;
		nodeHash = 0;
	}

	return (kDataOkay);
}

const ObjectStructure *NodeStructure::GetObjectStructure(void) const
{
	return (nullptr);
}

void NodeStructure::CalculateTransforms(const OpenGexDataDescription *dataDescription)
{
	nodeTransform.SetIdentity();
	objectTransform.SetIdentity();

	const ObjectStructure *objectStructure = GetObjectStructure();

	const Structure *structure = GetFirstSubnode();
	while (structure)
	{
		if (structure->GetBaseStructureType() == kStructureMatrix)
		{
			const MatrixStructure *matrixStructure = static_cast<const MatrixStructure *>(structure);
			if (!matrixStructure->GetObjectFlag())
			{
				nodeTransform = nodeTransform * matrixStructure->GetMatrix();
			}
			else if (objectStructure)
			{
				objectTransform = objectTransform * matrixStructure->GetMatrix();
			}
		}

		structure = structure->Next();
	}

	dataDescription->AdjustTransform(nodeTransform);
	dataDescription->AdjustTransform(objectTransform);

	objectTransform *= GetTweakTransform();
	inverseObjectTransform = Inverse(objectTransform);
}

Node *NodeStructure::ConstructNode(const OpenGexDataDescription *dataDescription) const
{
	return (new Node);
}

Transform4D NodeStructure::GetTweakTransform(void) const
{
	return (K::identity_4D);
}

Transform4D NodeStructure::CalculateFinalNodeTransform(void) const
{
	Transform4D transform = nodeTransform * objectTransform;

	const Structure *superStructure = GetSuperNode();
	if (superStructure->GetBaseStructureType() == kStructureNode)
	{
		transform = static_cast<const NodeStructure *>(superStructure)->GetInverseObjectTransform() * transform;
	}

	return (transform);
}

void NodeStructure::UpdateNodeTransforms(const OpenGexDataDescription *dataDescription)
{
	CalculateTransforms(dataDescription);

	Structure *structure = GetFirstSubnode();
	while (structure)
	{
		if (structure->GetBaseStructureType() == kStructureNode)
		{
			static_cast<NodeStructure *>(structure)->UpdateNodeTransforms(dataDescription);
		}

		structure = structure->Next();
	}
}

Node *NodeStructure::BuildNodeTree(const OpenGexDataDescription *dataDescription) const
{
	Node *root = ConstructNode(dataDescription);

	if (nodeName)
	{
		root->SetNodeName(nodeName);
	}

	root->SetNodeTransform(CalculateFinalNodeTransform());

	const Structure *structure = GetFirstSubnode();
	while (structure)
	{
		if (structure->GetBaseStructureType() == kStructureNode)
		{
			Node *node = static_cast<const NodeStructure *>(structure)->BuildNodeTree(dataDescription);
			root->AddSubnode(node);
		}

		structure = structure->Next();
	}

	return (root);
}


BoneNodeStructure::BoneNodeStructure() : NodeStructure(kStructureBoneNode)
{
}

BoneNodeStructure::~BoneNodeStructure()
{
}

Node *BoneNodeStructure::ConstructNode(const OpenGexDataDescription *dataDescription) const
{
	return (new Bone);
}


GeometryNodeStructure::GeometryNodeStructure() : NodeStructure(kStructureGeometryNode)
{
	visibleFlag[0] = false;
	shadowFlag[0] = false;
	motionBlurFlag[0] = false;
}

GeometryNodeStructure::~GeometryNodeStructure()
{
}

bool GeometryNodeStructure::ValidateProperty(const DataDescription *dataDescription, const String<>& identifier, DataType *type, void **value)
{
	if (identifier == "visible")
	{
		*type = kDataBool;
		*value = &visibleFlag[1];
		visibleFlag[0] = true;
		return (true);
	}

	if (identifier == "shadow")
	{
		*type = kDataBool;
		*value = &shadowFlag[1];
		shadowFlag[0] = true;
		return (true);
	}

	if (identifier == "motion_blur")
	{
		*type = kDataBool;
		*value = &motionBlurFlag[1];
		motionBlurFlag[0] = true;
		return (true);
	}

	return (false);
}

bool GeometryNodeStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	StructureType type = structure->GetStructureType();
	if ((type == kStructureObjectRef) || (type == kStructureMaterialRef))
	{
		return (true);
	}

	return (NodeStructure::ValidateSubstructure(dataDescription, structure));
}

DataResult GeometryNodeStructure::ProcessData(DataDescription *dataDescription)
{
	DataResult result = NodeStructure::ProcessData(dataDescription);
	if (result != kDataOkay)
	{
		return (result);
	}

	bool objectFlag = false;
	bool materialFlag[256] = {false};
	int32 maxMaterialIndex = -1;

	Structure *structure = GetFirstSubnode();
	while (structure)
	{
		StructureType type = structure->GetStructureType();
		if (type == kStructureObjectRef)
		{
			if (objectFlag)
			{
				return (kDataExtraneousSubstructure);
			}

			objectFlag = true;

			Structure *objectStructure = static_cast<ObjectRefStructure *>(structure)->GetTargetStructure();
			if (objectStructure->GetStructureType() != kStructureGeometryObject)
			{
				return (kDataOpenGexInvalidObjectRef);
			}

			geometryObjectStructure = static_cast<GeometryObjectStructure *>(objectStructure);
		}
		else if (type == kStructureMaterialRef)
		{
			const MaterialRefStructure *materialRefStructure = static_cast<MaterialRefStructure *>(structure);

			unsigned_int32 index = materialRefStructure->GetMaterialIndex();
			if (index > 255)
			{
				return (kDataOpenGexMaterialIndexUnsupported);
			}

			if (materialFlag[index])
			{
				return (kDataOpenGexDuplicateMaterialRef);
			}

			materialFlag[index] = true;
			maxMaterialIndex = Max(maxMaterialIndex, index);
		}

		structure = structure->Next();
	}

	if (!objectFlag)
	{
		return (kDataMissingSubstructure);
	}

	if (maxMaterialIndex >= 0)
	{
		for (machine a = 0; a <= maxMaterialIndex; a++)
		{
			if (!materialFlag[a])
			{
				return (kDataOpenGexMissingMaterialRef);
			}
		}

		materialStructureArray.SetElementCount(maxMaterialIndex + 1);

		structure = GetFirstSubnode();
		while (structure)
		{
			if (structure->GetStructureType() == kStructureMaterialRef)
			{
				const MaterialRefStructure *materialRefStructure = static_cast<const MaterialRefStructure *>(structure);
				materialStructureArray[materialRefStructure->GetMaterialIndex()] = materialRefStructure->GetTargetStructure();
			}

			structure = structure->Next();
		}
	}

	return (kDataOkay);
}

const ObjectStructure *GeometryNodeStructure::GetObjectStructure(void) const
{
	return (geometryObjectStructure);
}

Node *GeometryNodeStructure::ConstructNode(const OpenGexDataDescription *dataDescription) const
{
	GenericGeometry *geometry = new GenericGeometry;
	GenericGeometryObject *geometryObject = geometryObjectStructure->GetGeometryObject(dataDescription);
	geometry->SetObject(geometryObject);

	unsigned_int32 geometryFlags = geometryObject->GetGeometryFlags();

	if (visibleFlag[0])
	{
		if (visibleFlag[1])
		{
			geometryFlags &= ~kGeometryInvisible;
		}
		else
		{
			geometryFlags |= kGeometryInvisible;
		}
	}

	if (shadowFlag[0])
	{
		if (shadowFlag[1])
		{
			geometryFlags = (geometryFlags & ~kGeometryShadowInhibit) | kGeometryRenderShadowMap;
		}
		else
		{
			geometryFlags = (geometryFlags & ~kGeometryRenderShadowMap) | kGeometryShadowInhibit;
		}
	}

	if (motionBlurFlag[0])
	{
		if (motionBlurFlag[1])
		{
			geometryFlags &= ~kGeometryMotionBlurInhibit;
		}
		else
		{
			geometryFlags |= kGeometryMotionBlurInhibit;
		}
	}

	geometryObject->SetGeometryFlags(geometryFlags);

	if (geometryObject->GetGeometryLevel(0)->GetWeightData())
	{
		geometry->SetController(new SkinController);
	}

	int32 materialStructureCount = materialStructureArray.GetElementCount();
	if (materialStructureCount != 0)
	{
		int32 materialCount = 1;

		const MeshStructure *meshStructure = geometryObjectStructure->GetMeshMap()->First();
		while (meshStructure)
		{
			const IndexArrayStructure *indexArrayStructure = meshStructure->GetIndexArrayList()->First();
			while (indexArrayStructure)
			{
				materialCount = Max(materialCount, indexArrayStructure->GetMaterialIndex() + 1);
				indexArrayStructure = indexArrayStructure->Next();
			}

			meshStructure = meshStructure->Next();
		}

		geometry->SetMaterialCount(materialCount);

		meshStructure = geometryObjectStructure->GetMeshMap()->First();
		while (meshStructure)
		{
			const IndexArrayStructure *indexArrayStructure = meshStructure->GetIndexArrayList()->First();
			while (indexArrayStructure)
			{
				int32 materialIndex = indexArrayStructure->GetMaterialIndex();
				geometry->SetMaterialObject(materialIndex, materialStructureArray[materialIndex % materialStructureCount]->GetMaterialObject());

				indexArrayStructure = indexArrayStructure->Next();
			}

			meshStructure = meshStructure->Next();
		}
	}

	geometry->OptimizeMaterials();
	return (geometry);
}


LightNodeStructure::LightNodeStructure() : NodeStructure(kStructureLightNode)
{
	shadowFlag[0] = false;
}

LightNodeStructure::~LightNodeStructure()
{
}

bool LightNodeStructure::ValidateProperty(const DataDescription *dataDescription, const String<>& identifier, DataType *type, void **value)
{
	if (identifier == "shadow")
	{
		*type = kDataBool;
		*value = &shadowFlag[1];
		shadowFlag[0] = true;
		return (true);
	}

	return (false);
}

bool LightNodeStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	if (structure->GetStructureType() == kStructureObjectRef)
	{
		return (true);
	}

	return (NodeStructure::ValidateSubstructure(dataDescription, structure));
}

DataResult LightNodeStructure::ProcessData(DataDescription *dataDescription)
{
	DataResult result = NodeStructure::ProcessData(dataDescription);
	if (result != kDataOkay)
	{
		return (result);
	}

	bool objectFlag = false;

	const Structure *structure = GetFirstSubnode();
	while (structure)
	{
		if (structure->GetStructureType() == kStructureObjectRef)
		{
			if (objectFlag)
			{
				return (kDataExtraneousSubstructure);
			}

			objectFlag = true;

			const Structure *objectStructure = static_cast<const ObjectRefStructure *>(structure)->GetTargetStructure();
			if (objectStructure->GetStructureType() != kStructureLightObject)
			{
				return (kDataOpenGexInvalidObjectRef);
			}

			lightObjectStructure = static_cast<const LightObjectStructure *>(objectStructure);
		}

		structure = structure->Next();
	}

	if (!objectFlag)
	{
		return (kDataMissingSubstructure);
	}

	return (kDataOkay);
}

const ObjectStructure *LightNodeStructure::GetObjectStructure(void) const
{
	return (lightObjectStructure);
}

Node *LightNodeStructure::ConstructNode(const OpenGexDataDescription *dataDescription) const
{
	Light	*light;

	LightType type = lightObjectStructure->GetLightType();
	const ColorRGB& color = lightObjectStructure->GetLightColor();

	if (type == kLightInfinite)
	{
		light = new InfiniteLight(color);
	}
	else if (type == kLightPoint)
	{
		light = new PointLight(color, lightObjectStructure->GetLightRange());
	}
	else
	{
		const char *name = lightObjectStructure->GetTextureName();
		if (name[0] == 0)
		{
			name = "C4/spot";
		}

		light = new SpotLight(color, lightObjectStructure->GetLightRange(), lightObjectStructure->GetSpotApex(), name);
	}

	LightObject *lightObject = light->GetObject();
	unsigned_int32 lightFlags = lightObject->GetLightFlags();

	if (!((shadowFlag[0]) ? shadowFlag[1] : lightObjectStructure->GetShadowFlag()))
	{
		lightFlags |= kLightShadowInhibit;
	}

	lightObject->SetLightFlags(lightFlags);

	return (light);
}

Transform4D LightNodeStructure::GetTweakTransform(void) const
{
	if (lightObjectStructure->GetLightType() == kLightSpot)
	{
		return (Transform4D(1.0F, 0.0F, 0.0F, 0.0F, 0.0F, -1.0F, 0.0F, 0.0F, 0.0F, 0.0F, -1.0F, 0.0F));
	}

	return (K::identity_4D);
}


CameraNodeStructure::CameraNodeStructure() : NodeStructure(kStructureCameraNode)
{
}

CameraNodeStructure::~CameraNodeStructure()
{
}

bool CameraNodeStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	if (structure->GetStructureType() == kStructureObjectRef)
	{
		return (true);
	}

	return (NodeStructure::ValidateSubstructure(dataDescription, structure));
}

DataResult CameraNodeStructure::ProcessData(DataDescription *dataDescription)
{
	DataResult result = NodeStructure::ProcessData(dataDescription);
	if (result != kDataOkay)
	{
		return (result);
	}

	bool objectFlag = false;

	const Structure *structure = GetFirstSubnode();
	while (structure)
	{
		if (structure->GetStructureType() == kStructureObjectRef)
		{
			if (objectFlag)
			{
				return (kDataExtraneousSubstructure);
			}

			objectFlag = true;

			const Structure *objectStructure = static_cast<const ObjectRefStructure *>(structure)->GetTargetStructure();
			if (objectStructure->GetStructureType() != kStructureCameraObject)
			{
				return (kDataOpenGexInvalidObjectRef);
			}

			cameraObjectStructure = static_cast<const CameraObjectStructure *>(objectStructure);
		}

		structure = structure->Next();
	}

	if (!objectFlag)
	{
		return (kDataMissingSubstructure);
	}

	return (kDataOkay);
}

const ObjectStructure *CameraNodeStructure::GetObjectStructure(void) const
{
	return (cameraObjectStructure);
}

Node *CameraNodeStructure::ConstructNode(const OpenGexDataDescription *dataDescription) const
{
	FrustumCamera *camera = new FrustumCamera(cameraObjectStructure->GetFocalLength(), 1.0F);
	FrustumCameraObject *object = camera->GetObject();

	float near = cameraObjectStructure->GetNearDepth();
	float far = cameraObjectStructure->GetFarDepth();

	if (far > near)
	{
		object->SetNearDepth(near);
		object->SetFarDepth(far);
	}

	return (camera);
}

Transform4D CameraNodeStructure::GetTweakTransform(void) const
{
	return (Transform4D(1.0F, 0.0F, 0.0F, 0.0F, 0.0F, -1.0F, 0.0F, 0.0F, 0.0F, 0.0F, -1.0F, 0.0F));
}


ObjectStructure::ObjectStructure(StructureType type) : Structure(type)
{
	SetBaseStructureType(kStructureObject);
}

ObjectStructure::~ObjectStructure()
{
}


GeometryObjectStructure::GeometryObjectStructure() : ObjectStructure(kStructureGeometryObject)
{
	visibleFlag = true;
	shadowFlag = true;
	motionBlurFlag = true;

	geometryObject = nullptr;
}

GeometryObjectStructure::~GeometryObjectStructure()
{
	meshMap.RemoveAll();

	if (geometryObject)
	{
		geometryObject->Release();
	}
}

bool GeometryObjectStructure::ValidateProperty(const DataDescription *dataDescription, const String<>& identifier, DataType *type, void **value)
{
	if (identifier == "visible")
	{
		*type = kDataBool;
		*value = &visibleFlag;
		return (true);
	}

	if (identifier == "shadow")
	{
		*type = kDataBool;
		*value = &shadowFlag;
		return (true);
	}

	if (identifier == "motion_blur")
	{
		*type = kDataBool;
		*value = &motionBlurFlag;
		return (true);
	}

	return (false);
}

bool GeometryObjectStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	return (structure->GetStructureType() == kStructureMesh);
}

DataResult GeometryObjectStructure::ProcessData(DataDescription *dataDescription)
{
	DataResult result = Structure::ProcessData(dataDescription);
	if (result != kDataOkay)
	{
		return (result);
	}

	Structure *structure = GetFirstSubnode();
	if (!structure)
	{
		return (kDataMissingSubstructure);
	}

	int32 levelCount = 0;
	int32 skinCount = 0;

	do
	{
		MeshStructure *meshStructure = static_cast<MeshStructure *>(structure);
		if (!meshMap.Insert(meshStructure))
		{
			return (kDataOpenGexDuplicateLod);
		}

		levelCount++;
		skinCount += (meshStructure->GetSkinStructure() != nullptr);

		structure = structure->Next();
	} while (structure);

	if ((skinCount != 0) && (skinCount != levelCount))
	{
		return (kDataOpenGexMissingLodSkin);
	}

	return (kDataOkay);
}

GenericGeometryObject *GeometryObjectStructure::GetGeometryObject(const OpenGexDataDescription *dataDescription)
{
	if (!geometryObject)
	{
		Array<const List<GeometrySurface> *, 8>		surfaceListArray;
		Array<int32, 8>								materialIndexArray;
		Array<const SkinData *, 8>					skinDataArray;

		const MeshStructure *meshStructure = meshMap.First();
		const List<GeometrySurface> *surfaceList = meshStructure->GetSurfaceList();
		surfaceListArray.AddElement(surfaceList);

		const GeometrySurface *surface = surfaceList->First();
		while (surface)
		{
			materialIndexArray.AddElement(surface->materialIndex);
			surface = surface->Next();
		}

		SkinStructure *skinStructure = meshStructure->GetSkinStructure();
		if (skinStructure)
		{
			skinDataArray.AddElement(skinStructure->BuildSkinData(dataDescription));
		}

		for (;;)
		{
			meshStructure = meshStructure->Next();
			if (!meshStructure)
			{
				break;
			}

			surfaceListArray.AddElement(meshStructure->GetSurfaceList());

			skinStructure = meshStructure->GetSkinStructure();
			if (skinStructure)
			{
				skinDataArray.AddElement(skinStructure->BuildSkinData(dataDescription));
			}
		}

		const SkinData *const *skinDataTable = (!skinDataArray.Empty()) ? skinDataArray : (const SkinData *const *) nullptr;
		geometryObject = new GenericGeometryObject(surfaceListArray.GetElementCount(), surfaceListArray, surfaceList->GetElementCount(), materialIndexArray, skinDataTable);

		unsigned_int32 geometryFlags = geometryObject->GetGeometryFlags();

		if (!visibleFlag)
		{
			geometryFlags |= kGeometryInvisible;
		}

		if (!shadowFlag)
		{
			geometryFlags = (geometryFlags & ~kGeometryRenderShadowMap) | kGeometryShadowInhibit;
		}

		if (!motionBlurFlag)
		{
			geometryFlags |= kGeometryMotionBlurInhibit;
		}

		geometryObject->SetGeometryFlags(geometryFlags);
	}

	return (geometryObject);
}


LightObjectStructure::LightObjectStructure() : ObjectStructure(kStructureLightObject)
{
	shadowFlag = true;
	spotApex = 1.0F;
	textureName = nullptr;
}

LightObjectStructure::~LightObjectStructure()
{
}

bool LightObjectStructure::ValidateProperty(const DataDescription *dataDescription, const String<>& identifier, DataType *type, void **value)
{
	if (identifier == "type")
	{
		*type = kDataString;
		*value = &typeString;
		return (true);
	}

	if (identifier == "shadow")
	{
		*type = kDataBool;
		*value = &shadowFlag;
		return (true);
	}

	return (false);
}

bool LightObjectStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	return ((structure->GetBaseStructureType() == kStructureAttrib) || (structure->GetStructureType() == kStructureAtten));
}

DataResult LightObjectStructure::ProcessData(DataDescription *dataDescription)
{
	DataResult result = Structure::ProcessData(dataDescription);
	if (result != kDataOkay)
	{
		return (result);
	}

	if (typeString == "infinite")
	{
		lightType = kLightInfinite;
	}
	else if (typeString == "point")
	{
		lightType = kLightPoint;
	}
	else if (typeString == "spot")
	{
		lightType = kLightSpot;
	}
	else
	{
		return (kDataOpenGexUndefinedLightType);
	}

	ColorRGB color(1.0F, 1.0F, 1.0F);
	float intensity = 1.0F;
	float range = 0.0F;

	const Structure *structure = GetFirstSubnode();
	while (structure)
	{
		StructureType type = structure->GetStructureType();
		if (type == kStructureColor)
		{
			const ColorStructure *colorStructure = static_cast<const ColorStructure *>(structure);
			if (colorStructure->GetAttribString() == "light")
			{
				color = colorStructure->GetColor().GetColorRGB();
			}
			else
			{
				return (kDataOpenGexUndefinedAttrib);
			}
		}
		else if (type == kStructureParam)
		{
			const ParamStructure *paramStructure = static_cast<const ParamStructure *>(structure);
			if (paramStructure->GetAttribString() == "intensity")
			{
				intensity = paramStructure->GetParam();
			}
			else
			{
				return (kDataOpenGexUndefinedAttrib);
			}
		}
		else if (type == kStructureTexture)
		{
			const TextureStructure *textureStructure = static_cast<const TextureStructure *>(structure);
			if (textureStructure->GetAttribString() == "projection")
			{
				textureName = textureStructure->GetTextureName();
			}
			else
			{
				return (kDataOpenGexUndefinedAttrib);
			}
		}
		else if (type == kStructureAtten)
		{
			const AttenStructure *attenStructure = static_cast<const AttenStructure *>(structure);
			const String<>& attenKind = attenStructure->GetAttenKind();
			const String<>& curveType = attenStructure->GetCurveType();

			if (attenKind == "distance")
			{
				if ((curveType == "linear") || (curveType == "smooth"))
				{
					float beginParam = attenStructure->GetBeginParam();
					float endParam = attenStructure->GetEndParam();

					if (endParam > beginParam)
					{
						range = (range == 0.0F) ? endParam : Fmin(range, endParam);
					}
				}
				else if (curveType == "inverse")
				{
					float scaleParam = attenStructure->GetScaleParam();
					float linearParam = attenStructure->GetLinearParam();

					if ((scaleParam > K::min_float) && (Fabs(linearParam) > K::min_float))
					{
						float offsetParam = attenStructure->GetOffsetParam();
						float constantParam = attenStructure->GetConstantParam();

						float f = (Fabs(offsetParam) < K::min_float) ? -256.0F : 1.0F / offsetParam;

						float r = -scaleParam * (constantParam + f) / linearParam;
						if (r > 0.0F)
						{
							range = (range == 0.0F) ? r : Fmin(range, r);
						}
					}
				}
				else if (curveType == "inverse_square")
				{
					float scaleParam = attenStructure->GetScaleParam();
					float quadraticParam = attenStructure->GetQuadraticParam();

					if ((scaleParam > K::min_float) && (Fabs(quadraticParam) > K::min_float))
					{
						float offsetParam = attenStructure->GetOffsetParam();
						float constantParam = attenStructure->GetConstantParam();
						float linearParam = attenStructure->GetLinearParam();

						float f = (Fabs(offsetParam) < K::min_float) ? -256.0F : 1.0F / offsetParam;

						float d = linearParam * linearParam - 4.0F * quadraticParam * (constantParam + f);
						if (d > K::min_float)
						{
							float r = -scaleParam * 0.5F / quadraticParam * (linearParam - Sqrt(d));
							if (r > 0.0F)
							{
								range = (range == 0.0F) ? r : Fmin(range, r);
							}
						}
					}
				}
				else
				{
					return (kDataOpenGexUndefinedCurve);
				}
			}
			else if (attenKind == "angle")
			{
				float endParam = attenStructure->GetEndParam();
				spotApex = Tan(endParam);
			}
			else if (attenKind == "cos_angle")
			{
				float endParam = attenStructure->GetEndParam();
				if (endParam > K::min_float)
				{
					spotApex = endParam * InverseSqrt(1.0F - endParam * endParam);
				}
			}
			else
			{
				return (kDataOpenGexUndefinedAtten);
			}
		}

		structure = structure->Next();
	}

	lightColor = color * intensity;
	lightRange = (range > 0.0F) ? range : 1.0F;

	return (kDataOkay);
}


CameraObjectStructure::CameraObjectStructure() : ObjectStructure(kStructureCameraObject)
{
}

CameraObjectStructure::~CameraObjectStructure()
{
}

bool CameraObjectStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	return (structure->GetStructureType() == kStructureParam);
}

DataResult CameraObjectStructure::ProcessData(DataDescription *dataDescription)
{
	DataResult result = Structure::ProcessData(dataDescription);
	if (result != kDataOkay)
	{
		return (result);
	}

	focalLength = 2.0F;
	nearDepth = 0.1F;
	farDepth = 1000.0F;

	const OpenGexDataDescription *openGexDataDescription = static_cast<OpenGexDataDescription *>(dataDescription);
	float distanceScale = openGexDataDescription->GetDistanceScale();
	float angleScale = openGexDataDescription->GetAngleScale();

	const Structure *structure = GetFirstSubnode();
	while (structure)
	{
		const ParamStructure *paramStructure = static_cast<const ParamStructure *>(structure);
		const String<>& attribString = paramStructure->GetAttribString();
		float param = paramStructure->GetParam();

		if (attribString == "fov")
		{
			float t = Tan(param * angleScale * 0.5F);
			if (t > K::min_float)
			{
				focalLength = 1.0F / t;
			}
		}
		else if (attribString == "near")
		{
			if (param > K::min_float)
			{
				nearDepth = param * distanceScale;
			}
		}
		else if (attribString == "far")
		{
			if (param > K::min_float)
			{
				farDepth = param * distanceScale;
			}
		}

		structure = structure->Next();
	}

	return (kDataOkay);
}


AnimatableStructure::AnimatableStructure(StructureType type) : Structure(type)
{
}

AnimatableStructure::~AnimatableStructure()
{
}


MatrixStructure::MatrixStructure(StructureType type) : AnimatableStructure(type)
{
	SetBaseStructureType(kStructureMatrix);

	objectFlag = false;
	matrixValue.SetIdentity();
}

MatrixStructure::~MatrixStructure()
{
}

bool MatrixStructure::ValidateProperty(const DataDescription *dataDescription, const String<>& identifier, DataType *type, void **value)
{
	if (identifier == "object")
	{
		*type = kDataBool;
		*value = &objectFlag;
		return (true);
	}

	return (false);
}


TransformStructure::TransformStructure() : MatrixStructure(kStructureTransform)
{
}

TransformStructure::~TransformStructure()
{
}

bool TransformStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	if (structure->GetStructureType() != kDataFloat)
	{
		return (false);
	}

	const PrimitiveStructure *primitiveStructure = static_cast<const PrimitiveStructure *>(structure);
	return (primitiveStructure->GetArraySize() == 16);
}

DataResult TransformStructure::ProcessData(DataDescription *dataDescription)
{
	const Structure *structure = GetFirstSubnode();
	if (!structure)
	{
		return (kDataMissingSubstructure);
	}

	if (GetLastSubnode() != structure)
	{
		return (kDataExtraneousSubstructure);
	}

	const DataStructure<FloatDataType> *dataStructure = static_cast<const DataStructure<FloatDataType> *>(structure);

	transformCount = dataStructure->GetDataElementCount() / 16;
	if (transformCount == 0)
	{
		return (kDataInvalidDataFormat);
	}

	transformArray = reinterpret_cast<const Transform4D *>(&dataStructure->GetDataElement(0));
	matrixValue = transformArray[0];

	return (kDataOkay);
}

void TransformStructure::UpdateAnimation(const OpenGexDataDescription *dataDescription, const float *data)
{
	matrixValue = *reinterpret_cast<const Transform4D *>(data);
}


TranslationStructure::TranslationStructure() :
		MatrixStructure(kStructureTranslation),
		translationKind("xyz")
{
}

TranslationStructure::~TranslationStructure()
{
}

bool TranslationStructure::ValidateProperty(const DataDescription *dataDescription, const String<>& identifier, DataType *type, void **value)
{
	if (identifier == "kind")
	{
		*type = kDataString;
		*value = &translationKind;
		return (true);
	}

	return (false);
}

bool TranslationStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	if (structure->GetStructureType() != kDataFloat)
	{
		return (false);
	}

	const PrimitiveStructure *primitiveStructure = static_cast<const PrimitiveStructure *>(structure);
	unsigned_int32 arraySize = primitiveStructure->GetArraySize();
	return ((arraySize == 0) || (arraySize == 3));
}

DataResult TranslationStructure::ProcessData(DataDescription *dataDescription)
{
	const Structure *structure = GetFirstSubnode();
	if (!structure)
	{
		return (kDataMissingSubstructure);
	}

	if (GetLastSubnode() != structure)
	{
		return (kDataExtraneousSubstructure);
	}

	const DataStructure<FloatDataType> *dataStructure = static_cast<const DataStructure<FloatDataType> *>(structure);
	unsigned_int32 arraySize = dataStructure->GetArraySize();

	if ((translationKind == "x") || (translationKind == "y") || (translationKind == "z"))
	{
		if ((arraySize != 0) || (dataStructure->GetDataElementCount() != 1))
		{
			return (kDataInvalidDataFormat);
		}
	}
	else if (translationKind == "xyz")
	{
		if ((arraySize != 3) || (dataStructure->GetDataElementCount() != 3))
		{
			return (kDataInvalidDataFormat);
		}
	}
	else
	{
		return (kDataOpenGexInvalidTranslationKind);
	}

	TranslationStructure::UpdateAnimation(static_cast<const OpenGexDataDescription *>(dataDescription), &dataStructure->GetDataElement(0));
	return (kDataOkay);
}

void TranslationStructure::UpdateAnimation(const OpenGexDataDescription *dataDescription, const float *data)
{
	if (translationKind == "x")
	{
		matrixValue.SetTranslation(data[0], 0.0F, 0.0F);
	}
	else if (translationKind == "y")
	{
		matrixValue.SetTranslation(0.0F, data[0], 0.0F);
	}
	else if (translationKind == "z")
	{
		matrixValue.SetTranslation(0.0F, 0.0F, data[0]);
	}
	else
	{
		matrixValue.SetTranslation(data[0], data[1], data[2]);
	}
}


RotationStructure::RotationStructure() :
		MatrixStructure(kStructureRotation),
		rotationKind("axis")
{
}

RotationStructure::~RotationStructure()
{
}

bool RotationStructure::ValidateProperty(const DataDescription *dataDescription, const String<>& identifier, DataType *type, void **value)
{
	if (identifier == "kind")
	{
		*type = kDataString;
		*value = &rotationKind;
		return (true);
	}

	return (false);
}

bool RotationStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	if (structure->GetStructureType() != kDataFloat)
	{
		return (false);
	}

	const PrimitiveStructure *primitiveStructure = static_cast<const PrimitiveStructure *>(structure);
	unsigned_int32 arraySize = primitiveStructure->GetArraySize();
	return ((arraySize == 0) || (arraySize == 4));
}

DataResult RotationStructure::ProcessData(DataDescription *dataDescription)
{
	const Structure *structure = GetFirstSubnode();
	if (!structure)
	{
		return (kDataMissingSubstructure);
	}

	if (GetLastSubnode() != structure)
	{
		return (kDataExtraneousSubstructure);
	}

	const DataStructure<FloatDataType> *dataStructure = static_cast<const DataStructure<FloatDataType> *>(structure);
	unsigned_int32 arraySize = dataStructure->GetArraySize();

	if ((rotationKind == "x") || (rotationKind == "y") || (rotationKind == "z"))
	{
		if ((arraySize != 0) || (dataStructure->GetDataElementCount() != 1))
		{
			return (kDataInvalidDataFormat);
		}
	}
	else if ((rotationKind == "axis") || (rotationKind == "quaternion"))
	{
		if ((arraySize != 4) || (dataStructure->GetDataElementCount() != 4))
		{
			return (kDataInvalidDataFormat);
		}
	}
	else
	{
		return (kDataOpenGexInvalidRotationKind);
	}

	RotationStructure::UpdateAnimation(static_cast<const OpenGexDataDescription *>(dataDescription), &dataStructure->GetDataElement(0));
	return (kDataOkay);
}

void RotationStructure::UpdateAnimation(const OpenGexDataDescription *dataDescription, const float *data)
{
	float scale = dataDescription->GetAngleScale();

	if (rotationKind == "x")
	{
		matrixValue.SetRotationAboutX(data[0] * scale);
	}
	else if (rotationKind == "y")
	{
		matrixValue.SetRotationAboutY(data[0] * scale);
	}
	else if (rotationKind == "z")
	{
		matrixValue.SetRotationAboutZ(data[0] * scale);
	}
	else if (rotationKind == "axis")
	{
		matrixValue.SetRotationAboutAxis(data[0] * scale, Antivector3D(data[1], data[2], data[3]).Normalize());
	}
	else
	{
		matrixValue.SetMatrix3D(Quaternion(data[0], data[1], data[2], data[3]).Normalize().GetRotationMatrix());
	}
}


ScaleStructure::ScaleStructure() :
		MatrixStructure(kStructureScale),
		scaleKind("xyz")
{
}

ScaleStructure::~ScaleStructure()
{
}

bool ScaleStructure::ValidateProperty(const DataDescription *dataDescription, const String<>& identifier, DataType *type, void **value)
{
	if (identifier == "kind")
	{
		*type = kDataString;
		*value = &scaleKind;
		return (true);
	}

	return (false);
}

bool ScaleStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	if (structure->GetStructureType() != kDataFloat)
	{
		return (false);
	}

	const PrimitiveStructure *primitiveStructure = static_cast<const PrimitiveStructure *>(structure);
	unsigned_int32 arraySize = primitiveStructure->GetArraySize();
	return ((arraySize == 0) || (arraySize == 3));
}

DataResult ScaleStructure::ProcessData(DataDescription *dataDescription)
{
	const Structure *structure = GetFirstSubnode();
	if (!structure)
	{
		return (kDataMissingSubstructure);
	}

	if (GetLastSubnode() != structure)
	{
		return (kDataExtraneousSubstructure);
	}

	const DataStructure<FloatDataType> *dataStructure = static_cast<const DataStructure<FloatDataType> *>(structure);
	unsigned_int32 arraySize = dataStructure->GetArraySize();

	if ((scaleKind == "x") || (scaleKind == "y") || (scaleKind == "z"))
	{
		if ((arraySize != 0) || (dataStructure->GetDataElementCount() != 1))
		{
			return (kDataInvalidDataFormat);
		}
	}
	else if (scaleKind == "xyz")
	{
		if ((arraySize != 3) || (dataStructure->GetDataElementCount() != 3))
		{
			return (kDataInvalidDataFormat);
		}
	}
	else
	{
		return (kDataOpenGexInvalidScaleKind);
	}

	ScaleStructure::UpdateAnimation(static_cast<const OpenGexDataDescription *>(dataDescription), &dataStructure->GetDataElement(0));
	return (kDataOkay);
}

void ScaleStructure::UpdateAnimation(const OpenGexDataDescription *dataDescription, const float *data)
{
	if (scaleKind == "x")
	{
		matrixValue.SetScale(data[0], 1.0F, 1.0F);
	}
	else if (scaleKind == "y")
	{
		matrixValue.SetScale(1.0F, data[0], 1.0F);
	}
	else if (scaleKind == "z")
	{
		matrixValue.SetScale(1.0F, 1.0F, data[0]);
	}
	else if (scaleKind == "xyz")
	{
		matrixValue.SetScale(data[0], data[1], data[2]);
	}
}


NameStructure::NameStructure() : Structure(kStructureName)
{
}

NameStructure::~NameStructure()
{
}

bool NameStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	return (structure->GetStructureType() == kDataString);
}

DataResult NameStructure::ProcessData(DataDescription *dataDescription)
{
	const Structure *structure = GetFirstSubnode();
	if (!structure)
	{
		return (kDataMissingSubstructure);
	}

	if (GetLastSubnode() != structure)
	{
		return (kDataExtraneousSubstructure);
	}

	const DataStructure<StringDataType> *dataStructure = static_cast<const DataStructure<StringDataType> *>(structure);
	if (dataStructure->GetDataElementCount() != 1)
	{
		return (kDataInvalidDataFormat);
	}

	name = dataStructure->GetDataElement(0);
	return (kDataOkay);
}


ObjectRefStructure::ObjectRefStructure() : Structure(kStructureObjectRef)
{
	targetStructure = nullptr;
}

ObjectRefStructure::~ObjectRefStructure()
{
}

bool ObjectRefStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	return (structure->GetStructureType() == kDataRef);
}

DataResult ObjectRefStructure::ProcessData(DataDescription *dataDescription)
{
	const Structure *structure = GetFirstSubnode();
	if (!structure)
	{
		return (kDataMissingSubstructure);
	}

	if (GetLastSubnode() != structure)
	{
		return (kDataExtraneousSubstructure);
	}

	const DataStructure<RefDataType> *dataStructure = static_cast<const DataStructure<RefDataType> *>(structure);
	if (dataStructure->GetDataElementCount() != 0)
	{
		Structure *objectStructure = dataDescription->FindStructure(dataStructure->GetDataElement(0));
		if (objectStructure)
		{
			targetStructure = objectStructure;
			return (kDataOkay);
		}
	}

	return (kDataBrokenRef);
}


MaterialRefStructure::MaterialRefStructure() : Structure(kStructureMaterialRef)
{
	materialIndex = 0;
	targetStructure = nullptr;
}

MaterialRefStructure::~MaterialRefStructure()
{
}

bool MaterialRefStructure::ValidateProperty(const DataDescription *dataDescription, const String<>& identifier, DataType *type, void **value)
{
	if (identifier == "index")
	{
		*type = kDataUnsignedInt32;
		*value = &materialIndex;
		return (true);
	}

	return (false);
}

bool MaterialRefStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	return (structure->GetStructureType() == kDataRef);
}

DataResult MaterialRefStructure::ProcessData(DataDescription *dataDescription)
{
	const Structure *structure = GetFirstSubnode();
	if (!structure)
	{
		return (kDataMissingSubstructure);
	}

	if (GetLastSubnode() != structure)
	{
		return (kDataExtraneousSubstructure);
	}

	const DataStructure<RefDataType> *dataStructure = static_cast<const DataStructure<RefDataType> *>(structure);
	if (dataStructure->GetDataElementCount() != 0)
	{
		const Structure *materialStructure = dataDescription->FindStructure(dataStructure->GetDataElement(0));
		if (materialStructure)
		{
			if (materialStructure->GetStructureType() != kStructureMaterial)
			{
				return (kDataOpenGexInvalidMaterialRef);
			}

			targetStructure = static_cast<const MaterialStructure *>(materialStructure);
			return (kDataOkay);
		}
	}

	return (kDataBrokenRef);
}


MorphStructure::MorphStructure() : AnimatableStructure(kStructureMorph)
{
	morphWeightArray = nullptr;
}

MorphStructure::~MorphStructure()
{
	delete[] morphWeightArray;
}

bool MorphStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	return (structure->GetStructureType() == kDataFloat);
}

DataResult MorphStructure::ProcessData(DataDescription *dataDescription)
{
	const Structure *structure = GetFirstSubnode();
	if (!structure)
	{
		return (kDataMissingSubstructure);
	}

	if (GetLastSubnode() != structure)
	{
		return (kDataExtraneousSubstructure);
	}

	const DataStructure<FloatDataType> *dataStructure = static_cast<const DataStructure<FloatDataType> *>(structure);
	if (dataStructure->GetArraySize() == 0)
	{
		return (kDataInvalidDataFormat);
	}

	int32 count = dataStructure->GetDataElementCount();
	morphWeightCount = count;

	morphWeightArray = new float[count];
	MemoryMgr::CopyMemory(&dataStructure->GetDataElement(0), morphWeightArray, count * 4);

	return (kDataOkay);
}

void MorphStructure::UpdateAnimation(const OpenGexDataDescription *dataDescription, const float *data)
{
	MemoryMgr::CopyMemory(data, morphWeightArray, morphWeightCount * 4);
}


BoneRefArrayStructure::BoneRefArrayStructure() : Structure(kStructureBoneRefArray)
{
	boneNodeArray = nullptr;
}

BoneRefArrayStructure::~BoneRefArrayStructure()
{
	delete[] boneNodeArray;
}

bool BoneRefArrayStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	return (structure->GetStructureType() == kDataRef);
}

DataResult BoneRefArrayStructure::ProcessData(DataDescription *dataDescription)
{
	const Structure *structure = GetFirstSubnode();
	if (!structure)
	{
		return (kDataMissingSubstructure);
	}

	if (GetLastSubnode() != structure)
	{
		return (kDataExtraneousSubstructure);
	}

	const DataStructure<RefDataType> *dataStructure = static_cast<const DataStructure<RefDataType> *>(structure);
	boneCount = dataStructure->GetDataElementCount();

	if (boneCount != 0)
	{
		boneNodeArray = new const BoneNodeStructure *[boneCount];

		for (machine a = 0; a < boneCount; a++)
		{
			const StructureRef& reference = dataStructure->GetDataElement(a);
			const Structure *boneStructure = dataDescription->FindStructure(reference);
			if (!boneStructure)
			{
				return (kDataBrokenRef);
			}

			if (boneStructure->GetStructureType() != kStructureBoneNode)
			{
				return (kDataOpenGexInvalidBoneRef);
			}

			boneNodeArray[a] = static_cast<const BoneNodeStructure *>(boneStructure);
		}
	}

	return (kDataOkay);
}


BoneCountArrayStructure::BoneCountArrayStructure() : Structure(kStructureBoneCountArray)
{
	arrayStorage = nullptr;
}

BoneCountArrayStructure::~BoneCountArrayStructure()
{
	delete[] arrayStorage;
}

bool BoneCountArrayStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	StructureType type = structure->GetStructureType();
	return ((type == kDataUnsignedInt8) || (type == kDataUnsignedInt16) || (type == kDataUnsignedInt32) || (type == kDataUnsignedInt64));
}

DataResult BoneCountArrayStructure::ProcessData(DataDescription *dataDescription)
{
	const Structure *structure = GetFirstSubnode();
	if (!structure)
	{
		return (kDataMissingSubstructure);
	}

	if (GetLastSubnode() != structure)
	{
		return (kDataExtraneousSubstructure);
	}

	const PrimitiveStructure *primitiveStructure = static_cast<const PrimitiveStructure *>(structure);
	if (primitiveStructure->GetArraySize() != 0)
	{
		return (kDataInvalidDataFormat);
	}

	StructureType type = primitiveStructure->GetStructureType();
	if (type == kDataUnsignedInt16)
	{
		const DataStructure<UnsignedInt16DataType> *dataStructure = static_cast<const DataStructure<UnsignedInt16DataType> *>(primitiveStructure);
		vertexCount = dataStructure->GetDataElementCount();
		boneCountArray = &dataStructure->GetDataElement(0);
	}
	else if (type == kDataUnsignedInt8)
	{
		const DataStructure<UnsignedInt8DataType> *dataStructure = static_cast<const DataStructure<UnsignedInt8DataType> *>(primitiveStructure);
		vertexCount = dataStructure->GetDataElementCount();

		const unsigned_int8 *data = &dataStructure->GetDataElement(0);
		arrayStorage = new unsigned_int16[vertexCount];
		boneCountArray = arrayStorage;

		for (machine a = 0; a < vertexCount; a++)
		{
			arrayStorage[a] = data[a];
		}
	}
	else if (type == kDataUnsignedInt32)
	{
		const DataStructure<UnsignedInt32DataType> *dataStructure = static_cast<const DataStructure<UnsignedInt32DataType> *>(primitiveStructure);
		vertexCount = dataStructure->GetDataElementCount();

		const unsigned_int32 *data = &dataStructure->GetDataElement(0);
		arrayStorage = new unsigned_int16[vertexCount];
		boneCountArray = arrayStorage;

		for (machine a = 0; a < vertexCount; a++)
		{
			unsigned_int32 index = data[a];
			if (index > 65535)
			{
				return (kDataOpenGexIndexValueUnsupported);
			}

			arrayStorage[a] = (unsigned_int16) index;
		}
	}
	else
	{
		const DataStructure<UnsignedInt64DataType> *dataStructure = static_cast<const DataStructure<UnsignedInt64DataType> *>(primitiveStructure);
		vertexCount = dataStructure->GetDataElementCount();

		const unsigned_int64 *data = &dataStructure->GetDataElement(0);
		arrayStorage = new unsigned_int16[vertexCount];
		boneCountArray = arrayStorage;

		for (machine a = 0; a < vertexCount; a++)
		{
			unsigned_int64 index = data[a];
			if (index > 65535)
			{
				return (kDataOpenGexIndexValueUnsupported);
			}

			arrayStorage[a] = (unsigned_int16) index;
		}
	}

	return (kDataOkay);
}


BoneIndexArrayStructure::BoneIndexArrayStructure() : Structure(kStructureBoneIndexArray)
{
	arrayStorage = nullptr;
}

BoneIndexArrayStructure::~BoneIndexArrayStructure()
{
	delete[] arrayStorage;
}

bool BoneIndexArrayStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	StructureType type = structure->GetStructureType();
	return ((type == kDataUnsignedInt8) || (type == kDataUnsignedInt16) || (type == kDataUnsignedInt32) || (type == kDataUnsignedInt64));
}

DataResult BoneIndexArrayStructure::ProcessData(DataDescription *dataDescription)
{
	const Structure *structure = GetFirstSubnode();
	if (!structure)
	{
		return (kDataMissingSubstructure);
	}

	if (GetLastSubnode() != structure)
	{
		return (kDataExtraneousSubstructure);
	}

	const PrimitiveStructure *primitiveStructure = static_cast<const PrimitiveStructure *>(structure);
	if (primitiveStructure->GetArraySize() != 0)
	{
		return (kDataInvalidDataFormat);
	}

	StructureType type = primitiveStructure->GetStructureType();
	if (type == kDataUnsignedInt16)
	{
		const DataStructure<UnsignedInt16DataType> *dataStructure = static_cast<const DataStructure<UnsignedInt16DataType> *>(primitiveStructure);
		boneIndexCount = dataStructure->GetDataElementCount();
		boneIndexArray = &dataStructure->GetDataElement(0);
	}
	else if (type == kDataUnsignedInt8)
	{
		const DataStructure<UnsignedInt8DataType> *dataStructure = static_cast<const DataStructure<UnsignedInt8DataType> *>(primitiveStructure);
		boneIndexCount = dataStructure->GetDataElementCount();

		const unsigned_int8 *data = &dataStructure->GetDataElement(0);
		arrayStorage = new unsigned_int16[boneIndexCount];
		boneIndexArray = arrayStorage;

		for (machine a = 0; a < boneIndexCount; a++)
		{
			arrayStorage[a] = data[a];
		}
	}
	else if (type == kDataUnsignedInt32)
	{
		const DataStructure<UnsignedInt32DataType> *dataStructure = static_cast<const DataStructure<UnsignedInt32DataType> *>(primitiveStructure);
		boneIndexCount = dataStructure->GetDataElementCount();

		const unsigned_int32 *data = &dataStructure->GetDataElement(0);
		arrayStorage = new unsigned_int16[boneIndexCount];
		boneIndexArray = arrayStorage;

		for (machine a = 0; a < boneIndexCount; a++)
		{
			unsigned_int32 index = data[a];
			if (index > 65535)
			{
				return (kDataOpenGexIndexValueUnsupported);
			}

			arrayStorage[a] = (unsigned_int16) index;
		}
	}
	else
	{
		const DataStructure<UnsignedInt64DataType> *dataStructure = static_cast<const DataStructure<UnsignedInt64DataType> *>(primitiveStructure);
		boneIndexCount = dataStructure->GetDataElementCount();

		const unsigned_int64 *data = &dataStructure->GetDataElement(0);
		arrayStorage = new unsigned_int16[boneIndexCount];
		boneIndexArray = arrayStorage;

		for (machine a = 0; a < boneIndexCount; a++)
		{
			unsigned_int64 index = data[a];
			if (index > 65535)
			{
				return (kDataOpenGexIndexValueUnsupported);
			}

			arrayStorage[a] = (unsigned_int16) index;
		}
	}

	return (kDataOkay);
}


BoneWeightArrayStructure::BoneWeightArrayStructure() : Structure(kStructureBoneWeightArray)
{
}

BoneWeightArrayStructure::~BoneWeightArrayStructure()
{
}

bool BoneWeightArrayStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	return (structure->GetStructureType() == kDataFloat);
}

DataResult BoneWeightArrayStructure::ProcessData(DataDescription *dataDescription)
{
	const Structure *structure = GetFirstSubnode();
	if (!structure)
	{
		return (kDataMissingSubstructure);
	}

	if (GetLastSubnode() != structure)
	{
		return (kDataExtraneousSubstructure);
	}

	const DataStructure<FloatDataType> *dataStructure = static_cast<const DataStructure<FloatDataType> *>(structure);
	if (dataStructure->GetArraySize() != 0)
	{
		return (kDataInvalidDataFormat);
	}

	boneWeightCount = dataStructure->GetDataElementCount();
	boneWeightArray = &dataStructure->GetDataElement(0);
	return (kDataOkay);
}


SkeletonStructure::SkeletonStructure() : Structure(kStructureSkeleton)
{
}

SkeletonStructure::~SkeletonStructure()
{
}

bool SkeletonStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	StructureType type = structure->GetStructureType();
	return ((type == kStructureBoneRefArray) || (type == kStructureTransform));
}

DataResult SkeletonStructure::ProcessData(DataDescription *dataDescription)
{
	DataResult result = Structure::ProcessData(dataDescription);
	if (result != kDataOkay)
	{
		return (result);
	}

	const Structure *structure = GetFirstSubstructure(kStructureBoneRefArray);
	if (!structure)
	{
		return (kDataMissingSubstructure);
	}

	if (GetLastSubstructure(kStructureBoneRefArray) != structure)
	{
		return (kDataExtraneousSubstructure);
	}

	boneRefArrayStructure = static_cast<const BoneRefArrayStructure *>(structure);

	structure = GetFirstSubstructure(kStructureTransform);
	if (!structure)
	{
		return (kDataMissingSubstructure);
	}

	if (GetLastSubstructure(kStructureTransform) != structure)
	{
		return (kDataExtraneousSubstructure);
	}

	transformStructure = static_cast<const TransformStructure *>(structure);

	if (boneRefArrayStructure->GetBoneCount() != transformStructure->GetTransformCount())
	{
		return (kDataOpenGexBoneCountMismatch);
	}

	return (kDataOkay);
}


SkinStructure::SkinStructure() : Structure(kStructureSkin)
{
	skinData.nodeHashArray = nullptr;
	skinData.inverseBindTransformArray = nullptr;
	skinData.weightDataTable = nullptr;

	weightDataStorage = nullptr;
}

SkinStructure::~SkinStructure()
{
	delete[] weightDataStorage;

	delete[] skinData.weightDataTable;
	delete[] skinData.inverseBindTransformArray;
	delete[] skinData.nodeHashArray;
}

bool SkinStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	StructureType type = structure->GetStructureType();
	return ((type == kStructureTransform) || (type == kStructureSkeleton) || (type == kStructureBoneCountArray) || (type == kStructureBoneIndexArray) || (type == kStructureBoneWeightArray));
}

DataResult SkinStructure::ProcessData(DataDescription *dataDescription)
{
	DataResult result = Structure::ProcessData(dataDescription);
	if (result != kDataOkay)
	{
		return (result);
	}

	const Structure *structure = GetFirstSubstructure(kStructureTransform);
	if (structure)
	{
		if (GetLastSubstructure(kStructureTransform) != structure)
		{
			return (kDataExtraneousSubstructure);
		}

		skinTransform = static_cast<const TransformStructure *>(structure)->GetTransform();
		static_cast<OpenGexDataDescription *>(dataDescription)->AdjustTransform(skinTransform);
	}
	else
	{
		skinTransform.SetIdentity();
	}

	structure = GetFirstSubstructure(kStructureSkeleton);
	if (!structure)
	{
		return (kDataMissingSubstructure);
	}

	if (GetLastSubstructure(kStructureSkeleton) != structure)
	{
		return (kDataExtraneousSubstructure);
	}

	skeletonStructure = static_cast<const SkeletonStructure *>(structure);

	structure = GetFirstSubstructure(kStructureBoneCountArray);
	if (!structure)
	{
		return (kDataMissingSubstructure);
	}

	if (GetLastSubstructure(kStructureBoneCountArray) != structure)
	{
		return (kDataExtraneousSubstructure);
	}

	boneCountArrayStructure = static_cast<const BoneCountArrayStructure *>(structure);

	structure = GetFirstSubstructure(kStructureBoneIndexArray);
	if (!structure)
	{
		return (kDataMissingSubstructure);
	}

	if (GetLastSubstructure(kStructureBoneIndexArray) != structure)
	{
		return (kDataExtraneousSubstructure);
	}

	boneIndexArrayStructure = static_cast<const BoneIndexArrayStructure *>(structure);

	structure = GetFirstSubstructure(kStructureBoneWeightArray);
	if (!structure)
	{
		return (kDataMissingSubstructure);
	}

	if (GetLastSubstructure(kStructureBoneWeightArray) != structure)
	{
		return (kDataExtraneousSubstructure);
	}

	boneWeightArrayStructure = static_cast<const BoneWeightArrayStructure *>(structure);

	int32 boneIndexCount = boneIndexArrayStructure->GetBoneIndexCount();
	if (boneWeightArrayStructure->GetBoneWeightCount() != boneIndexCount)
	{
		return (kDataOpenGexBoneWeightCountMismatch);
	}

	int32 vertexCount = boneCountArrayStructure->GetVertexCount();
	const unsigned_int16 *boneCountArray = boneCountArrayStructure->GetBoneCountArray();

	int32 boneWeightCount = 0;
	for (machine a = 0; a < vertexCount; a++)
	{
		unsigned_int32 count = boneCountArray[a];
		boneWeightCount += count;
	}

	if (boneWeightCount != boneIndexCount)
	{
		return (kDataOpenGexBoneWeightCountMismatch);
	}

	return (kDataOkay);
}

SkinData *SkinStructure::BuildSkinData(const OpenGexDataDescription *dataDescription)
{
	const BoneRefArrayStructure *boneRefArrayStructure = skeletonStructure->GetBoneRefArrayStructure();
	const TransformStructure *boneTransformStructure = skeletonStructure->GetTransformStructure();

	int32 boneCount = boneRefArrayStructure->GetBoneCount();
	skinData.boneCount = boneCount;

	int32 vertexCount = boneCountArrayStructure->GetVertexCount();
	int32 boneWeightCount = boneWeightArrayStructure->GetBoneWeightCount();

	unsigned_int32 *nodeHashArray = new unsigned_int32[boneCount];
	skinData.nodeHashArray = nodeHashArray;

	Transform4D *inverseBindTransformArray = new Transform4D[boneCount];
	skinData.inverseBindTransformArray = inverseBindTransformArray;

	const WeightedVertex **weightDataTable = new const WeightedVertex *[vertexCount];
	skinData.weightDataTable = weightDataTable;

	unsigned_int32 weightDataSize = vertexCount * (sizeof(WeightedVertex) - sizeof(BoneWeight)) + boneWeightCount * sizeof(BoneWeight);
	weightDataStorage = new char[weightDataSize];

	const BoneNodeStructure *const *boneNodeArray = boneRefArrayStructure->GetBoneNodeArray();
	for (machine a = 0; a < boneCount; a++)
	{
		const BoneNodeStructure *boneNode = boneNodeArray[a];
		nodeHashArray[a] = boneNode->GetNodeHash();

		Transform4D transform = boneTransformStructure->GetTransform(a);
		dataDescription->AdjustTransform(transform);

		inverseBindTransformArray[a] = Inverse(transform) * skinTransform;
	}

	const unsigned_int16 *boneCountArray = boneCountArrayStructure->GetBoneCountArray();
	const unsigned_int16 *boneIndexArray = boneIndexArrayStructure->GetBoneIndexArray();
	const float *boneWeightArray = boneWeightArrayStructure->GetBoneWeightArray();

	WeightedVertex *weightedVertex = reinterpret_cast<WeightedVertex *>(weightDataStorage);
	for (machine a = 0; a < vertexCount; a++)
	{
		skinData.weightDataTable[a] = weightedVertex;

		int32 count = boneCountArray[a];
		weightedVertex->boneCount = count;

		BoneWeight *boneWeight = weightedVertex->boneWeight;
		for (machine b = 0; b < count; b++)
		{
			boneWeight->boneIndex = boneIndexArray[b];
			boneWeight->weight = boneWeightArray[b];
			boneWeight++;
		}

		weightedVertex = reinterpret_cast<WeightedVertex *>(boneWeight);
		boneIndexArray += count;
		boneWeightArray += count;
	}

	return (&skinData);
}


MaterialStructure::MaterialStructure() : Structure(kStructureMaterial)
{
	twoSidedFlag = false;
	materialName = nullptr;
	materialObject = nullptr;
}

MaterialStructure::~MaterialStructure()
{
	if (materialObject)
	{
		materialObject->Release();
	}
}

bool MaterialStructure::ValidateProperty(const DataDescription *dataDescription, const String<>& identifier, DataType *type, void **value)
{
	if (identifier == "two_sided")
	{
		*type = kDataBool;
		*value = &twoSidedFlag;
		return (true);
	}

	return (false);
}

bool MaterialStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	return ((structure->GetBaseStructureType() == kStructureAttrib) || (structure->GetStructureType() == kStructureName));
}

DataResult MaterialStructure::ProcessData(DataDescription *dataDescription)
{
	DataResult result = Structure::ProcessData(dataDescription);
	if (result != kDataOkay)
	{
		return (result);
	}

	Editor *editor = static_cast<OpenGexDataDescription *>(dataDescription)->GetEditor();

	const Structure *structure = GetFirstSubstructure(kStructureName);
	if (structure)
	{
		if (GetLastSubstructure(kStructureName) != structure)
		{
			return (kDataExtraneousSubstructure);
		}

		if (editor)
		{
			materialName = static_cast<const NameStructure *>(structure)->GetName();

			materialObject = editor->GetEditorObject()->FindNamedMaterial(materialName);
			if (materialObject)
			{
				materialObject->Retain();
				return (kDataOkay);
			}
		}
	}

	materialObject = new MaterialObject;

	if (twoSidedFlag)
	{
		materialObject->SetMaterialFlags(materialObject->GetMaterialFlags() | kMaterialTwoSided);
	}

	structure = GetFirstSubnode();
	while (structure)
	{
		if (structure->GetBaseStructureType() == kStructureAttrib)
		{
			static_cast<const AttribStructure *>(structure)->UpdateMaterial(materialObject);
		}

		structure = structure->Next();
	}

	if (editor)
	{
		EditorObject *editorObject = editor->GetEditorObject();

		MaterialObject *object = editorObject->FindMatchingMaterial(materialObject);
		if (object)
		{
			materialObject->Release();
			materialObject = object;
			object->Retain();
		}
		else
		{
			editorObject->AddMaterial(materialObject, materialName);
		}
	}

	return (kDataOkay);
}


AttribStructure::AttribStructure(StructureType type) : Structure(type)
{
	SetBaseStructureType(kStructureAttrib);
}

AttribStructure::~AttribStructure()
{
}

bool AttribStructure::ValidateProperty(const DataDescription *dataDescription, const String<>& identifier, DataType *type, void **value)
{
	if (identifier == "attrib")
	{
		*type = kDataString;
		*value = &attribString;
		return (true);
	}

	return (false);
}


ParamStructure::ParamStructure() : AttribStructure(kStructureParam)
{
}

ParamStructure::~ParamStructure()
{
}

bool ParamStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	if (structure->GetStructureType() == kDataFloat)
	{
		const DataStructure<FloatDataType> *dataStructure = static_cast<const DataStructure<FloatDataType> *>(structure);
		unsigned_int32 arraySize = dataStructure->GetArraySize();
		return (arraySize == 0);
	}

	return (false);
}

DataResult ParamStructure::ProcessData(DataDescription *dataDescription)
{
	const Structure *structure = GetFirstSubnode();
	if (!structure)
	{
		return (kDataMissingSubstructure);
	}

	if (GetLastSubnode() != structure)
	{
		return (kDataExtraneousSubstructure);
	}

	const DataStructure<FloatDataType> *dataStructure = static_cast<const DataStructure<FloatDataType> *>(structure);
	if (dataStructure->GetDataElementCount() == 1)
	{
		param = dataStructure->GetDataElement(0);
	}
	else
	{
		return (kDataInvalidDataFormat);
	}

	return (kDataOkay);
}

void ParamStructure::UpdateMaterial(MaterialObject *materialObject) const
{
	if (GetAttribString() == "specular_power")
	{
		Attribute *attribute = materialObject->FindAttribute(kAttributeSpecular);
		if (!attribute)
		{
			materialObject->AddAttribute(new SpecularAttribute(K::white, param));
		}
		else
		{
			static_cast<SpecularAttribute *>(attribute)->SetSpecularExponent(param);
		}
	}
}


ColorStructure::ColorStructure() : AttribStructure(kStructureColor)
{
}

ColorStructure::~ColorStructure()
{
}

bool ColorStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	if (structure->GetStructureType() == kDataFloat)
	{
		const DataStructure<FloatDataType> *dataStructure = static_cast<const DataStructure<FloatDataType> *>(structure);
		unsigned_int32 arraySize = dataStructure->GetArraySize();
		return ((arraySize >= 3) && (arraySize <= 4));
	}

	return (false);
}

DataResult ColorStructure::ProcessData(DataDescription *dataDescription)
{
	const Structure *structure = GetFirstSubnode();
	if (!structure)
	{
		return (kDataMissingSubstructure);
	}

	if (GetLastSubnode() != structure)
	{
		return (kDataExtraneousSubstructure);
	}

	const DataStructure<FloatDataType> *dataStructure = static_cast<const DataStructure<FloatDataType> *>(structure);
	unsigned_int32 arraySize = dataStructure->GetArraySize();
	if (dataStructure->GetDataElementCount() == arraySize)
	{
		const float *data = &dataStructure->GetDataElement(0);

		if (arraySize == 3)
		{
			color.Set(data[0], data[1], data[2], 1.0F);
		}
		else
		{
			color.Set(data[0], data[1], data[2], data[3]);
		}
	}
	else
	{
		return (kDataInvalidDataFormat);
	}

	return (kDataOkay);
}

void ColorStructure::UpdateMaterial(MaterialObject *materialObject) const
{
	const String<>& attribString = GetAttribString();

	if (attribString == "diffuse")
	{
		Attribute *attribute = materialObject->FindAttribute(kAttributeDiffuse);
		if (!attribute)
		{
			materialObject->AddAttribute(new DiffuseAttribute(color));
		}
		else
		{
			static_cast<DiffuseAttribute *>(attribute)->SetDiffuseColor(color);
		}
	}
	else if (attribString == "specular")
	{
		Attribute *attribute = materialObject->FindAttribute(kAttributeSpecular);
		if (!attribute)
		{
			materialObject->AddAttribute(new SpecularAttribute(color, 50.0F));
		}
		else
		{
			static_cast<SpecularAttribute *>(attribute)->SetSpecularColor(color);
		}
	}
	else if (attribString == "emission")
	{
		Attribute *attribute = materialObject->FindAttribute(kAttributeEmission);
		if (!attribute)
		{
			materialObject->AddAttribute(new EmissionAttribute(color));
		}
		else
		{
			static_cast<EmissionAttribute *>(attribute)->SetEmissionColor(color);
		}
	}
}


TextureStructure::TextureStructure() : AttribStructure(kStructureTexture)
{
	texcoordIndex = 0;
	texcoordTransform.SetIdentity();
}

TextureStructure::~TextureStructure()
{
}

bool TextureStructure::ValidateProperty(const DataDescription *dataDescription, const String<>& identifier, DataType *type, void **value)
{
	if (identifier == "texcoord")
	{
		*type = kDataUnsignedInt32;
		*value = &texcoordIndex;
		return (true);
	}

	return (AttribStructure::ValidateProperty(dataDescription, identifier, type, value));
}

bool TextureStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	return ((structure->GetStructureType() == kDataString) || (structure->GetBaseStructureType() == kStructureMatrix));
}

DataResult TextureStructure::ProcessData(DataDescription *dataDescription)
{
	DataResult result = AttribStructure::ProcessData(dataDescription);
	if (result != kDataOkay)
	{
		return (result);
	}

	bool nameFlag = false;

	const Structure *structure = GetFirstSubnode();
	while (structure)
	{
		StructureType type = structure->GetStructureType();

		if (type == kDataString)
		{
			if (!nameFlag)
			{
				nameFlag = true;

				const DataStructure<StringDataType> *dataStructure = static_cast<const DataStructure<StringDataType> *>(structure);
				if (dataStructure->GetDataElementCount() == 1)
				{
					SetTextureName(static_cast<OpenGexDataDescription *>(dataDescription), dataStructure->GetDataElement(0));
				}
				else
				{
					return (kDataInvalidDataFormat);
				}
			}
			else
			{
				return (kDataExtraneousSubstructure);
			}
		}
		else
		{
			const MatrixStructure *matrixStructure = static_cast<const MatrixStructure *>(structure);
			texcoordTransform = texcoordTransform * matrixStructure->GetMatrix();
		}

		structure = structure->Next();
	}

	if (!nameFlag)
	{
		return (kDataMissingSubstructure);
	}

	return (kDataOkay);
}

void TextureStructure::SetTextureName(const OpenGexDataDescription *dataDescription, const char *name)
{
	if (name[0] == '/')
	{
		int32 start = Text::FindTextCaseless(name, "/Import/");
		if (start >= 0)
		{
			const char *importName = name + (start + 8);
			importName += Text::GetPrefixDirectoryLength(importName);

			if (importName[0] != 0)
			{
				textureName = importName;
				return;
			}
		}

		textureName = name;
	}
	else
	{
		textureName = dataDescription->GetBaseImportPath();
		textureName += name;
	}
}

void TextureStructure::UpdateMaterial(MaterialObject *materialObject) const
{
	if (textureName[0] != 0)
	{
		MapAttribute *attribute = nullptr;

		const String<>& attribString = GetAttribString();

		if (attribString == "diffuse")
		{
			attribute = new TextureMapAttribute(textureName);
		}
		else if (attribString == "specular")
		{
			attribute = new GlossMapAttribute(textureName);
		}
		else if (attribString == "normal")
		{
			attribute = new NormalMapAttribute(textureName);
		}
		else if (attribString == "emission")
		{
			attribute = new EmissionMapAttribute(textureName);
		}
		else if (attribString == "opacity")
		{
			attribute = new OpacityMapAttribute(textureName);
		}

		if (attribute)
		{
			materialObject->AddAttribute(attribute);

			int32 index = Min(texcoordIndex, kMaxMaterialTexcoordCount);
			attribute->SetTexcoordIndex(index);

			materialObject->SetTexcoordScale(index, Vector2D(texcoordTransform(0,0), texcoordTransform(1,1)));
			materialObject->SetTexcoordOffset(index, Vector2D(texcoordTransform(0,3), texcoordTransform(1,3)));
		}
	}
}


AttenStructure::AttenStructure() :
		Structure(kStructureAtten),
		attenKind("distance"),
		curveType("linear")
{
	beginParam = 0.0F;
	endParam = 1.0F;

	scaleParam = 1.0F;
	offsetParam = 0.0F;

	constantParam = 0.0F;
	linearParam = 0.0F;
	quadraticParam = 1.0F;

	powerParam = 1.0F;
}

AttenStructure::~AttenStructure()
{
}

bool AttenStructure::ValidateProperty(const DataDescription *dataDescription, const String<>& identifier, DataType *type, void **value)
{
	if (identifier == "kind")
	{
		*type = kDataString;
		*value = &attenKind;
		return (true);
	}

	if (identifier == "curve")
	{
		*type = kDataString;
		*value = &curveType;
		return (true);
	}

	return (false);
}

bool AttenStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	return (structure->GetStructureType() == kStructureParam);
}

DataResult AttenStructure::ProcessData(DataDescription *dataDescription)
{
	DataResult result = Structure::ProcessData(dataDescription);
	if (result != kDataOkay)
	{
		return (result);
	}

	if (curveType == "inverse")
	{
		linearParam = 1.0F;
	}

	const OpenGexDataDescription *openGexDataDescription = static_cast<OpenGexDataDescription *>(dataDescription);
	float distanceScale = openGexDataDescription->GetDistanceScale();
	float angleScale = openGexDataDescription->GetAngleScale();

	const Structure *structure = GetFirstSubnode();
	while (structure)
	{
		const ParamStructure *paramStructure = static_cast<const ParamStructure *>(structure);
		const String<>& attribString = paramStructure->GetAttribString();

		if (attribString == "begin")
		{
			beginParam = paramStructure->GetParam();

			if (attenKind == "distance")
			{
				beginParam *= distanceScale;
			}
			else if (attenKind == "angle")
			{
				beginParam *= angleScale;
			}
		}
		else if (attribString == "end")
		{
			endParam = paramStructure->GetParam();

			if (attenKind == "distance")
			{
				endParam *= distanceScale;
			}
			else if (attenKind == "angle")
			{
				endParam *= angleScale;
			}
		}
		else if (attribString == "scale")
		{
			scaleParam = paramStructure->GetParam();

			if (attenKind == "distance")
			{
				scaleParam *= distanceScale;
			}
			else if (attenKind == "angle")
			{
				scaleParam *= angleScale;
			}
		}
		else if (attribString == "offset")
		{
			offsetParam = paramStructure->GetParam();
		}
		else if (attribString == "constant")
		{
			constantParam = paramStructure->GetParam();
		}
		else if (attribString == "linear")
		{
			linearParam = paramStructure->GetParam();
		}
		else if (attribString == "quadratic")
		{
			quadraticParam = paramStructure->GetParam();
		}
		else if (attribString == "power")
		{
			powerParam = paramStructure->GetParam();
		}
		else
		{
			return (kDataOpenGexUndefinedAttrib);
		}

		structure = structure->Next();
	}

	return (kDataOkay);
}


KeyStructure::KeyStructure() :
		Structure(kStructureKey),
		keyKind("value")
{
}

KeyStructure::~KeyStructure()
{
}

bool KeyStructure::ValidateProperty(const DataDescription *dataDescription, const String<>& identifier, DataType *type, void **value)
{
	if (identifier == "kind")
	{
		*type = kDataString;
		*value = &keyKind;
		return (true);
	}

	return (false);
}

bool KeyStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	return (structure->GetStructureType() == kDataFloat);
}

DataResult KeyStructure::ProcessData(DataDescription *dataDescription)
{
	DataResult result = Structure::ProcessData(dataDescription);
	if (result != kDataOkay)
	{
		return (result);
	}

	const Structure *structure = GetFirstSubnode();
	if (!structure)
	{
		return (kDataMissingSubstructure);
	}

	if (GetLastSubnode() != structure)
	{
		return (kDataExtraneousSubstructure);
	}

	const DataStructure<FloatDataType> *dataStructure = static_cast<const DataStructure<FloatDataType> *>(structure);
	if (dataStructure->GetDataElementCount() == 0)
	{
		return (kDataOpenGexEmptyKeyStructure);
	}

	if ((keyKind == "value") || (keyKind == "-control") || (keyKind == "+control"))
	{
		scalarFlag = false;
	}
	else if ((keyKind == "tension") || (keyKind == "continuity") || (keyKind == "bias"))
	{
		scalarFlag = true;

		if (dataStructure->GetArraySize() != 0)
		{
			return (kDataInvalidDataFormat);
		}
	}
	else
	{
		return (kDataOpenGexInvalidKeyKind);
	}

	return (kDataOkay);
}


CurveStructure::CurveStructure(StructureType type) :
		Structure(type),
		curveType("linear")
{
	SetBaseStructureType(kStructureCurve);
}

CurveStructure::~CurveStructure()
{
}

bool CurveStructure::ValidateProperty(const DataDescription *dataDescription, const String<>& identifier, DataType *type, void **value)
{
	if (identifier == "curve")
	{
		*type = kDataString;
		*value = &curveType;
		return (true);
	}

	return (false);
}

bool CurveStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	return (structure->GetStructureType() == kStructureKey);
}

DataResult CurveStructure::ProcessData(DataDescription *dataDescription)
{
	DataResult result = Structure::ProcessData(dataDescription);
	if (result != kDataOkay)
	{
		return (result);
	}

	keyValueStructure = nullptr;
	keyControlStructure[0] = nullptr;
	keyControlStructure[1] = nullptr;
	keyTensionStructure = nullptr;
	keyContinuityStructure = nullptr;
	keyBiasStructure = nullptr;

	const Structure *structure = GetFirstSubnode();
	while (structure)
	{
		const KeyStructure *keyStructure = static_cast<const KeyStructure *>(structure);
		const String<>& keyKind = keyStructure->GetKeyKind();

		if (keyKind == "value")
		{
			if (!keyValueStructure)
			{
				keyValueStructure = keyStructure;
			}
			else
			{
				return (kDataExtraneousSubstructure);
			}
		}
		else if (keyKind == "-control")
		{
			if (curveType != "bezier")
			{
				return (kDataOpenGexInvalidKeyKind);
			}

			if (!keyControlStructure[0])
			{
				keyControlStructure[0] = keyStructure;
			}
			else
			{
				return (kDataExtraneousSubstructure);
			}
		}
		else if (keyKind == "+control")
		{
			if (curveType != "bezier")
			{
				return (kDataOpenGexInvalidKeyKind);
			}

			if (!keyControlStructure[1])
			{
				keyControlStructure[1] = keyStructure;
			}
			else
			{
				return (kDataExtraneousSubstructure);
			}
		}
		else if (keyKind == "tension")
		{
			if (curveType != "tcb")
			{
				return (kDataOpenGexInvalidKeyKind);
			}

			if (!keyTensionStructure)
			{
				keyTensionStructure = keyStructure;
			}
			else
			{
				return (kDataExtraneousSubstructure);
			}
		}
		else if (keyKind == "continuity")
		{
			if (curveType != "tcb")
			{
				return (kDataOpenGexInvalidKeyKind);
			}

			if (!keyContinuityStructure)
			{
				keyContinuityStructure = keyStructure;
			}
			else
			{
				return (kDataExtraneousSubstructure);
			}
		}
		else if (keyKind == "bias")
		{
			if (curveType != "tcb")
			{
				return (kDataOpenGexInvalidKeyKind);
			}

			if (!keyBiasStructure)
			{
				keyBiasStructure = keyStructure;
			}
			else
			{
				return (kDataExtraneousSubstructure);
			}
		}

		structure = structure->Next();
	}

	if (!keyValueStructure)
	{
		return (kDataMissingSubstructure);
	}

	if (curveType == "bezier")
	{
		if ((!keyControlStructure[0]) || (!keyControlStructure[1]))
		{
			return (kDataMissingSubstructure);
		}
	}
	else if (curveType == "tcb")
	{
		if ((!keyTensionStructure) || (!keyContinuityStructure) || (!keyBiasStructure))
		{
			return (kDataMissingSubstructure);
		}
	}

	return (kDataOkay);
}


TimeStructure::TimeStructure() : CurveStructure(kStructureTime)
{
}

TimeStructure::~TimeStructure()
{
}

DataResult TimeStructure::ProcessData(DataDescription *dataDescription)
{
	DataResult result = CurveStructure::ProcessData(dataDescription);
	if (result != kDataOkay)
	{
		return (result);
	}

	const String<>& curveType = GetCurveType();
	if ((curveType != "linear") && (curveType != "bezier"))
	{
		return (kDataOpenGexInvalidCurveType);
	}

	int32 elementCount = 0;

	const Structure *structure = GetFirstSubnode();
	while (structure)
	{
		const DataStructure<FloatDataType> *dataStructure = static_cast<DataStructure<FloatDataType> *>(structure->GetFirstSubnode());
		if (dataStructure->GetArraySize() != 0)
		{
			return (kDataInvalidDataFormat);
		}

		int32 count = dataStructure->GetDataElementCount();
		if (elementCount == 0)
		{
			elementCount = count;
		}
		else if (count != elementCount)
		{
			return (kDataOpenGexKeyCountMismatch);
		}

		structure = structure->Next();
	}

	keyDataElementCount = elementCount;
	return (kDataOkay);
}

int32 TimeStructure::CalculateInterpolationParameter(float time, float *param) const
{
	const DataStructure<FloatDataType> *valueStructure = static_cast<DataStructure<FloatDataType> *>(GetKeyValueStructure()->GetFirstSubnode());
	const float *value = &valueStructure->GetDataElement(0);

	int32 count = keyDataElementCount;
	int32 index = 0;

	for (; index < count; index++)
	{
		if (time < value[index])
		{
			break;
		}
	}

	if ((index > 0) && (index < count))
	{
		float t0 = value[index - 1];
		float t3 = value[index];

		float u = 0.0F;
		float dt = t3 - t0;
		if (dt > K::min_float)
		{
			u = (time - t0) / dt;
		}

		if (GetCurveType() == "bezier")
		{
			float t1 = static_cast<DataStructure<FloatDataType> *>(GetKeyControlStructure(1)->GetFirstSubnode())->GetDataElement(index - 1);
			float t2 = static_cast<DataStructure<FloatDataType> *>(GetKeyControlStructure(0)->GetFirstSubnode())->GetDataElement(index);

			float a0 = dt + (t1 - t2) * 3.0F;
			float a1 = a0 * 3.0F;
			float b0 = 3.0F * (t0 - t1 * 2.0F + t2);
			float b1 = b0 * 2.0F;
			float c = (t1 - t0) * 3.0F;
			float d = t0 - time;

			for (machine k = 0; k < 3; k++)
			{
				u = Saturate(u - ((((a0 * u) + b0) * u + c) * u + d) / (((a1 * u) + b1) * u + c));
			}
		}

		*param = u;
	}
	else
	{
		*param = 0.0F;
	}

	return (index - 1);
}


ValueStructure::ValueStructure() : CurveStructure(kStructureValue)
{
}

ValueStructure::~ValueStructure()
{
}

DataResult ValueStructure::ProcessData(DataDescription *dataDescription)
{
	DataResult result = CurveStructure::ProcessData(dataDescription);
	if (result != kDataOkay)
	{
		return (result);
	}

	const String<>& curveType = GetCurveType();
	if ((curveType != "linear") && (curveType != "bezier") && (curveType != "tcb"))
	{
		return (kDataOpenGexInvalidCurveType);
	}

	const AnimatableStructure *targetStructure = static_cast<TrackStructure *>(GetSuperNode())->GetTargetStructure();
	const Structure *targetDataStructure = targetStructure->GetFirstSubnode();
	if ((targetDataStructure) && (targetDataStructure->GetStructureType() == kDataFloat))
	{
		unsigned_int32 targetArraySize = static_cast<const PrimitiveStructure *>(targetDataStructure)->GetArraySize();
		int32 elementCount = 0;

		const Structure *structure = GetFirstSubnode();
		while (structure)
		{
			const DataStructure<FloatDataType> *dataStructure = static_cast<DataStructure<FloatDataType> *>(structure->GetFirstSubnode());
			unsigned_int32 arraySize = dataStructure->GetArraySize();

			if ((!static_cast<const KeyStructure *>(structure)->GetScalarFlag()) && (arraySize != targetArraySize))
			{
				return (kDataInvalidDataFormat);
			}

			int32 count = dataStructure->GetDataElementCount() / Max(arraySize, 1);
			if (elementCount == 0)
			{
				elementCount = count;
			}
			else if (count != elementCount)
			{
				return (kDataOpenGexKeyCountMismatch);
			}

			structure = structure->Next();
		}

		keyDataElementCount = elementCount;
	}

	return (kDataOkay);
}

void ValueStructure::UpdateAnimation(const OpenGexDataDescription *dataDescription, int32 index, float param, AnimatableStructure *target) const
{
	float	data[16];

	const String<>& curveType = GetCurveType();
	const float *value = &static_cast<DataStructure<FloatDataType> *>(GetKeyValueStructure()->GetFirstSubnode())->GetDataElement(0);

	int32 count = keyDataElementCount;
	int32 arraySize = Max(static_cast<PrimitiveStructure *>(target->GetFirstSubnode())->GetArraySize(), 1);

	if (index < 0)
	{
		target->UpdateAnimation(dataDescription, value);
	}
	else if (index >= count - 1)
	{
		target->UpdateAnimation(dataDescription, value + arraySize * (count - 1));
	}
	else
	{
		const float *p1 = value + arraySize * index;
		const float *p2 = p1 + arraySize;
		const float u = 1.0F - param;

		if (curveType == "linear")
		{
			for (machine k = 0; k < arraySize; k++)
			{
				data[k] = p1[k] * u + p2[k] * param;
			}
		}
		else if (curveType == "bezier")
		{
			const float *c1 = &static_cast<DataStructure<FloatDataType> *>(GetKeyControlStructure(1)->GetFirstSubnode())->GetDataElement(arraySize * index);
			const float *c2 = &static_cast<DataStructure<FloatDataType> *>(GetKeyControlStructure(0)->GetFirstSubnode())->GetDataElement(arraySize * (index + 1));

			float u2 = u * u;
			float u3 = u2 * u;
			float v2 = param * param;
			float v3 = v2 * param;
			float f1 = u2 * param * 3.0F;
			float f2 = u * v2 * 3.0F;

			for (machine k = 0; k < arraySize; k++)
			{
				data[k] = p1[k] * u3 + c1[k] * f1 + c2[k] * f2 + p2[k] * v3;
			}
		}
		else
		{
			const float *p0 = value + arraySize * MaxZero(index - 1);
			const float *p3 = value + arraySize * Min(index + 2, count - 1);

			const float *tension = &static_cast<DataStructure<FloatDataType> *>(GetKeyTensionStructure()->GetFirstSubnode())->GetDataElement(0);
			const float *continuity = &static_cast<DataStructure<FloatDataType> *>(GetKeyContinuityStructure()->GetFirstSubnode())->GetDataElement(0);
			const float *bias = &static_cast<DataStructure<FloatDataType> *>(GetKeyBiasStructure()->GetFirstSubnode())->GetDataElement(0);

			float m1 = (1.0F - tension[index]) * (1.0F + continuity[index]) * (1.0F + bias[index]) * 0.5F;
			float n1 = (1.0F - tension[index]) * (1.0F - continuity[index]) * (1.0F - bias[index]) * 0.5F;
			float m2 = (1.0F - tension[index + 1]) * (1.0F - continuity[index + 1]) * (1.0F + bias[index + 1]) * 0.5F;
			float n2 = (1.0F - tension[index + 1]) * (1.0F + continuity[index + 1]) * (1.0F - bias[index + 1]) * 0.5F;

			float u2 = u * u;
			float v2 = param * param;
			float v3 = v2 * param;
			float f1 = 1.0F - v2 * 3.0F + v3 * 2.0F;
			float f2 = v2 * (3.0F - param * 2.0F);
			float f3 = param * u2;
			float f4 = u * v2;

			for (machine k = 0; k < arraySize; k++)
			{
				float t1 = (p1[k] - p0[k]) * m1 + (p2[k] - p1[k]) * n1;
				float t2 = (p2[k] - p1[k]) * m2 + (p3[k] - p2[k]) * n2;
				data[k] = p1[k] * f1 + p2[k] * f2 + t1 * f3 - t2 * f4;
			}
		}

		target->UpdateAnimation(dataDescription, data);
	}
}


TrackStructure::TrackStructure() : Structure(kStructureTrack)
{
	targetStructure = nullptr;
}

TrackStructure::~TrackStructure()
{
}

bool TrackStructure::ValidateProperty(const DataDescription *dataDescription, const String<>& identifier, DataType *type, void **value)
{
	if (identifier == "target")
	{
		*type = kDataRef;
		*value = &targetRef;
		return (true);
	}

	return (false);
}

bool TrackStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	return (structure->GetBaseStructureType() == kStructureCurve);
}

DataResult TrackStructure::ProcessData(DataDescription *dataDescription)
{
	if (targetRef.GetGlobalRefFlag())
	{
		return (kDataOpenGexTargetRefNotLocal);
	}

	Structure *target = GetSuperNode()->GetSuperNode()->FindStructure(targetRef);
	if (!target)
	{
		return (kDataBrokenRef);
	}

	if ((target->GetBaseStructureType() != kStructureMatrix) && (target->GetStructureType() != kStructureMorph))
	{
		return (kDataOpenGexInvalidTargetStruct);
	}

	targetStructure = static_cast<AnimatableStructure *>(target);

	timeStructure = nullptr;
	valueStructure = nullptr;

	const Structure *structure = GetFirstSubnode();
	while (structure)
	{
		StructureType type = structure->GetStructureType();
		if (type == kStructureTime)
		{
			if (!timeStructure)
			{
				timeStructure = static_cast<const TimeStructure *>(structure);
			}
			else
			{
				return (kDataExtraneousSubstructure);
			}
		}
		else if (type == kStructureValue)
		{
			if (!valueStructure)
			{
				valueStructure = static_cast<const ValueStructure *>(structure);
			}
			else
			{
				return (kDataExtraneousSubstructure);
			}
		}

		structure = structure->Next();
	}

	if ((!timeStructure) || (!valueStructure))
	{
		return (kDataMissingSubstructure);
	}

	DataResult result = Structure::ProcessData(dataDescription);
	if (result != kDataOkay)
	{
		return (result);
	}

	if (timeStructure->GetKeyDataElementCount() != valueStructure->GetKeyDataElementCount())
	{
		return (kDataOpenGexKeyCountMismatch);
	}

	static_cast<AnimationStructure *>(GetSuperNode())->AddTrack(this);
	return (kDataOkay);
}

void TrackStructure::UpdateAnimation(const OpenGexDataDescription *dataDescription, float time) const
{
	float	param;

	int32 index = timeStructure->CalculateInterpolationParameter(time, &param);
	valueStructure->UpdateAnimation(dataDescription, index, param, targetStructure);
}


AnimationStructure::AnimationStructure() : Structure(kStructureAnimation)
{
	clipIndex = 0;
	beginFlag = false;
	endFlag = false;
}

AnimationStructure::~AnimationStructure()
{
	trackList.RemoveAll();
}

bool AnimationStructure::ValidateProperty(const DataDescription *dataDescription, const String<>& identifier, DataType *type, void **value)
{
	if (identifier == "clip")
	{
		*type = kDataInt32;
		*value = &clipIndex;
		return (true);
	}

	if (identifier == "begin")
	{
		beginFlag = true;
		*type = kDataFloat;
		*value = &beginTime;
		return (true);
	}

	if (identifier == "end")
	{
		endFlag = true;
		*type = kDataFloat;
		*value = &endTime;
		return (true);
	}

	return (false);
}

bool AnimationStructure::ValidateSubstructure(const DataDescription *dataDescription, const Structure *structure) const
{
	return (structure->GetStructureType() == kStructureTrack);
}

DataResult AnimationStructure::ProcessData(DataDescription *dataDescription)
{
	DataResult result = Structure::ProcessData(dataDescription);
	if (result != kDataOkay)
	{
		return (result);
	}

	if (trackList.Empty())
	{
		return (kDataMissingSubstructure);
	}

	static_cast<OpenGexDataDescription *>(dataDescription)->AddAnimation(this);
	return (kDataOkay);
}

Range<float> AnimationStructure::GetAnimationTimeRange(void) const
{
	float min = K::infinity;
	float max = 0.0F;

	const TrackStructure *trackStructure = trackList.First();
	while (trackStructure)
	{
		const KeyStructure *keyStructure = trackStructure->GetTimeStructure()->GetKeyValueStructure();
		const DataStructure<FloatDataType> *dataStructure = static_cast<DataStructure<FloatDataType> *>(keyStructure->GetFirstSubnode());

		min = Fmin(min, dataStructure->GetDataElement(0));
		max = Fmax(max, dataStructure->GetDataElement(dataStructure->GetDataElementCount() - 1));

		trackStructure = trackStructure->Next();
	}

	if (beginFlag)
	{
		min = beginTime;
	}

	if (endFlag)
	{
		max = endTime;
	}

	return (Range<float>(min, Fmax(min, max)));
}

void AnimationStructure::UpdateAnimation(const OpenGexDataDescription *dataDescription, float time) const
{
	const TrackStructure *trackStructure = trackList.First();
	while (trackStructure)
	{
		trackStructure->UpdateAnimation(dataDescription, time);
		trackStructure = trackStructure->Next();
	}
}


OpenGexDataDescription::OpenGexDataDescription(Editor *editor, const char *file) :
		worldEditor(editor),
		baseImportPath(&file[Text::GetPrefixDirectoryLength(file)])
{
	baseImportPath[Text::GetDirectoryPathLength(baseImportPath)] = 0;

	distanceScale = 1.0F;
	angleScale = 1.0F;
	timeScale = 1.0F;
	upDirection = 2;
}

OpenGexDataDescription::~OpenGexDataDescription()
{
	animationList.RemoveAll();
}

Structure *OpenGexDataDescription::ConstructStructure(const String<>& identifier) const
{
	if (identifier == "Metric")
	{
		return (new MetricStructure);
	}

	if (identifier == "VertexArray")
	{
		return (new VertexArrayStructure);
	}

	if (identifier == "IndexArray")
	{
		return (new IndexArrayStructure);
	}

	if (identifier == "Mesh")
	{
		return (new MeshStructure);
	}

	if (identifier == "Node")
	{
		return (new NodeStructure);
	}

	if (identifier == "BoneNode")
	{
		return (new BoneNodeStructure);
	}

	if (identifier == "GeometryNode")
	{
		return (new GeometryNodeStructure);
	}

	if (identifier == "LightNode")
	{
		return (new LightNodeStructure);
	}

	if (identifier == "CameraNode")
	{
		return (new CameraNodeStructure);
	}

	if (identifier == "GeometryObject")
	{
		return (new GeometryObjectStructure);
	}

	if (identifier == "LightObject")
	{
		return (new LightObjectStructure);
	}

	if (identifier == "CameraObject")
	{
		return (new CameraObjectStructure);
	}

	if (identifier == "Transform")
	{
		return (new TransformStructure);
	}

	if (identifier == "Translation")
	{
		return (new TranslationStructure);
	}

	if (identifier == "Rotation")
	{
		return (new RotationStructure);
	}

	if (identifier == "Scale")
	{
		return (new ScaleStructure);
	}

	if (identifier == "Name")
	{
		return (new NameStructure);
	}

	if (identifier == "ObjectRef")
	{
		return (new ObjectRefStructure);
	}

	if (identifier == "MaterialRef")
	{
		return (new MaterialRefStructure);
	}

	if (identifier == "Morph")
	{
		return (new MorphStructure);
	}

	if (identifier == "BoneRefArray")
	{
		return (new BoneRefArrayStructure);
	}

	if (identifier == "BoneCountArray")
	{
		return (new BoneCountArrayStructure);
	}

	if (identifier == "BoneIndexArray")
	{
		return (new BoneIndexArrayStructure);
	}

	if (identifier == "BoneWeightArray")
	{
		return (new BoneWeightArrayStructure);
	}

	if (identifier == "Skeleton")
	{
		return (new SkeletonStructure);
	}

	if (identifier == "Skin")
	{
		return (new SkinStructure);
	}

	if (identifier == "Material")
	{
		return (new MaterialStructure);
	}

	if (identifier == "Param")
	{
		return (new ParamStructure);
	}

	if (identifier == "Color")
	{
		return (new ColorStructure);
	}

	if (identifier == "Texture")
	{
		return (new TextureStructure);
	}

	if (identifier == "Atten")
	{
		return (new AttenStructure);
	}

	if (identifier == "Key")
	{
		return (new KeyStructure);
	}

	if (identifier == "Time")
	{
		return (new TimeStructure);
	}

	if (identifier == "Value")
	{
		return (new ValueStructure);
	}

	if (identifier == "Track")
	{
		return (new TrackStructure);
	}

	if (identifier == "Animation")
	{
		return (new AnimationStructure);
	}

	return (nullptr);
}

bool OpenGexDataDescription::ValidateTopLevelStructure(const Structure *structure) const
{
	StructureType type = structure->GetBaseStructureType();
	if ((type == kStructureNode) || (type == kStructureObject))
	{
		return (true);
	}

	type = structure->GetStructureType();
	return ((type == kStructureMetric) || (type == kStructureMaterial));
}

DataResult OpenGexDataDescription::ProcessData(void)
{
	DataResult result = DataDescription::ProcessData();
	if (result == kDataOkay)
	{
		Structure *structure = GetRootStructure()->GetFirstSubnode();
		while (structure)
		{
			if (structure->GetBaseStructureType() == kStructureNode)
			{
				static_cast<NodeStructure *>(structure)->UpdateNodeTransforms(this);
			}

			structure = structure->Next();
		}
	}

	return (result);
}

void OpenGexDataDescription::AdjustTransform(Transform4D& transform) const
{
	transform.SetTranslation(transform.GetTranslation() * distanceScale);

	if (upDirection == 1)
	{
		transform.Set( transform(0,0), -transform(0,2),  transform(0,1),  transform(0,3),
					  -transform(2,0),  transform(2,2), -transform(2,1), -transform(2,3),
					   transform(1,0), -transform(1,2),  transform(1,1),  transform(1,3));
	}
}

NodeStructure *OpenGexDataDescription::FindNodeStructure(unsigned_int32 hash) const
{
	if (hash != 0)
	{
		const Structure *rootStructure = GetRootStructure();
		Structure *structure = rootStructure->GetFirstSubnode();
		while (structure)
		{
			if (structure->GetBaseStructureType() == kStructureNode)
			{
				NodeStructure *nodeStructure = static_cast<NodeStructure *>(structure);
				if (nodeStructure->GetNodeHash() == hash)
				{
					return (nodeStructure);
				}
			}

			structure = rootStructure->GetNextNode(structure);
		}
	}

	return (nullptr);
}

Range<float> OpenGexDataDescription::GetAnimationTimeRange(int32 clip) const
{
	Range<float>	timeRange;

	bool animationFlag = false;

	const AnimationStructure *animationStructure = animationList.First();
	while (animationStructure)
	{
		if (animationStructure->GetClipIndex() == clip)
		{
			if (animationFlag)
			{
				Range<float> range = animationStructure->GetAnimationTimeRange();
				timeRange.min = Fmin(timeRange.min, range.min);
				timeRange.max = Fmax(timeRange.max, range.max);
			}
			else
			{
				animationFlag = true;
				timeRange = animationStructure->GetAnimationTimeRange();
			}
		}

		animationStructure = animationStructure->Next();
	}

	if (animationFlag)
	{
		timeRange.min *= timeScale;
		timeRange.max *= timeScale;
		return (timeRange);
	}

	return (Range<float>(0.0F, 0.0F));
}

void OpenGexDataDescription::UpdateAnimation(int32 clip, float time) const
{
	time /= timeScale;

	const AnimationStructure *animationStructure = animationList.First();
	while (animationStructure)
	{
		if (animationStructure->GetClipIndex() == clip)
		{
			animationStructure->UpdateAnimation(this, time);
		}

		animationStructure = animationStructure->Next();
	}

	Structure *structure = GetRootStructure()->GetFirstSubnode();
	while (structure)
	{
		if (structure->GetBaseStructureType() == kStructureNode)
		{
			static_cast<NodeStructure *>(structure)->UpdateNodeTransforms(this);
		}

		structure = structure->Next();
	}
}


OpenGexImporter::OpenGexImporter() :
		Singleton<OpenGexImporter>(TheOpenGexImporter),
		stringTable("OpenGexImporter/strings")
{
}

OpenGexImporter::~OpenGexImporter()
{
}

const char *OpenGexImporter::GetPluginName(void) const
{
	return ("OpenGEX Import Tool");
}

const ResourceDescriptor *OpenGexImporter::GetImportResourceDescriptor(SceneImportType type) const
{
	return (OpenGexResource::GetDescriptor());
}

const char *OpenGexImporter::GetOpenGexResultString(DataResult result) const
{
	const char *string = Engine::GetDataResultString(result);
	if (StringTable::MissingString(string))
	{
		string = stringTable.GetString(StringID('OGEX', result));
	}

	return (string);
}

void OpenGexImporter::RemoveDeadNodes(Node *node)
{
	Node *subnode = node->GetFirstSubnode();
	while (subnode)
	{
		Node *next = subnode->Next();
		RemoveDeadNodes(subnode);
		subnode = next;
	}

	if ((node->GetNodeType() == kNodeGroup) && (!node->GetFirstSubnode()))
	{
		delete node;
	}
}

void OpenGexImporter::ImportGeometry(Editor *editor, const char *name, const GeometryImportData *importData)
{
	OpenGexResource *resource = OpenGexResource::Get(name, 0, ThePluginMgr->GetImportCatalog());
	if (resource)
	{
		OpenGexDataDescription openGexDataDescription(editor, name);

		DataResult result = openGexDataDescription.ProcessText(resource->GetText());
		if (result == kDataOkay)
		{
			Node *root = new Node;

			const Structure *structure = openGexDataDescription.GetRootStructure()->GetFirstSubnode();
			while (structure)
			{
				if (structure->GetBaseStructureType() == kStructureNode)
				{
					Node *node = static_cast<const NodeStructure *>(structure)->BuildNodeTree(&openGexDataDescription);
					root->AddSubnode(node);
				}

				structure = structure->Next();
			}

			if (root->GetFirstSubnode())
			{
				RemoveDeadNodes(root);
				editor->ImportScene(root);
			}

			delete root;
		}
		else
		{
			const char *title = stringTable.GetString(StringID('ERRR'));
			const char *message = stringTable.GetString(StringID('EMES'));

			String<> error(stringTable.GetString(StringID('LINE')));
			((error += openGexDataDescription.GetErrorLine()) += ": ") += GetOpenGexResultString(result);

			ErrorDialog *dialog = new ErrorDialog(title, message, error, ResourcePath(name) += OpenGexResource::GetDescriptor()->GetExtension());
			editor->AddSubwindow(dialog);
		}

		resource->Release();
	}
}

bool OpenGexImporter::GenerateAnimation(const OpenGexDataDescription *dataDescription, const char *name, Model *model, const AnimationImportData *importData)
{
	Matrix3D		rotationMatrix;
	File			outputFile;
	ResourcePath	outputPath;

	int32 nodeCount = model->GetAnimatedNodeCount();
	const Node *const *nodeTable = model->GetAnimatedNodeTable();

	Array<AnimationNode> animationNodeArray(nodeCount);

	for (machine a = 0; a < nodeCount; a++)
	{
		const Node *modelNode = nodeTable[a];
		NodeStructure *nodeStructure = dataDescription->FindNodeStructure(modelNode->GetNodeHash());
		if ((nodeStructure) || (importData->importFlags & kAnimationImportPreserveMissing))
		{
			animationNodeArray.AddElement(AnimationNode(modelNode, nodeStructure));
		}
	}

	nodeCount = animationNodeArray.GetElementCount();
	int32 bucketCount = Min(Power2Ceil(nodeCount), kMaxAnimationBucketCount);
	unsigned_int32 bucketMask = bucketCount - 1;

	Array<AnimationHash> *bucketArrayTable = new Array<AnimationHash>[bucketCount];
	for (machine a = 0; a < nodeCount; a++)
	{
		unsigned_int32 hash = animationNodeArray[a].modelNode->GetNodeHash();
		bucketArrayTable[hash & bucketMask].AddElement(AnimationHash(hash, a));
	}

	float frameDuration = 33.0F;
	Range<float> timeRange = dataDescription->GetAnimationTimeRange(0);
	float duration = (timeRange.max - timeRange.min) * 1000.0F;
	int32 frameCount = (int32) (duration / frameDuration) + 1;

	unsigned_int32 headerSize = sizeof(AnimationHeader);
	unsigned_int32 hashTableSize = sizeof(AnimationHeader::HashTable) + (bucketCount - 1) * sizeof(AnimationHeader::BucketData) + nodeCount * sizeof(AnimationHeader::HashData);
	unsigned_int32 transformSequenceSize = sizeof(TransformSequenceHeader) + nodeCount * frameCount * sizeof(TransformFrameData);
	unsigned_int32 bufferSize = headerSize + hashTableSize + transformSequenceSize;
	Buffer buffer(bufferSize);

	AnimationHeader *animationHeader = buffer.GetPtr<AnimationHeader>();
	animationHeader->nodeCount = nodeCount;
	animationHeader->hashTableOffset = headerSize;
	animationHeader->sequenceCount = 1;
	animationHeader->sequenceData[0].sequenceType = kSequenceTransform;
	animationHeader->sequenceData[0].sequenceOffset = headerSize + hashTableSize;

	AnimationHeader::HashTable *hashTable = const_cast<AnimationHeader::HashTable *>(animationHeader->GetNodeHashTable());
	hashTable->bucketCount = bucketCount;

	unsigned_int32 bucketOffset = 0;
	AnimationHeader::BucketData *bucketData = hashTable->bucketData;
	AnimationHeader::HashData *hashData = const_cast<AnimationHeader::HashData *>(hashTable->GetHashData());

	for (machine a = 0; a < bucketCount; a++)
	{
		const Array<AnimationHash>& bucketArray = bucketArrayTable[a];

		int32 bucketSize = bucketArray.GetElementCount();
		bucketData->bucketSize = (unsigned_int16) bucketSize;
		bucketData->bucketOffset = (unsigned_int16) bucketOffset;

		const AnimationHash *animationHash = bucketArray;
		for (machine b = 0; b < bucketSize; b++)
		{
			hashData->hashValue = animationHash->hashValue;
			hashData->nodeIndex = animationHash->nodeIndex;

			hashData++;
			animationHash++;
		}

		bucketOffset += bucketSize;
		bucketData++;
	}

	delete[] bucketArrayTable;

	TransformSequenceHeader *transformSequenceHeader = static_cast<TransformSequenceHeader *>(const_cast<void *>(animationHeader->GetSequenceHeader(0)));
	transformSequenceHeader->frameCount = frameCount;
	transformSequenceHeader->frameDuration = frameDuration;

	Point3D *positionTable = new Point3D[nodeCount];
	rotationMatrix.SetRotationAboutZ(importData->importRotation);

	for (machine frame = 0; frame < frameCount; frame++)
	{
		dataDescription->UpdateAnimation(0, timeRange.min + (float) (frame * frameDuration) * 0.001F);

		TransformFrameData *transformFrameData = const_cast<TransformFrameData *>(transformSequenceHeader->GetFrameData() + frame * nodeCount);
		for (machine a = 0; a < nodeCount; a++)
		{
			const AnimationNode *animationNode = &animationNodeArray[a];
			const Node *modelNode = animationNode->modelNode;
			const NodeStructure *nodeStructure = animationNode->nodeStructure;

			Transform4D transform = (nodeStructure) ? nodeStructure->CalculateFinalNodeTransform() : modelNode->GetNodeTransform();
			transformFrameData[a].transform = transform.GetMatrix3D();

			if (modelNode->GetSuperNode()->GetNodeType() != kNodeModel)
			{
				transformFrameData[a].position = transform.GetTranslation();
			}
			else
			{
				Point3D position = transform.GetTranslation();

				unsigned_int32 flags = importData->importFlags;
				if (flags & (kAnimationImportAnchorXY | kAnimationImportAnchorZ | kAnimationImportFreezeRoot))
				{
					if (frame != 0)
					{
						const Point3D& p = positionTable[a];
						if (flags & kAnimationImportFreezeRoot)
						{
							position -= p;
						}
						else
						{
							if (flags & kAnimationImportAnchorXY)
							{
								position.x = p.x;
								position.y = p.y;
							}

							if (flags & kAnimationImportAnchorZ)
							{
								position.z = p.z;
							}
						}
					}
					else
					{
						if (flags & kAnimationImportFreezeRoot)
						{
							positionTable[a] = position - modelNode->GetNodePosition();
							position = modelNode->GetNodePosition();
						}
						else
						{
							positionTable[a] = position;
						}
					}
				}

				if (modelNode->GetNodeType() == kNodeGeometry)
				{
					const Controller *controller = modelNode->GetController();
					if ((controller) && (controller->GetControllerType() == kControllerSkin))
					{
						transformFrameData[a].position = position;
						continue;
					}
				}

				transformFrameData[a].position = rotationMatrix * position;
				transformFrameData[a].transform = rotationMatrix * transformFrameData[a].transform;
			}
		}
	}

	delete[] positionTable;

	TheResourceMgr->GetGenericCatalog()->GetResourcePath(AnimationResource::GetDescriptor(), name, &outputPath);
	TheResourceMgr->CreateDirectoryPath(outputPath);

	if (outputFile.Open(outputPath, kFileCreate) == kFileOkay)
	{
		int32 endian = 1;
		outputFile.Write(&endian, 4);
		outputFile.Write(buffer, bufferSize);
		return (true);
	}

	return (false);
}

bool OpenGexImporter::ImportAnimation(Window *window, const char *name, Model *model, const AnimationImportData *importData)
{
	bool success = false;

	OpenGexResource *resource = OpenGexResource::Get(name, 0, ThePluginMgr->GetImportCatalog());
	if (resource)
	{
		OpenGexDataDescription openGexDataDescription(nullptr, name);

		DataResult result = openGexDataDescription.ProcessText(resource->GetText());
		if (result == kDataOkay)
		{
			if (!openGexDataDescription.GetAnimationList()->Empty())
			{
				success = GenerateAnimation(&openGexDataDescription, name, model, importData);
			}
		}
		else
		{
			const char *title = stringTable.GetString(StringID('ERRR'));
			const char *message = stringTable.GetString(StringID('EMES'));

			String<> error(stringTable.GetString(StringID('LINE')));
			((error += openGexDataDescription.GetErrorLine()) += ": ") += GetOpenGexResultString(result);

			ErrorDialog *dialog = new ErrorDialog(title, message, error, ResourcePath(name) += OpenGexResource::GetDescriptor()->GetExtension());
			window->AddSubwindow(dialog);
		}

		resource->Release();
	}

	return (success);
}

// ZYUTNLM
