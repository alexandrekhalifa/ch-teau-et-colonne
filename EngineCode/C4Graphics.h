//=============================================================
//
// C4 Engine version 3.5.1
// Copyright 1999-2013, by Terathon Software LLC
//
// This copy is licensed to the following:
//
//     Registered user: Université de Sherbrooke
//     Maximum number of users: Site License
//     License #C4T0033897
//
// License is granted under terms of the license agreement
// entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#ifndef C4GraphicsMgr_h
#define C4GraphicsMgr_h


//# \component	Graphics Manager
//# \prefix		GraphicsMgr/


#define C4CREATE_DEBUG_CONTEXT		0


#include "C4Threads.h"
#include "C4Variables.h"
#include "C4Renderable.h"
#include "C4CameraObjects.h"
#include "C4LightObjects.h"


namespace C4
{
	typedef EngineResult	GraphicsResult;


	enum
	{
		kGraphicsOkay				= kEngineOkay,
		kGraphicsFormatFailed		= (kManagerGraphics << 16) | 0x0001,
		kGraphicsContextFailed		= (kManagerGraphics << 16) | 0x0002,
		kGraphicsNoHardware			= (kManagerGraphics << 16) | 0x0003
	};


	enum
	{
		kExtensionColorBufferFloat,
		kExtensionConservativeDepth,
		kExtensionDebugOutput,
		kExtensionDepthClamp,
		kExtensionFramebufferObject,
		kExtensionFramebufferBlit,
		kExtensionFramebufferMultisample,
		kExtensionFramebufferSRGB,
		kExtensionGeometryShader4,
		kExtensionGetProgramBinary,
		kExtensionHalfFloatPixel,
		kExtensionInstancedArrays,
		kExtensionInvalidateSubdata,
		kExtensionMapBufferRange,
		kExtensionPackedDepthStencil,
		kExtensionSampleShading,
		kExtensionSeamlessCubeMap,
		kExtensionShaderTextureLod,
		kExtensionTessellationShader,
		kExtensionTextureArray,
		kExtensionTextureCompressionRGTC,
		kExtensionTextureFloat,
		kExtensionTextureRectangle,
		kExtensionTextureRG,
		kExtensionTextureSwizzle,
		kExtensionTimerQuery,
		kExtensionUniformBufferObject,
		kExtensionVertexArrayObject,
		kExtensionDepthBoundsTest,
		kExtensionDirectStateAccess,
		kExtensionTextureCompressionS3TC,
		kExtensionTextureFilterAnisotropic,
		kExtensionTextureMirrorClamp,
		kExtensionConditionalRender,
		kExtensionExplicitMultisample,
		kExtensionFramebufferMultisampleCoverage,
		kExtensionShaderBufferLoad,
		kExtensionTransformFeedback,
		kExtensionVertexBufferUnifiedMemory,
		kGraphicsExtensionCount
	};


	enum
	{
		kCapabilityFramebufferFloat,
		kCapabilityTextureSwizzle,
		kCapabilityUnifiedMemory,
		kGraphicsCapabilityCount
	};


	#if C4WINDOWS

		enum
		{
			kWindowSystemExtensionCreateContext,
			kWindowSystemExtensionPixelFormat,
			kWindowSystemExtensionSwapControl,
			kWindowSystemExtensionSwapControlTear,
			kWindowSystemExtensionCount
		};

	#elif C4LINUX

		enum
		{ 
			kWindowSystemExtensionCreateContextProfile,
			kWindowSystemExtensionSwapControl,
			kWindowSystemExtensionSwapControlTear, 
			kWindowSystemExtensionCount
		}; 
 
	#endif


	enum 
	{
		kRenderOptionNormalizeBumps			= 1 << 0,
		kRenderOptionParallaxMapping		= 1 << 1,
		kRenderOptionHorizonMapping			= 1 << 2,
		kRenderOptionTerrainBumps			= 1 << 3, 
		kRenderOptionStructureEffects		= 1 << 4,
		kRenderOptionAmbientOcclusion		= 1 << 5,
		kRenderOptionMotionBlur				= 1 << 6,
		kRenderOptionDistortion				= 1 << 7,
		kRenderOptionGlowBloom				= 1 << 8
	};


	enum
	{
		kGraphicsActiveTimer				= 1 << 0,
		kGraphicsActiveStructureRendering	= 1 << 1,
		kGraphicsActiveStructureEffects		= 1 << 2,
		kGraphicsActiveAmbientOcclusion		= 1 << 3
	};


	enum
	{
		kStructureClearBuffer				= 1 << 0,
		kStructureZeroBackgroundVelocity	= 1 << 1,
		kStructureRenderVelocity			= 1 << 2,
		kStructureRenderDepth				= 1 << 3
	};


	enum
	{
		kWireframeColor						= 1 << 0,
		kWireframeTwoSided					= 1 << 1,
		kWireframeDepthTest					= 1 << 2
	};


	enum
	{
		kProcessGridWidth					= 16,
		kProcessGridHeight					= 12
	};


	enum
	{
		kTextureUnitLightProjection			= 15
	};


	#define TEXTURE_UNIT_LIGHT_PROJECTION	"15"


	enum
	{
		kGraphicsTimeIndexBeginRendering,
		kGraphicsTimeIndexBeginStructure,
		kGraphicsTimeIndexEndStructure,
		kGraphicsTimeIndexBeginOcclusion = kGraphicsTimeIndexEndStructure,
		kGraphicsTimeIndexEndOcclusion,
		kGraphicsTimeIndexBeginShadow,
		kGraphicsTimeIndexEndShadow,
		kGraphicsTimeIndexBeginPost,
		kGraphicsTimeIndexEndPost,
		kGraphicsTimeIndexEndRendering,
		kGraphicsTimeIndexCount
	};


	enum
	{
		kDiagnosticWireframe				= 1 << 0,
		kDiagnosticDepthTest				= 1 << 1,
		kDiagnosticNormals					= 1 << 2,
		kDiagnosticTangents					= 1 << 3,
		kDiagnosticShadows					= 1 << 4,
		kDiagnosticShadowBounds				= 1 << 5,
		kDiagnosticTimer					= 1 << 6
	};


	enum
	{
		kGraphicsCounterDirectVertices,
		kGraphicsCounterDirectPrimitives,
		kGraphicsCounterDirectCommands,
		kGraphicsCounterShadowVertices,
		kGraphicsCounterShadowPrimitives,
		kGraphicsCounterShadowCommands,
		kGraphicsCounterStencilVertices,
		kGraphicsCounterStencilPrimitives,
		kGraphicsCounterStencilCommands,
		kGraphicsCounterVelocityVertices,
		kGraphicsCounterVelocityPrimitives,
		kGraphicsCounterVelocityCommands,
		kGraphicsCounterDistortionVertices,
		kGraphicsCounterDistortionPrimitives,
		kGraphicsCounterDistortionCommands,
		kGraphicsCounterStencilClears,
		kGraphicsCounterCount
	};


	enum RenderTargetMode
	{
		kRenderTargetFrameBufferObject,
		kRenderTargetCopyTexture
	};


	enum RenderTargetType
	{
		kRenderTargetNone = -1,
		kRenderTargetDisplay,
		kRenderTargetPrimary,
		kRenderTargetReflection,
		kRenderTargetRefraction,
		kRenderTargetGlowBloom = kRenderTargetReflection,
		kRenderTargetDistortion = kRenderTargetRefraction,
		kRenderTargetStructure,
		kRenderTargetOcclusion1,
		kRenderTargetOcclusion2,
		kRenderTargetCount
	};


	enum StencilType
	{
		kStencilInfiniteExtrusion,
		kStencilPointExtrusion,
		kStencilEndcapProjection,
		kStencilEndcapIdentity,
		kStencilTypeCount
	};


	enum StencilMode
	{
		kStencilNone,
		kStencilPass,
		kStencilFail
	};


	enum AmbientMode
	{
		kAmbientNormal,
		kAmbientBright,
		kAmbientDark
	};


	const float kShaderTimePeriod = 120000.0F;
	const float kInverseShaderTimePeriod = 1.0F / kShaderTimePeriod;


	class FogSpaceObject;


	struct GraphicsCapabilities
	{
		unsigned_int32		openglVersion;
		unsigned_int32		hardwareSpeed;

		bool				extensionFlag[kGraphicsExtensionCount];
		bool				capabilityFlag[kGraphicsCapabilityCount];

		#if C4WINDOWS || C4LINUX

			bool			windowSystemExtensionFlag[kWindowSystemExtensionCount];

		#endif

		int32				maxTextureSize;
		int32				max3DTextureSize;
		int32				maxCubeTextureSize;
		int32				maxArrayTextureLayers;
		float				maxTextureAnisotropy;
		float				maxTextureLodBias;
		int32				maxCombinedTextureImageUnits;

		int32				queryCounterBits;

		int32				maxColorAttachments;
		int32				maxRenderbufferSize;
		int32				maxMultisampleSamples;

		int32				maxVertexAttribs;
		int32				maxVaryingComponents;
		int32				maxVertexUniformComponents;
		int32				maxVertexTextureImageUnits;

		int32				maxFragmentUniformComponents;
		int32				maxFragmentTextureImageUnits;

		int32				maxVertexVaryingComponents;
		int32				maxGeometryVaryingComponents;
		int32				maxGeometryUniformComponents;
		int32				maxGeometryOutputVertices;
		int32				maxGeometryTotalOutputComponents;
		int32				maxGeometryTextureImageUnits;

		int32				programBinaryFormats;
		int32				*programBinaryFormatArray;

		int32				maxTransformFeedbackInterleavedComponents;
		int32				maxTransformFeedbackSeparateComponents;
		int32				maxTransformFeedbackSeparateAttribs;

		GraphicsCapabilities();
		~GraphicsCapabilities();
	};


	struct GraphicsExtensionData
	{
		const char			*name1;
		const char			*name2;
		unsigned_int16		version;
		bool				required;
		mutable bool		enabled;
	};


	#if C4WINDOWS || C4LINUX

		struct WindowSystemExtensionData
		{
			const char		*name;
			mutable bool	enabled;
		};

	#endif


	#if C4PS3 //[ 

			// -- PS3 code hidden --

	#endif //]


	class NormalFrameBuffer : public Render::FrameBufferObject
	{
		private:

			unsigned_int32					renderTargetMask;
			Render::TextureObject			textureObject[kRenderTargetCount];

			Render::RenderBufferObject		depthRenderBuffer;

		public:

			NormalFrameBuffer(int32 width, int32 height, unsigned_int32 mask);
			~NormalFrameBuffer();

			unsigned_int32 GetRenderTargetMask(void) const
			{
				return (renderTargetMask);
			}

			const Render::TextureObject *GetRenderTargetTexture(int32 target) const
			{
				return (&textureObject[target]);
			}

			void Invalidate(void);
	};


	class ShadowFrameBuffer : public Render::FrameBufferObject
	{
		private:

			Render::TextureObject			textureObject;

		public:

			ShadowFrameBuffer(int32 width, int32 height);
			~ShadowFrameBuffer();

			Render::TextureObject *GetRenderTargetTexture(void)
			{
				return (&textureObject);
			}

			void SetTextureAnisotropy(int32 anisotropy);

			void Invalidate(void);
	};


	class MultisampleFrameBuffer : public Render::FrameBufferObject
	{
		private:

			Render::RenderBufferObject		colorRenderBuffer;
			Render::RenderBufferObject		depthRenderBuffer;

			float							sampleDivider;

		public:

			MultisampleFrameBuffer(int32 width, int32 height, int32 sampleCount);
			~MultisampleFrameBuffer();

			float GetSampleDivider(void) const
			{
				return (sampleDivider);
			}
	};


	//# \class	GraphicsMgr		The Graphics Manager class.
	//
	//# \def	class GraphicsMgr : public Manager<GraphicsMgr>
	//
	//# \desc
	//# The $GraphicsMgr$ class encapsulates the 3D graphics rendering facilities of the C4 Engine.
	//# The single instance of the Graphics Manager is constructed during an application's initialization
	//# and destroyed at termination.
	//# 
	//# The Graphics Manager's member functions are accessed through the global pointer $TheGraphicsMgr$.
	//
	//# \also	$@Renderable@$


	//# \function	GraphicsMgr::GetCameraObject	Returns the current camera object.
	//
	//# \proto	const CameraObject *GetCameraObject(void) const;
	//
	//# \desc
	//# 
	//
	//# \also	$@GraphicsMgr::GetCameraTransformable@$
	//# \also	$@CameraObject@$


	//# \function	GraphicsMgr::GetCameraTransformable		Returns the current camera transform.
	//
	//# \proto	const Transformable *GetCameraTransformable(void) const;
	//
	//# \desc
	//# 
	//
	//# \also	$@GraphicsMgr::GetCameraObject@$
	//# \also	$@Utilities/Transformable@$


	//# \function	GraphicsMgr::GetLightObject		Returns the current light object.
	//
	//# \proto	const LightObject *GetLightObject(void) const;
	//
	//# \desc
	//# 
	//
	//# \also	$@GraphicsMgr::GetLightTransformable@$
	//# \also	$@LightObject@$


	//# \function	GraphicsMgr::GetLightTransformable		Returns the current light transform.
	//
	//# \proto	const Transformable *GetLightTransformable(void) const;
	//
	//# \desc
	//# 
	//
	//# \also	$@GraphicsMgr::GetLightObject@$
	//# \also	$@Utilities/Transformable@$


	//# \function	GraphicsMgr::DrawRenderList		Draws a list of renderable objects.
	//
	//# \proto	void DrawRenderList(const List<Renderable> *renderList);
	//
	//# \desc
	//# 
	//
	//# \also	$@Renderable@$


	class GraphicsMgr : public Manager<GraphicsMgr>
	{
		private:

			#if C4WINDOWS

				HDC								deviceContext;
				HGLRC							openglContext;

			#elif C4MACOS

				CFBundleRef						openglBundle;
				NSOpenGLContext					*openglContext;

			#elif C4LINUX

				::Display						*openglDisplay;
				::Colormap						openglColormap;
				::Window						openglWindow;
				GLXContext						openglContext;

			#endif

			#if C4OPENGL

				String<>						extensionsString;

				#if C4WINDOWS || C4LINUX

					const char					*windowSystemExtensionsString;

				#endif

			#endif

			Storage<GraphicsCapabilities>		capabilities;
			unsigned_int32						driverVersion;

			int32								dynamicShadowMapSize;
			unsigned_int32						targetDisableMask;

			Storage<Signal>						syncRenderSignal;
			NullClass							*volatile syncRenderObject;
			const void							*volatile syncRenderData;
			void								(NullClass::*volatile syncRenderFunction)(const void *);
			volatile bool						syncLoadFlag;

			const CameraObject					*cameraObject;
			const Transformable					*cameraTransformable;
			Vector4D							cameraPosition4D;
			Point3D								directCameraPosition;
			Transform4D							cameraSpaceTransform;
			Transform4D							previousCameraSpaceTransform;

			const CameraObject					*savedCameraObject;
			const Transformable					*savedCameraTransformable;

			const FogSpaceObject				*fogSpaceObject;
			const Transformable					*fogSpaceTransformable;
			Antivector4D						worldFogPlane;

			const LightObject					*lightObject;
			const Transformable					*lightTransformable;
			const LightShadowData				*lightShadowData;

			const Transformable					*geometryTransformable;
			Vector4D							geometryLightPosition;

			unsigned_int32						graphicsActiveFlags;

			unsigned_int32						currentBlendState;
			StencilMode							currentStencilMode;

			unsigned_int32						currentGraphicsState;
			unsigned_int32						currentRenderState;
			unsigned_int32						disabledRenderState;

			unsigned_int32						currentMaterialState;
			ShaderType							currentShaderType;
			ShaderVariant						currentShaderVariant;
			AmbientMode							currentAmbientMode;

			float								currentNearDepth;
			unsigned_int32						currentFrustumFlags;

			Matrix4D							cameraProjectionMatrix;
			Matrix4D							standardProjectionMatrix;
			Matrix4D							currentProjectionMatrix;
			Matrix4D							currentMVPMatrix;

			unsigned_int32						currentStructureFlags;
			float								occlusionPlaneScale;
			float								renderTargetOffsetSize;
			float								depthOffsetConstant;
			float								cameraLensMultiplier;

			unsigned_int32						colorTransformFlags;
			float								brightnessMultiplier;
			ColorRGBA							finalColorScale[3];
			ColorRGBA							finalColorBias;

			Rect								clipRect;
			Rect								viewportRect;
			Rect								cameraRect;
			Rect								scissorRect;
			Rect								lightRect;
			Rect								shadowRect;
			Range<float>						lightDepthBounds;

			unsigned_int32						currentArrayState;

			OcclusionQuery						*currentOcclusionQuery;
			List<OcclusionQuery>				occlusionQueryList;

			Antivector4D						distortionPlane;
			float								occlusionAreaNormalizer;

			float								motionBlurBoxLeftOffset;
			float								motionBlurBoxRightOffset;
			float								motionBlurBoxBottomOffset;
			float								motionBlurBoxTopOffset;

			Texture								*nullTexture;

			unsigned_int32						renderOptionFlags;
			int32								textureDetailLevel;
			int32								paletteDetailLevel;
			int32								textureFilterAnisotropy;

			RenderTargetType					currentRenderTargetType;
			int32								renderTargetHeight;

			Render::FrameBufferObject			genericFrameBuffer;
			NormalFrameBuffer					*normalFrameBuffer;
			ShadowFrameBuffer					*shadowFrameBuffer;
			MultisampleFrameBuffer				*multisampleFrameBuffer;

			Render::QueryObject					timerQuery[kGraphicsTimeIndexCount];

			VertexBuffer						fullscreenVertexBuffer;
			VertexBuffer						glowBloomVertexBuffer;
			VertexBuffer						processGridVertexBuffer;
			VertexBuffer						processGridIndexBuffer;

			bool								motionGridFlag[kProcessGridWidth * kProcessGridHeight];

			unsigned_int32						diagnosticFlags;
			int32								graphicsCounter[kGraphicsCounterCount];

			VariableObserver<GraphicsMgr>		textureDetailLevelObserver;
			VariableObserver<GraphicsMgr>		paletteDetailLevelObserver;
			VariableObserver<GraphicsMgr>		textureAnisotropyObserver;
			VariableObserver<GraphicsMgr>		renderNormalizeBumpsObserver;
			VariableObserver<GraphicsMgr>		renderParallaxMappingObserver;
			VariableObserver<GraphicsMgr>		renderHorizonMappingObserver;
			VariableObserver<GraphicsMgr>		renderTerrainBumpsObserver;
			VariableObserver<GraphicsMgr>		renderStructureEffectsObserver;
			VariableObserver<GraphicsMgr>		renderAmbientOcclusionObserver;
			VariableObserver<GraphicsMgr>		postBrightnessObserver;
			VariableObserver<GraphicsMgr>		postMotionBlurObserver;
			VariableObserver<GraphicsMgr>		postDistortionObserver;
			VariableObserver<GraphicsMgr>		postGlowBloomObserver;

			static GraphicsExtensionData		extensionData[kGraphicsExtensionCount];

			#if C4WINDOWS || C4LINUX

				static WindowSystemExtensionData	windowSystemExtensionData[kWindowSystemExtensionCount];

				#if C4WINDOWS

					void InitializeWglExtensions(PIXELFORMATDESCRIPTOR *formatDescriptor);
					static LRESULT CALLBACK WglWindowProc(HWND window, UINT message, WPARAM wparam, LPARAM lparam);

				#elif C4LINUX

					void InitializeGlxExtensions(void);

				#endif

			#endif

			#if C4OPENGL && C4DEBUG && C4CREATE_DEBUG_CONTEXT

				static void OPENGLAPI DebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, void *userParam);

			#endif

			static void LogExtensions(const char *string);
			void UpdateLog(void) const;

			GraphicsResult InitializeGraphicsContext(int32 frameWidth, int32 frameHeight, int32 displayWidth, int32 displayHeight, unsigned_int32 displayFlags);
			void TerminateGraphicsContext(void);

			void SetDisplaySyncMode(const unsigned_int32 *displayFlags);

			void DetermineHardwareSpeed(void);
			bool InitializeOpenglExtensions(void);

			void InitializeVariables(void);
			void HandleTextureDetailLevelEvent(Variable *variable);
			void HandlePaletteDetailLevelEvent(Variable *variable);
			void HandleTextureAnisotropyEvent(Variable *variable);
			void HandleRenderNormalizeBumpsEvent(Variable *variable);
			void HandleRenderParallaxMappingEvent(Variable *variable);
			void HandleRenderHorizonMappingEvent(Variable *variable);
			void HandleRenderTerrainBumpsEvent(Variable *variable);
			void HandleRenderStructureEffectsEvent(Variable *variable);
			void HandleRenderAmbientOcclusionEvent(Variable *variable);
			void HandlePostBrightnessEvent(Variable *variable);
			void HandlePostMotionBlurEvent(Variable *variable);
			void HandlePostDistortionEvent(Variable *variable);
			void HandlePostGlowBloomEvent(Variable *variable);

			void InitializeProcessGrid(void);
			void InitializeActiveFlags(void);

			static void SyncRenderTask(void (NullClass::*proc)(const void *), NullClass *object, const void *data);

			void SetPostProcessingShader(unsigned_int32 postFlags, const VertexSnippet *snippet);
			void SetDisplayWarpingShader(void);

			template <int32 index> static void GroupRenderSublist(List<Renderable> *list, unsigned_int32 bit, List<Renderable> *finalList);
			static void SortRenderSublist(List<Renderable> *list, float zmin, float zmax, List<Renderable> *finalList);

			void SetModelviewMatrix(const Transform4D& matrix);
			void SetGeometryModelviewMatrix(const Transform4D& matrix);

			VertexShader *GetLocalVertexShader(const VertexSnippet *snippet);
			VertexShader *GetLocalVertexShader(int32 snippetCount, const VertexSnippet *const *snippet);
			FragmentShader *GetLocalFragmentShader(int32 shaderIndex);
			GeometryShader *GetLocalGeometryShader(int32 shaderIndex);

			void SetBlendState(unsigned_int32 newBlendState);
			void SetRenderState(unsigned_int32 newRenderState);
			void SetMaterialState(unsigned_int32 newMaterialState);
			void ResetArrayState(void);

			void SetVertexArray(const ShaderData *shaderData);
			void SetPosition1Array(const ShaderData *shaderData);
			void SetPreviousArray(const ShaderData *shaderData);
			void SetNormalArray(const ShaderData *shaderData);
			void SetTangentArray(const ShaderData *shaderData);
			void SetOffsetArray(const ShaderData *shaderData);
			void SetColorArray(const ShaderData *shaderData);
			void SetAuxColorArray(const ShaderData *shaderData, int32 index);
			void SetTexcoordArray(const ShaderData *shaderData, int32 index);

			#if C4DIAGNOSTICS

				void DrawShadowBoundsDiagnostic(const StencilShadow *shadow);
				void DrawStencilDiagnostic(const StencilShadow *shadow, StencilType type, StencilMode mode);

			#endif

		public:

			GraphicsMgr(int);
			~GraphicsMgr();

			EngineResult Construct(void);
			void Destruct(void);

			#if C4WINDOWS

				HDC GetDeviceContext(void) const
				{
					return (deviceContext);
				}

			#endif

			const GraphicsCapabilities *GetCapabilities(void) const
			{
				return (capabilities);
			}

			static const GraphicsExtensionData *GetExtensionData(void)
			{
				return (extensionData);
			}

			#if C4OPENGL

				#if C4WINDOWS || C4LINUX

					static const WindowSystemExtensionData *GetWindowSystemExtensionData(void)
					{
						return (windowSystemExtensionData);
					}

				#endif

				static const char *GetOpenGLVendor(void)
				{
					return (reinterpret_cast<const char *>(glGetString(GL_VENDOR)));
				}

				static const char *GetOpenGLRenderer(void)
				{
					return (reinterpret_cast<const char *>(glGetString(GL_RENDERER)));
				}

				static const char *GetOpenGLVersion(void)
				{
					return (reinterpret_cast<const char *>(glGetString(GL_VERSION)));
				}

				static const char *GetGLSLVersion(void)
				{
					return (reinterpret_cast<const char *>(glGetString(GL_SHADING_LANGUAGE_VERSION)));
				}

			#endif

			int32 GetDynamicShadowMapSize(void) const
			{
				return (dynamicShadowMapSize);
			}

			unsigned_int32 GetTargetDisableMask(void) const
			{
				return (targetDisableMask);
			}

			void SetTargetDisableMask(unsigned_int32 mask)
			{
				targetDisableMask = mask;
			}

			const CameraObject *GetCameraObject(void) const
			{
				return (cameraObject);
			}

			const Transformable *GetCameraTransformable(void) const
			{
				return (cameraTransformable);
			}

			const Vector4D& GetCameraPosition4D(void) const
			{
				return (cameraPosition4D);
			}

			const Point3D& GetDirectCameraPosition(void) const
			{
				return (directCameraPosition);
			}

			const Transform4D& GetCameraSpaceTransform(void) const
			{
				return (cameraSpaceTransform);
			}

			const FogSpaceObject *GetFogSpaceObject(void) const
			{
				return (fogSpaceObject);
			}

			const Transformable *GetFogSpaceTransformable(void) const
			{
				return (fogSpaceTransformable);
			}

			const Antivector4D& GetFogPlane(void) const
			{
				return (worldFogPlane);
			}

			const LightObject *GetLightObject(void) const
			{
				return (lightObject);
			}

			const Transformable *GetLightTransformable(void) const
			{
				return (lightTransformable);
			}

			const LightShadowData *GetLightShadowData(void) const
			{
				return (lightShadowData);
			}

			unsigned_int32 GetGraphicsActiveFlags(void) const
			{
				return (graphicsActiveFlags);
			}

			AmbientMode GetAmbientMode(void) const
			{
				return (currentAmbientMode);
			}

			void SetAmbientMode(AmbientMode mode)
			{
				currentAmbientMode = mode;
			}

			unsigned_int32 GetStructureFlags(void) const
			{
				return (currentStructureFlags);
			}

			float GetBrightnessMultiplier(void) const
			{
				return (brightnessMultiplier);
			}

			void SetBrightnessMultiplier(float brightness)
			{
				brightnessMultiplier = brightness;
			}

			const ColorRGBA& GetFinalColorScale(int32 index = 0) const
			{
				return (finalColorScale[index]);
			}

			const ColorRGBA& GetFinalColorBias(void) const
			{
				return (finalColorBias);
			}

			float GetRenderTargetOffsetSize(void) const
			{
				return (renderTargetOffsetSize);
			}

			void SetCameraLensMultiplier(float multiplier)
			{
				cameraLensMultiplier = multiplier;
			}

			Texture *GetNullTexture(void) const
			{
				return (nullTexture);
			}

			const Render::TextureObject *GetPrimaryTexture(void) const
			{
				return (normalFrameBuffer->GetRenderTargetTexture(kRenderTargetPrimary));
			}

			const Render::TextureObject *GetReflectionTexture(void) const
			{
				return (normalFrameBuffer->GetRenderTargetTexture(kRenderTargetReflection));
			}

			const Render::TextureObject *GetRefractionTexture(void) const
			{
				return (normalFrameBuffer->GetRenderTargetTexture(kRenderTargetRefraction));
			}

			const Render::TextureObject *GetStructureTexture(void) const
			{
				return (normalFrameBuffer->GetRenderTargetTexture(kRenderTargetStructure));
			}

			const Render::TextureObject *GetOcclusionTexture(void) const
			{
				return (normalFrameBuffer->GetRenderTargetTexture(kRenderTargetOcclusion2));
			}

			const Render::TextureObject *GetShadowMapTexture(void) const
			{
				return (shadowFrameBuffer->GetRenderTargetTexture());
			}

			unsigned_int32 GetRenderOptionFlags(void) const
			{
				return (renderOptionFlags);
			}

			int32 GetTextureDetailLevel(void) const
			{
				return (textureDetailLevel);
			}

			int32 GetPaletteDetailLevel(void) const
			{
				return (paletteDetailLevel);
			}

			int32 GetTextureFilterAnisotropy(void) const
			{
				return (textureFilterAnisotropy);
			}

			void SetOcclusionQuery(OcclusionQuery *query)
			{
				currentOcclusionQuery = query;
				Render::BeginOcclusionQuery(query);
			}

			const Antivector4D& GetDistortionPlane(void) const
			{
				return (distortionPlane);
			}

			unsigned_int32 GetDiagnosticFlags(void) const
			{
				return (diagnosticFlags);
			}

			int32 GetGraphicsCounter(int32 index) const
			{
				return (graphicsCounter[index]);
			}

			template <typename objectType, typename dataType> static void SyncRenderTask(void (objectType::*proc)(const dataType *), objectType *object, const dataType *data = nullptr)
			{
				SyncRenderTask(reinterpret_cast<void (NullClass::*)(const void *)>(proc), reinterpret_cast<NullClass *>(object), data);
			}

			void SetSyncLoadFlag(bool flag);

			C4API static void SetShaderTime(float time, float delta);
			C4API static void SetImpostorDepthParams(float scale, float offset, float tangent);

			static void ResetShaders(void);

			void BeginRendering(void);
			void EndRendering(void);

			void GetRenderingTimeStamps(unsigned_int64 *stamp);

			void SetFinalColorTransform(const ColorRGBA& scale, const ColorRGBA& bias);
			void SetFinalColorTransform(const ColorRGBA& red, const ColorRGBA& green, const ColorRGBA& blue, const ColorRGBA& bias);

			void SetRenderTarget(RenderTargetType type);
			void SetDisplayRenderTarget(void);
			void SetFullFrameRenderTarget(void);

			void CopyRenderTarget(Texture *texture, const Rect& rect);

			void ActivateOrthoCamera(void);
			void ActivateFrustumCamera(void);
			void ActivateRemoteCamera(void);

			C4API void SetRenderOptionFlags(unsigned_int32 flags);
			C4API void SetDiagnosticFlags(unsigned_int32 flags);

			C4API void SetCamera(const CameraObject *camera, const Transformable *transformable, unsigned_int32 clearMask = ~0, bool reset = true);
			C4API void SetFogSpace(const FogSpaceObject *fogSpace, const Transformable *transformable);

			C4API void SetAmbient(void);
			C4API void BeginClip(const Rect& rect);
			C4API void EndClip(void);

			C4API void SetInfiniteLight(const InfiniteLightObject *light, const Transformable *transformable, const LightShadowData *shadowData);
			C4API bool SetPointLight(const PointLightObject *light, const Transformable *transformable);
			C4API const Vector4D& SetGeometryTransformable(const Transformable *transformable);

			C4API void GroupAmbientRenderList(List<Renderable> *renderList);
			C4API void GroupLightRenderList(List<Renderable> *renderList);
			C4API void SortRenderList(List<Renderable> *renderList);

			C4API void DrawRenderList(const List<Renderable> *renderList);

			bool BeginStructureRendering(const Transform4D& previousCameraWorldTransform, unsigned_int32 structureFlags, float velocityScale);
			void EndStructureRendering(void);
			void DrawStructureList(const List<Renderable> *renderList);
			void DrawStructureDepthList(const List<Renderable> *renderList);

			bool BeginDistortionRendering(void);
			void EndDistortionRendering(void);
			void DrawDistortionList(const List<Renderable> *renderList);

			bool ActivateShadowBounds(const StencilShadow *shadow);
			void DeactivateShadowBounds(void);

			C4API void BeginStencilShadow(void);
			C4API void EndStencilShadow(void);
			C4API void DrawStencilShadow(const StencilShadow *shadow, StencilType type, StencilMode mode);

			C4API void BeginShadowMap(void);
			C4API void EndShadowMap(void);
			C4API void DrawShadowMapList(const List<Renderable> *renderList);

			C4API void DrawWireframe(unsigned_int32 flags, const List<Renderable> *renderList);
			C4API void DrawVectors(int32 arrayIndex, const List<Renderable> *renderList);

			C4API void ProcessOcclusionQueries(void);

			C4API static void ReadImageBuffer(const Rect& rect, Color4C *image, int32 rowPixels);
	};


	C4API extern GraphicsMgr *TheGraphicsMgr;
}


#endif

// ZYUTNLM
