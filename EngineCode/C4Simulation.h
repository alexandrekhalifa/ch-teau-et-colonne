//=============================================================
//
// C4 Engine version 3.5.1
// Copyright 1999-2013, by Terathon Software LLC
//
// This copy is licensed to the following:
//
//     Registered user: Université de Sherbrooke
//     Maximum number of users: Site License
//     License #C4T0033897
//
// License is granted under terms of the license agreement
// entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#ifndef C4Simulation_h
#define C4Simulation_h


namespace C4
{
	enum
	{
		kSlowMotionMultiplier			= 1,
		kPhysicsTimeStep				= 16 * kSlowMotionMultiplier,
		kRopeStepRatio					= 2,
		kClothStepRatio					= 2,
		kRopeTimeStep					= kPhysicsTimeStep / kRopeStepRatio,
		kClothTimeStep					= kPhysicsTimeStep / kClothStepRatio,
		kMaxPhysicsStepCount			= 4,
		kMaxConstraintIterationCount	= 100,
		kRigidBodySleepStepCount		= 20
	};


	const float kTimeStep = (float) (kPhysicsTimeStep / kSlowMotionMultiplier) * 0.001F;
	const float kInverseTimeStep = 1.0F / kTimeStep;
	const float kInversePhysicsTimeStep = 1.0F / (float) kPhysicsTimeStep;
	const float kContactStabilizeFactor = kInverseTimeStep * 0.25F;


	const float kMinRigidBodyMass = 0.0009765625F;
	const float kDefaultMaxLinearSpeed = 200.0F;
	const float kDefaultMaxAngularSpeed = 25.132741F;	// 4 tau


	const float kRigidBodySleepBoxSize = 1.0e-3F;
	const float kCollisionSweepEpsilon = 1.0e-3F;
	const float kContactEpsilon = -0.005F;


	const float kMaxShapeShrinkSize = 0.03125F;
	const float kSupportPointTolerance = 1.0e-5F;
	const float kSimplexVertexEpsilon = 1.0e-5F;
	const float kSimplexDimensionEpsilon = 1.0e-3F;
	const float kIntersectionDisplacementEpsilon = 1.0e-4F;
	const float kSemiInfiniteIntersectionDepth = -64.0F;


	const float kMaxSubcontactSquaredDelta = 1.0e-4F;
	const float kMaxSubcontactSquaredTangentialSeparation = 0.001F;
};


#endif

// ZYUTNLM
