//=============================================================
//
// C4 Engine version 3.5.1
// Copyright 1999-2013, by Terathon Software LLC
//
// This copy is licensed to the following:
//
//     Registered user: Université de Sherbrooke
//     Maximum number of users: Site License
//     License #C4T0033897
//
// License is granted under terms of the license agreement
// entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#ifndef C4Horizon_h
#define C4Horizon_h


//# \component	Graphics Manager
//# \prefix		GraphicsMgr/


#include "C4Processes.h"


namespace C4
{
	enum
	{
		kProcessHorizon		= 'HRZN'
	};


	//# \enum	HorizonFlags

	enum
	{
		kHorizonExcludeInfiniteLight	= 1 << 0,	//## Do not render horizon map for lights with the infinite base type.
		kHorizonExcludePointLight		= 1 << 1	//## Do not render horizon map for lights with the point base type.
	};


	//# \class	HorizonMapAttribute		Material attribute for a horizon map.
	//
	//# The $HorizonMapAttribute$ class represents the material attribute for a horizon map.
	//
	//# \def	class HorizonMapAttribute : public MapAttribute
	//
	//# \ctor	explicit HorizonMapAttribute(const char *name);
	//# \ctor	explicit HorizonMapAttribute(Texture *texture);
	//# \ctor	HorizonMapAttribute(const TextureHeader *header, const void *image = nullptr);
	//
	//# \param	name		The name of the texture map to load.
	//# \param	texture		The texture object to use.
	//# \param	header		A texture header from which to construct a new texture object.
	//# \param	image		A pointer to a texture image that is used if the texture header does not specify an offset to an image.
	//
	//# \desc
	//# The $HorizonMapAttribute$ class represents the material attribute for a horizon map.
	//# Two specially-computed horizon maps are required for horizon mapping to work properly.
	//# 
	//# See the $@MapAttribute@$ class for a description of the differences among the various constructors.
	//
	//# \base	MapAttribute	All attributes using a texture map are subclasses of $MapAttribute$.


	class HorizonMapAttribute : public MapAttribute
	{
		private:

			unsigned_int32		horizonFlags;

			Attribute *Replicate(void) const override;

		public:

			C4API HorizonMapAttribute();
			C4API explicit HorizonMapAttribute(const char *name);
			C4API explicit HorizonMapAttribute(Texture *texture);
			C4API HorizonMapAttribute(const TextureHeader *header, const void *image = nullptr);
			C4API HorizonMapAttribute(const HorizonMapAttribute& horizonMapAttribute);
			C4API ~HorizonMapAttribute();

			unsigned_int32 GetHorizonFlags(void) const
			{
				return (horizonFlags);
			}

			void SetHorizonFlags(unsigned_int32 flags)
			{
				horizonFlags = flags;
			}

			void Pack(Packer& data, unsigned_int32 packFlags) const override;
			void Unpack(Unpacker& data, unsigned_int32 unpackFlags) override;
			bool UnpackChunk(const ChunkHeader *chunkHeader, Unpacker& data, unsigned_int32 unpackFlags);
			void *BeginSettingsUnpack(void) override;

			bool operator ==(const Attribute& attribute) const;
	};


	class HorizonProcess : public TextureMapProcess
	{
		private:

			unsigned_int32		horizonFlags;

			Texture				*secondaryTextureObject;
			ResourceName		secondaryTextureName;

			static Texture		*horizonTexture;

			HorizonProcess(const HorizonProcess& horizonProcess);
 
			Process *Replicate(void) const override;

			bool ProcessEnabled(const ShaderCompileData *compileData) const; 

		public: 
 
			HorizonProcess();
			~HorizonProcess();

			static void Initialize(void); 
			static void Terminate(void);

			unsigned_int32 GetHorizonFlags(void) const
			{
				return (horizonFlags); 
			}

			void SetHorizonFlags(unsigned_int32 flags)
			{
				horizonFlags = flags;
			}

			void Pack(Packer& data, unsigned_int32 packFlags) const override;
			void Unpack(Unpacker& data, unsigned_int32 unpackFlags) override;
			void *BeginSettingsUnpack(void) override;

			int32 GetSettingCount(void) const override;
			Setting *GetSetting(int32 index) const override;
			void SetSetting(const Setting *setting) override;

			C4API void SetSecondaryTexture(const char *name);

			bool operator ==(const Process& process) const override;

			int32 GetPortCount(void) const override;
			const char *GetPortName(int32 index) const override;

			#if C4PS3

				unsigned_int32 GetPortCompileFlags(int32 index) const override;

			#endif

			int32 GenerateProcessSignature(const ShaderCompileData *compileData, unsigned_int32 *signature) const override;
			int32 GenerateDerivedInterpolantTypes(const ShaderCompileData *compileData, ProcessType *type) const override;
			void GenerateProcessData(const ShaderCompileData *compileData, ProcessData *data) const override;
			int32 GenerateOutputIdentifier(const ShaderCompileData *compileData, const ShaderAllocationData *allocData, SwizzleData *swizzleData, char *name) const override;
			int32 GenerateShaderCode(const ShaderCompileData *compileData, const char **shaderCode) const override;
	};
}


#endif

// ZYUTNLM
