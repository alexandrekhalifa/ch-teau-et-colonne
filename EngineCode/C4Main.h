//=============================================================
//
// C4 Engine version 3.5.1
// Copyright 1999-2013, by Terathon Software LLC
//
// This copy is licensed to the following:
//
//     Registered user: Université de Sherbrooke
//     Maximum number of users: Site License
//     License #C4T0033897
//
// License is granted under terms of the license agreement
// entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#ifndef C4Main_h
#define C4Main_h


#include "C4Types.h"


#if C4WINDOWS

	int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int);

#elif C4MACOS

	int main(int argc, const char **argv);

#elif C4LINUX

	int main(int argc, const char **argv);

#elif C4ORBIS //[ 

			// -- Orbis code hidden --

#elif C4PS3 //[ 

			// -- PS3 code hidden --

#endif //]


#endif

// ZYUTNLM
