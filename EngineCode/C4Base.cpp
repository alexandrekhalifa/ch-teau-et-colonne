//=============================================================
//
// C4 Engine version 3.5.1
// Copyright 1999-2013, by Terathon Software LLC
//
// This copy is licensed to the following:
//
//     Registered user: Université de Sherbrooke
//     Maximum number of users: Site License
//     License #C4T0033897
//
// License is granted under terms of the license agreement
// entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#include "C4Base.h"


using namespace C4;


#if C4DEBUG

	void C4::Assert(bool condition, const char *message)
	{
		if (!condition)
		{
			#if C4WINDOWS


			__debugbreak();

			#elif C4POSIX

				raise(SIGTRAP);

			#elif C4ORBIS || C4PS3 //[ 

			// -- PS3 code hidden --

			#endif //]
		}
	}

#endif

// ZYUTNLM
