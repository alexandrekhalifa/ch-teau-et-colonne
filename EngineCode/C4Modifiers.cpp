//=============================================================
//
// C4 Engine version 3.5.1
// Copyright 1999-2013, by Terathon Software LLC
//
// This copy is licensed to the following:
//
//     Registered user: Université de Sherbrooke
//     Maximum number of users: Site License
//     License #C4T0033897
//
// License is granted under terms of the license agreement
// entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#include "C4World.h"
#include "C4Instances.h"
#include "C4Configuration.h"


using namespace C4;


namespace C4
{
	template class Registrable<Modifier, ModifierRegistration>;
}


const NodeType Modifier::modifierNodeType[kModifierNodeTypeCount] =
{
	kNodeWildcard, kNodeBone, kNodeCamera, kNodeEffect, kNodeEmitter, kNodeGeometry, kNodeImpostor, kNodeInstance, kNodeLight, kNodeMarker, kNodeModel, kNodePortal, kNodeSource, kNodeSpace, kNodeTrigger, kNodeZone
};


ModifierRegistration::ModifierRegistration(ModifierType type, const char *name) : Registration<Modifier, ModifierRegistration>(type)
{
	modifierName = name;
}

ModifierRegistration::~ModifierRegistration()
{
}


Modifier::Modifier(ModifierType type)
{
	modifierType = type;
}

Modifier::Modifier(const Modifier& modifier)
{
	modifierType = modifier.modifierType;
}

Modifier::~Modifier()
{
}

Modifier *Modifier::Replicate(void) const
{
	return (nullptr);
}

Modifier *Modifier::New(ModifierType type)
{
	Type	data[2];

	data[0] = type;
	data[1] = 0;

	Unpacker unpacker(data);
	return (Construct(unpacker));
}

bool Modifier::ValidInstance(const Instance *instance)
{
	return (true);
}

void Modifier::RegisterStandardModifiers(void)
{
	const StringTable *table = TheInterfaceMgr->GetStringTable();

	static ModifierReg<AugmentInstanceModifier> augmentInstanceRegistration(kModifierAugmentInstance, table->GetString(StringID('MDFR', kModifierAugmentInstance)));
	static ModifierReg<WakeControllerModifier> wakeControllerRegistration(kModifierWakeController, table->GetString(StringID('MDFR', kModifierWakeController)));
	static ModifierReg<SleepControllerModifier> sleepControllerRegistration(kModifierSleepController, table->GetString(StringID('MDFR', kModifierSleepController)));
	static ModifierReg<ConnectInstanceModifier> connectInstanceRegistration(kModifierConnectInstance, table->GetString(StringID('MDFR', kModifierConnectInstance)));
	static ModifierReg<MoveConnectorModifier> moveConnectorRegistration(kModifierMoveConnector, table->GetString(StringID('MDFR', kModifierMoveConnector)));
	static ModifierReg<DeleteNodesModifier> deleteNodesRegistration(kModifierDeleteNodes, table->GetString(StringID('MDFR', kModifierDeleteNodes)));
	static ModifierReg<SetPerspectiveMaskModifier> setPerspectiveMaskRegistration(kModifierSetPerspectiveMask, table->GetString(StringID('MDFR', kModifierSetPerspectiveMask)));
	static ModifierReg<EnableInteractivityModifier> enableInteractivityRegistration(kModifierEnableInteractivity, table->GetString(StringID('MDFR', kModifierEnableInteractivity)));
	static ModifierReg<DisableInteractivityModifier> disableInteractivityRegistration(kModifierDisableInteractivity, table->GetString(StringID('MDFR', kModifierDisableInteractivity)));
	static ModifierReg<ReplaceMaterialModifier> replaceMaterialRegistration(kModifierReplaceMaterial, table->GetString(StringID('MDFR', kModifierReplaceMaterial)));
	static ModifierReg<RemovePhysicsModifier> removePhysicsRegistration(kModifierRemovePhysics, table->GetString(StringID('MDFR', kModifierRemovePhysics)));

	#if C4LEGACY

		static Constructor<Modifier> modifierConstructor(&ConstructModifier);
		Modifier::InstallConstructor(&modifierConstructor);

	#endif
}

#if C4LEGACY

	Modifier *Modifier::ConstructModifier(Unpacker& data, unsigned_int32 unpackFlags)
	{
		Type type = data.GetType();

		if (type == 'RLIT')
		{
			DeleteNodesModifier *modifier = new DeleteNodesModifier(kNodeLight, "");
			modifier->SetConvertedFlag();
			return (modifier);
		} 

		if (type == 'RSRC')
		{ 
			DeleteNodesModifier *modifier = new DeleteNodesModifier(kNodeSource, "");
			modifier->SetConvertedFlag(); 
			return (modifier); 
		}

		return (nullptr);
	} 

#endif

void Modifier::PackType(Packer& data) const
{ 
	data << modifierType;
}

Setting *Modifier::GetNodeTypeSetting(NodeType type)
{
	int32 selection = 0;
	for (machine a = 1; a < kModifierNodeTypeCount; a++)
	{
		if (type == modifierNodeType[a])
		{
			selection = a;
			break;
		}
	}

	const StringTable *table = TheInterfaceMgr->GetStringTable();

	const char *title = table->GetString(StringID('MDFR', 'TYPE'));
	MenuSetting *menu = new MenuSetting('TYPE', selection, title, kModifierNodeTypeCount);

	menu->SetMenuItemString(0, table->GetString(StringID('MDFR', 'TYPE', 'NONE')));
	for (machine a = 1; a < kModifierNodeTypeCount; a++)
	{
		menu->SetMenuItemString(a, table->GetString(StringID('MDFR', 'TYPE', modifierNodeType[a])));
	}

	return (menu);
}

bool Modifier::operator ==(const Modifier& modifier) const
{
	return (false);
}

void Modifier::Apply(World *world, Instance *instance)
{
}

bool Modifier::KeepNode(const Node *node) const
{
	return (true);
}


AugmentInstanceModifier::AugmentInstanceModifier() : Modifier(kModifierAugmentInstance)
{
	worldName[0] = 0;
}

AugmentInstanceModifier::AugmentInstanceModifier(const char *name) : Modifier(kModifierAugmentInstance)
{
	worldName = name;
}

AugmentInstanceModifier::AugmentInstanceModifier(const AugmentInstanceModifier& augmentInstanceModifier) : Modifier(augmentInstanceModifier)
{
	worldName = augmentInstanceModifier.worldName;
}

AugmentInstanceModifier::~AugmentInstanceModifier()
{
}

Modifier *AugmentInstanceModifier::Replicate(void) const
{
	return (new AugmentInstanceModifier(*this));
}

void AugmentInstanceModifier::Pack(Packer& data, unsigned_int32 packFlags) const
{
	Modifier::Pack(data, packFlags);

	PackHandle handle = data.BeginChunk('WRLD');
	data << worldName;
	data.EndChunk(handle);

	data << TerminatorChunk;
}

void AugmentInstanceModifier::Unpack(Unpacker& data, unsigned_int32 unpackFlags)
{
	Modifier::Unpack(data, unpackFlags);
	UnpackChunkList<AugmentInstanceModifier>(data, unpackFlags);
}

bool AugmentInstanceModifier::UnpackChunk(const ChunkHeader *chunkHeader, Unpacker& data, unsigned_int32 unpackFlags)
{
	switch (chunkHeader->chunkType)
	{
		case 'WRLD':

			data >> worldName;
			return (true);
	}

	return (false);
}

int32 AugmentInstanceModifier::GetSettingCount(void) const
{
	return (1);
}

Setting *AugmentInstanceModifier::GetSetting(int32 index) const
{
	if (index == 0)
	{
		const StringTable *table = TheInterfaceMgr->GetStringTable();

		const char *title = table->GetString(StringID('MDFR', kModifierAugmentInstance, 'WRLD'));
		const char *picker = table->GetString(StringID('MDFR', kModifierAugmentInstance, 'PICK'));
		return (new ResourceSetting('WRLD', worldName, title, picker, WorldResource::GetDescriptor()));
	}

	return (nullptr);
}

void AugmentInstanceModifier::SetSetting(const Setting *setting)
{
	if (setting->GetSettingIdentifier() == 'WRLD')
	{
		worldName = static_cast<const ResourceSetting *>(setting)->GetResourceName();
	}
}

bool AugmentInstanceModifier::operator ==(const Modifier& modifier) const
{
	if (modifier.GetModifierType() == kModifierAugmentInstance)
	{
		const AugmentInstanceModifier *augmentInstanceModifier = static_cast<const AugmentInstanceModifier *>(&modifier);

		if (augmentInstanceModifier->worldName == worldName)
		{
			return (true);
		}
	}

	return (false);
}

void AugmentInstanceModifier::Apply(World *world, Instance *instance)
{
	Node *worldRoot = world->NewInstancedWorld(worldName);
	if (worldRoot)
	{
		Node *root = instance;

		Node *node = instance->GetFirstSubnode();
		while (node)
		{
			const Controller *controller = node->GetController();
			if ((controller) && (controller->GetBaseControllerType() == kControllerRigidBody))
			{
				root = node;
				break;
			}

			node = node->Next();
		}

		for (;;)
		{
			node = worldRoot->GetFirstSubnode();
			if (!node)
			{
				break;
			}

			root->AddSubnode(node);
		}

		delete worldRoot;
	}
}


WakeControllerModifier::WakeControllerModifier() : Modifier(kModifierWakeController)
{
}

WakeControllerModifier::WakeControllerModifier(const WakeControllerModifier& wakeControllerModifier) : Modifier(wakeControllerModifier)
{
}

WakeControllerModifier::~WakeControllerModifier()
{
}

Modifier *WakeControllerModifier::Replicate(void) const
{
	return (new WakeControllerModifier(*this));
}

bool WakeControllerModifier::operator ==(const Modifier& modifier) const
{
	if (modifier.GetModifierType() == kModifierWakeController)
	{
		return (true);
	}

	return (false);
}

void WakeControllerModifier::Apply(World *world, Instance *instance)
{
	const Node *node = instance->GetFirstSubnode();
	while (node)
	{
		Controller *controller = node->GetController();
		if (controller)
		{
			controller->SetControllerFlags(controller->GetControllerFlags() & ~kControllerAsleep);
		}

		node = node->Next();
	}
}


SleepControllerModifier::SleepControllerModifier() : Modifier(kModifierSleepController)
{
}

SleepControllerModifier::SleepControllerModifier(const SleepControllerModifier& sleepControllerModifier) : Modifier(sleepControllerModifier)
{
}

SleepControllerModifier::~SleepControllerModifier()
{
}

Modifier *SleepControllerModifier::Replicate(void) const
{
	return (new SleepControllerModifier(*this));
}

bool SleepControllerModifier::operator ==(const Modifier& modifier) const
{
	if (modifier.GetModifierType() == kModifierSleepController)
	{
		return (true);
	}

	return (false);
}

void SleepControllerModifier::Apply(World *world, Instance *instance)
{
	const Node *node = instance->GetFirstSubnode();
	while (node)
	{
		Controller *controller = node->GetController();
		if (controller)
		{
			controller->SetControllerFlags(controller->GetControllerFlags() | kControllerAsleep);
		}

		node = node->Next();
	}
}


ConnectInstanceModifier::ConnectInstanceModifier() : Modifier(kModifierConnectInstance)
{
	connectorKey[0] = 0;
	targetNodeName[0] = 0;
}

ConnectInstanceModifier::ConnectInstanceModifier(const char *key, const char *name) : Modifier(kModifierConnectInstance)
{
	connectorKey = key;
	targetNodeName = name;
}

ConnectInstanceModifier::ConnectInstanceModifier(const ConnectInstanceModifier& connectInstanceModifier) : Modifier(connectInstanceModifier)
{
	connectorKey = connectInstanceModifier.connectorKey;
	targetNodeName = connectInstanceModifier.targetNodeName;
}

ConnectInstanceModifier::~ConnectInstanceModifier()
{
}

Modifier *ConnectInstanceModifier::Replicate(void) const
{
	return (new ConnectInstanceModifier(*this));
}

void ConnectInstanceModifier::Pack(Packer& data, unsigned_int32 packFlags) const
{
	Modifier::Pack(data, packFlags);

	PackHandle handle = data.BeginChunk('CONN');
	data << connectorKey;
	data.EndChunk(handle);

	handle = data.BeginChunk('NAME');
	data << targetNodeName;
	data.EndChunk(handle);

	data << TerminatorChunk;
}

void ConnectInstanceModifier::Unpack(Unpacker& data, unsigned_int32 unpackFlags)
{
	Modifier::Unpack(data, unpackFlags);
	UnpackChunkList<ConnectInstanceModifier>(data, unpackFlags);
}

bool ConnectInstanceModifier::UnpackChunk(const ChunkHeader *chunkHeader, Unpacker& data, unsigned_int32 unpackFlags)
{
	switch (chunkHeader->chunkType)
	{
		case 'CONN':

			data >> connectorKey;
			return (true);

		#if C4LEGACY

			case 'CKEY':
			{
				Type	key;

				data >> key;
				connectorKey = Text::TypeToString(key);
				return (true);
			}

		#endif

		case 'NAME':

			data >> targetNodeName;
			return (true);
	}

	return (false);
}

int32 ConnectInstanceModifier::GetSettingCount(void) const
{
	return (2);
}

Setting *ConnectInstanceModifier::GetSetting(int32 index) const
{
	const StringTable *table = TheInterfaceMgr->GetStringTable();

	if (index == 0)
	{
		const char *title = table->GetString(StringID('MDFR', kModifierConnectInstance, 'CONN'));
		return (new TextSetting('CONN', connectorKey, title, kMaxConnectorKeyLength, &Connector::ConnectorKeyFilter));
	}

	if (index == 1)
	{
		const char *title = table->GetString(StringID('MDFR', kModifierConnectInstance, 'NAME'));
		return (new TextSetting('NAME', targetNodeName, title, kMaxModifierNodeNameLength));
	}

	return (nullptr);
}

void ConnectInstanceModifier::SetSetting(const Setting *setting)
{
	Type identifier = setting->GetSettingIdentifier();

	if (identifier == 'CONN')
	{
		connectorKey = static_cast<const TextSetting *>(setting)->GetText();
	}
	else if (identifier == 'NAME')
	{
		targetNodeName = static_cast<const TextSetting *>(setting)->GetText();
	}
}

bool ConnectInstanceModifier::operator ==(const Modifier& modifier) const
{
	if (modifier.GetModifierType() == kModifierConnectInstance)
	{
		const ConnectInstanceModifier *connectInstanceModifier = static_cast<const ConnectInstanceModifier *>(&modifier);

		if (connectInstanceModifier->connectorKey != connectorKey)
		{
			return (false);
		}

		if (connectInstanceModifier->targetNodeName == targetNodeName)
		{
			return (true);
		}
	}

	return (false);
}

void ConnectInstanceModifier::Apply(World *world, Instance *instance)
{
	if (!instance->GetManipulator())
	{
		const Hub *hub = instance->GetHub();
		if (hub)
		{
			unsigned_int32 hash = Text::GetTextHash(targetNodeName);

			Connector *connector = hub->GetFirstOutgoingEdge();
			while (connector)
			{
				if (connector->GetConnectorKey() == connectorKey)
				{
					Node *node = instance->GetFirstSubnode();
					while (node)
					{
						if (node->GetNodeHash() == hash)
						{
							connector->SetConnectorTarget(node);
							break;
						}

						node = instance->GetNextNode(node);
					}

					break;
				}

				connector = connector->GetNextOutgoingEdge();
			}
		}
	}
}


MoveConnectorModifier::MoveConnectorModifier() : Modifier(kModifierMoveConnector)
{
	connectorKey[0] = 0;
	targetNodeName[0] = 0;
}

MoveConnectorModifier::MoveConnectorModifier(const char *key, const char *name) : Modifier(kModifierMoveConnector)
{
	connectorKey = key;
	targetNodeName = name;
}

MoveConnectorModifier::MoveConnectorModifier(const MoveConnectorModifier& moveConnectorModifier) : Modifier(moveConnectorModifier)
{
	connectorKey = moveConnectorModifier.connectorKey;
	targetNodeName = moveConnectorModifier.targetNodeName;
}

MoveConnectorModifier::~MoveConnectorModifier()
{
}

Modifier *MoveConnectorModifier::Replicate(void) const
{
	return (new MoveConnectorModifier(*this));
}

void MoveConnectorModifier::Pack(Packer& data, unsigned_int32 packFlags) const
{
	Modifier::Pack(data, packFlags);

	PackHandle handle = data.BeginChunk('CONN');
	data << connectorKey;
	data.EndChunk(handle);

	handle = data.BeginChunk('NAME');
	data << targetNodeName;
	data.EndChunk(handle);

	data << TerminatorChunk;
}

void MoveConnectorModifier::Unpack(Unpacker& data, unsigned_int32 unpackFlags)
{
	Modifier::Unpack(data, unpackFlags);
	UnpackChunkList<MoveConnectorModifier>(data, unpackFlags);
}

bool MoveConnectorModifier::UnpackChunk(const ChunkHeader *chunkHeader, Unpacker& data, unsigned_int32 unpackFlags)
{
	switch (chunkHeader->chunkType)
	{
		case 'CONN':

			data >> connectorKey;
			return (true);

		case 'NAME':

			data >> targetNodeName;
			return (true);
	}

	return (false);
}

int32 MoveConnectorModifier::GetSettingCount(void) const
{
	return (2);
}

Setting *MoveConnectorModifier::GetSetting(int32 index) const
{
	const StringTable *table = TheInterfaceMgr->GetStringTable();

	if (index == 0)
	{
		const char *title = table->GetString(StringID('MDFR', kModifierMoveConnector, 'CONN'));
		return (new TextSetting('CONN', connectorKey, title, kMaxConnectorKeyLength, &Connector::ConnectorKeyFilter));
	}

	if (index == 1)
	{
		const char *title = table->GetString(StringID('MDFR', kModifierMoveConnector, 'NAME'));
		return (new TextSetting('NAME', targetNodeName, title, kMaxModifierNodeNameLength));
	}

	return (nullptr);
}

void MoveConnectorModifier::SetSetting(const Setting *setting)
{
	Type identifier = setting->GetSettingIdentifier();

	if (identifier == 'CONN')
	{
		connectorKey = static_cast<const TextSetting *>(setting)->GetText();
	}
	else if (identifier == 'NAME')
	{
		targetNodeName = static_cast<const TextSetting *>(setting)->GetText();
	}
}

bool MoveConnectorModifier::operator ==(const Modifier& modifier) const
{
	if (modifier.GetModifierType() == kModifierMoveConnector)
	{
		const MoveConnectorModifier *moveConnectorModifier = static_cast<const MoveConnectorModifier *>(&modifier);

		if (moveConnectorModifier->connectorKey != connectorKey)
		{
			return (false);
		}

		if (moveConnectorModifier->targetNodeName == targetNodeName)
		{
			return (true);
		}
	}

	return (false);
}

void MoveConnectorModifier::Apply(World *world, Instance *instance)
{
	if (!instance->GetManipulator())
	{
		const Hub *hub = instance->GetHub();
		if (hub)
		{
			unsigned_int32 hash = Text::GetTextHash(targetNodeName);

			Connector *connector = hub->GetFirstIncomingEdge();
			while (connector)
			{
				Connector *next = connector->GetNextIncomingEdge();

				if (connector->GetConnectorKey() == connectorKey)
				{
					Node *node = instance->GetFirstSubnode();
					while (node)
					{
						if (node->GetNodeHash() == hash)
						{
							connector->SetConnectorTarget(node);
							connector->SetConnectorFlags(kConnectorSaveFinishPersistent);
							break;
						}

						node = instance->GetNextNode(node);
					}
				}

				connector = next;
			}
		}
	}
}


DeleteNodesModifier::DeleteNodesModifier() : Modifier(kModifierDeleteNodes)
{
	nodeType = kNodeWildcard;
	nodeHash = 0;
	nodeName[0] = 0;

	#if C4LEGACY

		convertedFlag = false;

	#endif
}

DeleteNodesModifier::DeleteNodesModifier(NodeType type, const char *name) : Modifier(kModifierDeleteNodes)
{
	nodeType = type;
	nodeName = name;
	nodeHash = Text::GetTextHash(name);

	#if C4LEGACY

		convertedFlag = false;

	#endif
}

DeleteNodesModifier::DeleteNodesModifier(const DeleteNodesModifier& deleteNodesModifier) : Modifier(deleteNodesModifier)
{
	nodeType = deleteNodesModifier.nodeType;
	nodeName = deleteNodesModifier.nodeName;
	nodeHash = Text::GetTextHash(nodeName);

	#if C4LEGACY

		convertedFlag = false;

	#endif
}

DeleteNodesModifier::~DeleteNodesModifier()
{
}

Modifier *DeleteNodesModifier::Replicate(void) const
{
	return (new DeleteNodesModifier(*this));
}

void DeleteNodesModifier::Pack(Packer& data, unsigned_int32 packFlags) const
{
	Modifier::Pack(data, packFlags);

	data << ChunkHeader('TYPE', 4);
	data << nodeType;

	PackHandle handle = data.BeginChunk('NAME');
	data << nodeName;
	data.EndChunk(handle);

	data << TerminatorChunk;
}

void DeleteNodesModifier::Unpack(Unpacker& data, unsigned_int32 unpackFlags)
{
	Modifier::Unpack(data, unpackFlags);

	#if C4LEGACY

		if (!convertedFlag)
		{

	#endif

			UnpackChunkList<DeleteNodesModifier>(data, unpackFlags);

	#if C4LEGACY

		}

	#endif
}

bool DeleteNodesModifier::UnpackChunk(const ChunkHeader *chunkHeader, Unpacker& data, unsigned_int32 unpackFlags)
{
	switch (chunkHeader->chunkType)
	{
		case 'TYPE':

			data >> nodeType;
			return (true);

		case 'NAME':

			data >> nodeName;
			nodeHash = Text::GetTextHash(nodeName);
			return (true);
	}

	return (false);
}

int32 DeleteNodesModifier::GetSettingCount(void) const
{
	return (2);
}

Setting *DeleteNodesModifier::GetSetting(int32 index) const
{
	if (index == 0)
	{
		return (GetNodeTypeSetting(nodeType));
	}

	if (index == 1)
	{
		const char *title = TheInterfaceMgr->GetStringTable()->GetString(StringID('MDFR', kModifierDeleteNodes, 'NAME'));
		return (new TextSetting('NAME', nodeName, title, kMaxModifierNodeNameLength));
	}

	return (nullptr);
}

void DeleteNodesModifier::SetSetting(const Setting *setting)
{
	Type identifier = setting->GetSettingIdentifier();

	if (identifier == 'TYPE')
	{
		nodeType = modifierNodeType[static_cast<const MenuSetting *>(setting)->GetMenuSelection()];
	}
	else if (identifier == 'NAME')
	{
		nodeName = static_cast<const TextSetting *>(setting)->GetText();
		nodeHash = Text::GetTextHash(nodeName);
	}
}

void DeleteNodesModifier::SetNodeName(const char *name)
{
	nodeName = name;
	nodeHash = Text::GetTextHash(nodeName);
}

bool DeleteNodesModifier::operator ==(const Modifier& modifier) const
{
	if (modifier.GetModifierType() == kModifierDeleteNodes)
	{
		const DeleteNodesModifier *deleteNodesModifier = static_cast<const DeleteNodesModifier *>(&modifier);

		if (deleteNodesModifier->nodeType != nodeType)
		{
			return (false);
		}

		if (deleteNodesModifier->nodeName == nodeName)
		{
			return (true);
		}
	}

	return (false);
}

bool DeleteNodesModifier::KeepNode(const Node *node) const
{
	NodeType type = nodeType;
	if ((type == kNodeWildcard) || (node->GetNodeType() == type))
	{
		unsigned_int32 hash = nodeHash;
		return ((hash != 0) && (node->GetNodeHash() != hash));
	}

	return (true);
}


SetPerspectiveMaskModifier::SetPerspectiveMaskModifier() : Modifier(kModifierSetPerspectiveMask)
{
	perspectiveExclusionMask = 0;

	nodeType = kNodeWildcard;
	nodeHash = 0;
	nodeName[0] = 0;
}

SetPerspectiveMaskModifier::SetPerspectiveMaskModifier(unsigned_int32 exclusionMask, NodeType type, const char *name) : Modifier(kModifierSetPerspectiveMask)
{
	perspectiveExclusionMask = exclusionMask;

	nodeType = type;
	nodeName = name;
	nodeHash = Text::GetTextHash(name);
}

SetPerspectiveMaskModifier::SetPerspectiveMaskModifier(const SetPerspectiveMaskModifier& setPerspectiveMaskModifier) : Modifier(setPerspectiveMaskModifier)
{
	perspectiveExclusionMask = setPerspectiveMaskModifier.perspectiveExclusionMask;

	nodeType = setPerspectiveMaskModifier.nodeType;
	nodeName = setPerspectiveMaskModifier.nodeName;
	nodeHash = Text::GetTextHash(nodeName);
}

SetPerspectiveMaskModifier::~SetPerspectiveMaskModifier()
{
}

Modifier *SetPerspectiveMaskModifier::Replicate(void) const
{
	return (new SetPerspectiveMaskModifier(*this));
}

void SetPerspectiveMaskModifier::Pack(Packer& data, unsigned_int32 packFlags) const
{
	Modifier::Pack(data, packFlags);

	data << ChunkHeader('EXCL', 4);
	data << perspectiveExclusionMask;

	data << ChunkHeader('TYPE', 4);
	data << nodeType;

	PackHandle handle = data.BeginChunk('NAME');
	data << nodeName;
	data.EndChunk(handle);

	data << TerminatorChunk;
}

void SetPerspectiveMaskModifier::Unpack(Unpacker& data, unsigned_int32 unpackFlags)
{
	Modifier::Unpack(data, unpackFlags);
	UnpackChunkList<SetPerspectiveMaskModifier>(data, unpackFlags);
}

bool SetPerspectiveMaskModifier::UnpackChunk(const ChunkHeader *chunkHeader, Unpacker& data, unsigned_int32 unpackFlags)
{
	switch (chunkHeader->chunkType)
	{
		case 'EXCL':

			data >> perspectiveExclusionMask;
			return (true);

		case 'TYPE':

			data >> nodeType;
			return (true);

		case 'NAME':

			data >> nodeName;
			nodeHash = Text::GetTextHash(nodeName);
			return (true);
	}

	return (false);
}

int32 SetPerspectiveMaskModifier::GetSettingCount(void) const
{
	return (13);
}

Setting *SetPerspectiveMaskModifier::GetSetting(int32 index) const
{
	const StringTable *table = TheInterfaceMgr->GetStringTable();

	if (index == 0)
	{
		return (GetNodeTypeSetting(nodeType));
	}

	if (index == 1)
	{
		const char *title = table->GetString(StringID('MDFR', kModifierSetPerspectiveMask, 'NAME'));
		return (new TextSetting('NAME', nodeName, title, kMaxModifierNodeNameLength));
	}

	if (index == 2)
	{
		const char *title = table->GetString(StringID('NODE', 'PRSP'));
		return (new HeadingSetting('PRSP', title));
	}

	if (index == 3)
	{
		const char *title = table->GetString(StringID('NODE', 'PRSP', 'DPRM'));
		return (new BooleanSetting('DPRM', ((perspectiveExclusionMask & kPerspectivePrimary) == 0), title));
	}

	if (index == 4)
	{
		const char *title = table->GetString(StringID('NODE', 'PRSP', 'DRFL'));
		return (new BooleanSetting('DRFL', ((perspectiveExclusionMask & kPerspectiveReflection) == 0), title));
	}

	if (index == 5)
	{
		const char *title = table->GetString(StringID('NODE', 'PRSP', 'DRFR'));
		return (new BooleanSetting('DRFR', ((perspectiveExclusionMask & kPerspectiveRefraction) == 0), title));
	}

	if (index == 6)
	{
		const char *title = table->GetString(StringID('NODE', 'PRSP', 'RPRM'));
		return (new BooleanSetting('RPRM', (((perspectiveExclusionMask >> kPerspectiveRemotePortalShift) & kPerspectivePrimary) == 0), title));
	}

	if (index == 7)
	{
		const char *title = table->GetString(StringID('NODE', 'PRSP', 'RRFL'));
		return (new BooleanSetting('RRFL', (((perspectiveExclusionMask >> kPerspectiveRemotePortalShift) & kPerspectiveReflection) == 0), title));
	}

	if (index == 8)
	{
		const char *title = table->GetString(StringID('NODE', 'PRSP', 'RRFR'));
		return (new BooleanSetting('RRFR', (((perspectiveExclusionMask >> kPerspectiveRemotePortalShift) & kPerspectiveRefraction) == 0), title));
	}

	if (index == 9)
	{
		const char *title = table->GetString(StringID('NODE', 'PRSP', 'CPRM'));
		return (new BooleanSetting('CPRM', (((perspectiveExclusionMask >> kPerspectiveCameraWidgetShift) & kPerspectivePrimary) == 0), title));
	}

	if (index == 10)
	{
		const char *title = table->GetString(StringID('NODE', 'PRSP', 'CRFL'));
		return (new BooleanSetting('CRFL', (((perspectiveExclusionMask >> kPerspectiveCameraWidgetShift) & kPerspectiveReflection) == 0), title));
	}

	if (index == 11)
	{
		const char *title = table->GetString(StringID('NODE', 'PRSP', 'CRFR'));
		return (new BooleanSetting('CRFR', (((perspectiveExclusionMask >> kPerspectiveCameraWidgetShift) & kPerspectiveRefraction) == 0), title));
	}

	if (index == 12)
	{
		const char *title = table->GetString(StringID('NODE', 'PRSP', 'LGHT'));
		return (new BooleanSetting('LGHT', ((perspectiveExclusionMask & kPerspectiveLightingSpace) == 0), title));
	}

	return (nullptr);
}

void SetPerspectiveMaskModifier::SetSetting(const Setting *setting)
{
	Type identifier = setting->GetSettingIdentifier();

	if (identifier == 'TYPE')
	{
		nodeType = modifierNodeType[static_cast<const MenuSetting *>(setting)->GetMenuSelection()];
	}
	else if (identifier == 'NAME')
	{
		nodeName = static_cast<const TextSetting *>(setting)->GetText();
		nodeHash = Text::GetTextHash(nodeName);
	}
	else if (identifier == 'DPRM')
	{
		if (!static_cast<const BooleanSetting *>(setting)->GetBooleanValue())
		{
			perspectiveExclusionMask |= kPerspectivePrimary;
		}
		else
		{
			perspectiveExclusionMask &= ~kPerspectivePrimary;
		}
	}
	else if (identifier == 'DRFL')
	{
		if (!static_cast<const BooleanSetting *>(setting)->GetBooleanValue())
		{
			perspectiveExclusionMask |= kPerspectiveReflection;
		}
		else
		{
			perspectiveExclusionMask &= ~kPerspectiveReflection;
		}
	}
	else if (identifier == 'DRFR')
	{
		if (!static_cast<const BooleanSetting *>(setting)->GetBooleanValue())
		{
			perspectiveExclusionMask |= kPerspectiveRefraction;
		}
		else
		{
			perspectiveExclusionMask &= ~kPerspectiveRefraction;
		}
	}
	else if (identifier == 'RPRM')
	{
		if (!static_cast<const BooleanSetting *>(setting)->GetBooleanValue())
		{
			perspectiveExclusionMask |= kPerspectivePrimary << kPerspectiveRemotePortalShift;
		}
		else
		{
			perspectiveExclusionMask &= ~(kPerspectivePrimary << kPerspectiveRemotePortalShift);
		}
	}
	else if (identifier == 'RRFL')
	{
		if (!static_cast<const BooleanSetting *>(setting)->GetBooleanValue())
		{
			perspectiveExclusionMask |= kPerspectiveReflection << kPerspectiveRemotePortalShift;
		}
		else
		{
			perspectiveExclusionMask &= ~(kPerspectiveReflection << kPerspectiveRemotePortalShift);
		}
	}
	else if (identifier == 'RRFR')
	{
		if (!static_cast<const BooleanSetting *>(setting)->GetBooleanValue())
		{
			perspectiveExclusionMask |= kPerspectiveRefraction << kPerspectiveRemotePortalShift;
		}
		else
		{
			perspectiveExclusionMask &= ~(kPerspectiveRefraction << kPerspectiveRemotePortalShift);
		}
	}
	else if (identifier == 'CPRM')
	{
		if (!static_cast<const BooleanSetting *>(setting)->GetBooleanValue())
		{
			perspectiveExclusionMask |= kPerspectivePrimary << kPerspectiveCameraWidgetShift;
		}
		else
		{
			perspectiveExclusionMask &= ~(kPerspectivePrimary << kPerspectiveCameraWidgetShift);
		}
	}
	else if (identifier == 'CRFL')
	{
		if (!static_cast<const BooleanSetting *>(setting)->GetBooleanValue())
		{
			perspectiveExclusionMask |= kPerspectiveReflection << kPerspectiveCameraWidgetShift;
		}
		else
		{
			perspectiveExclusionMask &= ~(kPerspectiveReflection << kPerspectiveCameraWidgetShift);
		}
	}
	else if (identifier == 'CRFR')
	{
		if (!static_cast<const BooleanSetting *>(setting)->GetBooleanValue())
		{
			perspectiveExclusionMask |= kPerspectiveRefraction << kPerspectiveCameraWidgetShift;
		}
		else
		{
			perspectiveExclusionMask &= ~(kPerspectiveRefraction << kPerspectiveCameraWidgetShift);
		}
	}
	else if (identifier == 'LGHT')
	{
		if (!static_cast<const BooleanSetting *>(setting)->GetBooleanValue())
		{
			perspectiveExclusionMask |= kPerspectiveLightingSpace;
		}
		else
		{
			perspectiveExclusionMask &= ~kPerspectiveLightingSpace;
		}
	}
}

bool SetPerspectiveMaskModifier::operator ==(const Modifier& modifier) const
{
	if (modifier.GetModifierType() == kModifierSetPerspectiveMask)
	{
		const SetPerspectiveMaskModifier *setPerspectiveMaskModifier = static_cast<const SetPerspectiveMaskModifier *>(&modifier);

		if (setPerspectiveMaskModifier->perspectiveExclusionMask != perspectiveExclusionMask)
		{
			return (false);
		}

		if (setPerspectiveMaskModifier->nodeType != nodeType)
		{
			return (false);
		}

		if (setPerspectiveMaskModifier->nodeName == nodeName)
		{
			return (true);
		}
	}

	return (false);
}

void SetPerspectiveMaskModifier::Apply(World *world, Instance *instance)
{
	NodeType type = nodeType;
	unsigned_int32 hash = nodeHash;

	Node *node = instance->GetFirstSubnode();
	while (node)
	{
		if ((type == kNodeWildcard) || (node->GetNodeType() == type))
		{
			if ((hash == 0) || (node->GetNodeHash() == hash))
			{
				node->SetPerspectiveExclusionMask(perspectiveExclusionMask);
			}
		}

		node = instance->GetNextNode(node);
	}
}


EnableInteractivityModifier::EnableInteractivityModifier() : Modifier(kModifierEnableInteractivity)
{
	nodeHash = 0;
	nodeName[0] = 0;
}

EnableInteractivityModifier::EnableInteractivityModifier(const char *name) : Modifier(kModifierEnableInteractivity)
{
	nodeName = name;
	nodeHash = Text::GetTextHash(name);
}

EnableInteractivityModifier::EnableInteractivityModifier(const EnableInteractivityModifier& enableInteractivityModifier) : Modifier(enableInteractivityModifier)
{
	nodeName = enableInteractivityModifier.nodeName;
	nodeHash = Text::GetTextHash(nodeName);
}

EnableInteractivityModifier::~EnableInteractivityModifier()
{
}

Modifier *EnableInteractivityModifier::Replicate(void) const
{
	return (new EnableInteractivityModifier(*this));
}

void EnableInteractivityModifier::Pack(Packer& data, unsigned_int32 packFlags) const
{
	Modifier::Pack(data, packFlags);

	PackHandle handle = data.BeginChunk('NAME');
	data << nodeName;
	data.EndChunk(handle);

	data << TerminatorChunk;
}

void EnableInteractivityModifier::Unpack(Unpacker& data, unsigned_int32 unpackFlags)
{
	Modifier::Unpack(data, unpackFlags);
	UnpackChunkList<EnableInteractivityModifier>(data, unpackFlags);
}

bool EnableInteractivityModifier::UnpackChunk(const ChunkHeader *chunkHeader, Unpacker& data, unsigned_int32 unpackFlags)
{
	switch (chunkHeader->chunkType)
	{
		case 'NAME':

			data >> nodeName;
			nodeHash = Text::GetTextHash(nodeName);
			return (true);
	}

	return (false);
}

int32 EnableInteractivityModifier::GetSettingCount(void) const
{
	return (1);
}

Setting *EnableInteractivityModifier::GetSetting(int32 index) const
{
	if (index == 0)
	{
		const char *title = TheInterfaceMgr->GetStringTable()->GetString(StringID('MDFR', kModifierEnableInteractivity, 'NAME'));
		return (new TextSetting('NAME', nodeName, title, kMaxModifierNodeNameLength));
	}

	return (nullptr);
}

void EnableInteractivityModifier::SetSetting(const Setting *setting)
{
	if (setting->GetSettingIdentifier() == 'NAME')
	{
		nodeName = static_cast<const TextSetting *>(setting)->GetText();
		nodeHash = Text::GetTextHash(nodeName);
	}
}

bool EnableInteractivityModifier::operator ==(const Modifier& modifier) const
{
	if (modifier.GetModifierType() == kModifierEnableInteractivity)
	{
		const EnableInteractivityModifier *enableInteractivityModifier = static_cast<const EnableInteractivityModifier *>(&modifier);

		if (enableInteractivityModifier->nodeName == nodeName)
		{
			return (true);
		}
	}

	return (false);
}

void EnableInteractivityModifier::Apply(World *world, Instance *instance)
{
	if (nodeHash == 0)
	{
		Node *node = instance->GetFirstSubnode();
		while (node)
		{
			Property *property = node->GetProperty(kPropertyInteraction);
			if (property)
			{
				property->SetPropertyFlags(property->GetPropertyFlags() & ~kPropertyDisabled);
			}

			node = instance->GetNextNode(node);
		}
	}
	else
	{
		Node *node = instance->GetFirstSubnode();
		while (node)
		{
			if (node->GetNodeHash() == nodeHash)
			{
				Property *property = node->GetProperty(kPropertyInteraction);
				if (property)
				{
					property->SetPropertyFlags(property->GetPropertyFlags() & ~kPropertyDisabled);
				}
			}

			node = instance->GetNextNode(node);
		}
	}
}


DisableInteractivityModifier::DisableInteractivityModifier() : Modifier(kModifierDisableInteractivity)
{
	nodeHash = 0;
	nodeName[0] = 0;
}

DisableInteractivityModifier::DisableInteractivityModifier(const char *name) : Modifier(kModifierDisableInteractivity)
{
	nodeName = name;
	nodeHash = Text::GetTextHash(name);
}

DisableInteractivityModifier::DisableInteractivityModifier(const DisableInteractivityModifier& disableInteractivityModifier) : Modifier(disableInteractivityModifier)
{
	nodeName = disableInteractivityModifier.nodeName;
	nodeHash = Text::GetTextHash(nodeName);
}

DisableInteractivityModifier::~DisableInteractivityModifier()
{
}

Modifier *DisableInteractivityModifier::Replicate(void) const
{
	return (new DisableInteractivityModifier(*this));
}

void DisableInteractivityModifier::Pack(Packer& data, unsigned_int32 packFlags) const
{
	Modifier::Pack(data, packFlags);

	PackHandle handle = data.BeginChunk('NAME');
	data << nodeName;
	data.EndChunk(handle);

	data << TerminatorChunk;
}

void DisableInteractivityModifier::Unpack(Unpacker& data, unsigned_int32 unpackFlags)
{
	Modifier::Unpack(data, unpackFlags);
	UnpackChunkList<DisableInteractivityModifier>(data, unpackFlags);
}

bool DisableInteractivityModifier::UnpackChunk(const ChunkHeader *chunkHeader, Unpacker& data, unsigned_int32 unpackFlags)
{
	switch (chunkHeader->chunkType)
	{
		case 'NAME':

			data >> nodeName;
			nodeHash = Text::GetTextHash(nodeName);
			return (true);
	}

	return (false);
}

int32 DisableInteractivityModifier::GetSettingCount(void) const
{
	return (1);
}

Setting *DisableInteractivityModifier::GetSetting(int32 index) const
{
	if (index == 0)
	{
		const char *title = TheInterfaceMgr->GetStringTable()->GetString(StringID('MDFR', kModifierDisableInteractivity, 'NAME'));
		return (new TextSetting('NAME', nodeName, title, kMaxModifierNodeNameLength));
	}

	return (nullptr);
}

void DisableInteractivityModifier::SetSetting(const Setting *setting)
{
	if (setting->GetSettingIdentifier() == 'NAME')
	{
		nodeName = static_cast<const TextSetting *>(setting)->GetText();
		nodeHash = Text::GetTextHash(nodeName);
	}
}

bool DisableInteractivityModifier::operator ==(const Modifier& modifier) const
{
	if (modifier.GetModifierType() == kModifierDisableInteractivity)
	{
		const DisableInteractivityModifier *disableInteractivityModifier = static_cast<const DisableInteractivityModifier *>(&modifier);

		if (disableInteractivityModifier->nodeName == nodeName)
		{
			return (true);
		}
	}

	return (false);
}

void DisableInteractivityModifier::Apply(World *world, Instance *instance)
{
	if (nodeHash == 0)
	{
		Node *node = instance->GetFirstSubnode();
		while (node)
		{
			Property *property = node->GetProperty(kPropertyInteraction);
			if (property)
			{
				property->SetPropertyFlags(property->GetPropertyFlags() | kPropertyDisabled);
			}

			node = instance->GetNextNode(node);
		}
	}
	else
	{
		Node *node = instance->GetFirstSubnode();
		while (node)
		{
			if (node->GetNodeHash() == nodeHash)
			{
				Property *property = node->GetProperty(kPropertyInteraction);
				if (property)
				{
					property->SetPropertyFlags(property->GetPropertyFlags() | kPropertyDisabled);
				}
			}

			node = instance->GetNextNode(node);
		}
	}
}


ReplaceMaterialModifier::ReplaceMaterialModifier() : Modifier(kModifierReplaceMaterial)
{
	nodeHash = 0;
	nodeName[0] = 0;

	materialObject = nullptr;
}

ReplaceMaterialModifier::ReplaceMaterialModifier(const char *name) : Modifier(kModifierReplaceMaterial)
{
	nodeName = name;
	nodeHash = Text::GetTextHash(name);

	materialObject = nullptr;
}

ReplaceMaterialModifier::ReplaceMaterialModifier(const ReplaceMaterialModifier& replaceMaterialModifier) : Modifier(replaceMaterialModifier)
{
	nodeName = replaceMaterialModifier.nodeName;
	nodeHash = Text::GetTextHash(nodeName);

	materialObject = replaceMaterialModifier.materialObject;
	if (materialObject)
	{
		materialObject->Retain();
	}
}

ReplaceMaterialModifier::~ReplaceMaterialModifier()
{
	if (materialObject)
	{
		materialObject->Release();
	}
}

Modifier *ReplaceMaterialModifier::Replicate(void) const
{
	return (new ReplaceMaterialModifier(*this));
}

void ReplaceMaterialModifier::Prepack(List<Object> *linkList) const
{
	if (materialObject)
	{
		linkList->Append(materialObject);
	}
}

void ReplaceMaterialModifier::Pack(Packer& data, unsigned_int32 packFlags) const
{
	Modifier::Pack(data, packFlags);

	if (nodeName[0] != 0)
	{
		PackHandle handle = data.BeginChunk('NAME');
		data << nodeName;
		data.EndChunk(handle);
	}

	if ((materialObject) && (!(packFlags & kPackSettings)))
	{
		data << ChunkHeader('MATL', 4);
		data << materialObject->GetObjectIndex();
	}

	data << TerminatorChunk;
}

void ReplaceMaterialModifier::Unpack(Unpacker& data, unsigned_int32 unpackFlags)
{
	Modifier::Unpack(data, unpackFlags);
	UnpackChunkList<ReplaceMaterialModifier>(data, unpackFlags);
}

bool ReplaceMaterialModifier::UnpackChunk(const ChunkHeader *chunkHeader, Unpacker& data, unsigned_int32 unpackFlags)
{
	switch (chunkHeader->chunkType)
	{
		case 'NAME':

			data >> nodeName;
			nodeHash = Text::GetTextHash(nodeName);
			return (true);

		case 'MATL':
		{
			int32	objectIndex;

			data >> objectIndex;
			data.AddObjectLink(objectIndex, &MaterialObjectLinkProc, this);
			return (true);
		}
	}

	return (false);
}

void *ReplaceMaterialModifier::BeginSettingsUnpack(void)
{
	nodeName[0] = 0;
	return (Modifier::BeginSettingsUnpack());
}

void ReplaceMaterialModifier::MaterialObjectLinkProc(Object *object, void *cookie)
{
	ReplaceMaterialModifier *replaceMaterialModifier = static_cast<ReplaceMaterialModifier *>(cookie);
	replaceMaterialModifier->SetMaterialObject(static_cast<MaterialObject *>(object));
}

int32 ReplaceMaterialModifier::GetSettingCount(void) const
{
	return (1);
}

Setting *ReplaceMaterialModifier::GetSetting(int32 index) const
{
	if (index == 0)
	{
		const char *title = TheInterfaceMgr->GetStringTable()->GetString(StringID('MDFR', kModifierReplaceMaterial, 'NAME'));
		return (new TextSetting('NAME', nodeName, title, kMaxModifierNodeNameLength));
	}

	return (nullptr);
}

void ReplaceMaterialModifier::SetSetting(const Setting *setting)
{
	if (setting->GetSettingIdentifier() == 'NAME')
	{
		nodeName = static_cast<const TextSetting *>(setting)->GetText();
		nodeHash = Text::GetTextHash(nodeName);
	}
}

void ReplaceMaterialModifier::SetMaterialObject(MaterialObject *object)
{
	if (materialObject != object)
	{
		if (materialObject)
		{
			materialObject->Release();
		}

		if (object)
		{
			object->Retain();
		}

		materialObject = object;
	}
}

bool ReplaceMaterialModifier::operator ==(const Modifier& modifier) const
{
	if (modifier.GetModifierType() == kModifierReplaceMaterial)
	{
		const ReplaceMaterialModifier *replaceMaterialModifier = static_cast<const ReplaceMaterialModifier *>(&modifier);

		if (replaceMaterialModifier->nodeName != nodeName)
		{
			return (false);
		}

		if (replaceMaterialModifier->materialObject == materialObject)
		{
			return (true);
		}
	}

	return (false);
}

void ReplaceMaterialModifier::Apply(World *world, Instance *instance)
{
	if (materialObject)
	{
		if (nodeHash == 0)
		{
			Node *node = instance->GetFirstSubnode();
			while (node)
			{
				if (node->GetNodeType() == kNodeGeometry)
				{
					Geometry *geometry = static_cast<Geometry *>(node);
					geometry->SetMaterialObject(0, materialObject);
				}

				node = instance->GetNextNode(node);
			}
		}
		else
		{
			Node *node = instance->GetFirstSubnode();
			while (node)
			{
				if (node->GetNodeHash() == nodeHash)
				{
					if (node->GetNodeType() == kNodeGeometry)
					{
						Geometry *geometry = static_cast<Geometry *>(node);
						geometry->SetMaterialObject(0, materialObject);
					}
				}

				node = instance->GetNextNode(node);
			}
		}
	}
}


RemovePhysicsModifier::RemovePhysicsModifier() : Modifier(kModifierRemovePhysics)
{
}

RemovePhysicsModifier::RemovePhysicsModifier(const RemovePhysicsModifier& removePhysicsModifier) : Modifier(removePhysicsModifier)
{
}

RemovePhysicsModifier::~RemovePhysicsModifier()
{
}

Modifier *RemovePhysicsModifier::Replicate(void) const
{
	return (new RemovePhysicsModifier(*this));
}

bool RemovePhysicsModifier::operator ==(const Modifier& modifier) const
{
	if (modifier.GetModifierType() == kModifierRemovePhysics)
	{
		return (true);
	}

	return (false);
}

void RemovePhysicsModifier::Apply(World *world, Instance *instance)
{
	const Node *node = instance->GetFirstSubnode();
	while (node)
	{
		const Controller *controller = node->GetController();
		if ((controller) && (controller->GetBaseControllerType() == kControllerRigidBody))
		{
			delete controller;
		}

		node = node->Next();
	}
}

bool RemovePhysicsModifier::KeepNode(const Node *node) const
{
	NodeType type = node->GetNodeType();
	return ((type != kNodeShape) && (type != kNodeJoint));
}

// ZYUTNLM
