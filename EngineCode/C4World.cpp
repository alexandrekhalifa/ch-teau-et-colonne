//=============================================================
//
// C4 Engine version 3.5.1
// Copyright 1999-2013, by Terathon Software LLC
//
// This copy is licensed to the following:
//
//     Registered user: Université de Sherbrooke
//     Maximum number of users: Site License
//     License #C4T0033897
//
// License is granted under terms of the license agreement
// entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#include "C4World.h"
#include "C4Particles.h"
#include "C4Forces.h"
#include "C4Fields.h"
#include "C4Blockers.h"
#include "C4Skybox.h"
#include "C4Triggers.h"
#include "C4Terrain.h"
#include "C4Water.h"
#include "C4Movies.h"


using namespace C4;


namespace
{
	const float kCameraNearDepth			= 0.1F;
	const float kCameraFarDepth				= 500.0F;
	const float kNearClipEpsilon			= 0.01F;
	const float kCameraLightClipEpsilon		= 0.01F;
	const float kDetailEpsilon				= 0.01F;
	const float kDiagnosticRegionSize		= 64.0F;


	enum
	{
		kMaxCollisionEdgeCount		= 32,
		kMaxRemoteRecursionCount	= 3,
		kMaxCameraRecursionCount	= 3
	};


	enum
	{
		kPortalGroupReflection,
		kPortalGroupRefraction,
		kPortalGroupRemote,
		kPortalGroupCamera,
		kPortalGroupCount
	};


	enum
	{
		kWorldUnfogEnable		= 1 << 0
	};
}


WorldMgr *C4::TheWorldMgr = nullptr;


namespace C4
{
	template <> WorldMgr Manager<WorldMgr>::managerObject(0);
	template <> WorldMgr **Manager<WorldMgr>::managerPointer = &TheWorldMgr;

	template <> const char *const Manager<WorldMgr>::resultString[] =
	{
		nullptr,
		"World failed to load"
	};

	template <> const unsigned_int32 Manager<WorldMgr>::resultIdentifier[] =
	{
		0, 'LOAD'
	};

	template class Manager<WorldMgr>;

	template <> Heap C4::Memory<PortalData>::heap("PortalData", 4096, kHeapMutexless);


	struct CollisionParams
	{
		const RigidBodyController	*excludedRigidBody;
		const Point3D				*colliderPosition[2];
		float						colliderRadius;
		Box3D						colliderBox;
		unsigned_int32				collisionKind;
	};


	struct ProximityParams
	{
		World::ProximityProc		*proximityProc;
		void						*proximityCookie;
		const Point3D				*proximityCenter;
		float						proximityRadius;
		Box3D						proximityBox;
	};


	struct InteractionData
	{
		float			param;
		Point3D			position;
		Node			*interaction;
	};
 

	struct ControllerData
	{ 
		Controller		*controller;
 
		int32			prevControllerIndex; 
		int32			nextControllerIndex;
	};

 
	struct ShadowRenderData
	{
		const Node			*excludeNode;
		const LightRegion	*lightRegion;
 
		List<Region>		shadowRegionList;
		Region				nearClipRegion;
	};


	class PortalData : public MapElement<PortalData>, public Memory<PortalData>
	{
		private:

			Portal		*targetPortal;
			Zone		*originZone;

			int32		portalVertexCount;
			Point3D		portalVertex[kMaxPortalVertexCount];

		public:

			typedef Portal *KeyType;

			PortalData(Portal *portal, Zone *zone, int32 vertexCount, const Point3D *vertex);
			~PortalData();

			KeyType GetKey(void) const
			{
				return (targetPortal);
			}

			Portal *GetPortal(void) const
			{
				return (targetPortal);
			}

			Zone *GetOriginZone(void) const
			{
				return (originZone);
			}

			int32 GetVertexCount(void) const
			{
				return (portalVertexCount);
			}

			const Point3D *GetVertexArray(void) const
			{
				return (portalVertex);
			}
	};


	class CollisionThreadData
	{
		private:

			int32								threadFlag;
			Array<Geometry *, 64>				geometryArray;

		protected:

			int32 GetThreadFlag(void) const
			{
				return (threadFlag);
			}

		public:

			CollisionThreadData(int32 flag);
			~CollisionThreadData();

			bool AddGeometry(Geometry *geometry);
	};


	class QueryThreadData : public CollisionThreadData
	{
		private:

			Array<RigidBodyController *, 32>	rigidBodyArray;

		public:

			QueryThreadData(int32 flag);
			~QueryThreadData();

			bool AddRigidBody(RigidBodyController *rigidBody);
	};


	class InteractionThreadData : public CollisionThreadData
	{
		private:

			Array<PanelEffect *, 8>				panelEffectArray;

		public:

			InteractionThreadData(int32 flag);
			~InteractionThreadData();

			bool AddPanelEffect(PanelEffect *panelEffect);
	};


	struct WorldContext
	{
		const FrustumCamera			*renderCamera;

		Skybox						*skyboxNode;
		bool						skyboxFlag;

		mutable bool				shadowMapKeepFlag;
		mutable unsigned_int32		shadowInhibitMask;

		unsigned_int32				perspectiveFlags;
		int32						cameraMinDetailLevel;
		float						cameraDetailBias;

		int8						deepestBoxComponent[3];
		float						maxGeometryDepth;

		const CameraRegion			*shadowReceiveRegion;

		List<Region>				occlusionList;
		Map<PortalData>				portalGroup[kPortalGroupCount];

		const FogSpace				**fogSpacePtr;
		List<Region>				unfoggedList;
		Region						unfoggedRegion;

		WorldContext(const FrustumCamera *camera)
		{
			renderCamera = camera;
			skyboxFlag = false;
			shadowMapKeepFlag = false;
			unfoggedRegion.SetPlaneCount(1);
		}
	};
}


ResourceDescriptor WorldResource::descriptor("wld");
ResourceDescriptor SaveResource::descriptor("sav");


// The maximum number of faces that a closed polyhedron with n vertices can have is 2n - 4.
// For 16-bit vertex indexes, n = 65535 after reserving index 0xFFFF for special meaning.
// This gives 131066 possible faces, which we double for two-sided geometries.

bool World::geometryFrontArray[262132];


#if C4DIAGNOSTICS

	List<Renderable> World::lightRegionRenderList;
	Renderable World::lightRegionRenderable(kRenderLineLoop);
	VertexBuffer World::lightRegionVertexBuffer(kVertexBufferAttribute | kVertexBufferDynamic);
	List<Attribute> World::lightRegionAttributeList;
	DiffuseAttribute World::lightRegionDiffuseColor(ColorRGBA(1.0F, 1.0F, 0.0F, 1.0F));

	List<Renderable> World::sourcePathRenderList;
	Renderable World::sourcePathRenderable(kRenderLines);
	VertexBuffer World::sourcePathVertexBuffer(kVertexBufferAttribute | kVertexBufferDynamic);
	List<Attribute> World::sourcePathAttributeList;
	DiffuseAttribute World::sourcePathDiffuseColor(ColorRGBA(0.0F, 0.5F, 1.0F, 1.0F));

#endif


PortalData::PortalData(Portal *portal, Zone *zone, int32 vertexCount, const Point3D *vertex)
{
	targetPortal = portal;
	originZone = zone;

	portalVertexCount = vertexCount;
	for (machine a = 0; a < vertexCount; a++)
	{
		portalVertex[a] = vertex[a];
	}
}

PortalData::~PortalData()
{
}


CollisionThreadData::CollisionThreadData(int32 flag)
{
	threadFlag = flag;
}

CollisionThreadData::~CollisionThreadData()
{
	int32 mask = ~threadFlag;

	int32 count = geometryArray.GetElementCount();
	for (machine a = 0; a < count; a++)
	{
		AtomicAnd(geometryArray[a]->GetQueryThreadFlags(), mask);
	}
}

bool CollisionThreadData::AddGeometry(Geometry *geometry)
{
	int32 flag = threadFlag;

	volatile int32 *geometryFlags = geometry->GetQueryThreadFlags();
	if (AtomicOr(geometryFlags, flag) & flag)
	{
		return (false);
	}

	geometryArray.AddElement(geometry);
	return (true);
}


QueryThreadData::QueryThreadData(int32 flag) : CollisionThreadData(flag)
{
}

QueryThreadData::~QueryThreadData()
{
	int32 mask = ~GetThreadFlag();

	int32 count = rigidBodyArray.GetElementCount();
	for (machine a = 0; a < count; a++)
	{
		AtomicAnd(rigidBodyArray[a]->GetQueryThreadFlags(), mask);
	}
}

bool QueryThreadData::AddRigidBody(RigidBodyController *rigidBody)
{
	int32 flag = GetThreadFlag();

	volatile int32 *rigidBodyFlags = rigidBody->GetQueryThreadFlags();
	if (AtomicOr(rigidBodyFlags, flag) & flag)
	{
		return (false);
	}

	rigidBodyArray.AddElement(rigidBody);
	return (true);
}


InteractionThreadData::InteractionThreadData(int32 flag) : CollisionThreadData(flag)
{
}

InteractionThreadData::~InteractionThreadData()
{
	int32 mask = ~GetThreadFlag();

	int32 count = panelEffectArray.GetElementCount();
	for (machine a = 0; a < count; a++)
	{
		AtomicAnd(panelEffectArray[a]->GetQueryThreadFlags(), mask);
	}
}

bool InteractionThreadData::AddPanelEffect(PanelEffect *panelEffect)
{
	int32 flag = GetThreadFlag();

	volatile int32 *panelEffectFlags = panelEffect->GetQueryThreadFlags();
	if (AtomicOr(panelEffectFlags, flag) & flag)
	{
		return (false);
	}

	panelEffectArray.AddElement(panelEffect);
	return (true);
}


WorldResource::WorldResource(const char *name, ResourceCatalog *catalog) : Resource<WorldResource>(name, catalog)
{
}

WorldResource::~WorldResource()
{
}

int32 WorldResource::GetControllerCount(void) const
{
	const int32 *data = static_cast<const int32 *>(GetData());

	int32 controllerCount = data[2];
	if (data[0] != 1)
	{
		Reverse(&controllerCount);
	}

	return (controllerCount);
}

ResourceResult WorldResource::LoadObjectOffsetTable(ResourceLoader *loader, WorldHeader *worldHeader, int32 **offsetTable) const
{
	ResourceResult result = loader->Read(worldHeader, 0, sizeof(WorldHeader));
	if (result != kResourceOkay)
	{
		return (result);
	}

	int32 endian = worldHeader->endian;
	if (endian != 1)
	{
		Reverse(worldHeader);
	}

	int32 offsetCount = worldHeader->offsetCount;
	int32 *table = new int32[offsetCount];

	result = loader->Read(table, sizeof(WorldHeader), offsetCount * 4);
	if (result == kResourceOkay)
	{
		if (endian != 1)
		{
			for (machine a = 0; a < offsetCount; a++)
			{
				Reverse(&table[a]);
			}
		}

		*offsetTable = table;
		return (kResourceOkay);
	}

	delete[] table;
	return (result);
}

ResourceResult WorldResource::LoadAllObjects(ResourceLoader *loader, const WorldHeader *header, const int32 *offsetTable, char **objectData) const
{
	int32 start = offsetTable[0];
	unsigned_int32 size = offsetTable[header->offsetCount - 1] - start;
	char *data = new char[size];

	ResourceResult result = loader->Read(data, start, size);
	if (result == kResourceOkay)
	{
		*objectData = data;
		return (kResourceOkay);
	}

	delete[] data;
	return (result);
}

ResourceResult WorldResource::LoadObject(ResourceLoader *loader, int32 index, const int32 *offsetTable, char **objectData) const
{
	int32 start = offsetTable[index];
	unsigned_int32 size = offsetTable[index + 1] - start;
	char *data = new char[size];

	ResourceResult result = loader->Read(data, start, size);
	if (result == kResourceOkay)
	{
		*objectData = data;
		return (kResourceOkay);
	}

	delete[] data;
	return (result);
}


SaveResource::SaveResource(const char *name, ResourceCatalog *catalog) : Resource<SaveResource>(name, catalog)
{
}

SaveResource::~SaveResource()
{
}

int32 SaveResource::GetControllerCount(void) const
{
	const int32 *data = static_cast<const int32 *>(GetData());

	int32 controllerCount = data[2];
	if (data[0] != 1)
	{
		Reverse(&controllerCount);
	}

	return (controllerCount);
}


InstancedWorldData::InstancedWorldData(unsigned_int32 hash, Node *node)
{
	worldHash = hash;
	prototypeCopy = node;
}

InstancedWorldData::~InstancedWorldData()
{
	delete prototypeCopy;
}


GenericModelData::GenericModelData(unsigned_int32 hash, GenericModel *model)
{
	modelHash = hash;
	modelList.Append(model);
}

GenericModelData::~GenericModelData()
{
}


Interactor::Interactor()
{
}

Interactor::~Interactor()
{
}

void Interactor::SetInteractionProbe(const Point3D& p1, const Point3D& p2)
{
	interactionPoint[0] = p1;
	interactionPoint[1] = p2;
}

void Interactor::HandleInteractionEvent(InteractionEventType type, Node *node, const Point3D *position)
{
	switch (type)
	{
		case kInteractionEventEngage:

			interactionNode = node;
			interactionPosition = *position;
			break;

		case kInteractionEventDisengage:

			interactionNode = nullptr;
			break;

		case kInteractionEventTrack:

			interactionPosition = *position;
			break;
	}
}

void Interactor::DetectInteraction(const World *world)
{
	InteractionData		data;

	data.param = 1.0F;
	if (world->DetectInteraction(interactionPoint[0], interactionPoint[1], &data))
	{
		Node *node = data.interaction;
		Point3D p = node->GetInverseWorldTransform() * data.position;

		if (node == interactionNode)
		{
			HandleInteractionEvent(kInteractionEventTrack, node, &p);
		}
		else
		{
			if (interactionNode)
			{
				HandleInteractionEvent(kInteractionEventDisengage, interactionNode);
			}

			HandleInteractionEvent(kInteractionEventEngage, node, &p);
		}
	}
	else
	{
		if (interactionNode)
		{
			HandleInteractionEvent(kInteractionEventDisengage, interactionNode);
		}
	}
}


World::World(const char *name, unsigned_int32 flags) : updateObservable(this)
{
	worldName = name;
	worldFlags = flags;
	rootNode = nullptr;
}

World::World(Node *root, unsigned_int32 flags) : updateObservable(this)
{
	worldName[0] = 0;
	worldFlags = flags;
	rootNode = root;
}

World::~World()
{
	engagedSourceList.RemoveAll();

	activeTriggerList[0].RemoveAll();
	activeTriggerList[1].RemoveAll();

	controllerList[0].RemoveAll();
	controllerList[1].RemoveAll();

	delete rootNode;
	SetCamera(nullptr);

	delete previousWorld.GetTarget();
}

WorldResult World::Preprocess(void)
{
	if (!rootNode)
	{
		int32	controllerCount;

		if (worldFlags & kWorldRestore)
		{
			SaveResource *resource = SaveResource::Get(worldName, 0, TheResourceMgr->GetSaveCatalog());
			if (resource)
			{
				rootNode = Node::UnpackDeltaTree(resource->GetData(), worldName, previousWorld);
				controllerCount = resource->GetControllerCount();
				resource->Release();
			}
		}
		else
		{
			WorldResource *resource = WorldResource::Get(worldName, 0, nullptr, &resourceLocation);
			if (resource)
			{
				rootNode = Node::UnpackTree(resource->GetData(), 0);
				controllerCount = resource->GetControllerCount();
				resource->Release();
			}
		}

		if (!rootNode)
		{
			return (kWorldLoadFailed);
		}

		loadContext.loadMagnitude = ExpandInstancedWorlds(rootNode);

		staticControllerCount = controllerCount;
		controllerArray.SetElementCount(controllerCount);
		for (machine a = 0; a < controllerCount; a++)
		{
			controllerArray[a].controller = nullptr;
		}

		#if C4ORBIS //[ 

			// -- Orbis code hidden --

		#elif C4PS3 //[ 

			// -- PS3 code hidden --

		#endif //]
	}
	else
	{
		staticControllerCount = 0;
	}

	SetCamera(nullptr);

	worldFlags &= ~kWorldRestore;
	worldPerspective = 0;

	shaderTime = 0.0F;
	velocityNormalizationTime = TheWorldMgr->GetDefaultVelocityNormalizationTime();

	finalColorScale[0].Set(1.0F, 1.0F, 1.0F, 1.0F);
	finalColorBias.Set(0.0F, 0.0F, 0.0F, 0.0F);

	renderWidth = TheDisplayMgr->GetDisplayWidth();
	renderHeight = TheDisplayMgr->GetDisplayHeight();

	#if C4OCULUS

		if (TheDisplayMgr->GetDisplayFlags() & kDisplayOculus)
		{
			worldFlags |= kWorldOculusCamera;
		}

	#endif

	ambientRenderStamp = 0xFFFFFFFF;
	lightRenderStamp = 0xFFFFFFFF;
	shadowRenderStamp = 0xFFFFFFFF;
	triggerActivationStamp = 0xFFFFFFFF;

	controllerParity = 0;
	effectParity = 0;
	sourceParity = 0;
	triggerParity = 0;

	firstFreeControllerIndex = kControllerUnassigned;
	lastFreeControllerIndex = kControllerUnassigned;

	for (machine a = 0; a < kWorldCounterCount; a++)
	{
		worldCounter[a] = 0;
	}

	#if C4DIAGNOSTICS

		diagnosticFlags = 0;

	#endif

	ProcessWorldProperties();

	rootNode->SetWorld(this);
	rootNode->Preprocess();
	rootNode->Update();

	return (kWorldOkay);
}

void World::ProcessWorldProperties(void)
{
	worldSkybox = nullptr;
	clearColor = nullptr;

	Node *node = rootNode->GetFirstSubnode();
	while (node)
	{
		if (node->GetNodeType() == kNodeSkybox)
		{
			worldSkybox = static_cast<Skybox *>(node);
			break;
		}

		node = node->Next();
	}

	const Property *property = rootNode->GetFirstProperty();
	while (property)
	{
		PropertyType type = property->GetPropertyType();
		if (type == kPropertyClear)
		{
			worldFlags |= kWorldClearColor;
			clearColor = &static_cast<const ClearProperty *>(property)->GetClearColor();
		}

		property = property->Next();
	}
}

int32 World::ExpandInstancedWorlds(Node *root, int32 depth)
{
	int32 count = 1;

	Node *node = root->GetFirstSubnode();
	while (node)
	{
		count++;

		if (node->GetNodeType() == kNodeInstance)
		{
			Node *next = root->GetNextLevelNode(node);

			if (static_cast<Instance *>(node)->Expand(this))
			{
				count += node->GetSubtreeNodeCount();
				if (depth < kWorldMaxInstanceDepth)
				{
					ExpandInstancedWorlds(node, depth + 1);
				}
			}

			node = next;
			continue;
		}

		node = root->GetNextNode(node);
	}

	return (count);
}

Node *World::NewInstancedWorld(const char *name, Node::CloneFilterProc *filterProc, void *filterCookie)
{
	unsigned_int32 hash = Text::GetTextHash(name);
	InstancedWorldData *data = instancedWorldDataMap.Find(hash);
	if (data)
	{
		return (data->GetPrototypeCopy()->Clone(filterProc, filterCookie));
	}

	WorldResource *resource = WorldResource::Get(name);
	if (!resource)
	{
		return (nullptr);
	}

	unsigned_int32 unpackFlags = (rootNode->GetManipulator()) ? kUnpackEditor | kUnpackNonpersistent | kUnpackExternal : kUnpackNonpersistent | kUnpackExternal;
	Node *node = Node::UnpackTree(resource->GetData(), unpackFlags);
	resource->Release();

	data = new InstancedWorldData(hash, node);
	instancedWorldDataMap.Insert(data);
	return (node->Clone(filterProc, filterCookie));
}

Node *World::NewGenericModel(const char *name, GenericModel *model)
{
	unsigned_int32 hash = Text::GetTextHash(name);
	GenericModelData *data = genericModelDataMap.Find(hash);
	if (data)
	{
		Node *node = data->GetGenericModel();
		data->AddGenericModel(model);

		if (node)
		{
			return (node->Clone());
		}
	}

	ModelResource *resource = ModelResource::Get(name);
	if (!resource)
	{
		return (nullptr);
	}

	Node *node = Node::UnpackTree(resource->GetData(), kUnpackNonpersistent | kUnpackExternal);
	resource->Release();

	if (!data)
	{
		genericModelDataMap.Insert(new GenericModelData(hash, model));
	}

	return (node);
}

ImpostorSystem *World::GetImpostorSystem(MaterialObject *material, const float *clipData)
{
	ImpostorSystem *system = impostorSystemMap.Find(material);
	if (system)
	{
		return (system);
	}

	system = new ImpostorSystem(material, clipData);
	impostorSystemMap.Insert(system);
	return (system);
}

PhysicsController *World::FindPhysicsController(void) const
{
	PhysicsNode *physicsNode = GetRootNode()->GetPhysicsNode();
	if (physicsNode)
	{
		Controller *controller = physicsNode->GetController();
		if ((controller) && (controller->GetControllerType() == kControllerPhysics))
		{
			return (static_cast<PhysicsController *>(controller));
		}
	}

	return (nullptr);
}

void World::AddController(Controller *controller)
{
	unsigned_int32 flags = controller->GetControllerFlags();
	if (!(flags & kControllerAsleep))
	{
		controller->Wake();
	}

	if (!(flags & kControllerLocal))
	{
		int32 count = controllerArray.GetElementCount();
		int32 index = controller->GetControllerIndex();

		if (index == kControllerUnassigned)
		{
			index = NewControllerIndex();
			controller->SetControllerIndex(index);
		}
		else if (index >= count)
		{
			controllerArray.SetElementCount(index + 1);
			for (machine a = count; a < index; a++)
			{
				controllerArray[a].controller = nullptr;
			}
		}

		controllerArray[index].controller = controller;
	}
}

void World::RemoveController(Controller *controller)
{
	int32 index = controller->GetControllerIndex();
	if ((unsigned_int32) index < (unsigned_int32) controllerArray.GetElementCount())
	{
		ControllerData *data = &controllerArray[index];
		data->controller = nullptr;

		if (index >= staticControllerCount)
		{
			int32 last = lastFreeControllerIndex;
			if (last != kControllerUnassigned)
			{
				lastFreeControllerIndex = index;
				data->prevControllerIndex = last;
				data->nextControllerIndex = kControllerUnassigned;
				controllerArray[last].nextControllerIndex = index;
			}
			else
			{
				firstFreeControllerIndex = index;
				lastFreeControllerIndex = index;
				data->prevControllerIndex = kControllerUnassigned;
				data->nextControllerIndex = kControllerUnassigned;
			}
		}
	}

	List<Controller> *list = controller->GetOwningList();
	if (list)
	{
		list->Remove(controller);
	}
}

void World::WakeController(Controller *controller)
{
	if (!controller->GetOwningList())
	{
		unsigned_int32 flags = controller->GetControllerFlags();

		if (flags & kControllerPhysicsSimulation)
		{
			physicsControllerList.Append(controller);
		}
		else if (!(flags & kControllerMoveInhibit))
		{
			controllerList[controllerParity].Append(controller);
		}
	}
}

void World::SleepController(Controller *controller)
{
	List<Controller> *list = controller->GetOwningList();
	if (list)
	{
		list->Remove(controller);
	}
}

Controller *World::GetController(int32 index) const
{
	if ((unsigned_int32) index < (unsigned_int32) controllerArray.GetElementCount())
	{
		return (controllerArray[index].controller);
	}

	return (nullptr);
}

int32 World::NewControllerIndex(void)
{
	int32 first = firstFreeControllerIndex;
	if (first != kControllerUnassigned)
	{
		int32 next = controllerArray[first].nextControllerIndex;
		firstFreeControllerIndex = next;

		if (next != kControllerUnassigned)
		{
			controllerArray[next].prevControllerIndex = kControllerUnassigned;
		}
		else
		{
			lastFreeControllerIndex = kControllerUnassigned;
		}

		return (first);
	}

	int32 count = controllerArray.GetElementCount();
	controllerArray.SetElementCount(count + 1);
	controllerArray[count].controller = nullptr;
	return (count);
}

ControllerMessage *World::ConstructControllerMessage(ControllerMessageType controllerMessageType, int32 controllerIndex, Decompressor& data, void *world)
{
	Controller *controller = static_cast<World *>(world)->GetController(controllerIndex);
	if (controller)
	{
		return (controller->ConstructMessage(controllerMessageType));
	}

	return (nullptr);
}

void World::ReceiveControllerMessage(const ControllerMessage *message, void *world)
{
	Controller *controller = static_cast<World *>(world)->GetController(message->GetControllerIndex());
	if (controller)
	{
		unsigned_int32 flags = message->GetMessageFlags();
		if ((flags & (kMessageDestroyer | kMessageJournaled)) == kMessageJournaled)
		{
			ControllerMessage *journaledMessage = controller->GetFirstJournaledMessage();
			while (journaledMessage)
			{
				ControllerMessage *next = journaledMessage->Next();

				if (message->OverridesMessage(journaledMessage))
				{
					delete journaledMessage;
				}

				journaledMessage = next;
			}

			controller->AddJournaledMessage(const_cast<ControllerMessage *>(message));
		}

		if (!message->HandleControllerMessage(controller))
		{
			controller->ReceiveMessage(message);
		}
	}
}

void World::SetCameraClearParams(CameraObject *object) const
{
	if (worldFlags & kWorldClearColor)
	{
		if (clearColor)
		{
			object->SetClearColor(*clearColor);
		}

		object->SetClearFlags(kClearColorBuffer | kClearDepthStencilBuffer);
	}
	else
	{
		object->SetClearFlags(kClearDepthStencilBuffer);
	}
}

void World::SetCamera(FrustumCamera *camera)
{
	currentCamera = camera;

	unsigned_int32 flags = worldFlags;
	if (camera)
	{
		camera->SetWorld(this);

		if (!(flags & kWorldListenerInhibit))
		{
			TheSoundMgr->SetListenerTransformable(camera);
		}

		FrustumCameraObject *object = camera->GetObject();
		SetCameraClearParams(object);

		object->SetFrustumFlags(kFrustumInfinite);
		object->SetNearDepth(kCameraNearDepth);
		object->SetFarDepth(kCameraFarDepth);

		#if C4OCULUS

			if (flags & kWorldOculusCamera)
			{
				object->SetProjectionOffset(Oculus::GetLensCenter());
				object->SetFocalLength(Oculus::GetLensFocalLength());
			}

		#endif
	}
	else
	{
		if (!(flags & kWorldListenerInhibit))
		{
			TheSoundMgr->SetListenerTransformable(nullptr);
		}
	}
}

bool World::DetectGeometryCollision(Geometry *geometry, const CollisionParams *collisionParams, CollisionData *collisionData, CollisionThreadData *threadData)
{
	const GeometryObject *object = geometry->GetObject();
	if ((object->GetCollisionExclusionMask() & collisionParams->collisionKind) == 0)
	{
		if (threadData->AddGeometry(geometry))
		{
			GeometryHitData		geometryHitData;

			const Transform4D& inverseTransform = geometry->GetInverseWorldTransform();
			if (object->DetectCollision(inverseTransform * *collisionParams->colliderPosition[0], inverseTransform * *collisionParams->colliderPosition[1], collisionParams->colliderRadius, &geometryHitData))
			{
				float t = geometryHitData.param;
				if (t < collisionData->param)
				{
					collisionData->param = t;
					collisionData->position = geometry->GetWorldTransform() * geometryHitData.position;
					collisionData->normal = geometryHitData.normal * inverseTransform;
					collisionData->geometry = geometry;
					collisionData->triangleIndex = geometryHitData.triangleIndex;
					return (true);
				}
			}
		}
	}

	return (false);
}

bool World::DetectNodeCollision(Node *node, const CollisionParams *collisionParams, CollisionData *collisionData, CollisionThreadData *threadData)
{
	if (node->Enabled())
	{
		bool result = false;

		if (node->GetNodeType() == kNodeGeometry)
		{
			result = DetectGeometryCollision(static_cast<Geometry *>(node), collisionParams, collisionData, threadData);
		}

		const Bond *bond = node->GetFirstOutgoingEdge();
		while (bond)
		{
			Site *site = bond->GetFinishElement();
			if (site->GetWorldBoundingBox().Intersection(collisionParams->colliderBox))
			{
				result |= DetectNodeCollision(static_cast<Node *>(site), collisionParams, collisionData, threadData);
			}

			bond = bond->GetNextOutgoingEdge();
		}

		return (result);
	}

	return (false);
}

bool World::DetectCellCollision(const Site *cell, const CollisionParams *collisionParams, CollisionData *collisionData, CollisionThreadData *threadData)
{
	bool result = false;

	const Bond *bond = cell->GetFirstOutgoingEdge();
	while (bond)
	{
		Site *site = bond->GetFinishElement();
		if (site->GetWorldBoundingBox().Intersection(collisionParams->colliderBox))
		{
			if (site->GetCellIndex() < 0)
			{
				result |= DetectNodeCollision(static_cast<Node *>(site), collisionParams, collisionData, threadData);
			}
			else
			{
				result |= DetectCellCollision(site, collisionParams, collisionData, threadData);
			}
		}

		bond = bond->GetNextOutgoingEdge();
	}

	return (result);
}

bool World::DetectZoneCollision(Zone *zone, const CollisionParams *collisionParams, CollisionData *collisionData, CollisionThreadData *threadData)
{
	const Point3D& p1 = *collisionParams->colliderPosition[0];
	const Transform4D& transform = zone->GetInverseWorldTransform();
	if (!zone->GetObject()->ExteriorSweptSphere(transform * p1, transform * (p1 + (*collisionParams->colliderPosition[1] - p1) * collisionData->param), collisionParams->colliderRadius))
	{
		bool result = false;

		if (DetectCellCollision(zone->GetCellGraphSite(kCellGraphGeometry), collisionParams, collisionData, threadData))
		{
			result = true;
		}

		Zone *subzone = zone->GetFirstSubzone();
		while (subzone)
		{
			result |= DetectZoneCollision(subzone, collisionParams, collisionData, threadData);
			subzone = subzone->Next();
		}

		return (result);
	}

	return (false);
}

bool World::DetectCollision(const Point3D& p1, const Point3D& p2, float radius, unsigned_int32 kind, CollisionData *collisionData, int32 threadIndex) const
{
	if (p1 != p2)
	{
		CollisionParams		collisionParams;

		#if C4SIMD

			vec_float r = SimdLoadSmearScalar(&radius);
			vec_float q1 = SimdLoadUnaligned(&p1.x);
			vec_float q2 = SimdLoadUnaligned(&p2.x);
			collisionParams.colliderBox.Set(SimdSub(SimdMin(q1, q2), r), SimdAdd(SimdMax(q1, q2), r));

		#else

			collisionParams.colliderBox.min.Set(Fmin(p1.x, p2.x) - radius, Fmin(p1.y, p2.y) - radius, Fmin(p1.z, p2.z) - radius);
			collisionParams.colliderBox.max.Set(Fmax(p1.x, p2.x) + radius, Fmax(p1.y, p2.y) + radius, Fmax(p1.z, p2.z) + radius);

		#endif

		collisionParams.colliderPosition[0] = &p1;
		collisionParams.colliderPosition[1] = &p2;
		collisionParams.colliderRadius = radius;
		collisionParams.collisionKind = kind;

		CollisionThreadData threadData(1 << threadIndex);
		bool result = false;

		collisionData->param = 1.0F;
		if (DetectZoneCollision(GetRootNode(), &collisionParams, collisionData, &threadData))
		{
			collisionData->normal.Normalize();
			collisionData->position -= collisionData->normal * radius;
			result = true;
		}

		return (result);
	}

	return (false);
}

const AcousticsProperty *World::DetectObstruction(const Point3D& position) const
{
	CollisionData	collisionData;

	const Point3D& listenerPosition = currentCamera->GetWorldPosition();
	if (DetectCollision(position, listenerPosition, 0.0F, kCollisionSoundPath, &collisionData))
	{
		return (static_cast<const AcousticsProperty *>(collisionData.geometry->GetProperty(kPropertyAcoustics)));
	}

	return (nullptr);
}

CollisionState World::QueryNodeCollision(Node *node, const CollisionParams *collisionParams, CollisionData *collisionData, QueryThreadData *threadData)
{
	if (node->Enabled())
	{
		CollisionState result = kCollisionStateNone;

		Controller *controller = node->GetController();
		if ((controller) && (controller->GetBaseControllerType() == kControllerRigidBody))
		{
			RigidBodyController *rigidBody = static_cast<RigidBodyController *>(controller);
			if (threadData->AddRigidBody(rigidBody))
			{
				if (((rigidBody->GetCollisionExclusionMask() & collisionParams->collisionKind) == 0) && (rigidBody != collisionParams->excludedRigidBody))
				{
					BodyHitData		bodyHitData;

					if (rigidBody->DetectSegmentIntersection(*collisionParams->colliderPosition[0], *collisionParams->colliderPosition[1], collisionParams->colliderRadius, &bodyHitData))
					{
						float t = bodyHitData.param;
						if (t < collisionData->param)
						{
							result = kCollisionStateRigidBody;

							collisionData->param = t;
							collisionData->position = bodyHitData.position;
							collisionData->normal = bodyHitData.normal;
							collisionData->rigidBody = rigidBody;
							collisionData->shape = bodyHitData.shape;
						}
					}
				}
			}

			return (result);
		}
		else
		{
			if ((node->GetNodeType() == kNodeGeometry) && (DetectGeometryCollision(static_cast<Geometry *>(node), collisionParams, collisionData, threadData)))
			{
				result = kCollisionStateGeometry;
			}

			const Bond *bond = node->GetFirstOutgoingEdge();
			while (bond)
			{
				Site *site = bond->GetFinishElement();
				if (site->GetWorldBoundingBox().Intersection(collisionParams->colliderBox))
				{
					CollisionState state = QueryNodeCollision(static_cast<Node *>(site), collisionParams, collisionData, threadData);
					if (state != kCollisionStateNone)
					{
						result = state;
					}
				}

				bond = bond->GetNextOutgoingEdge();
			}
		}

		return (result);
	}

	return (kCollisionStateNone);
}

CollisionState World::QueryCellCollision(const Site *cell, const CollisionParams *collisionParams, CollisionData *collisionData, QueryThreadData *threadData)
{
	CollisionState result = kCollisionStateNone;

	const Bond *bond = cell->GetFirstOutgoingEdge();
	while (bond)
	{
		Site *site = bond->GetFinishElement();
		if (site->GetWorldBoundingBox().Intersection(collisionParams->colliderBox))
		{
			if (site->GetCellIndex() < 0)
			{
				CollisionState state = QueryNodeCollision(static_cast<Node *>(site), collisionParams, collisionData, threadData);
				if (state != kCollisionStateNone)
				{
					result = state;
				}
			}
			else
			{
				CollisionState state = QueryCellCollision(site, collisionParams, collisionData, threadData);
				if (state != kCollisionStateNone)
				{
					result = state;
				}
			}
		}

		bond = bond->GetNextOutgoingEdge();
	}

	return (result);
}

CollisionState World::QueryZoneCollision(Zone *zone, const CollisionParams *collisionParams, CollisionData *collisionData, QueryThreadData *threadData)
{
	const Point3D& p1 = *collisionParams->colliderPosition[0];
	const Transform4D& transform = zone->GetInverseWorldTransform();
	if (!zone->GetObject()->ExteriorSweptSphere(transform * p1, transform * (p1 + (*collisionParams->colliderPosition[1] - p1) * collisionData->param), collisionParams->colliderRadius))
	{
		CollisionState result = kCollisionStateNone;

		CollisionState state = QueryCellCollision(zone->GetCellGraphSite(kCellGraphGeometry), collisionParams, collisionData, threadData);
		if (state != kCollisionStateNone)
		{
			result = state;
		}

		Zone *subzone = zone->GetFirstSubzone();
		while (subzone)
		{
			state = QueryZoneCollision(subzone, collisionParams, collisionData, threadData);
			if (state != kCollisionStateNone)
			{
				result = state;
			}

			subzone = subzone->Next();
		}

		return (result);
	}

	return (kCollisionStateNone);
}

CollisionState World::QueryCollision(const Point3D& p1, const Point3D& p2, float radius, unsigned_int32 kind, CollisionData *collisionData, const RigidBodyController *excludeBody, int32 threadIndex) const
{
	if (p1 != p2)
	{
		CollisionParams		collisionParams;

		#if C4SIMD

			vec_float r = SimdLoadSmearScalar(&radius);
			vec_float q1 = SimdLoadUnaligned(&p1.x);
			vec_float q2 = SimdLoadUnaligned(&p2.x);
			collisionParams.colliderBox.Set(SimdSub(SimdMin(q1, q2), r), SimdAdd(SimdMax(q1, q2), r));

		#else

			collisionParams.colliderBox.min.Set(Fmin(p1.x, p2.x) - radius, Fmin(p1.y, p2.y) - radius, Fmin(p1.z, p2.z) - radius);
			collisionParams.colliderBox.max.Set(Fmax(p1.x, p2.x) + radius, Fmax(p1.y, p2.y) + radius, Fmax(p1.z, p2.z) + radius);

		#endif

		collisionParams.excludedRigidBody = excludeBody;
		collisionParams.colliderPosition[0] = &p1;
		collisionParams.colliderPosition[1] = &p2;
		collisionParams.colliderRadius = radius;
		collisionParams.collisionKind = kind;

		QueryThreadData threadData(1 << threadIndex);
		CollisionState result = kCollisionStateNone;

		collisionData->param = 1.0F;
		CollisionState state = QueryZoneCollision(GetRootNode(), &collisionParams, collisionData, &threadData);
		if (state != kCollisionStateNone)
		{
			collisionData->normal.Normalize();
			collisionData->position -= collisionData->normal * radius;
			result = state;
		}

		return (result);
	}

	return (kCollisionStateNone);
}

ProximityResult World::QueryNodeProximity(Node *node, const ProximityParams *proximityParams, QueryThreadData *threadData)
{
	if (node->Enabled())
	{
		Controller *controller = node->GetController();
		if ((controller) && (controller->GetBaseControllerType() == kControllerRigidBody))
		{
			RigidBodyController *rigidBody = static_cast<RigidBodyController *>(controller);
			if (threadData->AddRigidBody(rigidBody))
			{
				ProximityResult result = (*proximityParams->proximityProc)(node, *proximityParams->proximityCenter, proximityParams->proximityRadius, proximityParams->proximityCookie);
				if (result == kProximityStop)
				{
					return (kProximityStop);
				}
			}
		}
		else
		{
			if ((node->GetNodeType() == kNodeGeometry) && (threadData->AddGeometry(static_cast<Geometry *>(node))))
			{
				ProximityResult result = (*proximityParams->proximityProc)(node, *proximityParams->proximityCenter, proximityParams->proximityRadius, proximityParams->proximityCookie);
				if (result == kProximityStop)
				{
					return (kProximityStop);
				}

				if (result == kProximitySkipSuccessors)
				{
					return (kProximityContinue);
				}
			}

			const Bond *bond = node->GetFirstOutgoingEdge();
			while (bond)
			{
				const Bond *next = bond->GetNextOutgoingEdge();

				Site *site = bond->GetFinishElement();
				if (site->GetWorldBoundingBox().Intersection(proximityParams->proximityBox))
				{
					if (QueryNodeProximity(static_cast<Node *>(site), proximityParams, threadData) == kProximityStop)
					{
						return (kProximityStop);
					}
				}

				bond = next;
			}
		}
	}

	return (kProximityContinue);
}

ProximityResult World::QueryCellProximity(const Site *cell, const ProximityParams *proximityParams, QueryThreadData *threadData)
{
	const Bond *bond = cell->GetFirstOutgoingEdge();
	while (bond)
	{
		const Bond *next = bond->GetNextOutgoingEdge();

		Site *site = bond->GetFinishElement();
		if (site->GetWorldBoundingBox().Intersection(proximityParams->proximityBox))
		{
			if (site->GetCellIndex() < 0)
			{
				if (QueryNodeProximity(static_cast<Node *>(site), proximityParams, threadData) == kProximityStop)
				{
					return (kProximityStop);
				}
			}
			else
			{
				if (QueryCellProximity(site, proximityParams, threadData) == kProximityStop)
				{
					return (kProximityStop);
				}
			}
		}

		bond = next;
	}

	return (kProximityContinue);
}

ProximityResult World::QueryZoneProximity(Zone *zone, const ProximityParams *proximityParams, QueryThreadData *threadData)
{
	const Transform4D& transform = zone->GetInverseWorldTransform();
	if (!zone->GetObject()->ExteriorSphere(transform * *proximityParams->proximityCenter, proximityParams->proximityRadius))
	{
		if (QueryCellProximity(zone->GetCellGraphSite(kCellGraphGeometry), proximityParams, threadData) == kProximityStop)
		{
			return (kProximityStop);
		}

		Zone *subzone = zone->GetFirstSubzone();
		while (subzone)
		{
			if (QueryZoneProximity(subzone, proximityParams, threadData) == kProximityStop)
			{
				return (kProximityStop);
			}

			subzone = subzone->Next();
		}
	}

	return (kProximityContinue);
}

void World::QueryProximity(const Point3D& center, float radius, ProximityProc *proc, void *cookie, int32 threadIndex) const
{
	ProximityParams		proximityParams;

	#if C4SIMD

		vec_float r = SimdLoadSmearScalar(&radius);
		vec_float p = SimdLoadUnaligned(&center.x);
		proximityParams.proximityBox.Set(SimdSub(p, r), SimdAdd(p, r));

	#else

		proximityParams.proximityBox.min.Set(center.x - radius, center.y - radius, center.z - radius);
		proximityParams.proximityBox.max.Set(center.x + radius, center.y + radius, center.z + radius);

	#endif

	proximityParams.proximityProc = proc;
	proximityParams.proximityCookie = cookie;
	proximityParams.proximityCenter = &center;
	proximityParams.proximityRadius = radius;

	QueryThreadData threadData(1 << threadIndex);

	QueryZoneProximity(GetRootNode(), &proximityParams, &threadData);
}

bool World::DetectGeometryNodeInteraction(Node *node, const Box3D& box, const Point3D& p1, const Point3D& p2, InteractionData *interactionData, InteractionThreadData *threadData)
{
	bool result = false;

	if (node->Enabled())
	{
		if (node->GetNodeType() == kNodeGeometry)
		{
			Geometry *geometry = static_cast<Geometry *>(node);

			const GeometryObject *object = geometry->GetObject();
			if (!(object->GetCollisionExclusionMask() & kCollisionInteraction))
			{
				const Property *property = geometry->GetProperty(kPropertyInteraction);
				if ((property) && (!(property->GetPropertyFlags() & kPropertyDisabled)))
				{
					if (threadData->AddGeometry(geometry))
					{
						GeometryHitData		geometryHitData;

						const Transform4D& inverseTransform = geometry->GetInverseWorldTransform();
						if (object->DetectCollision(inverseTransform * p1, inverseTransform * p2, 0.0F, &geometryHitData))
						{
							float t = geometryHitData.param;
							if (t < interactionData->param)
							{
								interactionData->param = t;
								interactionData->position = geometry->GetWorldTransform() * geometryHitData.position;
								interactionData->interaction = geometry;
								result = true;
							}
						}
					}
				}
			}
		}

		const Bond *bond = node->GetFirstOutgoingEdge();
		while (bond)
		{
			Site *site = bond->GetFinishElement();
			if (site->GetWorldBoundingBox().Intersection(box))
			{
				result |= DetectGeometryNodeInteraction(static_cast<Node *>(site), box, p1, p2, interactionData, threadData);
			}

			bond = bond->GetNextOutgoingEdge();
		}
	}

	return (result);
}

bool World::DetectGeometryCellInteraction(const Site *cell, const Box3D& box, const Point3D& p1, const Point3D& p2, InteractionData *interactionData, InteractionThreadData *threadData)
{
	bool result = false;

	const Bond *bond = cell->GetFirstOutgoingEdge();
	while (bond)
	{
		Site *site = bond->GetFinishElement();
		if (site->GetWorldBoundingBox().Intersection(box))
		{
			if (site->GetCellIndex() < 0)
			{
				result |= DetectGeometryNodeInteraction(static_cast<Node *>(site), box, p1, p2, interactionData, threadData);
			}
			else
			{
				result |= DetectGeometryCellInteraction(site, box, p1, p2, interactionData, threadData);
			}
		}

		bond = bond->GetNextOutgoingEdge();
	}

	return (result);
}

bool World::DetectEffectNodeInteraction(Effect *effect, const Box3D& box, const Point3D& p1, const Point3D& p2, InteractionData *interactionData, InteractionThreadData *threadData)
{
	bool result = false;

	if ((effect->Enabled()) && (effect->GetEffectType() == kEffectPanel))
	{
		const Property *property = effect->GetProperty(kPropertyInteraction);
		if ((property) && (!(property->GetPropertyFlags() & kPropertyDisabled)))
		{
			PanelEffect *panelEffect = static_cast<PanelEffect *>(effect);

			if (threadData->AddPanelEffect(panelEffect))
			{
				CollisionPoint		collisionPoint;

				const Transform4D& inverseTransform = effect->GetInverseWorldTransform();
				if (static_cast<PanelEffect *>(effect)->DetectCollision(inverseTransform * p1, inverseTransform * p2, &collisionPoint))
				{
					float t = collisionPoint.param;
					if (t < interactionData->param)
					{
						interactionData->param = t;
						interactionData->position = p1 + (p2 - p1) * t;
						interactionData->interaction = effect;
						result = true;
					}
				}
			}
		}
	}

	return (result);
}

bool World::DetectEffectCellInteraction(const Site *cell, const Box3D& box, const Point3D& p1, const Point3D& p2, InteractionData *interactionData, InteractionThreadData *threadData)
{
	bool result = false;

	const Bond *bond = cell->GetFirstOutgoingEdge();
	while (bond)
	{
		Site *site = bond->GetFinishElement();
		if (site->GetWorldBoundingBox().Intersection(box))
		{
			if (site->GetCellIndex() < 0)
			{
				result |= DetectEffectNodeInteraction(static_cast<Effect *>(site), box, p1, p2, interactionData, threadData);
			}
			else
			{
				result |= DetectEffectCellInteraction(site, box, p1, p2, interactionData, threadData);
			}
		}

		bond = bond->GetNextOutgoingEdge();
	}

	return (result);
}

bool World::DetectZoneInteraction(const Zone *zone, const Box3D& box, const Point3D& p1, const Point3D& p2, InteractionData *interactionData, InteractionThreadData *threadData)
{
	const Transform4D& transform = zone->GetInverseWorldTransform();
	if (!zone->GetObject()->ExteriorSweptSphere(transform * p1, transform * (p1 + (p2 - p1) * interactionData->param), 0.0F))
	{
		bool result = DetectGeometryCellInteraction(zone->GetCellGraphSite(kCellGraphGeometry), box, p1, p2, interactionData, threadData);
		result |= DetectEffectCellInteraction(zone->GetCellGraphSite(kCellGraphEffect), box, p1, p2, interactionData, threadData);

		const Zone *subzone = zone->GetFirstSubzone();
		while (subzone)
		{
			result |= DetectZoneInteraction(subzone, box, p1, p2, interactionData, threadData);
			subzone = subzone->Next();
		}

		return (result);
	}

	return (false);
}

bool World::DetectInteraction(const Point3D& p1, const Point3D& p2, InteractionData *interactionData) const
{
	Box3D	box;

	#if C4SIMD

		vec_float q1 = SimdLoadUnaligned(&p1.x);
		vec_float q2 = SimdLoadUnaligned(&p2.x);
		box.Set(SimdMin(q1, q2), SimdMax(q1, q2));

	#else

		box.min.Set(Fmin(p1.x, p2.x), Fmin(p1.y, p2.y), Fmin(p1.z, p2.z));
		box.max.Set(Fmax(p1.x, p2.x), Fmax(p1.y, p2.y), Fmax(p1.z, p2.z));

	#endif

	InteractionThreadData threadData(1 << JobMgr::kMaxWorkerThreadCount);
	return (DetectZoneInteraction(GetRootNode(), box, p1, p2, interactionData, &threadData));
}

void World::ActivateCellTriggers(Site *cell, const Box3D& box, const Point3D& p1, const Point3D& p2, float radius, List<Trigger> *triggerList)
{
	const Bond *bond = cell->GetFirstOutgoingEdge();
	while (bond)
	{
		const Bond *next = bond->GetNextOutgoingEdge();

		Site *site = bond->GetFinishElement();
		if (site->GetWorldBoundingBox().Intersection(box))
		{
			if (site->GetCellIndex() < 0)
			{
				Trigger *trigger = static_cast<Trigger *>(site);

				unsigned_int32 stamp = triggerActivationStamp;
				if (trigger->GetSiteStamp() != stamp)
				{
					trigger->SetSiteStamp(stamp);

					if ((trigger->Enabled()) && (!trigger->ListElement<Trigger>::GetOwningList()))
					{
						const Transform4D& transform = trigger->GetInverseWorldTransform();
						if (trigger->GetObject()->IntersectSegment(transform * p1, transform * p2, radius))
						{
							triggerList->Append(trigger);
						}
					}
				}
			}
			else
			{
				ActivateCellTriggers(site, box, p1, p2, radius, triggerList);
			}
		}

		bond = next;
	}
}

void World::ActivateZoneTriggers(Zone *zone, const Point3D& p1, const Point3D& p2, float radius, List<Trigger> *triggerList)
{
	const Transform4D& zoneTransform = zone->GetInverseWorldTransform();
	if (!zone->GetObject()->ExteriorSweptSphere(zoneTransform * p1, zoneTransform * p2, radius))
	{
		Box3D	box;

		#if C4SIMD

			vec_float r = SimdLoadSmearScalar(&radius);
			vec_float q1 = SimdLoadUnaligned(&p1.x);
			vec_float q2 = SimdLoadUnaligned(&p2.x);
			box.Set(SimdSub(SimdMin(q1, q2), r), SimdAdd(SimdMax(q1, q2), r));

		#else

			box.min.Set(Fmin(p1.x, p2.x) - radius, Fmin(p1.y, p2.y) - radius, Fmin(p1.z, p2.z) - radius);
			box.max.Set(Fmax(p1.x, p2.x) + radius, Fmax(p1.y, p2.y) + radius, Fmax(p1.z, p2.z) + radius);

		#endif

		ActivateCellTriggers(zone->GetCellGraphSite(kCellGraphTrigger), box, p1, p2, radius, triggerList);

		Zone *subzone = zone->GetFirstSubzone();
		while (subzone)
		{
			ActivateZoneTriggers(subzone, p1, p2, radius, triggerList);
			subzone = subzone->Next();
		}
	}
}

void World::ActivateTriggers(const Point3D& p1, const Point3D& p2, float radius, Node *initiator)
{
	List<Trigger>	activateList;

	triggerActivationStamp++;
	ActivateZoneTriggers(GetRootNode(), p1, p2, radius, &activateList);

	for (;;)
	{
		Trigger *trigger = activateList.First();
		if (!trigger)
		{
			break;
		}

		unsigned_int32 triggerFlags = trigger->GetObject()->GetTriggerFlags();
		if (triggerFlags & kTriggerActivateDisable)
		{
			trigger->SetNodeFlags(trigger->GetNodeFlags() | kNodeDisabled);
			activateList.Remove(trigger);
		}
		else if (!(triggerFlags & kTriggerContinuouslyActivated))
		{
			activeTriggerList[triggerParity ^ 1].Append(trigger);
		}
		else
		{
			activateList.Remove(trigger);
		}

		trigger->Activate(initiator);
	}

	List<Trigger> *triggerList = &activeTriggerList[triggerParity];
	Trigger *trigger = triggerList->First();
	while (trigger)
	{
		Trigger *next = trigger->Next();

		if (trigger->Enabled())
		{
			const Transform4D& transform = trigger->GetInverseWorldTransform();
			if (trigger->GetObject()->IntersectSegment(transform * p1, transform * p2, radius))
			{
				activeTriggerList[triggerParity ^ 1].Append(trigger);
			}
		}
		else
		{
			triggerList->Remove(trigger);
		}

		trigger = next;
	}
}

RigidBodyStatus World::HandleNewRigidBodyContact(RigidBodyController *rigidBody, const RigidBodyContact *contact, RigidBodyController *contactBody)
{
	return (kRigidBodyUnchanged);
}

RigidBodyStatus World::HandleNewGeometryContact(RigidBodyController *rigidBody, const GeometryContact *contact)
{
	return (kRigidBodyUnchanged);
}

void World::HandlePhysicsSpaceExit(RigidBodyController *rigidBody)
{
}

void World::HandleWaterSubmergence(RigidBodyController *rigidBody)
{
}

void World::MoveControllers(unsigned_int32 parity)
{
	List<Controller> *currentList = &controllerList[parity];
	List<Controller> *nextList = &controllerList[parity ^ 1];

	for (;;)
	{
		Controller *controller = currentList->First();
		if (!controller)
		{
			break;
		}

		nextList->Append(controller);
		controller->Move();
	}

	Controller *controller = physicsControllerList.First();
	if (controller)
	{
		controller->Move();
	}
}

void World::MoveEffects(unsigned_int32 parity)
{
	List<Effect> *currentList = &movingEffectList[parity];
	List<Effect> *nextList = &movingEffectList[parity ^ 1];

	for (;;)
	{
		Effect *effect = currentList->First();
		if (!effect)
		{
			break;
		}

		nextList->Append(effect);
		effect->Move();
	}
}

void World::MoveSources(unsigned_int32 parity)
{
	List<OmniSource> *currentList = &playingSourceList[parity];
	List<OmniSource> *nextList = &playingSourceList[parity ^ 1];
	for (;;)
	{
		OmniSource *source = currentList->First();
		if (!source)
		{
			break;
		}

		nextList->Append(source);
		source->Move();

		worldCounter[kWorldCounterPlayingSource]++;
	}

	OmniSource *source = engagedSourceList.First();
	while (source)
	{
		OmniSource *next = source->Next();
		source->Move();
		source = next;

		worldCounter[kWorldCounterEngagedSource]++;
	}

	worldCounter[kWorldCounterPlayingSource] += worldCounter[kWorldCounterEngagedSource];
}

void World::Move(void)
{
	for (machine a = 0; a < kWorldCounterCount; a++)
	{
		worldCounter[a] = 0;
	}

	for (;;)
	{
		DeferredTask *task = deferredTaskList.First();
		if (!task)
		{
			break;
		}

		deferredTaskList.Remove(task);
		task->CallCompletionProc();
	}

	unsigned_int8 parity = controllerParity;
	MoveControllers(parity);
	controllerParity = parity ^ 1;

	parity = effectParity;
	MoveEffects(parity);
	effectParity = parity ^ 1;

	parity = sourceParity;
	MoveSources(parity);
	sourceParity = parity ^ 1;

	parity = triggerParity;
	List<Trigger> *triggerList = &activeTriggerList[parity];
	for (;;)
	{
		Trigger *trigger = triggerList->First();
		if (!trigger)
		{
			break;
		}

		triggerList->Remove(trigger);
		trigger->Deactivate();
	}

	triggerParity = parity ^ 1;
}

void World::Update(void)
{
	rootNode->Update();

	FrustumCamera *camera = currentCamera;
	if (camera)
	{
		if (!(worldFlags & kWorldViewport))
		{
			FrustumCameraObject *object = camera->GetObject();
			object->SetViewRect(Rect(0, 0, renderWidth, renderHeight));
			object->SetAspectRatio((float) renderHeight / (float) renderWidth);
		}

		camera->Move();
		camera->Invalidate();
		camera->Update();

		camera->UpdateRootRegions(static_cast<Zone *>(rootNode));
	}

	updateObservable.PostEvent();
}

void World::Interact(void)
{
	Interactor *interactor = interactorList.First();
	while (interactor)
	{
		interactor->DetectInteraction(this);
		interactor = interactor->Next();
	}
}

void World::Listen(void)
{
	const FrustumCamera *camera = currentCamera;
	if (camera)
	{
		if (!(worldFlags & kWorldListenerInhibit))
		{
			OmniSource *source = engagedSourceList.First();
			while (source)
			{
				source->BeginUpdate();
				source = source->Next();
			}

			const Point3D& listenerPosition = camera->GetWorldPosition();
			SoundRoom *listenerRoom = nullptr;

			const RootCameraRegion *cameraRegion = camera->GetFirstRootRegion();
			while (cameraRegion)
			{
				Zone *zone = cameraRegion->GetZone();
				if (zone->GetTraversalExclusionMask() == 0)
				{
					zone->SetTraversalExclusionMask(kZoneTraversalLocal);

					const AcousticsSpace *acousticsSpace = zone->GetConnectedAcousticsSpace();
					if (acousticsSpace)
					{
						listenerRoom = acousticsSpace->GetSoundRoom();
					}

					SourceRegion *sourceRegion = zone->GetFirstSourceRegion();
					while (sourceRegion)
					{
						OmniSource *omniSource = sourceRegion->GetSource();

						const Point3D& sourcePosition = sourceRegion->GetPermeatedPosition();
						if (Magnitude(sourcePosition - listenerPosition) + sourceRegion->GetPermeatedPathLength() < omniSource->GetSourceRange())
						{
							unsigned_int32 state = omniSource->sourceState;
							if (!(state & kSourceEngaged))
							{
								engagedSourceList.Append(omniSource);
								omniSource->BeginUpdate();
							}

							omniSource->sourceState = state | kSourceAudible;
							omniSource->AddPlayRegion(sourceRegion, listenerPosition);
						}

						sourceRegion = sourceRegion->GetNextSourceRegion();
					}
				}

				cameraRegion = cameraRegion->Next();
			}

			TheSoundMgr->SetListenerRoom(listenerRoom);

			cameraRegion = camera->GetFirstRootRegion();
			while (cameraRegion)
			{
				cameraRegion->GetZone()->SetTraversalExclusionMask(0);
				cameraRegion = cameraRegion->Next();
			}

			source = engagedSourceList.First();
			while (source)
			{
				OmniSource *next = source->Next();

				unsigned_int32 state = source->sourceState;
				if (state & kSourceAudible)
				{
					if (!(state & kSourceEngaged))
					{
						if (!source->Engage())
						{
							playingSourceList[sourceParity].Append(source);
						}
					}
					else
					{
						source->EndUpdate();
					}
				}
				else
				{
					source->Disengage();
					playingSourceList[sourceParity].Append(source);
				}

				source = next;
			}
		}
	}
}

#if C4DIAGNOSTICS

	void World::RenderSourcePaths(Zone *zone, const Transform4D& listenerTransform)
	{
		if (!sourcePathVertexBuffer.Active())
		{
			sourcePathVertexBuffer.Establish(sizeof(Point3D) * 2);
		}

		const SourceRegion *sourceRegion = zone->GetFirstSourceRegion();
		while (sourceRegion)
		{
			const OmniSource *source = sourceRegion->GetSource();
			if (source->sourceState & kSourceEngaged)
			{
				const SourceRegion *region = sourceRegion->GetPrimaryRegion();

				volatile Point3D *restrict vertex = sourcePathVertexBuffer.BeginUpdate<Point3D>();
				vertex[0] = listenerTransform.GetTranslation() + listenerTransform[2];
				vertex[1] = region->GetAudiblePosition();
				sourcePathVertexBuffer.EndUpdate();

				TheGraphicsMgr->DrawRenderList(&sourcePathRenderList);

				const SourceRegion *superRegion = region->GetSuperNode();
				while (superRegion)
				{
					const SourceRegion *nextRegion = superRegion->GetSuperNode();
					if (superRegion->GetAudibleSubregion() == region)
					{
						vertex = sourcePathVertexBuffer.BeginUpdate<Point3D>();
						vertex[0] = region->GetAudiblePosition();
						vertex[1] = superRegion->GetAudiblePosition();
						sourcePathVertexBuffer.EndUpdate();

						TheGraphicsMgr->DrawRenderList(&sourcePathRenderList);

						region = superRegion;
					}

					superRegion = nextRegion;
				}
			}

			sourceRegion = sourceRegion->GetNextSourceRegion();
		}
	}

#endif

bool World::WorldBoundingBoxVisible(const Box3D& box, const Region *region, const List<Region> *occlusionList)
{
	if (region->BoxVisible(box))
	{
		region = occlusionList->First();
		while (region)
		{
			if (region->BoxOccluded(box))
			{
				return (false);
			}

			region = region->Next();
		}

		return (true);
	}

	return (false);
}

bool World::LightNodeVisible(const Node *node, const List<Reference<LightRegion>> *regionList)
{
	const Reference<LightRegion> *lightRegion = regionList->First();
	while (lightRegion)
	{
		if (node->Visible(lightRegion->GetTarget()))
		{
			return (true);
		}

		lightRegion = lightRegion->Next();
	}

	return (false);
}

bool World::ShadowNodeVisible(const Node *node, const List<Region> *shadowRegionList)
{
	const Region *region = shadowRegionList->First();
	while (region)
	{
		if (node->Visible(region))
		{
			return (true);
		}

		region = region->Next();
	}

	return (false);
}

bool World::ShadowCellVisible(const Site *cell, const List<Region> *shadowRegionList)
{
	const Region *region = shadowRegionList->First();
	while (region)
	{
		if (region->BoxVisible(cell->GetWorldBoundingBox()))
		{
			return (true);
		}

		region = region->Next();
	}

	return (false);
}

void World::UpdateMaxGeometryDepth(WorldContext *worldContext, Geometry *geometry)
{
	const Box3D& box = geometry->GetWorldBoundingBox();
	const int8 *index = worldContext->deepestBoxComponent;
	Point3D p(box.GetComponent(index[0]), box.GetComponent(index[1]), box.GetComponent(index[2]));
	float depth = worldContext->renderCamera->GetInverseWorldTransform().GetRow(2) ^ p;
	worldContext->maxGeometryDepth = Fmax(worldContext->maxGeometryDepth, depth);
}

void World::SetNodeFogState(const WorldContext *worldContext, const Node *node, Renderable *renderable)
{
	unsigned_int32 renderableFlags = renderable->GetRenderableFlags() & ~kRenderableUnfog;

	const Region *region = worldContext->unfoggedList.First();
	if ((region) && (region->BoxOccluded(node->GetWorldBoundingBox())))
	{
		renderableFlags |= kRenderableUnfog;
	}

	renderable->SetRenderableFlags(renderableFlags);
}

void World::ProcessGeometry(const WorldContext *worldContext, Geometry *geometry)
{
	const GeometryObject *object = geometry->GetObject();
	int32 geometryLevelCount = object->GetGeometryLevelCount();
	if ((geometryLevelCount > 1) || (object->GetGeometryFlags() & kGeometryShaderDetailEnable))
	{
		int32 minLevel = Max(geometry->GetMinDetailLevel(), worldContext->cameraMinDetailLevel);

		const BoundingSphere *sphere = geometry->GetBoundingSphere();
		const Point3D& center = sphere->GetCenter();

		const FrustumCamera *camera = worldContext->renderCamera;
		Vector3D direction = center - camera->GetWorldPosition();
		float d = Magnitude(direction);
		if ((d > kDetailEpsilon) && (camera->GetWorldTransform()[2] * direction > 0.0F))
		{
			float focalLength = static_cast<FrustumCameraObject *>(camera->Node::GetObject())->GetFocalLength();
			float r = sphere->GetRadius() * focalLength / d;
			float t = worldContext->cameraDetailBias - Log(r);

			int32 level = Min(Max((int32) (t - 1.5F + object->GetGeometryDetailBias()), minLevel), geometryLevelCount - 1);
			if (geometry->GetDetailLevel() != level)
			{
				geometry->SetDetailLevel(level);
			}

			if (object->GetGeometryFlags() & kGeometryShaderDetailEnable)
			{
				float u = t + 1.0F + object->GetShaderDetailBias();
				geometry->SetShaderDetailLevel(Max((int32) u, minLevel));
				geometry->SetShaderDetailParameter(FmaxZero(Fmin(1.0F - u, 1.0F)));
			}
		}
		else
		{
			if (geometry->GetDetailLevel() != 0)
			{
				geometry->SetDetailLevel(0);
			}

			geometry->SetShaderDetailLevel(0);
			geometry->SetShaderDetailParameter(1.0F);
		}
	}

	Controller *controller = geometry->GetController();
	if ((controller) && (controller->GetControllerFlags() & kControllerUpdate))
	{
		controller->Update();
	}
}

void World::UpdateGeometry(Geometry *geometry)
{
	unsigned_int32 ambientStamp = ambientRenderStamp;
	if (geometry->GetProcessStamp() != ambientStamp)
	{
		geometry->SetProcessStamp(ambientStamp);
		ProcessGeometry(currentWorldContext, geometry);
	}
}

void World::RenderAmbientGeometry(WorldContext *worldContext, const CameraRegion *cameraRegion, Geometry *geometry)
{
	unsigned_int32 flags = geometry->GetObject()->GetGeometryFlags();
	if (!(flags & kGeometryInvisible))
	{
		geometry->SetProcessStamp(ambientRenderStamp);

		ProcessGeometry(worldContext, geometry);
		UpdateMaxGeometryDepth(worldContext, geometry);
		SetNodeFogState(worldContext, geometry, geometry);

		renderStageList[geometry->GetGeometryRenderStage()].Append(geometry);
		worldCounter[kWorldCounterGeometry]++;

		if (geometry->GetGeometryType() == kGeometryWater)
		{
			float d = SquaredMag(geometry->GetBoundingSphere()->GetCenter() - worldContext->renderCamera->GetWorldPosition());
			static_cast<WaterGeometry *>(geometry)->UpdateWater(d);

			worldCounter[kWorldCounterWater]++;
		}
	}
}

void World::RenderAmbientTerrain(WorldContext *worldContext, const CameraRegion *cameraRegion, TerrainGeometry *terrain)
{
	const TerrainGeometryObject *object = terrain->GetObject();
	int32 level = object->GetDetailLevel();

	if (level <= worldContext->cameraMinDetailLevel)
	{
		unsigned_int32 ambientStamp = ambientRenderStamp;
		if (terrain->GetSiteStamp() != ambientStamp)
		{
			terrain->SetSiteStamp(ambientRenderStamp);

			if (!(object->GetGeometryFlags() & kGeometryInvisible))
			{
				UpdateMaxGeometryDepth(worldContext, terrain);
				SetNodeFogState(worldContext, terrain, terrain);

				renderStageList[kRenderStageDefault].Append(terrain);
				worldCounter[kWorldCounterTerrain]++;

				if (level != 0)
				{
					terrainList.Append(static_cast<TerrainLevelGeometry *>(terrain));
				}
			}
		}
	}
	else
	{
		const FrustumCamera *camera = worldContext->renderCamera;
		Vector3D direction = terrain->GetWorldCenter() - camera->GetWorldPosition();
		float d = Magnitude(direction) / camera->GetObject()->GetFocalLength();

		if ((d > terrain->GetRenderDistance()) && (camera->GetWorldTransform()[2] * direction > 0.0F))
		{
			unsigned_int32 ambientStamp = ambientRenderStamp;
			if (terrain->GetSiteStamp() != ambientStamp)
			{
				terrain->SetSiteStamp(ambientRenderStamp);

				if (!(object->GetGeometryFlags() & kGeometryInvisible))
				{
					UpdateMaxGeometryDepth(worldContext, terrain);
					SetNodeFogState(worldContext, terrain, terrain);

					terrainList.Append(static_cast<TerrainLevelGeometry *>(terrain));
					renderStageList[kRenderStageDefault].Append(terrain);
					worldCounter[kWorldCounterTerrain]++;
				}
			}
		}
		else
		{
			terrain->SetSiteStamp(ambientRenderStamp);

			const Bond *bond = terrain->GetFirstOutgoingEdge();
			while (bond)
			{
				TerrainGeometry *subterrain = static_cast<TerrainGeometry *>(bond->GetFinishElement());
				if ((subterrain->Enabled()) && ((subterrain->GetPerspectiveExclusionMask() & worldContext->perspectiveFlags) == 0) && (subterrain->Visible(cameraRegion, &worldContext->occlusionList)))
				{
					RenderAmbientTerrain(worldContext, cameraRegion, subterrain);
				}

				bond = bond->GetNextOutgoingEdge();
			}
		}
	}
}

void World::RenderAmbientNode(WorldContext *worldContext, const CameraRegion *cameraRegion, Node *node)
{
	if ((node->Enabled()) && ((node->GetPerspectiveExclusionMask() & worldContext->perspectiveFlags) == 0))
	{
		NodeType type = node->GetNodeType();

		if (node->Visible(cameraRegion, &worldContext->occlusionList))
		{
			if (type == kNodeGeometry)
			{
				Geometry *geometry = static_cast<Geometry *>(node);
				if (geometry->GetGeometryType() != kGeometryTerrain)
				{
					unsigned_int32 ambientStamp = ambientRenderStamp;
					if (geometry->GetSiteStamp() != ambientStamp)
					{
						geometry->SetSiteStamp(ambientRenderStamp);
						RenderAmbientGeometry(worldContext, cameraRegion, geometry);
					}
				}
				else
				{
					RenderAmbientTerrain(worldContext, cameraRegion, static_cast<TerrainGeometry *>(geometry));
					return;
				}
			}
			else if (type == kNodeImpostor)
			{
				Impostor *impostor = static_cast<Impostor *>(node);

				float distance = SquaredMag(impostor->GetWorldPosition().GetVector2D() - worldContext->renderCamera->GetWorldPosition().GetVector2D());
				if (distance > impostor->GetSquaredRenderDistance())
				{
					unsigned_int32 ambientStamp = ambientRenderStamp;
					if (impostor->GetSiteStamp() != ambientStamp)
					{
						impostor->SetSiteStamp(ambientRenderStamp);
						impostor->Render();

						worldCounter[kWorldCounterImpostor]++;
					}

					if (distance > impostor->GetSquaredGeometryDistance())
					{
						return;
					}
				}
				else
				{
					node->SetSiteStamp(ambientRenderStamp);
				}
			}
			else
			{
				node->SetSiteStamp(ambientRenderStamp);
			}

			const Bond *bond = node->GetFirstOutgoingEdge();
			while (bond)
			{
				RenderAmbientNode(worldContext, cameraRegion, static_cast<Node *>(bond->GetFinishElement()));
				bond = bond->GetNextOutgoingEdge();
			}
		}
		else if (type == kNodeGeometry)
		{
			Geometry *geometry = static_cast<Geometry *>(node);
			if (geometry->GetGeometryType() == kGeometryWater)
			{
				float d = SquaredMag(geometry->GetBoundingSphere()->GetCenter() - worldContext->renderCamera->GetWorldPosition());
				static_cast<WaterGeometry *>(geometry)->UpdateInvisibleWater(d);
			}
		}
	}
}

void World::RenderAmbientCell(WorldContext *worldContext, const CameraRegion *cameraRegion, Site *cell)
{
	if (WorldBoundingBoxVisible(cell->GetWorldBoundingBox(), cameraRegion, &worldContext->occlusionList))
	{
		cell->SetSiteStamp(ambientRenderStamp);

		const Bond *bond = cell->GetFirstOutgoingEdge();
		while (bond)
		{
			Site *site = bond->GetFinishElement();
			if (site->GetCellIndex() < 0)
			{
				RenderAmbientNode(worldContext, cameraRegion, static_cast<Node *>(site));
			}
			else
			{
				RenderAmbientCell(worldContext, cameraRegion, site);
			}

			bond = bond->GetNextOutgoingEdge();
		}
	}
}

void World::RenderAmbientEffectNode(const WorldContext *worldContext, const CameraRegion *cameraRegion, Effect *effect)
{
	unsigned_int32 ambientStamp = ambientRenderStamp;
	if (effect->GetSiteStamp() != ambientStamp)
	{
		if ((effect->Enabled()) && ((effect->GetPerspectiveExclusionMask() & worldContext->perspectiveFlags) == 0) && (effect->Visible(cameraRegion, &worldContext->occlusionList)))
		{
			effect->SetSiteStamp(ambientStamp);

			SetNodeFogState(worldContext, effect, effect);
			effect->Render(worldContext->renderCamera, &renderStageList[kRenderStageFirstEffect]);

			if ((effect->GetEffectListIndex() == kEffectListLight) && (effect->Rendering()))
			{
				effect->SetRenderStamp(ambientStamp);
			}
		}
	}
}

void World::RenderAmbientEffectCell(WorldContext *worldContext, const CameraRegion *cameraRegion, Site *cell)
{
	if (WorldBoundingBoxVisible(cell->GetWorldBoundingBox(), cameraRegion, &worldContext->occlusionList))
	{
		cell->SetSiteStamp(ambientRenderStamp);

		const Bond *bond = cell->GetFirstOutgoingEdge();
		while (bond)
		{
			Site *site = bond->GetFinishElement();
			if (site->GetCellIndex() < 0)
			{
				RenderAmbientEffectNode(worldContext, cameraRegion, static_cast<Effect *>(site));
			}
			else
			{
				RenderAmbientEffectCell(worldContext, cameraRegion, site);
			}

			bond = bond->GetNextOutgoingEdge();
		}
	}
}

void World::RenderAmbientRegion(WorldContext *worldContext, const CameraRegion *cameraRegion)
{
	const Zone *zone = cameraRegion->GetZone();

	if (zone->GetObject()->GetZoneFlags() & kZoneRenderSkybox)
	{
		worldContext->skyboxFlag = true;
	}

	Region *unfoggedRegion = worldContext->unfoggedList.First();
	if ((unfoggedRegion) && (!zone->GetFirstFogSpace()) && (!zone->GetConnectedFogSpace()))
	{
		worldContext->unfoggedList.Remove(unfoggedRegion);
	}

	const Bond *bond = zone->GetCellGraphSite(kCellGraphGeometry)->GetFirstOutgoingEdge();
	while (bond)
	{
		Site *site = bond->GetFinishElement();
		if (site->GetCellIndex() < 0)
		{
			RenderAmbientNode(worldContext, cameraRegion, static_cast<Node *>(site));
		}
		else
		{
			RenderAmbientCell(worldContext, cameraRegion, site);
		}

		bond = bond->GetNextOutgoingEdge();
	}

	bond = zone->GetCellGraphSite(kCellGraphEffect)->GetFirstOutgoingEdge();
	while (bond)
	{
		Site *site = bond->GetFinishElement();
		if (site->GetCellIndex() < 0)
		{
			RenderAmbientEffectNode(worldContext, cameraRegion, static_cast<Effect *>(site));
		}
		else
		{
			RenderAmbientEffectCell(worldContext, cameraRegion, site);
		}

		bond = bond->GetNextOutgoingEdge();
	}

	if (unfoggedRegion)
	{
		worldContext->unfoggedList.Append(unfoggedRegion);
	}

	CameraRegion *subregion = cameraRegion->GetFirstSubnode();
	while (subregion)
	{
		RenderAmbientRegion(worldContext, subregion);
		subregion = subregion->Tree<CameraRegion>::Next();
	}
}

void World::CalculateInfiniteNearClipRegion(const FrustumCamera *camera, const Vector3D& lightDirection, Region *nearClipRegion)
{
	const FrustumCameraObject *cameraObject = camera->GetObject();
	Antivector4D *plane = nearClipRegion->GetPlaneArray();

	if ((cameraObject->GetCameraType() != kCameraRemote) || (!(cameraObject->GetFrustumFlags() & kFrustumOblique)))
	{
		const Point3D *vertex = camera->GetFrustumVertexArray();
		const Point3D& center = camera->GetNearPlaneCenter();

		const Transform4D& transform = camera->GetWorldTransform();

		float lz = transform[2] * lightDirection;
		if (lz > kNearClipEpsilon)
		{
			nearClipRegion->SetPlaneCount(5);
			plane[0].Set(transform[2], center);

			const Point3D *v1 = &vertex[3];
			for (machine a = 0; a < 4; a++)
			{
				const Point3D *v2 = &vertex[a];
				Vector3D normal = Normalize((*v2 - *v1) % lightDirection);
				plane[a + 1].Set(normal, *v1);
				v1 = v2;
			}
		}
		else if (lz < -kNearClipEpsilon)
		{
			nearClipRegion->SetPlaneCount(5);
			plane[0].Set(-transform[2], center);

			const Point3D *v1 = &vertex[3];
			for (machine a = 0; a < 4; a++)
			{
				const Point3D *v2 = &vertex[a];
				Vector3D normal = Normalize((*v1 - *v2) % lightDirection);
				plane[a + 1].Set(normal, *v1);
				v1 = v2;
			}
		}
		else
		{
			nearClipRegion->SetPlaneCount(2);
			plane[0].Set(transform[2], center);
			plane[1] = -plane[0];
		}
	}
	else
	{
		const RemoteCamera *remoteCamera = static_cast<const RemoteCamera *>(camera);
		int32 vertexCount = remoteCamera->GetRemoteVertexCount();
		const Point3D *vertex = remoteCamera->GetRemoteVertexArray();

		const RemoteCameraObject *remoteCameraObject = remoteCamera->GetObject();
		const Antivector4D& clipPlane = remoteCameraObject->GetRemoteClipPlane();
		float m = remoteCameraObject->GetRemoteDeterminant();

		float ld = clipPlane ^ lightDirection;
		if (ld > kNearClipEpsilon)
		{
			nearClipRegion->SetPlaneCount(vertexCount + 1);
			plane[0] = clipPlane;

			const Point3D *v1 = &vertex[vertexCount - 1];
			for (machine a = 0; a < vertexCount; a++)
			{
				const Point3D *v2 = &vertex[a];
				Vector3D normal = (*v2 - *v1) % lightDirection;
				normal *= InverseMag(normal) * m;
				plane[a + 1].Set(normal, *v1);
				v1 = v2;
			}
		}
		else if (ld < -kNearClipEpsilon)
		{
			nearClipRegion->SetPlaneCount(vertexCount + 1);
			plane[0] = -clipPlane;

			const Point3D *v1 = &vertex[vertexCount - 1];
			for (machine a = 0; a < vertexCount; a++)
			{
				const Point3D *v2 = &vertex[a];
				Vector3D normal = (*v1 - *v2) % lightDirection;
				normal *= InverseMag(normal) * m;
				plane[a + 1].Set(normal, *v1);
				v1 = v2;
			}
		}
		else
		{
			nearClipRegion->SetPlaneCount(2);
			plane[0] = clipPlane;
			plane[1] = -clipPlane;
		}
	}
}

void World::CalculateInfiniteShadowRegion(const FrustumCamera *camera, const CameraRegion *cameraRegion, const Vector3D& lightDirection, ShadowRegion *shadowRegion)
{
	int32 cameraPlaneCount = cameraRegion->GetPlaneCount();
	int32 nonlateralPlaneCount = cameraRegion->GetNonlateralPlaneCount();
	int32 lateralPlaneCount = cameraPlaneCount - nonlateralPlaneCount;

	const Antivector4D *cameraPlane = cameraRegion->GetPlaneArray();
	Antivector4D *shadowPlane = shadowRegion->GetPlaneArray();

	if (cameraRegion->ContainsInfiniteLight(lightDirection))
	{
		shadowPlane[0].Set(camera->GetWorldTransform()[2], camera->GetWorldPosition());

		for (machine a = 0; a < lateralPlaneCount; a++)
		{
			shadowPlane[a + 1] = cameraPlane[a];
		}

		shadowRegion->SetPlaneCount(lateralPlaneCount + 1);
	}
	else
	{
		bool	frontArray[kMaxPortalVertexCount];

		for (machine a = 0; a < lateralPlaneCount; a++)
		{
			frontArray[a] = ((cameraPlane[a] ^ lightDirection) > 0.0F);
		}

		int32 shadowPlaneCount = 0;

		const Antivector4D *plane1 = &cameraPlane[lateralPlaneCount - 1];
		bool front1 = frontArray[lateralPlaneCount - 1];

		for (machine a = 0; a < lateralPlaneCount; a++)
		{
			const Antivector4D *plane2 = &cameraPlane[a];
			bool front2 = frontArray[a];

			if (front2)
			{
				shadowPlane[shadowPlaneCount++] = *plane2;

				if (!front1)
				{
					Antivector4D& newPlane = shadowPlane[shadowPlaneCount++];
					newPlane = Bivector4D(*plane1, *plane2) ^ lightDirection;
					newPlane.Standardize();
				}
			}
			else
			{
				if (front1)
				{
					Antivector4D& newPlane = shadowPlane[shadowPlaneCount++];
					newPlane = Bivector4D(*plane2, *plane1) ^ lightDirection;
					newPlane.Standardize();
				}
			}

			plane1 = plane2;
			front1 = front2;
		}

		unsigned_int32 flags = cameraRegion->GetShadowRegionFlags();
		for (machine a = 0; a < nonlateralPlaneCount; a++)
		{
			if (flags & 1)
			{
				const Antivector4D& plane = cameraPlane[lateralPlaneCount + a];
				bool front = ((plane ^ lightDirection) > 0.0F);

				if (front)
				{
					shadowPlane[shadowPlaneCount] = plane;

					if (++shadowPlaneCount == kMaxRegionPlaneCount)
					{
						break;
					}

					for (machine b = 0; b < lateralPlaneCount; b++)
					{
						if (!frontArray[b])
						{
							Antivector4D& newPlane = shadowPlane[shadowPlaneCount];
							newPlane = Bivector4D(cameraPlane[b], plane) ^ lightDirection;
							newPlane.Standardize();

							if (++shadowPlaneCount == kMaxRegionPlaneCount)
							{
								goto full;
							}
						}
					}
				}
				else
				{
					for (machine b = 0; b < lateralPlaneCount; b++)
					{
						if (frontArray[b])
						{
							Antivector4D& newPlane = shadowPlane[shadowPlaneCount];
							newPlane = Bivector4D(plane, cameraPlane[b]) ^ lightDirection;
							newPlane.Standardize();

							if (++shadowPlaneCount == kMaxRegionPlaneCount)
							{
								goto full;
							}
						}
					}
				}
			}

			flags >>= 1;
		}

		full:
		shadowRegion->SetPlaneCount(shadowPlaneCount);
	}
}

void World::RenderInfiniteShadowVolumeGeometry(InfiniteLight *light, StencilMode stencilMode, Geometry *geometry)
{
	int32 detailLevel = geometry->GetDetailLevel();
	const GeometryObject *geometryObject = geometry->GetObject();
	const Mesh *mesh = geometryObject->GetGeometryLevel(detailLevel);

	int32 edgeCount = mesh->GetArrayDescriptor(kArrayEdge)->elementCount;
	if ((stencilMode == kStencilFail) || (edgeCount != 0))
	{
		StencilData		*stencilData;

		bool buildExtrusion = true;
		bool buildEndcaps = (stencilMode == kStencilFail);
		StencilVolume *stencilVolume = nullptr;

		unsigned_int32 lightFlags = light->GetObject()->GetLightFlags();
		if ((lightFlags & kLightStatic) && (!(geometryObject->GetGeometryFlags() & kGeometryDynamic)))
		{
			Link<StencilVolume> *stencilVolumeLink = geometry->GetStaticStencilVolume(light);
			if (stencilVolumeLink)
			{
				stencilVolume = *stencilVolumeLink;
				if (stencilVolume)
				{
					buildExtrusion = (stencilVolume->GetExtrusionDetailLevel() != detailLevel);
					if (buildEndcaps)
					{
						buildEndcaps = (stencilVolume->GetEndcapDetailLevel() != detailLevel);
					}
				}
				else
				{
					stencilVolume = new StencilVolume(geometry, light, stencilVolumeLink);
				}

				stencilData = stencilVolume;
			}
			else
			{
				stencilData = geometry->GetStencilData();
			}
		}
		else
		{
			stencilData = geometry->GetStencilData();
		}

		if (buildExtrusion)
		{
			stencilData->CalculateInfiniteShadowBounds(light);
		}

		const Vector4D& objectLightPosition = TheGraphicsMgr->SetGeometryTransformable(geometry->GetTransformable());
		if (TheGraphicsMgr->ActivateShadowBounds(stencilData))
		{
			if (buildExtrusion)
			{
				geometry->CalculateInfiniteShadowFrontArray(objectLightPosition.GetVector3D(), geometryFrontArray);

				const Point3D *vertex = static_cast<Point3D *>(geometry->GetArrayBundle(kArrayPosition)->pointer);
				const Edge *edge = mesh->GetArray<Edge>(kArrayEdge);
				const bool *front = geometryFrontArray;

				VertexBuffer *vertexBuffer = stencilData->GetExtrusionVertexBuffer();
				volatile Vector4D *restrict extrusionVertex = vertexBuffer->BeginUpdate<Vector4D>();

				int32 extrusionEdgeCount = 0;
				for (machine a = 0; a < edgeCount; a++)
				{
					bool f1 = front[edge->faceIndex[0]];
					bool f2 = front[edge->faceIndex[1]];
					if (f1 ^ f2)
					{
						const Point3D& p1 = vertex[edge->vertexIndex[0]];
						const Point3D& p2 = vertex[edge->vertexIndex[1]];

						if (f1)
						{
							extrusionVertex[0] = p2;
							extrusionVertex[1] = p1;
						}
						else
						{
							extrusionVertex[0] = p1;
							extrusionVertex[1] = p2;
						}

						extrusionVertex[2].Set(0.0F, 0.0F, 0.0F, 0.0F);

						extrusionEdgeCount++;
						extrusionVertex += 3;
					}

					edge++;
				}

				stencilData->SetExtrusionEdgeCount(extrusionEdgeCount);
				vertexBuffer->EndUpdate();

				if (stencilVolume)
				{
					stencilVolume->SetExtrusionDetailLevel(detailLevel);
				}
			}
			else if (buildEndcaps)
			{
				geometry->CalculateInfiniteShadowFrontArray(objectLightPosition.GetVector3D(), geometryFrontArray);
			}

			if (stencilData->GetExtrusionEdgeCount() != 0)
			{
				TheGraphicsMgr->DrawStencilShadow(stencilData, kStencilInfiniteExtrusion, stencilMode);

				if (stencilMode == kStencilFail)
				{
					if (buildEndcaps)
					{
						VertexBuffer *vertexBuffer = stencilData->GetEndcapIndexBuffer();
						volatile Triangle *restrict frontEndcapTriangleArray = vertexBuffer->BeginUpdate<Triangle>();

						int32 primitiveCount = mesh->GetPrimitiveCount();
						const Triangle *triangle = mesh->GetArray<Triangle>(kArrayPrimitive);
						const bool *front = geometryFrontArray;

						int32 frontEndcapTriangleCount = 0;

						const unsigned_int16 *planeIndex = mesh->GetArray<unsigned_int16>(kArrayPlaneIndex);
						if (planeIndex)
						{
							for (machine a = 0; a < primitiveCount; a++)
							{
								if (front[*planeIndex])
								{
									frontEndcapTriangleArray[frontEndcapTriangleCount] = *triangle;
									frontEndcapTriangleCount++;
								}

								triangle++;
								planeIndex++;
							}
						}
						else
						{
							for (machine a = 0; a < primitiveCount; a++)
							{
								if (front[a])
								{
									frontEndcapTriangleArray[frontEndcapTriangleCount] = *triangle;
									frontEndcapTriangleCount++;
								}

								triangle++;
							}
						}

						stencilData->SetFrontEndcapTriangleCount(frontEndcapTriangleCount);
						vertexBuffer->EndUpdate();

						if (stencilVolume)
						{
							stencilVolume->SetEndcapDetailLevel(detailLevel);
						}
					}

					if (stencilData->GetFrontEndcapTriangleCount() != 0)
					{
						unsigned_int32 bufferIndex = ((geometry->GetVertexBufferArrayFlags() & (1 << kArrayPosition)) != 0);
						stencilData->SetGeometryVertexData(geometry->GetVertexCount(), geometry->GetVertexBuffer(bufferIndex), geometry->GetVertexAttributeOffset(kArrayPosition), geometry->GetVertexBufferStride(bufferIndex));
						TheGraphicsMgr->DrawStencilShadow(stencilData, kStencilEndcapIdentity, kStencilFail);
					}
				}
			}
		}
	}
}

void World::RenderInfiniteShadowVolumeNode(const WorldContext *worldContext, InfiniteLight *light, const ShadowRenderData *renderData, Node *node)
{
	if ((node->Enabled()) && (((node->GetPerspectiveExclusionMask() >> kPerspectiveDirectShadowShift) & worldContext->perspectiveFlags) == 0) && (node->Visible(renderData->lightRegion)))
	{
		if (node->GetNodeType() == kNodeGeometry)
		{
			Geometry *geometry = static_cast<Geometry *>(node);

			unsigned_int32 shadowStamp = shadowRenderStamp;
			if (geometry->GetShadowStamp() != shadowStamp)
			{
				geometry->SetShadowStamp(shadowStamp);

				const GeometryObject *geometryObject = geometry->GetObject();
				if ((geometryObject->GetGeometryFlags() & worldContext->shadowInhibitMask) == 0)
				{
					if (ShadowNodeVisible(geometry, &renderData->shadowRegionList))
					{
						unsigned_int32 exclusionMask = geometry->GetPerspectiveExclusionMask();
						StencilMode stencilMode = ((exclusionMask & worldContext->perspectiveFlags) == 0) ? kStencilPass : kStencilFail;

						unsigned_int32 ambientStamp = ambientRenderStamp;
						if (geometry->GetProcessStamp() != ambientStamp)
						{
							geometry->SetProcessStamp(ambientStamp);

							if (exclusionMask & kPerspectivePrimary)
							{
								stencilMode = kStencilFail;
							}

							ProcessGeometry(worldContext, geometry);
							FinishWorldBatch();
						}

						if ((stencilMode == kStencilPass) && (geometry->Visible(&renderData->nearClipRegion)))
						{
							stencilMode = kStencilFail;
						}

						RenderInfiniteShadowVolumeGeometry(light, stencilMode, geometry);
						worldCounter[kWorldCounterStencilShadow]++;
					}
				}
			}
		}

		const Bond *bond = node->GetFirstOutgoingEdge();
		while (bond)
		{
			Node *subnode = static_cast<Node *>(bond->GetFinishElement());
			if (subnode != renderData->excludeNode)
			{
				RenderInfiniteShadowVolumeNode(worldContext, light, renderData, subnode);
			}

			bond = bond->GetNextOutgoingEdge();
		}
	}
}

void World::RenderInfiniteShadowVolumeCell(const WorldContext *worldContext, InfiniteLight *light, const ShadowRenderData *renderData, const Site *cell)
{
	if ((renderData->lightRegion->BoxVisible(cell->GetWorldBoundingBox())) && (ShadowCellVisible(cell, &renderData->shadowRegionList)))
	{
		const Bond *bond = cell->GetFirstOutgoingEdge();
		while (bond)
		{
			Site *site = bond->GetFinishElement();
			if (site->GetCellIndex() < 0)
			{
				Node *node = static_cast<Node *>(site);
				if (node != renderData->excludeNode)
				{
					RenderInfiniteShadowVolumeNode(worldContext, light, renderData, node);
				}
			}
			else
			{
				RenderInfiniteShadowVolumeCell(worldContext, light, renderData, site);
			}

			bond = bond->GetNextOutgoingEdge();
		}
	}
}

void World::RenderInfiniteShadowVolumes(const WorldContext *worldContext, InfiniteLight *light, List<Reference<LightRegion>> *regionList)
{
	ShadowRenderData	renderData;

	const Node *excludeNode = light->GetExclusionNode();
	renderData.excludeNode = excludeNode;

	const Vector3D& lightDirection = light->GetWorldTransform()[2];
	CalculateInfiniteNearClipRegion(worldContext->renderCamera, lightDirection, &renderData.nearClipRegion);

	Reference<LightRegion> *lightRegionReference = regionList->First();
	while (lightRegionReference)
	{
		LightRegion *lightRegion = lightRegionReference->GetTarget();
		do
		{
			CameraRegion *cameraRegion = lightRegion->GetZone()->GetFirstCameraRegion();
			while (cameraRegion)
			{
				if (cameraRegion->GetCamera() == worldContext->renderCamera)
				{
					ShadowRegion *shadowRegion = cameraRegion->GetShadowRegion();
					renderData.shadowRegionList.Append(shadowRegion);

					if (shadowRegion->GetLight() != light)
					{
						shadowRegion->SetLight(light);
						CalculateInfiniteShadowRegion(worldContext->renderCamera, cameraRegion, lightDirection, shadowRegion);

						#if C4DIAGNOSTICS

							if (diagnosticFlags & kDiagnosticShadowRegions)
							{
								shadowRegionDiagnosticList.Append(new RegionRenderable(shadowRegion, worldContext->renderCamera->GetWorldPosition(), kDiagnosticRegionSize));
							}

						#endif
					}
				}

				cameraRegion = cameraRegion->GetNextCameraRegion();
			}

			lightRegion = lightRegion->GetSuperNode();
		} while (lightRegion);

		lightRegionReference = lightRegionReference->Next();
	}

	TheGraphicsMgr->BeginStencilShadow();
	shadowRenderStamp++;

	lightRegionReference = regionList->First();
	while (lightRegionReference)
	{
		LightRegion *lightRegion = lightRegionReference->GetTarget();
		do
		{
			unsigned_int32 flags = lightRegion->GetLightRegionFlags();
			if (!(flags & kLightRegionShadowsRendered))
			{
				lightRegion->SetLightRegionFlags(flags | kLightRegionShadowsRendered);
				renderData.lightRegion = lightRegion;

				const Bond *bond = lightRegion->GetZone()->GetCellGraphSite(kCellGraphGeometry)->GetFirstOutgoingEdge();
				while (bond)
				{
					Site *site = bond->GetFinishElement();
					if (site->GetCellIndex() < 0)
					{
						Node *node = static_cast<Node *>(site);
						if (node != excludeNode)
						{
							RenderInfiniteShadowVolumeNode(worldContext, light, &renderData, node);
						}
					}
					else
					{
						RenderInfiniteShadowVolumeCell(worldContext, light, &renderData, site);
					}

					bond = bond->GetNextOutgoingEdge();
				}
			}

			lightRegion = lightRegion->GetSuperNode();
		} while (lightRegion);

		lightRegionReference = lightRegionReference->Next();
	}

	TheGraphicsMgr->EndStencilShadow();
	renderData.shadowRegionList.RemoveAll();
}

void World::RenderInfiniteLightTerrain(const WorldContext *worldContext, const List<Reference<LightRegion>> *regionList, TerrainGeometry *terrain)
{
	unsigned_int32 lightStamp = lightRenderStamp;
	if (terrain->GetLightStamp() != lightStamp)
	{
		const TerrainGeometryObject *object = terrain->GetObject();
		int32 level = object->GetDetailLevel();

		if (level <= worldContext->cameraMinDetailLevel)
		{
			terrain->SetLightStamp(lightStamp);

			if ((object->GetGeometryFlags() & (kGeometryInvisible | kGeometryAmbientOnly | kGeometryRenderEffectPass)) == 0)
			{
				renderStageList[kRenderStageDefault].Append(terrain);
			}
		}
		else
		{
			const FrustumCamera *camera = worldContext->renderCamera;
			Vector3D direction = terrain->GetWorldCenter() - camera->GetWorldPosition();
			float d = Magnitude(direction) / camera->GetObject()->GetFocalLength();

			if ((d > terrain->GetRenderDistance()) && (camera->GetWorldTransform()[2] * direction > 0.0F))
			{
				terrain->SetLightStamp(lightStamp);

				if ((object->GetGeometryFlags() & (kGeometryInvisible | kGeometryAmbientOnly | kGeometryRenderEffectPass)) == 0)
				{
					renderStageList[kRenderStageDefault].Append(terrain);
				}
			}
			else
			{
				const Bond *bond = terrain->GetFirstOutgoingEdge();
				while (bond)
				{
					Node *node = static_cast<Node *>(bond->GetFinishElement());
					if ((node->GetSiteStamp() == ambientRenderStamp) && (LightNodeVisible(node, regionList)))
					{
						RenderInfiniteLightTerrain(worldContext, regionList, static_cast<TerrainGeometry *>(node));
					}

					bond = bond->GetNextOutgoingEdge();
				}
			}
		}
	}
}

void World::RenderInfiniteLightNode(const WorldContext *worldContext, const InfiniteLight *light, const List<Reference<LightRegion>> *regionList, Node *node)
{
	if ((node != light->GetExclusionNode()) && (LightNodeVisible(node, regionList)))
	{
		NodeType type = node->GetNodeType();
		if (type == kNodeGeometry)
		{
			Geometry *geometry = static_cast<Geometry *>(node);
			if (geometry->GetGeometryType() != kGeometryTerrain)
			{
				unsigned_int32 lightStamp = lightRenderStamp;
				if (geometry->GetLightStamp() != lightStamp)
				{
					geometry->SetLightStamp(lightStamp);

					unsigned_int32 flags = geometry->GetObject()->GetGeometryFlags();
					if ((flags & (kGeometryInvisible | kGeometryAmbientOnly | kGeometryRenderEffectPass)) == 0)
					{
						renderStageList[kRenderStageDefault].Append(geometry);
					}
				}
			}
			else
			{
				RenderInfiniteLightTerrain(worldContext, regionList, static_cast<TerrainGeometry *>(geometry));
				return;
			}
		}
		else if (type == kNodeImpostor)
		{
			Impostor *impostor = static_cast<Impostor *>(node);

			float distance = SquaredMag(impostor->GetWorldPosition().GetVector2D() - worldContext->renderCamera->GetWorldPosition().GetVector2D());
			if (distance > impostor->GetSquaredRenderDistance())
			{
				unsigned_int32 lightStamp = lightRenderStamp;
				if (impostor->GetLightStamp() != lightStamp)
				{
					impostor->SetLightStamp(lightStamp);
					impostor->Render();
				}

				if (distance > impostor->GetSquaredGeometryDistance())
				{
					return;
				}
			}
		}

		const Bond *bond = node->GetFirstOutgoingEdge();
		while (bond)
		{
			Site *site = bond->GetFinishElement();
			if (site->GetSiteStamp() == ambientRenderStamp)
			{
				RenderInfiniteLightNode(worldContext, light, regionList, static_cast<Node *>(site));
			}

			bond = bond->GetNextOutgoingEdge();
		}
	}
}

void World::RenderInfiniteLightCell(const WorldContext *worldContext, const InfiniteLight *light, const List<Reference<LightRegion>> *regionList, const Site *cell)
{
	const Box3D& box = cell->GetWorldBoundingBox();

	const Reference<LightRegion> *lightRegion = regionList->First();
	while (lightRegion)
	{
		if (lightRegion->GetTarget()->BoxVisible(box))
		{
			const Bond *bond = cell->GetFirstOutgoingEdge();
			while (bond)
			{
				Site *site = bond->GetFinishElement();
				if (site->GetSiteStamp() == ambientRenderStamp)
				{
					if (site->GetCellIndex() < 0)
					{
						RenderInfiniteLightNode(worldContext, light, regionList, static_cast<Node *>(site));
					}
					else
					{
						RenderInfiniteLightCell(worldContext, light, regionList, site);
					}
				}

				bond = bond->GetNextOutgoingEdge();
			}

			break;
		}

		lightRegion = lightRegion->Next();
	}
}

void World::RenderInfiniteLightEffectNode(const WorldContext *worldContext, const InfiniteLight *light, const List<Reference<LightRegion>> *regionList, Effect *effect)
{
	unsigned_int32 lightStamp = lightRenderStamp;
	if (effect->GetLightStamp() != lightStamp)
	{
		effect->SetLightStamp(lightStamp);

		if ((effect->GetRenderStamp() == ambientRenderStamp) && (effect != light->GetExclusionNode()) && (LightNodeVisible(effect, regionList)))
		{
			renderStageList[kRenderStageDefault].Append(effect);
		}
	}
}

void World::RenderInfiniteLightEffectCell(const WorldContext *worldContext, const InfiniteLight *light, const List<Reference<LightRegion>> *regionList, const Site *cell)
{
	const Box3D& box = cell->GetWorldBoundingBox();

	const Reference<LightRegion> *lightRegion = regionList->First();
	while (lightRegion)
	{
		if (lightRegion->GetTarget()->BoxVisible(box))
		{
			const Bond *bond = cell->GetFirstOutgoingEdge();
			while (bond)
			{
				Site *site = bond->GetFinishElement();
				if (site->GetSiteStamp() == ambientRenderStamp)
				{
					if (site->GetCellIndex() < 0)
					{
						RenderInfiniteLightEffectNode(worldContext, light, regionList, static_cast<Effect *>(site));
					}
					else
					{
						RenderInfiniteLightEffectCell(worldContext, light, regionList, site);
					}
				}

				bond = bond->GetNextOutgoingEdge();
			}

			break;
		}

		lightRegion = lightRegion->Next();
	}
}

void World::RenderInfiniteLightZone(const WorldContext *worldContext, const InfiniteLight *light, const Zone *zone, const List<Reference<LightRegion>> *regionList)
{
	const Bond *bond = zone->GetCellGraphSite(kCellGraphGeometry)->GetFirstOutgoingEdge();
	while (bond)
	{
		Site *site = bond->GetFinishElement();
		if (site->GetSiteStamp() == ambientRenderStamp)
		{
			if (site->GetCellIndex() < 0)
			{
				RenderInfiniteLightNode(worldContext, light, regionList, static_cast<Node *>(site));
			}
			else
			{
				RenderInfiniteLightCell(worldContext, light, regionList, site);
			}
		}

		bond = bond->GetNextOutgoingEdge();
	}

	bond = zone->GetCellGraphSite(kCellGraphEffect)->GetFirstOutgoingEdge();
	while (bond)
	{
		Site *site = bond->GetFinishElement();
		if (site->GetSiteStamp() == ambientRenderStamp)
		{
			if (site->GetCellIndex() < 0)
			{
				RenderInfiniteLightEffectNode(worldContext, light, regionList, static_cast<Effect *>(site));
			}
			else
			{
				RenderInfiniteLightEffectCell(worldContext, light, regionList, site);
			}
		}

		bond = bond->GetNextOutgoingEdge();
	}
}

void World::RenderInfiniteLightTerrain(const WorldContext *worldContext, TerrainGeometry *terrain)
{
	unsigned_int32 lightStamp = lightRenderStamp;
	if (terrain->GetLightStamp() != lightStamp)
	{
		const TerrainGeometryObject *object = terrain->GetObject();
		int32 level = object->GetDetailLevel();

		if (level <= worldContext->cameraMinDetailLevel)
		{
			terrain->SetLightStamp(lightStamp);

			if ((object->GetGeometryFlags() & (kGeometryInvisible | kGeometryAmbientOnly | kGeometryRenderEffectPass)) == 0)
			{
				renderStageList[kRenderStageDefault].Append(terrain);
			}
		}
		else
		{
			const FrustumCamera *camera = worldContext->renderCamera;
			Vector3D direction = terrain->GetWorldCenter() - camera->GetWorldPosition();
			float d = Magnitude(direction) / camera->GetObject()->GetFocalLength();

			if ((d > terrain->GetRenderDistance()) && (camera->GetWorldTransform()[2] * direction > 0.0F))
			{
				terrain->SetLightStamp(lightStamp);

				if ((object->GetGeometryFlags() & (kGeometryInvisible | kGeometryAmbientOnly | kGeometryRenderEffectPass)) == 0)
				{
					renderStageList[kRenderStageDefault].Append(terrain);
				}
			}
			else
			{
				const Bond *bond = terrain->GetFirstOutgoingEdge();
				while (bond)
				{
					Node *node = static_cast<Node *>(bond->GetFinishElement());
					if (node->GetSiteStamp() == ambientRenderStamp)
					{
						RenderInfiniteLightTerrain(worldContext, static_cast<TerrainGeometry *>(node));
					}

					bond = bond->GetNextOutgoingEdge();
				}
			}
		}
	}
}

void World::RenderInfiniteLightNode(const WorldContext *worldContext, const InfiniteLight *light, Node *node)
{
	if (node != light->GetExclusionNode())
	{
		NodeType type = node->GetNodeType();
		if (type == kNodeGeometry)
		{
			Geometry *geometry = static_cast<Geometry *>(node);
			if (geometry->GetGeometryType() != kGeometryTerrain)
			{
				unsigned_int32 lightStamp = lightRenderStamp;
				if (geometry->GetLightStamp() != lightStamp)
				{
					geometry->SetLightStamp(lightStamp);

					unsigned_int32 flags = geometry->GetObject()->GetGeometryFlags();
					if ((flags & (kGeometryInvisible | kGeometryAmbientOnly | kGeometryRenderEffectPass)) == 0)
					{
						renderStageList[kRenderStageDefault].Append(geometry);
					}
				}
			}
			else
			{
				RenderInfiniteLightTerrain(worldContext, static_cast<TerrainGeometry *>(geometry));
				return;
			}
		}
		else if (type == kNodeImpostor)
		{
			Impostor *impostor = static_cast<Impostor *>(node);

			float distance = SquaredMag(impostor->GetWorldPosition().GetVector2D() - worldContext->renderCamera->GetWorldPosition().GetVector2D());
			if (distance > impostor->GetSquaredRenderDistance())
			{
				unsigned_int32 lightStamp = lightRenderStamp;
				if (impostor->GetLightStamp() != lightStamp)
				{
					impostor->SetLightStamp(lightStamp);
					impostor->Render();
				}

				if (distance > impostor->GetSquaredGeometryDistance())
				{
					return;
				}
			}
		}

		const Bond *bond = node->GetFirstOutgoingEdge();
		while (bond)
		{
			Site *site = bond->GetFinishElement();
			if (site->GetSiteStamp() == ambientRenderStamp)
			{
				RenderInfiniteLightNode(worldContext, light, static_cast<Node *>(site));
			}

			bond = bond->GetNextOutgoingEdge();
		}
	}
}

void World::RenderInfiniteLightCell(const WorldContext *worldContext, const InfiniteLight *light, const Site *cell)
{
	const Bond *bond = cell->GetFirstOutgoingEdge();
	while (bond)
	{
		Site *site = bond->GetFinishElement();
		if (site->GetSiteStamp() == ambientRenderStamp)
		{
			if (site->GetCellIndex() < 0)
			{
				RenderInfiniteLightNode(worldContext, light, static_cast<Node *>(site));
			}
			else
			{
				RenderInfiniteLightCell(worldContext, light, site);
			}
		}

		bond = bond->GetNextOutgoingEdge();
	}
}

void World::RenderInfiniteLightEffectNode(const WorldContext *worldContext, const InfiniteLight *light, Effect *effect)
{
	unsigned_int32 lightStamp = lightRenderStamp;
	if (effect->GetLightStamp() != lightStamp)
	{
		effect->SetLightStamp(lightStamp);

		if ((effect->GetRenderStamp() == ambientRenderStamp) && (effect != light->GetExclusionNode()))
		{
			renderStageList[kRenderStageDefault].Append(effect);
		}
	}
}

void World::RenderInfiniteLightEffectCell(const WorldContext *worldContext, const InfiniteLight *light, const Site *cell)
{
	const Bond *bond = cell->GetFirstOutgoingEdge();
	while (bond)
	{
		Site *site = bond->GetFinishElement();
		if (site->GetSiteStamp() == ambientRenderStamp)
		{
			if (site->GetCellIndex() < 0)
			{
				RenderInfiniteLightEffectNode(worldContext, light, static_cast<Effect *>(site));
			}
			else
			{
				RenderInfiniteLightEffectCell(worldContext, light, site);
			}
		}

		bond = bond->GetNextOutgoingEdge();
	}
}

void World::RenderInfiniteLightZone(const WorldContext *worldContext, const InfiniteLight *light, const Zone *zone)
{
	const Bond *bond = zone->GetCellGraphSite(kCellGraphGeometry)->GetFirstOutgoingEdge();
	while (bond)
	{
		Site *site = bond->GetFinishElement();
		if (site->GetSiteStamp() == ambientRenderStamp)
		{
			if (site->GetCellIndex() < 0)
			{
				RenderInfiniteLightNode(worldContext, light, static_cast<Node *>(site));
			}
			else
			{
				RenderInfiniteLightCell(worldContext, light, site);
			}
		}

		bond = bond->GetNextOutgoingEdge();
	}

	bond = zone->GetCellGraphSite(kCellGraphEffect)->GetFirstOutgoingEdge();
	while (bond)
	{
		Site *site = bond->GetFinishElement();
		if (site->GetSiteStamp() == ambientRenderStamp)
		{
			if (site->GetCellIndex() < 0)
			{
				RenderInfiniteLightEffectNode(worldContext, light, static_cast<Effect *>(site));
			}
			else
			{
				RenderInfiniteLightEffectCell(worldContext, light, site);
			}
		}

		bond = bond->GetNextOutgoingEdge();
	}
}

void World::RenderInfiniteLight(const WorldContext *worldContext, InfiniteLight *light)
{
	List<Reference<LightRegion>>	regionList;

	worldContext->shadowInhibitMask = kGeometryShadowInhibit;

	RootLightRegion *rootLightRegion = light->GetFirstRootRegion();
	while (rootLightRegion)
	{
		rootLightRegion->SetLightRegionFlags(rootLightRegion->GetLightRegionFlags() & ~kLightRegionRenderShadowMap);
		rootLightRegion = rootLightRegion->Next();
	}

	// Collect all of the light regions that correspond to the light being rendered
	// and put their references into regionList. These references all get destroyed
	// by the time this function returns because we are finished with this light.

	Reference<LightRegion> *lightRegionReference = lightRegionList.First();
	while (lightRegionReference)
	{
		Reference<LightRegion> *next = lightRegionReference->Next();

		LightRegion *lightRegion = lightRegionReference->GetTarget();
		if (lightRegion->GetLight() == light)
		{
			regionList.Append(lightRegionReference);

			LightRegion *rootRegion = lightRegion->GetRootNode();
			rootRegion->SetLightRegionFlags(rootRegion->GetLightRegionFlags() | kLightRegionRenderShadowMap);
		}

		lightRegionReference = next;
	}

	const LightShadowData *shadowData = nullptr;

	LightType lightType = light->GetLightType();
	if (lightType == kLightDepth)
	{
		shadowData = RenderDepthLightShadowMap(worldContext, static_cast<DepthLight *>(light));
	}
	else if (lightType == kLightLandscape)
	{
		shadowData = RenderLandscapeLightShadowMap(worldContext, static_cast<LandscapeLight *>(light));
	}

	const InfiniteLightObject *lightObject = light->GetObject();
	TheGraphicsMgr->SetInfiniteLight(lightObject, light, shadowData);

	if (!(lightObject->GetLightFlags() & kLightShadowInhibit))
	{
		RenderInfiniteShadowVolumes(worldContext, light, &regionList);
	}

	for (;;)
	{
		lightRegionReference = regionList.First();
		if (!lightRegionReference)
		{
			break;
		}

		// Collect all of the light regions that belong to the same zone as the
		// first light region and put them in zoneRegionList. These references
		// get destroyed at the end of the for (;;) loop.

		List<Reference<LightRegion>>	zoneRegionList;

		zoneRegionList.Append(lightRegionReference);

		const LightRegion *lightRegion = lightRegionReference->GetTarget();
		int32 maxPlaneCount = lightRegion->GetPlaneCount();
		const Zone *zone = lightRegion->GetZone();

		lightRegionReference = regionList.First();
		while (lightRegionReference)
		{
			Reference<LightRegion> *next = lightRegionReference->Next();

			lightRegion = lightRegionReference->GetTarget();
			if (lightRegion->GetZone() == zone)
			{
				maxPlaneCount = Max(maxPlaneCount, lightRegion->GetPlaneCount());
				zoneRegionList.Append(lightRegionReference);
			}

			lightRegionReference = next;
		}

		if (maxPlaneCount == 0)
		{
			RenderInfiniteLightZone(worldContext, light, zone);
		}
		else
		{
			RenderInfiniteLightZone(worldContext, light, zone, &zoneRegionList);
		}
	}

	TheGraphicsMgr->GroupLightRenderList(&renderStageList[kRenderStageDefault]);

	ImpostorSystem *system = impostorSystemMap.First();
	while (system)
	{
		system->RenderSystem(&renderStageList[kRenderStageDefault]);
		system = system->Next();
	}

	TheGraphicsMgr->DrawRenderList(&renderStageList[kRenderStageDefault]);
	renderStageList[kRenderStageDefault].RemoveAll();
}

void World::CalculatePointNearClipRegion(const FrustumCamera *camera, const Point3D& lightPosition, Region *nearClipRegion)
{
	const FrustumCameraObject *cameraObject = camera->GetObject();
	Antivector4D *plane = nearClipRegion->GetPlaneArray();

	if ((cameraObject->GetCameraType() != kCameraRemote) || (!(cameraObject->GetFrustumFlags() & kFrustumOblique)))
	{
		const Point3D *vertex = camera->GetFrustumVertexArray();
		const Point3D& center = camera->GetNearPlaneCenter();

		Vector3D backNormal = Normalize(center - lightPosition);
		plane[0].Set(backNormal, lightPosition);

		const Transform4D& transform = camera->GetWorldTransform();
		Antivector4D nearPlane(transform[2], center);

		float lz = nearPlane ^ lightPosition;
		if (lz > kNearClipEpsilon)
		{
			nearClipRegion->SetPlaneCount(6);
			plane[1] = nearPlane;

			const Point3D *v1 = &vertex[3];
			for (machine a = 0; a < 4; a++)
			{
				const Point3D *v2 = &vertex[a];
				Vector3D normal = Normalize((*v2 - *v1) % (lightPosition - *v1));
				plane[a + 2].Set(normal, *v1);
				v1 = v2;
			}
		}
		else if (lz < -kNearClipEpsilon)
		{
			nearClipRegion->SetPlaneCount(6);
			plane[1] = -nearPlane;

			const Point3D *v1 = &vertex[3];
			for (machine a = 0; a < 4; a++)
			{
				const Point3D *v2 = &vertex[a];
				Vector3D normal = Normalize((*v1 - *v2) % (lightPosition - *v1));
				plane[a + 2].Set(normal, *v1);
				v1 = v2;
			}
		}
		else
		{
			nearClipRegion->SetPlaneCount(3);
			plane[1] = nearPlane;
			plane[2] = -nearPlane;
		}
	}
	else
	{
		const RemoteCamera *remoteCamera = static_cast<const RemoteCamera *>(camera);
		int32 vertexCount = remoteCamera->GetRemoteVertexCount();
		const Point3D *vertex = remoteCamera->GetRemoteVertexArray();
		const Point3D& center = remoteCamera->GetRemoteCenter();

		const RemoteCameraObject *remoteCameraObject = remoteCamera->GetObject();
		const Antivector4D& clipPlane = remoteCameraObject->GetRemoteClipPlane();
		float m = remoteCameraObject->GetRemoteDeterminant();

		Vector3D backNormal = Normalize(center - lightPosition);
		plane[0].Set(backNormal, lightPosition);

		float ld = clipPlane ^ lightPosition;
		if (ld > kNearClipEpsilon)
		{
			nearClipRegion->SetPlaneCount(vertexCount + 2);
			plane[1] = clipPlane;

			const Point3D *v1 = &vertex[vertexCount - 1];
			for (machine a = 0; a < vertexCount; a++)
			{
				const Point3D *v2 = &vertex[a];
				Vector3D normal = (*v2 - *v1) % (lightPosition - *v1);
				normal *= InverseMag(normal) * m;
				plane[a + 2].Set(normal, *v1);
				v1 = v2;
			}
		}
		else if (ld < -kNearClipEpsilon)
		{
			nearClipRegion->SetPlaneCount(vertexCount + 2);
			plane[1] = -clipPlane;

			const Point3D *v1 = &vertex[vertexCount - 1];
			for (machine a = 0; a < vertexCount; a++)
			{
				const Point3D *v2 = &vertex[a];
				Vector3D normal = (*v1 - *v2) % (lightPosition - *v1);
				normal *= InverseMag(normal) * m;
				plane[a + 2].Set(normal, *v1);
				v1 = v2;
			}
		}
		else
		{
			nearClipRegion->SetPlaneCount(3);
			plane[1] = clipPlane;
			plane[2] = -clipPlane;
		}
	}
}

void World::CalculatePointShadowRegion(const FrustumCamera *camera, const CameraRegion *cameraRegion, const Point3D& lightPosition, ShadowRegion *shadowRegion)
{
	int32 cameraPlaneCount = cameraRegion->GetPlaneCount();
	int32 nonlateralPlaneCount = cameraRegion->GetNonlateralPlaneCount();
	int32 lateralPlaneCount = cameraPlaneCount - nonlateralPlaneCount;

	const Antivector4D *cameraPlane = cameraRegion->GetPlaneArray();
	Antivector4D *shadowPlane = shadowRegion->GetPlaneArray();

	if (cameraRegion->ContainsPointLight(lightPosition))
	{
		shadowPlane[0].Set(camera->GetWorldTransform()[2], camera->GetWorldPosition());

		for (machine a = 0; a < lateralPlaneCount; a++)
		{
			shadowPlane[a + 1] = cameraPlane[a];
		}

		shadowRegion->SetPlaneCount(lateralPlaneCount + 1);
	}
	else
	{
		bool	frontArray[kMaxPortalVertexCount];

		for (machine a = 0; a < lateralPlaneCount; a++)
		{
			frontArray[a] = ((cameraPlane[a] ^ lightPosition) > 0.0F);
		}

		int32 shadowPlaneCount = 1;
		Antivector3D backPlaneNormal(0.0F, 0.0F, 0.0F);

		const Antivector4D *plane1 = &cameraPlane[lateralPlaneCount - 1];
		bool front1 = frontArray[lateralPlaneCount - 1];

		for (machine a = 0; a < lateralPlaneCount; a++)
		{
			const Antivector4D *plane2 = &cameraPlane[a];
			bool front2 = frontArray[a];

			if (front2)
			{
				shadowPlane[shadowPlaneCount++] = *plane2;

				if (!front1)
				{
					Antivector4D& newPlane = shadowPlane[shadowPlaneCount++];
					newPlane = Bivector4D(*plane1, *plane2) ^ lightPosition;
					newPlane.Standardize();
				}
			}
			else
			{
				backPlaneNormal += plane2->GetAntivector3D();

				if (front1)
				{
					Antivector4D& newPlane = shadowPlane[shadowPlaneCount++];
					newPlane = Bivector4D(*plane2, *plane1) ^ lightPosition;
					newPlane.Standardize();
				}
			}

			plane1 = plane2;
			front1 = front2;
		}

		backPlaneNormal.Normalize();
		shadowPlane[0].Set(backPlaneNormal, lightPosition);

		unsigned_int32 flags = cameraRegion->GetShadowRegionFlags();
		for (machine a = 0; a < nonlateralPlaneCount; a++)
		{
			if (flags & 1)
			{
				const Antivector4D& plane = cameraPlane[lateralPlaneCount + a];
				bool front = ((plane ^ lightPosition) > 0.0F);

				if (front)
				{
					shadowPlane[shadowPlaneCount] = plane;

					if (++shadowPlaneCount == kMaxRegionPlaneCount)
					{
						break;
					}

					for (machine b = 0; b < lateralPlaneCount; b++)
					{
						if (!frontArray[b])
						{
							Antivector4D& newPlane = shadowPlane[shadowPlaneCount];
							newPlane = Bivector4D(cameraPlane[b], plane) ^ lightPosition;
							newPlane.Standardize();

							if (++shadowPlaneCount == kMaxRegionPlaneCount)
							{
								goto full;
							}
						}
					}
				}
				else
				{
					for (machine b = 0; b < lateralPlaneCount; b++)
					{
						if (frontArray[b])
						{
							Antivector4D& newPlane = shadowPlane[shadowPlaneCount];
							newPlane = Bivector4D(plane, cameraPlane[b]) ^ lightPosition;
							newPlane.Standardize();

							if (++shadowPlaneCount == kMaxRegionPlaneCount)
							{
								goto full;
							}
						}
					}
				}
			}

			flags >>= 1;
		}

		full:
		shadowRegion->SetPlaneCount(shadowPlaneCount);
	}
}

void World::RenderPointShadowVolumeGeometry(PointLight *light, StencilMode stencilMode, Geometry *geometry)
{
	int32 detailLevel = geometry->GetDetailLevel();
	const GeometryObject *geometryObject = geometry->GetObject();
	const Mesh *mesh = geometryObject->GetGeometryLevel(detailLevel);

	int32 edgeCount = mesh->GetArrayDescriptor(kArrayEdge)->elementCount;
	if ((stencilMode == kStencilFail) || (edgeCount != 0))
	{
		StencilData		*stencilData;

		bool buildExtrusion = true;
		bool buildEndcaps = (stencilMode == kStencilFail);
		StencilVolume *stencilVolume = nullptr;

		unsigned_int32 lightFlags = light->GetObject()->GetLightFlags();
		if ((lightFlags & kLightStatic) && (!(geometryObject->GetGeometryFlags() & kGeometryDynamic)))
		{
			Link<StencilVolume> *stencilVolumeLink = geometry->GetStaticStencilVolume(light);
			if (stencilVolumeLink)
			{
				stencilVolume = *stencilVolumeLink;
				if (stencilVolume)
				{
					buildExtrusion = (stencilVolume->GetExtrusionDetailLevel() != detailLevel);
					if (buildEndcaps)
					{
						buildEndcaps = (stencilVolume->GetEndcapDetailLevel() != detailLevel);
					}
				}
				else
				{
					stencilVolume = new StencilVolume(geometry, light, stencilVolumeLink);
				}

				stencilData = stencilVolume;
			}
			else
			{
				stencilData = geometry->GetStencilData();
			}
		}
		else
		{
			stencilData = geometry->GetStencilData();
		}

		if (buildExtrusion)
		{
			stencilData->CalculatePointShadowBounds(light);
		}

		const Vector4D& objectLightPosition = TheGraphicsMgr->SetGeometryTransformable(geometry->GetTransformable());
		if (TheGraphicsMgr->ActivateShadowBounds(stencilData))
		{
			if (buildExtrusion)
			{
				geometry->CalculatePointShadowFrontArray(objectLightPosition.GetPoint3D(), geometryFrontArray);

				const Point3D *vertex = static_cast<Point3D *>(geometry->GetArrayBundle(kArrayPosition)->pointer);
				const Edge *edge = mesh->GetArray<Edge>(kArrayEdge);
				const bool *front = geometryFrontArray;

				VertexBuffer *vertexBuffer = stencilData->GetExtrusionVertexBuffer();
				volatile Vector4D *restrict extrusionVertex = vertexBuffer->BeginUpdate<Vector4D>();

				int32 extrusionEdgeCount = 0;
				for (machine a = 0; a < edgeCount; a++)
				{
					bool f1 = front[edge->faceIndex[0]];
					bool f2 = front[edge->faceIndex[1]];
					if (f1 ^ f2)
					{
						const Point3D& p1 = vertex[edge->vertexIndex[0]];
						const Point3D& p2 = vertex[edge->vertexIndex[1]];

						if (f1)
						{
							extrusionVertex[0] = p2;
							extrusionVertex[1] = p1;
							extrusionVertex[2].Set(p1.x, p1.y, p1.z, 0.0F);
							extrusionVertex[3].Set(p2.x, p2.y, p2.z, 0.0F);
						}
						else
						{
							extrusionVertex[0] = p1;
							extrusionVertex[1] = p2;
							extrusionVertex[2].Set(p2.x, p2.y, p2.z, 0.0F);
							extrusionVertex[3].Set(p1.x, p1.y, p1.z, 0.0F);
						}

						extrusionEdgeCount++;
						extrusionVertex += 4;
					}

					edge++;
				}

				stencilData->SetExtrusionEdgeCount(extrusionEdgeCount);
				vertexBuffer->EndUpdate();

				if (stencilVolume)
				{
					stencilVolume->SetExtrusionDetailLevel(detailLevel);
				}
			}
			else if (buildEndcaps)
			{
				geometry->CalculatePointShadowFrontArray(objectLightPosition.GetPoint3D(), geometryFrontArray);
			}

			if (stencilData->GetExtrusionEdgeCount() != 0)
			{
				TheGraphicsMgr->DrawStencilShadow(stencilData, kStencilPointExtrusion, stencilMode);

				if (stencilMode == kStencilFail)
				{
					if (buildEndcaps)
					{
						VertexBuffer *vertexBuffer = stencilData->GetEndcapIndexBuffer();
						volatile Triangle *restrict frontEndcapTriangleArray = vertexBuffer->BeginUpdate<Triangle>();

						int32 primitiveCount = mesh->GetPrimitiveCount();
						const Triangle *triangle = mesh->GetArray<Triangle>(kArrayPrimitive);
						const bool *front = geometryFrontArray;

						int32 frontEndcapTriangleCount = 0;
						int32 backEndcapTriangleCount = 0;
						volatile Triangle *restrict backEndcapTriangleArray = frontEndcapTriangleArray + primitiveCount;

						const unsigned_int16 *planeIndex = mesh->GetArray<unsigned_int16>(kArrayPlaneIndex);
						if (planeIndex)
						{
							for (machine a = 0; a < primitiveCount; a++)
							{
								if (front[*planeIndex])
								{
									frontEndcapTriangleArray[frontEndcapTriangleCount] = *triangle;
									frontEndcapTriangleCount++;
								}
								else
								{
									*--backEndcapTriangleArray = *triangle;
									backEndcapTriangleCount++;
								}

								triangle++;
								planeIndex++;
							}
						}
						else
						{
							for (machine a = 0; a < primitiveCount; a++)
							{
								if (front[a])
								{
									frontEndcapTriangleArray[frontEndcapTriangleCount] = *triangle;
									frontEndcapTriangleCount++;
								}
								else
								{
									*--backEndcapTriangleArray = *triangle;
									backEndcapTriangleCount++;
								}

								triangle++;
							}
						}

						stencilData->SetFrontEndcapTriangleCount(frontEndcapTriangleCount);
						stencilData->SetBackEndcapTriangleCount(backEndcapTriangleCount);
						vertexBuffer->EndUpdate();

						if (stencilVolume)
						{
							stencilVolume->SetEndcapDetailLevel(detailLevel);
						}
					}

					unsigned_int32 bufferIndex = ((geometry->GetVertexBufferArrayFlags() & (1 << kArrayPosition)) != 0);
					stencilData->SetGeometryVertexData(geometry->GetVertexCount(), geometry->GetVertexBuffer(bufferIndex), geometry->GetVertexAttributeOffset(kArrayPosition), geometry->GetVertexBufferStride(bufferIndex));

					if (stencilData->GetFrontEndcapTriangleCount() != 0)
					{
						TheGraphicsMgr->DrawStencilShadow(stencilData, kStencilEndcapIdentity, kStencilFail);
					}

					if (stencilData->GetBackEndcapTriangleCount() != 0)
					{
						TheGraphicsMgr->DrawStencilShadow(stencilData, kStencilEndcapProjection, kStencilFail);
					}
				}
			}
		}
	}
}

void World::RenderPointShadowVolumeNode(const WorldContext *worldContext, PointLight *light, const ShadowRenderData *renderData, Node *node)
{
	if ((node->Enabled()) && (((node->GetPerspectiveExclusionMask() >> kPerspectiveDirectShadowShift) & worldContext->perspectiveFlags) == 0) && (node->Visible(renderData->lightRegion)))
	{
		if (node->GetNodeType() == kNodeGeometry)
		{
			Geometry *geometry = static_cast<Geometry *>(node);

			unsigned_int32 shadowStamp = shadowRenderStamp;
			if (geometry->GetShadowStamp() != shadowStamp)
			{
				geometry->SetShadowStamp(shadowStamp);

				const GeometryObject *geometryObject = geometry->GetObject();
				if ((geometryObject->GetGeometryFlags() & worldContext->shadowInhibitMask) == 0)
				{
					const Point3D& lightPosition = light->GetWorldPosition();
					float lightRange = light->GetObject()->GetLightRange();

					const BoundingSphere *sphere = geometry->GetBoundingSphere();
					float radius = sphere->GetRadius();

					Vector3D axis = sphere->GetCenter() - lightPosition;
					float d = radius + lightRange;
					float dist2 = SquaredMag(axis);
					if (dist2 < d * d)
					{
						if (!geometryObject->ExteriorSphere(geometry->GetInverseWorldTransform() * lightPosition, lightRange))
						{
							if (ShadowNodeVisible(geometry, &renderData->shadowRegionList))
							{
								unsigned_int32 exclusionMask = geometry->GetPerspectiveExclusionMask();
								StencilMode stencilMode = ((exclusionMask & worldContext->perspectiveFlags) == 0) ? kStencilPass : kStencilFail;

								unsigned_int32 ambientStamp = ambientRenderStamp;
								if (geometry->GetProcessStamp() != ambientStamp)
								{
									geometry->SetProcessStamp(ambientStamp);

									if (exclusionMask & kPerspectivePrimary)
									{
										stencilMode = kStencilFail;
									}

									ProcessGeometry(worldContext, geometry);
									FinishWorldBatch();
								}

								if ((stencilMode == kStencilPass) && (geometry->Visible(&renderData->nearClipRegion)))
								{
									stencilMode = kStencilFail;
								}

								RenderPointShadowVolumeGeometry(light, stencilMode, geometry);
								worldCounter[kWorldCounterStencilShadow]++;
							}
						}
					}
				}
			}
		}

		const Bond *bond = node->GetFirstOutgoingEdge();
		while (bond)
		{
			Node *subnode = static_cast<Node *>(bond->GetFinishElement());
			if (subnode != renderData->excludeNode)
			{
				RenderPointShadowVolumeNode(worldContext, light, renderData, subnode);
			}

			bond = bond->GetNextOutgoingEdge();
		}
	}
}

void World::RenderPointShadowVolumeCell(const WorldContext *worldContext, PointLight *light, const ShadowRenderData *renderData, const Site *cell)
{
	if ((renderData->lightRegion->BoxVisible(cell->GetWorldBoundingBox())) && (ShadowCellVisible(cell, &renderData->shadowRegionList)))
	{
		const Bond *bond = cell->GetFirstOutgoingEdge();
		while (bond)
		{
			Site *site = bond->GetFinishElement();
			if (site->GetCellIndex() < 0)
			{
				Node *node = static_cast<Node *>(site);
				if (node != renderData->excludeNode)
				{
					RenderPointShadowVolumeNode(worldContext, light, renderData, node);
				}
			}
			else
			{
				RenderPointShadowVolumeCell(worldContext, light, renderData, site);
			}

			bond = bond->GetNextOutgoingEdge();
		}
	}
}

void World::RenderPointShadowVolumes(const WorldContext *worldContext, PointLight *light, List<Reference<LightRegion>> *regionList)
{
	ShadowRenderData	renderData;

	worldContext->shadowInhibitMask = kGeometryShadowInhibit;
	if (light->GetLightType() == kLightCube)
	{
		worldContext->shadowInhibitMask |= kGeometryCubeLightInhibit;
	}

	const Node *excludeNode = light->GetExclusionNode();
	renderData.excludeNode = excludeNode;

	const Point3D& lightPosition = light->GetWorldPosition();
	CalculatePointNearClipRegion(worldContext->renderCamera, lightPosition, &renderData.nearClipRegion);

	Reference<LightRegion> *lightRegionReference = regionList->First();
	while (lightRegionReference)
	{
		LightRegion *lightRegion = lightRegionReference->GetTarget();
		do
		{
			CameraRegion *cameraRegion = lightRegion->GetZone()->GetFirstCameraRegion();
			while (cameraRegion)
			{
				if (cameraRegion->GetCamera() == worldContext->renderCamera)
				{
					ShadowRegion *shadowRegion = cameraRegion->GetShadowRegion();
					renderData.shadowRegionList.Append(shadowRegion);

					if (shadowRegion->GetLight() != light)
					{
						shadowRegion->SetLight(light);
						CalculatePointShadowRegion(worldContext->renderCamera, cameraRegion, lightPosition, shadowRegion);

						#if C4DIAGNOSTICS

							if (diagnosticFlags & kDiagnosticShadowRegions)
							{
								shadowRegionDiagnosticList.Append(new RegionRenderable(shadowRegion, worldContext->renderCamera->GetWorldPosition(), kDiagnosticRegionSize));
							}

						#endif
					}
				}

				cameraRegion = cameraRegion->GetNextCameraRegion();
			}

			lightRegion = lightRegion->GetSuperNode();
		} while (lightRegion);

		lightRegionReference = lightRegionReference->Next();
	}

	TheGraphicsMgr->BeginStencilShadow();
	shadowRenderStamp++;

	lightRegionReference = regionList->First();
	while (lightRegionReference)
	{
		LightRegion *lightRegion = lightRegionReference->GetTarget();
		do
		{
			unsigned_int32 flags = lightRegion->GetLightRegionFlags();
			if (!(flags & kLightRegionShadowsRendered))
			{
				lightRegion->SetLightRegionFlags(flags | kLightRegionShadowsRendered);
				renderData.lightRegion = lightRegion;

				const Bond *bond = lightRegion->GetZone()->GetCellGraphSite(kCellGraphGeometry)->GetFirstOutgoingEdge();
				while (bond)
				{
					Site *site = bond->GetFinishElement();
					if (site->GetCellIndex() < 0)
					{
						Node *node = static_cast<Node *>(site);
						if (node != excludeNode)
						{
							RenderPointShadowVolumeNode(worldContext, light, &renderData, node);
						}
					}
					else
					{
						RenderPointShadowVolumeCell(worldContext, light, &renderData, site);
					}

					bond = bond->GetNextOutgoingEdge();
				}
			}

			lightRegion = lightRegion->GetSuperNode();
		} while (lightRegion);

		lightRegionReference = lightRegionReference->Next();
	}

	TheGraphicsMgr->EndStencilShadow();
	renderData.shadowRegionList.RemoveAll();
}

void World::RenderPointLightTerrain(const WorldContext *worldContext, const List<Reference<LightRegion>> *regionList, TerrainGeometry *terrain)
{
	unsigned_int32 lightStamp = lightRenderStamp;
	if (terrain->GetLightStamp() != lightStamp)
	{
		const TerrainGeometryObject *object = terrain->GetObject();
		int32 level = object->GetDetailLevel();

		if (level <= worldContext->cameraMinDetailLevel)
		{
			terrain->SetLightStamp(lightStamp);

			if ((object->GetGeometryFlags() & (kGeometryInvisible | kGeometryAmbientOnly | kGeometryRenderEffectPass)) == 0)
			{
				renderStageList[kRenderStageDefault].Append(terrain);
			}
		}
		else
		{
			const FrustumCamera *camera = worldContext->renderCamera;
			Vector3D direction = terrain->GetWorldCenter() - camera->GetWorldPosition();
			float d = Magnitude(direction) / camera->GetObject()->GetFocalLength();

			if ((d > terrain->GetRenderDistance()) && (camera->GetWorldTransform()[2] * direction > 0.0F))
			{
				terrain->SetLightStamp(lightStamp);

				if ((object->GetGeometryFlags() & (kGeometryInvisible | kGeometryAmbientOnly | kGeometryRenderEffectPass)) == 0)
				{
					renderStageList[kRenderStageDefault].Append(terrain);
				}
			}
			else
			{
				const Bond *bond = terrain->GetFirstOutgoingEdge();
				while (bond)
				{
					Node *node = static_cast<Node *>(bond->GetFinishElement());
					if ((node->GetSiteStamp() == ambientRenderStamp) && (LightNodeVisible(node, regionList)))
					{
						RenderPointLightTerrain(worldContext, regionList, static_cast<TerrainGeometry *>(node));
					}

					bond = bond->GetNextOutgoingEdge();
				}
			}
		}
	}
}

void World::RenderPointLightNode(const WorldContext *worldContext, const PointLight *light, const List<Reference<LightRegion>> *regionList, Node *node)
{
	if ((node != light->GetExclusionNode()) && (node->GetWorldBoundingBox().Intersection(light->GetWorldBoundingBox())) && (LightNodeVisible(node, regionList)))
	{
		NodeType type = node->GetNodeType();
		if (type == kNodeGeometry)
		{
			Geometry *geometry = static_cast<Geometry *>(node);
			if (geometry->GetGeometryType() != kGeometryTerrain)
			{
				unsigned_int32 lightStamp = lightRenderStamp;
				if (geometry->GetLightStamp() != lightStamp)
				{
					geometry->SetLightStamp(lightStamp);

					if (!geometry->GetObject()->ExteriorSphere(geometry->GetInverseWorldTransform() * light->GetWorldPosition(), light->GetObject()->GetLightRange()))
					{
						unsigned_int32 flags = geometry->GetObject()->GetGeometryFlags();
						if ((flags & (kGeometryInvisible | kGeometryAmbientOnly | kGeometryRenderEffectPass)) == 0)
						{
							renderStageList[kRenderStageDefault].Append(geometry);
						}
					}
				}
			}
			else
			{
				RenderPointLightTerrain(worldContext, regionList, static_cast<TerrainGeometry *>(geometry));
				return;
			}
		}
		else if (type == kNodeImpostor)
		{
			Impostor *impostor = static_cast<Impostor *>(node);

			float distance = SquaredMag(impostor->GetWorldPosition().GetVector2D() - worldContext->renderCamera->GetWorldPosition().GetVector2D());
			if (distance > impostor->GetSquaredRenderDistance())
			{
				unsigned_int32 lightStamp = lightRenderStamp;
				if (impostor->GetLightStamp() != lightStamp)
				{
					impostor->SetLightStamp(lightStamp);
					impostor->Render();
				}

				if (distance > impostor->GetSquaredGeometryDistance())
				{
					return;
				}
			}
		}

		const Bond *bond = node->GetFirstOutgoingEdge();
		while (bond)
		{
			Site *site = bond->GetFinishElement();
			if (site->GetSiteStamp() == ambientRenderStamp)
			{
				RenderPointLightNode(worldContext, light, regionList, static_cast<Node *>(site));
			}

			bond = bond->GetNextOutgoingEdge();
		}
	}
}

void World::RenderPointLightCell(const WorldContext *worldContext, const PointLight *light, const List<Reference<LightRegion>> *regionList, const Site *cell)
{
	const Box3D& box = cell->GetWorldBoundingBox();
	if (box.Intersection(light->GetWorldBoundingBox()))
	{
		const Reference<LightRegion> *lightRegion = regionList->First();
		while (lightRegion)
		{
			if (lightRegion->GetTarget()->BoxVisible(box))
			{
				const Bond *bond = cell->GetFirstOutgoingEdge();
				while (bond)
				{
					Site *site = bond->GetFinishElement();
					if (site->GetSiteStamp() == ambientRenderStamp)
					{
						if (site->GetCellIndex() < 0)
						{
							RenderPointLightNode(worldContext, light, regionList, static_cast<Node *>(site));
						}
						else
						{
							RenderPointLightCell(worldContext, light, regionList, site);
						}
					}

					bond = bond->GetNextOutgoingEdge();
				}

				break;
			}

			lightRegion = lightRegion->Next();
		}
	}
}

void World::RenderPointLightEffectNode(const WorldContext *worldContext, const PointLight *light, const List<Reference<LightRegion>> *regionList, Effect *effect)
{
	unsigned_int32 lightStamp = lightRenderStamp;
	if (effect->GetLightStamp() != lightStamp)
	{
		effect->SetLightStamp(lightStamp);

		if ((effect->GetRenderStamp() == ambientRenderStamp) && (effect != light->GetExclusionNode()) && (effect->GetWorldBoundingBox().Intersection(light->GetWorldBoundingBox())) && (LightNodeVisible(effect, regionList)))
		{
			renderStageList[kRenderStageDefault].Append(effect);
		}
	}
}

void World::RenderPointLightEffectCell(const WorldContext *worldContext, const PointLight *light, const List<Reference<LightRegion>> *regionList, const Site *cell)
{
	const Box3D& box = cell->GetWorldBoundingBox();
	if (box.Intersection(light->GetWorldBoundingBox()))
	{
		const Reference<LightRegion> *lightRegion = regionList->First();
		while (lightRegion)
		{
			if (lightRegion->GetTarget()->BoxVisible(box))
			{
				const Bond *bond = cell->GetFirstOutgoingEdge();
				while (bond)
				{
					Site *site = bond->GetFinishElement();
					if (site->GetSiteStamp() == ambientRenderStamp)
					{
						if (site->GetCellIndex() < 0)
						{
							RenderPointLightEffectNode(worldContext, light, regionList, static_cast<Effect *>(site));
						}
						else
						{
							RenderPointLightEffectCell(worldContext, light, regionList, site);
						}
					}

					bond = bond->GetNextOutgoingEdge();
				}

				break;
			}

			lightRegion = lightRegion->Next();
		}
	}
}

void World::RenderPointLightZone(const WorldContext *worldContext, const PointLight *light, const Zone *zone, const List<Reference<LightRegion>> *regionList)
{
	const Bond *bond = zone->GetCellGraphSite(kCellGraphGeometry)->GetFirstOutgoingEdge();
	while (bond)
	{
		Site *site = bond->GetFinishElement();
		if (site->GetSiteStamp() == ambientRenderStamp)
		{
			if (site->GetCellIndex() < 0)
			{
				RenderPointLightNode(worldContext, light, regionList, static_cast<Node *>(site));
			}
			else
			{
				RenderPointLightCell(worldContext, light, regionList, site);
			}
		}

		bond = bond->GetNextOutgoingEdge();
	}

	bond = zone->GetCellGraphSite(kCellGraphEffect)->GetFirstOutgoingEdge();
	while (bond)
	{
		Site *site = bond->GetFinishElement();
		if (site->GetSiteStamp() == ambientRenderStamp)
		{
			if (site->GetCellIndex() < 0)
			{
				RenderPointLightEffectNode(worldContext, light, regionList, static_cast<Effect *>(site));
			}
			else
			{
				RenderPointLightEffectCell(worldContext, light, regionList, site);
			}
		}

		bond = bond->GetNextOutgoingEdge();
	}
}

void World::RenderPointLightTerrain(const WorldContext *worldContext, TerrainGeometry *terrain)
{
	unsigned_int32 lightStamp = lightRenderStamp;
	if (terrain->GetLightStamp() != lightStamp)
	{
		const TerrainGeometryObject *object = terrain->GetObject();
		int32 level = object->GetDetailLevel();

		if (level <= worldContext->cameraMinDetailLevel)
		{
			terrain->SetLightStamp(lightStamp);

			if ((object->GetGeometryFlags() & (kGeometryInvisible | kGeometryAmbientOnly | kGeometryRenderEffectPass)) == 0)
			{
				renderStageList[kRenderStageDefault].Append(terrain);
			}
		}
		else
		{
			const FrustumCamera *camera = worldContext->renderCamera;
			Vector3D direction = terrain->GetWorldCenter() - camera->GetWorldPosition();
			float d = Magnitude(direction) / camera->GetObject()->GetFocalLength();

			if ((d > terrain->GetRenderDistance()) && (camera->GetWorldTransform()[2] * direction > 0.0F))
			{
				terrain->SetLightStamp(lightStamp);

				if ((object->GetGeometryFlags() & (kGeometryInvisible | kGeometryAmbientOnly | kGeometryRenderEffectPass)) == 0)
				{
					renderStageList[kRenderStageDefault].Append(terrain);
				}
			}
			else
			{
				const Bond *bond = terrain->GetFirstOutgoingEdge();
				while (bond)
				{
					Node *node = static_cast<Node *>(bond->GetFinishElement());
					if (node->GetSiteStamp() == ambientRenderStamp)
					{
						RenderPointLightTerrain(worldContext, static_cast<TerrainGeometry *>(node));
					}

					bond = bond->GetNextOutgoingEdge();
				}
			}
		}
	}
}

void World::RenderPointLightNode(const WorldContext *worldContext, const PointLight *light, Node *node)
{
	if ((node != light->GetExclusionNode()) && (node->GetWorldBoundingBox().Intersection(light->GetWorldBoundingBox())))
	{
		NodeType type = node->GetNodeType();
		if (type == kNodeGeometry)
		{
			Geometry *geometry = static_cast<Geometry *>(node);
			if (geometry->GetGeometryType() != kGeometryTerrain)
			{
				unsigned_int32 lightStamp = lightRenderStamp;
				if (geometry->GetLightStamp() != lightStamp)
				{
					geometry->SetLightStamp(lightStamp);

					if (!geometry->GetObject()->ExteriorSphere(geometry->GetInverseWorldTransform() * light->GetWorldPosition(), light->GetObject()->GetLightRange()))
					{
						unsigned_int32 flags = geometry->GetObject()->GetGeometryFlags();
						if ((flags & (kGeometryInvisible | kGeometryAmbientOnly | kGeometryRenderEffectPass)) == 0)
						{
							renderStageList[kRenderStageDefault].Append(geometry);
						}
					}
				}
			}
			else
			{
				RenderPointLightTerrain(worldContext, static_cast<TerrainGeometry *>(geometry));
				return;
			}
		}
		else if (type == kNodeImpostor)
		{
			Impostor *impostor = static_cast<Impostor *>(node);

			float distance = SquaredMag(impostor->GetWorldPosition().GetVector2D() - worldContext->renderCamera->GetWorldPosition().GetVector2D());
			if (distance > impostor->GetSquaredRenderDistance())
			{
				unsigned_int32 lightStamp = lightRenderStamp;
				if (impostor->GetLightStamp() != lightStamp)
				{
					impostor->SetLightStamp(lightStamp);
					impostor->Render();
				}

				if (distance > impostor->GetSquaredGeometryDistance())
				{
					return;
				}
			}
		}

		const Bond *bond = node->GetFirstOutgoingEdge();
		while (bond)
		{
			Site *site = bond->GetFinishElement();
			if (site->GetSiteStamp() == ambientRenderStamp)
			{
				RenderPointLightNode(worldContext, light, static_cast<Node *>(site));
			}

			bond = bond->GetNextOutgoingEdge();
		}
	}
}

void World::RenderPointLightCell(const WorldContext *worldContext, const PointLight *light, const Site *cell)
{
	const Box3D& box = cell->GetWorldBoundingBox();
	if (box.Intersection(light->GetWorldBoundingBox()))
	{
		const Bond *bond = cell->GetFirstOutgoingEdge();
		while (bond)
		{
			Site *site = bond->GetFinishElement();
			if (site->GetSiteStamp() == ambientRenderStamp)
			{
				if (site->GetCellIndex() < 0)
				{
					RenderPointLightNode(worldContext, light, static_cast<Node *>(site));
				}
				else
				{
					RenderPointLightCell(worldContext, light, site);
				}
			}

			bond = bond->GetNextOutgoingEdge();
		}
	}
}

void World::RenderPointLightEffectNode(const WorldContext *worldContext, const PointLight *light, Effect *effect)
{
	unsigned_int32 lightStamp = lightRenderStamp;
	if (effect->GetLightStamp() != lightStamp)
	{
		effect->SetLightStamp(lightStamp);

		if ((effect->GetRenderStamp() == ambientRenderStamp) && (effect != light->GetExclusionNode()) && (effect->GetWorldBoundingBox().Intersection(light->GetWorldBoundingBox())))
		{
			renderStageList[kRenderStageDefault].Append(effect);
		}
	}
}

void World::RenderPointLightEffectCell(const WorldContext *worldContext, const PointLight *light, const Site *cell)
{
	const Box3D& box = cell->GetWorldBoundingBox();
	if (box.Intersection(light->GetWorldBoundingBox()))
	{
		const Bond *bond = cell->GetFirstOutgoingEdge();
		while (bond)
		{
			Site *site = bond->GetFinishElement();
			if (site->GetSiteStamp() == ambientRenderStamp)
			{
				if (site->GetCellIndex() < 0)
				{
					RenderPointLightEffectNode(worldContext, light, static_cast<Effect *>(site));
				}
				else
				{
					RenderPointLightEffectCell(worldContext, light, site);
				}
			}

			bond = bond->GetNextOutgoingEdge();
		}
	}
}

void World::RenderPointLightZone(const WorldContext *worldContext, const PointLight *light, const Zone *zone)
{
	const Bond *bond = zone->GetCellGraphSite(kCellGraphGeometry)->GetFirstOutgoingEdge();
	while (bond)
	{
		Site *site = bond->GetFinishElement();
		if (site->GetSiteStamp() == ambientRenderStamp)
		{
			if (site->GetCellIndex() < 0)
			{
				RenderPointLightNode(worldContext, light, static_cast<Node *>(site));
			}
			else
			{
				RenderPointLightCell(worldContext, light, site);
			}
		}

		bond = bond->GetNextOutgoingEdge();
	}

	bond = zone->GetCellGraphSite(kCellGraphEffect)->GetFirstOutgoingEdge();
	while (bond)
	{
		Site *site = bond->GetFinishElement();
		if (site->GetSiteStamp() == ambientRenderStamp)
		{
			if (site->GetCellIndex() < 0)
			{
				RenderPointLightEffectNode(worldContext, light, static_cast<Effect *>(site));
			}
			else
			{
				RenderPointLightEffectCell(worldContext, light, site);
			}
		}

		bond = bond->GetNextOutgoingEdge();
	}
}

void World::RenderPointLight(const WorldContext *worldContext, PointLight *light)
{
	List<Reference<LightRegion>>	regionList;

	// Collect all of the light regions that correspond to the light being rendered
	// and put their references into regionList. These references all get destroyed
	// by the time this function returns because we are finished with this light.

	Reference<LightRegion> *lightRegionReference = lightRegionList.First();
	while (lightRegionReference)
	{
		Reference<LightRegion> *next = lightRegionReference->Next();

		if (lightRegionReference->GetTarget()->GetLight() == light)
		{
			regionList.Append(lightRegionReference);
		}

		lightRegionReference = next;
	}

	// The GraphicsMgr::SetPointLight() function returns false if the projection of
	// the point light does not intersect the current viewport rectangle, in which
	// case we skip the light altogether.

	const PointLightObject *lightObject = light->GetObject();
	if (TheGraphicsMgr->SetPointLight(lightObject, light))
	{
		if (!(lightObject->GetLightFlags() & kLightShadowInhibit))
		{
			RenderPointShadowVolumes(worldContext, light, &regionList);
		}

		for (;;)
		{
			lightRegionReference = regionList.First();
			if (!lightRegionReference)
			{
				break;
			}

			// Collect all of the light regions that belong to the same zone as the
			// first light region and put them in zoneRegionList. These references
			// get destroyed at the end of the for (;;) loop.

			List<Reference<LightRegion>>	zoneRegionList;

			zoneRegionList.Append(lightRegionReference);

			const LightRegion *lightRegion = lightRegionReference->GetTarget();
			int32 maxPlaneCount = lightRegion->GetPlaneCount();
			const Zone *zone = lightRegion->GetZone();

			lightRegionReference = regionList.First();
			while (lightRegionReference)
			{
				Reference<LightRegion> *next = lightRegionReference->Next();

				lightRegion = lightRegionReference->GetTarget();
				if (lightRegion->GetZone() == zone)
				{
					maxPlaneCount = Max(maxPlaneCount, lightRegion->GetPlaneCount());
					zoneRegionList.Append(lightRegionReference);
				}

				lightRegionReference = next;
			}

			if (maxPlaneCount == 0)
			{
				RenderPointLightZone(worldContext, light, zone);
			}
			else
			{
				RenderPointLightZone(worldContext, light, zone, &zoneRegionList);
			}
		}

		TheGraphicsMgr->GroupLightRenderList(&renderStageList[kRenderStageDefault]);

		ImpostorSystem *system = impostorSystemMap.First();
		while (system)
		{
			system->RenderSystem(&renderStageList[kRenderStageDefault]);
			system = system->Next();
		}

		TheGraphicsMgr->DrawRenderList(&renderStageList[kRenderStageDefault]);
		renderStageList[kRenderStageDefault].RemoveAll();
	}
}

void World::RenderShadowMapTerrain(const WorldContext *worldContext, TerrainGeometry *terrain, const Region *cameraRegion, const Region *shadowRegion, const List<Region> *occlusionList)
{
	const TerrainGeometryObject *object = terrain->GetObject();
	int32 level = object->GetDetailLevel();

	if (level <= worldContext->cameraMinDetailLevel)
	{
		unsigned_int32 shadowStamp = shadowRenderStamp;
		if (terrain->GetShadowStamp() != shadowStamp)
		{
			terrain->SetShadowStamp(shadowStamp);

			if (object->GetGeometryFlags() & kGeometryRenderShadowMap)
			{
				renderStageList[kRenderStageDefault].Append(terrain);
				worldCounter[kWorldCounterDepthShadow]++;

				if (level != 0)
				{
					TerrainLevelGeometry *terrainLevel = static_cast<TerrainLevelGeometry *>(terrain);
					if (!terrainLevel->Rendering())
					{
						terrainLevel->UpdateBorderState();
					}
				}
			}
		}
	}
	else
	{
		const FrustumCamera *camera = worldContext->renderCamera;
		Vector3D direction = terrain->GetWorldCenter() - camera->GetWorldPosition();
		float d = Magnitude(direction)  / camera->GetObject()->GetFocalLength();

		if ((d > terrain->GetRenderDistance()) && (camera->GetWorldTransform()[2] * direction > 0.0F))
		{
			unsigned_int32 shadowStamp = shadowRenderStamp;
			if (terrain->GetShadowStamp() != shadowStamp)
			{
				terrain->SetShadowStamp(shadowStamp);

				if (object->GetGeometryFlags() & kGeometryRenderShadowMap)
				{
					renderStageList[kRenderStageDefault].Append(terrain);
					worldCounter[kWorldCounterDepthShadow]++;

					TerrainLevelGeometry *terrainLevel = static_cast<TerrainLevelGeometry *>(terrain);
					if (!terrainLevel->Rendering())
					{
						terrainLevel->UpdateBorderState();
					}
				}
			}
		}
		else
		{
			const Bond *bond = terrain->GetFirstOutgoingEdge();
			while (bond)
			{
				TerrainGeometry *subterrain = static_cast<TerrainGeometry *>(bond->GetFinishElement());
				if ((subterrain->Enabled()) && (((subterrain->GetPerspectiveExclusionMask() >> kPerspectiveDirectShadowShift) & worldContext->perspectiveFlags) == 0) && (subterrain->Visible(cameraRegion)) && (subterrain->Visible(shadowRegion, occlusionList)))
				{
					RenderShadowMapTerrain(worldContext, subterrain, cameraRegion, shadowRegion, occlusionList);
				}

				bond = bond->GetNextOutgoingEdge();
			}
		}
	}
}

void World::RenderShadowMapNode(const WorldContext *worldContext, Node *node, const Region *cameraRegion, const Region *shadowRegion, const List<Region> *occlusionList)
{
	if ((node->Enabled()) && (((node->GetPerspectiveExclusionMask() >> kPerspectiveDirectShadowShift) & worldContext->perspectiveFlags) == 0) && (node->Visible(cameraRegion)) && (node->Visible(shadowRegion, occlusionList)))
	{
		NodeType type = node->GetNodeType();
		if (type == kNodeGeometry)
		{
			Geometry *geometry = static_cast<Geometry *>(node);

			if (geometry->GetGeometryType() != kGeometryTerrain)
			{
				unsigned_int32 shadowStamp = shadowRenderStamp;
				if (geometry->GetShadowStamp() != shadowStamp)
				{
					geometry->SetShadowStamp(shadowStamp);

					if (geometry->GetObject()->GetGeometryFlags() & kGeometryRenderShadowMap)
					{
						unsigned_int32 ambientStamp = ambientRenderStamp;
						if (geometry->GetProcessStamp() != ambientStamp)
						{
							geometry->SetProcessStamp(ambientStamp);
							ProcessGeometry(worldContext, geometry);
						}

						renderStageList[kRenderStageDefault].Append(geometry);
						worldCounter[kWorldCounterDepthShadow]++;
					}
				}
			}
			else
			{
				RenderShadowMapTerrain(worldContext, static_cast<TerrainGeometry *>(geometry), cameraRegion, shadowRegion, occlusionList);
				return;
			}
		}
		else if (type == kNodeImpostor)
		{
			Impostor *impostor = static_cast<Impostor *>(node);

			float distance = SquaredMag(impostor->GetWorldPosition().GetVector2D() - worldContext->renderCamera->GetWorldPosition().GetVector2D());
			if (distance > impostor->GetSquaredRenderDistance())
			{
				unsigned_int32 shadowStamp = shadowRenderStamp;
				if (impostor->GetShadowStamp() != shadowStamp)
				{
					impostor->SetShadowStamp(shadowStamp);
					impostor->Render();
				}

				if (distance > impostor->GetSquaredGeometryDistance())
				{
					return;
				}
			}
		}

		const Bond *bond = node->GetFirstOutgoingEdge();
		while (bond)
		{
			RenderShadowMapNode(worldContext, static_cast<Node *>(bond->GetFinishElement()), cameraRegion, shadowRegion, occlusionList);
			bond = bond->GetNextOutgoingEdge();
		}
	}
}

void World::RenderShadowMapCell(const WorldContext *worldContext, const Site *cell, const Region *cameraRegion, const Region *shadowRegion, const List<Region> *occlusionList)
{
	const Box3D& box = cell->GetWorldBoundingBox();
	if ((WorldBoundingBoxVisible(box, cameraRegion, occlusionList))  && (shadowRegion->BoxVisible(box)))
	{
		const Bond *bond = cell->GetFirstOutgoingEdge();
		while (bond)
		{
			Site *site = bond->GetFinishElement();
			if (site->GetCellIndex() < 0)
			{
				RenderShadowMapNode(worldContext, static_cast<Node *>(site), cameraRegion, shadowRegion, occlusionList);
			}
			else
			{
				RenderShadowMapCell(worldContext, site, cameraRegion, shadowRegion, occlusionList);
			}

			bond = bond->GetNextOutgoingEdge();
		}
	}
}

void World::RenderShadowMapRegion(const WorldContext *worldContext, const CameraRegion *cameraRegion, const Region *shadowRegion, const List<Region> *occlusionList)
{
	const Zone *zone = cameraRegion->GetZone();
	if ((zone->Visible(cameraRegion)) && (zone->Visible(shadowRegion, occlusionList)))
	{
		const Bond *bond = zone->GetCellGraphSite(kCellGraphGeometry)->GetFirstOutgoingEdge();
		while (bond)
		{
			Site *site = bond->GetFinishElement();
			if (site->GetCellIndex() < 0)
			{
				RenderShadowMapNode(worldContext, static_cast<Node *>(site), cameraRegion, shadowRegion, occlusionList);
			}
			else
			{
				RenderShadowMapCell(worldContext, site, cameraRegion, shadowRegion, occlusionList);
			}

			bond = bond->GetNextOutgoingEdge();
		}
	}

	const CameraRegion *subregion = cameraRegion->GetFirstSubnode();
	while (subregion)
	{
		RenderShadowMapRegion(worldContext, subregion, shadowRegion, occlusionList);
		subregion = subregion->Tree<CameraRegion>::Next();
	}
}

void World::ProcessShadowMapRegion(const WorldContext *worldContext, const OrthoCamera *camera, CameraRegion *rootRegion, const Region *shadowRegion, List<Region> *occlusionList)
{
	const Point3D& cameraPosition = camera->GetWorldPosition();

	Zone *zone = rootRegion->GetZone();
	zone->SetTraversalExclusionMask(zone->GetTraversalExclusionMask() | kZoneTraversalLocal);

	const Portal *portal = zone->GetFirstPortal();
	while (portal)
	{
		if ((portal->Enabled()) && (portal->GetPortalType() == kPortalDirect))
		{
			const PortalObject *object = portal->GetObject();
			if (!(object->GetPortalFlags() & kPortalShadowMapInhibit))
			{
				Zone *connectedZone = portal->GetConnectedZone();
				if ((connectedZone) && (connectedZone->GetTraversalExclusionMask() == 0))
				{
					const Antivector4D& portalPlane = portal->GetWorldPlane();
					float distance = portalPlane ^ cameraPosition;
					if ((!(distance < 0.0F)) && ((portalPlane ^ camera->GetWorldTransform()[2]) < 0.0F))
					{
						const BoundingSphere *sphere = portal->GetBoundingSphere();
						if ((rootRegion->SphereVisible(sphere->GetCenter(), sphere->GetRadius())) && (shadowRegion->SphereVisible(sphere->GetCenter(), sphere->GetRadius())))
						{
							Point3D		temp[2][kMaxPortalVertexCount];

							int32 vertexCount = object->GetVertexCount();
							const Point3D *vertex = portal->GetWorldVertexArray();

							int32 count = Min(rootRegion->GetPlaneCount(), kMaxPortalVertexCount - vertexCount);
							const Antivector4D *plane = rootRegion->GetPlaneArray();
							for (machine a = 0; a < count; a++)
							{
								int8	location[kMaxPortalVertexCount];

								Point3D *result = temp[a & 1];
								vertexCount = Math::ClipPolygonAgainstPlane(vertexCount, vertex, plane[a], location, result);
								if (vertexCount == 0)
								{
									goto nextPortal;
								}

								vertex = result;
							}

							CameraRegion *newRegion = new CameraRegion(camera, connectedZone);
							newRegion->SetOrthoPortalPlanes(vertexCount, vertex, rootRegion);

							rootRegion->AddSubnode(newRegion);
							ProcessShadowMapRegion(worldContext, camera, newRegion, shadowRegion, occlusionList);
						}
					}
				}
			}
		}

		nextPortal:
		portal = portal->Next();
	}

	const Portal *occlusionPortal = zone->GetFirstOcclusionPortal();
	while (occlusionPortal)
	{
		if ((occlusionPortal->Enabled()) && ((occlusionPortal->GetPerspectiveExclusionMask() & worldContext->perspectiveFlags) == 0))
		{
			const BoundingSphere *sphere = occlusionPortal->GetBoundingSphere();
			if ((rootRegion->SphereVisible(sphere->GetCenter(), sphere->GetRadius())) && (shadowRegion->SphereVisible(sphere->GetCenter(), sphere->GetRadius())))
			{
				const Antivector4D& portalPlane = occlusionPortal->GetWorldPlane();
				float distance = portalPlane ^ camera->GetWorldPosition();
				if ((distance > 0.0F) && ((portalPlane ^ camera->GetWorldTransform()[2]) < 0.0F))
				{
					CameraRegion *newRegion = new CameraRegion(camera, zone);
					newRegion->SetOrthoPortalPlanes(occlusionPortal->GetObject()->GetVertexCount(), occlusionPortal->GetWorldVertexArray(), portalPlane);
					occlusionList->Append(newRegion);
				}
			}
		}

		occlusionPortal = occlusionPortal->Next();
	}

	const OcclusionSpace *occlusionSpace = zone->GetFirstOcclusionSpace();
	while (occlusionSpace)
	{
		if ((occlusionSpace->Enabled()) && (occlusionSpace->Visible(rootRegion, occlusionList)) && (occlusionSpace->Visible(shadowRegion, occlusionList)))
		{
			CameraRegion *newRegion = occlusionSpace->CalculateOrthoOcclusionRegion(camera, zone);
			if (newRegion)
			{
				occlusionList->Append(newRegion);
			}
		}

		occlusionSpace = occlusionSpace->Next();
	}

	zone->SetTraversalExclusionMask(zone->GetTraversalExclusionMask() & ~kZoneTraversalLocal);
}

void World::RenderShadowMap(const WorldContext *worldContext, DepthLight *depthLight, int32 cascadeIndex, const LightShadowData *shadowData, const Region *shadowRegion)
{
	OrthoCamera		orthoCamera;

	shadowRenderStamp++;

	orthoCamera.SetWorld(this);
	OrthoCameraObject *cameraObject = orthoCamera.GetObject();

	int32 shadowMapSize = TheGraphicsMgr->GetDynamicShadowMapSize();
	int32 y = shadowMapSize * (kMaxShadowCascadeCount - 1 - cascadeIndex);

	cameraObject->SetViewRect(Rect(0, y, shadowMapSize, y + shadowMapSize));
	cameraObject->SetNearDepth(0.0F);
	cameraObject->SetFarDepth(shadowData->shadowSize.z);

	float w = shadowData->shadowSize.x * 0.5F;
	float h = shadowData->shadowSize.y * 0.5F;
	cameraObject->SetOrthoRect(-w, w, -h, h);

	const Transform4D& transform = depthLight->GetWorldTransform();
	Point3D position(shadowData->shadowPosition.x, shadowData->shadowPosition.y, shadowData->shadowPosition.z + shadowData->texelSize * 2.0F);
	orthoCamera.SetNodeTransform(-transform[0], transform[1], -transform[2], transform * position);
	orthoCamera.Update();

	TheGraphicsMgr->SetCamera(cameraObject, &orthoCamera, 0, false);

	const RootLightRegion *rootLightRegion = depthLight->GetFirstRootRegion();
	while (rootLightRegion)
	{
		if (rootLightRegion->GetLightRegionFlags() & kLightRegionRenderShadowMap)
		{
			Zone *zone = rootLightRegion->GetZone();
			orthoCamera.AddRootRegion(new RootCameraRegion(&orthoCamera, zone));
			zone->SetTraversalExclusionMask(kZoneTraversalGlobal);
		}

		rootLightRegion = rootLightRegion->Next();
	}

	RootCameraRegion *rootCameraRegion = orthoCamera.GetFirstRootRegion();
	while (rootCameraRegion)
	{
		List<Region>	occlusionList;

		orthoCamera.CalculateOrthoCameraRegion(rootCameraRegion);

		ProcessShadowMapRegion(worldContext, &orthoCamera, rootCameraRegion, shadowRegion, &occlusionList);
		RenderShadowMapRegion(worldContext, rootCameraRegion, shadowRegion, &occlusionList);

		rootCameraRegion = rootCameraRegion->Next();
	}

	rootCameraRegion = orthoCamera.GetFirstRootRegion();
	while (rootCameraRegion)
	{
		rootCameraRegion->GetZone()->SetTraversalExclusionMask(0);
		rootCameraRegion = rootCameraRegion->Next();
	}

	ImpostorSystem *system = impostorSystemMap.First();
	while (system)
	{
		system->RenderSystem(&renderStageList[kRenderStageDefault]);
		system = system->Next();
	}

	FinishWorldBatch();

	TheGraphicsMgr->DrawShadowMapList(&renderStageList[kRenderStageDefault]);
	renderStageList[kRenderStageDefault].RemoveAll();
}

const LightShadowData *World::RenderDepthLightShadowMap(const WorldContext *worldContext, DepthLight *light)
{
	worldContext->shadowInhibitMask |= kGeometryRenderShadowMap;

	const LightShadowData *shadowData = light->CalculateShadowData(worldContext->renderCamera);

	if (!worldContext->shadowMapKeepFlag)
	{
		ShadowRegion	shadowRegion;

		TheGraphicsMgr->BeginShadowMap();

		const CameraRegion *receiveRegion = worldContext->shadowReceiveRegion;
		int32 nonlateralPlaneCount = receiveRegion->GetNonlateralPlaneCount();
		int32 planeCount = receiveRegion->GetPlaneCount() - nonlateralPlaneCount;
		nonlateralPlaneCount = Min(nonlateralPlaneCount, 1);	// Only keep near plane

		CameraRegion cascadeRegion(worldContext->renderCamera, nullptr);
		const Antivector4D *planeArray = receiveRegion->GetPlaneArray();
		Antivector4D *cascadePlaneArray = cascadeRegion.GetPlaneArray();
		for (machine b = 0; b < planeCount; b++)
		{
			*cascadePlaneArray++ = planeArray[b];
		}

		unsigned_int32 shadowRegionFlags = receiveRegion->GetShadowRegionFlags() & nonlateralPlaneCount;
		int32 shadowNonlateralPlaneCount = nonlateralPlaneCount;

		const ShadowSpace *shadowSpace = light->GetConnectedShadowSpace();
		if (shadowSpace)
		{
			*cascadePlaneArray++ = shadowSpace->GetInverseWorldTransform().GetRow(2);
			shadowRegionFlags = (shadowRegionFlags << 1) | 0x01;
			shadowNonlateralPlaneCount++;
		}

		planeArray += planeCount;
		for (machine b = 0; b < nonlateralPlaneCount; b++)
		{
			*cascadePlaneArray++ = planeArray[b];
		}

		cascadeRegion.SetPlaneCount(planeCount + shadowNonlateralPlaneCount);
		cascadeRegion.SetNonlateralPlaneCount(shadowNonlateralPlaneCount);
		cascadeRegion.SetShadowRegionFlags(shadowRegionFlags);

		CalculateInfiniteShadowRegion(worldContext->renderCamera, &cascadeRegion, light->GetWorldTransform()[2], &shadowRegion);
		RenderShadowMap(worldContext, light, 0, shadowData, &shadowRegion);
		worldCounter[kWorldCounterShadowCascade]++;

		TheGraphicsMgr->EndShadowMap();
	}

	return (shadowData);
}

const LightShadowData *World::RenderLandscapeLightShadowMap(const WorldContext *worldContext, LandscapeLight *light)
{
	worldContext->shadowInhibitMask |= kGeometryRenderShadowMap;

	const LightShadowData *shadowData = light->CalculateShadowData(worldContext->renderCamera);

	if (!worldContext->shadowMapKeepFlag)
	{
		ShadowRegion	shadowRegion;

		TheGraphicsMgr->BeginShadowMap();

		const CameraRegion *receiveRegion = worldContext->shadowReceiveRegion;
		int32 planeCount = receiveRegion->GetPlaneCount() - receiveRegion->GetNonlateralPlaneCount();
		const Antivector4D *planeArray = receiveRegion->GetPlaneArray();

		const ShadowSpace *shadowSpace = light->GetConnectedShadowSpace();

		for (machine a = 0; a < kMaxShadowCascadeCount; a++)
		{
			if (a > 0)
			{
				const Region *region = worldContext->occlusionList.First();
				while (region)
				{
					for (;;)
					{
						if (region->PolygonOccluded(4, shadowData[a].cascadePolygon))
						{
							if (++a < kMaxShadowCascadeCount)
							{
								region = worldContext->occlusionList.First();
								continue;
							}

							goto end;
						}

						break;
					}

					region = region->Next();
				}
			}

			CameraRegion cascadeRegion(worldContext->renderCamera, nullptr);
			Antivector4D *cascadePlaneArray = cascadeRegion.GetPlaneArray();
			for (machine b = 0; b < planeCount; b++)
			{
				*cascadePlaneArray++ = planeArray[b];
			}

			int32 nonlateralPlaneCount = 1 + (a < kMaxShadowCascadeCount - 1);

			if (shadowSpace)
			{
				*cascadePlaneArray++ = shadowSpace->GetInverseWorldTransform().GetRow(2);
				nonlateralPlaneCount++;
			}

			*cascadePlaneArray++ = shadowData[a].nearPlane;
			*cascadePlaneArray++ = shadowData[a].farPlane;

			cascadeRegion.SetPlaneCount(planeCount + nonlateralPlaneCount);
			cascadeRegion.SetNonlateralPlaneCount(nonlateralPlaneCount);
			cascadeRegion.SetShadowRegionFlags(~0);

			CalculateInfiniteShadowRegion(worldContext->renderCamera, &cascadeRegion, light->GetWorldTransform()[2], &shadowRegion);
			RenderShadowMap(worldContext, light, a, &shadowData[a], &shadowRegion);
			worldCounter[kWorldCounterShadowCascade]++;
		}

		end:
		TheGraphicsMgr->EndShadowMap();
	}

	return (shadowData);
}

bool World::PointLightVisible(const Light *light, const CameraRegion *cameraRegion, const List<Region> *occlusionList)
{
	const PointLightObject *object = static_cast<const PointLightObject *>(light->GetObject());
	const Point3D& center = light->GetWorldPosition();
	float radius = object->GetLightRange();

	if (!cameraRegion->SphereVisible(center, radius))
	{
		return (false);
	}

	const Region *region = occlusionList->First();
	while (region)
	{
		if (region->SphereOccluded(center, radius))
		{
			return (false);
		}

		region = region->Next();
	}

	return (true);
}

bool World::LightVisibleInTransition(const CameraRegion *cameraRegion, const LightRegion *lightRegion)
{
	const CameraRegion *cameraSuperRegion = cameraRegion->GetSuperNode();
	if (cameraSuperRegion)
	{
		const LightRegion *lightSuperRegion = lightRegion->GetSuperNode();
		if (lightSuperRegion)
		{
			const Zone *lightSuperZone = lightSuperRegion->GetZone();
			const Zone *cameraSuperZone = cameraSuperRegion->GetZone();

			const Portal *portal = cameraRegion->GetZone()->GetFirstPortal();
			while (portal)
			{
				if (portal->GetConnectedZone() == lightSuperZone)
				{
					if ((!portal->Enabled()) && (cameraSuperZone != lightSuperZone))
					{
						return (false);
					}

					break;
				}

				portal = portal->Next();
			}
		}
	}

	return (true);
}

bool World::ClipLightRegion(const WorldContext *worldContext, const LightRegion *lightRegion, const CameraRegion *cameraRegion)
{
	int32 polygonCount = lightRegion->GetBoundaryPolygonCount();
	if (polygonCount < 4)
	{
		return (true);
	}

	if ((!cameraRegion->GetSuperNode()) && (lightRegion->SphereVisible(worldContext->renderCamera->GetWorldPosition(), kCameraLightClipEpsilon)))
	{
		return (true);
	}

	const Point3D *polygonVertex = lightRegion->GetBoundaryPolygonVertexArray();

	int32 planeCount = cameraRegion->GetPlaneCount();
	const Antivector4D *plane = cameraRegion->GetPlaneArray();

	for (machine polygonIndex = 0; polygonIndex < polygonCount; polygonIndex++)
	{
		Point3D		temp[2][kMaxPortalVertexCount + kMaxRegionPlaneCount];

		int32 vertexCount = lightRegion->GetBoundaryPolygonVertexCount(polygonIndex);
		const Point3D *vertex = polygonVertex;
		polygonVertex += vertexCount;

		for (machine planeIndex = 0; planeIndex < planeCount; planeIndex++)
		{
			int8	location[kMaxPortalVertexCount + kMaxRegionPlaneCount];

			Point3D *result = temp[planeIndex & 1];
			vertexCount = Math::ClipPolygonAgainstPlane(vertexCount, vertex, plane[planeIndex], location, result);
			if (vertexCount == 0)
			{
				goto nextPolygon;
			}

			vertex = result;
		}

		return (true);

		nextPolygon:;
	}

	return (false);
}

void World::CollectLightRegions(const WorldContext *worldContext, const CameraRegion *cameraRegion)
{
	const Zone *zone = cameraRegion->GetZone();

	LightRegion *lightRegion = zone->GetFirstLightRegion();
	while (lightRegion)
	{
		const Light *light = lightRegion->GetLight();
		if ((light->Enabled()) && ((light->GetPerspectiveExclusionMask() & worldContext->perspectiveFlags) == 0))
		{
			if ((light->GetBaseLightType() == kLightInfinite) || (PointLightVisible(light, cameraRegion, &worldContext->occlusionList)))
			{
				unsigned_int32 flags = lightRegion->GetLightRegionFlags();
				if (!(flags & kLightRegionBoundaryCalculated))
				{
					lightRegion->SetLightRegionFlags(flags | kLightRegionBoundaryCalculated);
					light->CalculateBoundaryPolygons(lightRegion);
				}

				if (ClipLightRegion(worldContext, lightRegion, cameraRegion))
				{
					LightRegion *region = lightRegion;
					do
					{
						region->SetLightRegionFlags(region->GetLightRegionFlags() & ~kLightRegionShadowsRendered);
						region = region->GetSuperNode();
					} while (region);

					lightRegionList.Append(new RegionReference<LightRegion>(lightRegion));

					#if C4DIAGNOSTICS

						if (diagnosticFlags & kDiagnosticLightRegions)
						{
							#if C4ORBIS || C4PS3 //[ 

			// -- PS3 code hidden --

							#endif //]

							if (!lightRegionVertexBuffer.Active())
							{
								lightRegionVertexBuffer.Establish(sizeof(Point3D) * kMaxLightRegionVertexCount);
							}

							int32 polygonCount = lightRegion->GetBoundaryPolygonCount();
							const Point3D *polygonVertex = lightRegion->GetBoundaryPolygonVertexArray();

							for (machine polygonIndex = 0; polygonIndex < polygonCount; polygonIndex++)
							{
								volatile Point3D *restrict vertex = lightRegionVertexBuffer.BeginUpdate<Point3D>();

								int32 vertexCount = lightRegion->GetBoundaryPolygonVertexCount(polygonIndex);
								for (machine a = 0; a < vertexCount; a++)
								{
									vertex[a] = polygonVertex[a];
								}

								lightRegionVertexBuffer.EndUpdate();

								lightRegionRenderable.SetVertexCount(vertexCount);
								TheGraphicsMgr->DrawRenderList(&lightRegionRenderList);

								polygonVertex += vertexCount;
							}

							#if C4ORBIS || C4PS3 //[ 

			// -- PS3 code hidden --

							#endif //]
						}

					#endif
				}
			}
		}

		lightRegion = lightRegion->GetNextLightRegion();
	}

	const CameraRegion *subregion = cameraRegion->GetFirstSubnode();
	while (subregion)
	{
		CollectLightRegions(worldContext, subregion);
		subregion = subregion->Tree<CameraRegion>::Next();
	}
}

bool World::ProcessFogSpace(WorldContext *worldContext, const FogSpace *fogSpace, const CameraRegion *rootRegion)
{
	if ((fogSpace->GetPerspectiveExclusionMask() & worldContext->perspectiveFlags) == 0)
	{
		const FrustumCamera *camera = worldContext->renderCamera;
		const Point3D& cameraPosition = camera->GetWorldPosition();
		const Vector3D& cameraDirection = camera->GetWorldTransform()[2];

		Antivector4D fogPlane = fogSpace->GetInverseWorldTransform().GetRow(2);

		float F_wedge_C = fogPlane ^ cameraPosition;
		if (F_wedge_C < 0.0F)
		{
			*worldContext->fogSpacePtr = fogSpace;

			const FogSpaceObject *object = fogSpace->GetObject();
			unsigned_int32 flags = object->GetFogSpaceFlags();

			if (flags & kFogSpaceDistanceOcclusion)
			{
				const Antivector3D& planeNormal = fogPlane.GetAntivector3D();
				Vector3D parallelDirection = cameraDirection - ProjectOnto(cameraDirection, planeNormal);
				float m = SquaredMag(parallelDirection);
				if (m > K::min_float)
				{
					parallelDirection *= InverseSqrt(m);
					float d = object->CalculateOcclusionDistance(F_wedge_C);

					Region *region = new Region;
					Antivector4D *planeArray = region->GetPlaneArray();
					planeArray[0].Set(parallelDirection, -(cameraPosition * parallelDirection) - d);
					planeArray[1] = -fogPlane;
					region->SetPlaneCount(2);

					worldContext->occlusionList.Append(region);
				}
			}

			if ((flags & kFogSpaceDepthOcclusion) && ((fogPlane ^ cameraDirection) < camera->GetSineHalfField()))
			{
				float d = object->CalculateOcclusionDepth(F_wedge_C);

				Region *region = new Region;
				const Antivector3D& planeNormal = fogPlane.GetAntivector3D();
				region->GetPlaneArray()[0].Set(-planeNormal, cameraPosition * planeNormal - d);
				region->SetPlaneCount(1);

				worldContext->occlusionList.Append(region);
			}
		}
		else if (((fogPlane ^ cameraDirection) < camera->GetSineHalfField()) && (fogSpace->Visible(rootRegion, &worldContext->occlusionList)))
		{
			*worldContext->fogSpacePtr = fogSpace;
			worldContext->unfoggedRegion.GetPlaneArray()[0] = fogPlane;
			worldContext->unfoggedList.Append(&worldContext->unfoggedRegion);

			const FogSpaceObject *object = fogSpace->GetObject();
			if (object->GetFogSpaceFlags() & kFogSpaceDepthOcclusion)
			{
				float d = object->CalculateOcclusionDepth(0.0F);

				Region *region = new Region;
				region->GetPlaneArray()[0].Set(-fogPlane.GetAntivector3D(), -fogPlane.w - d);
				region->SetPlaneCount(1);

				worldContext->occlusionList.Append(region);
			}
		}

		return (true);
	}

	return (false);
}

void World::ProcessPortal(WorldContext *worldContext, Portal *portal, CameraRegion *rootRegion)
{
	if ((portal->Enabled()) && ((portal->GetPerspectiveExclusionMask() & worldContext->perspectiveFlags) == 0))
	{
		const BoundingSphere *sphere = portal->GetBoundingSphere();
		if (rootRegion->SphereVisible(sphere->GetCenter(), sphere->GetRadius()))
		{
			const FrustumCamera *camera = worldContext->renderCamera;
			const Point3D& cameraPosition = camera->GetWorldPosition();

			const Antivector4D& portalPlane = portal->GetWorldPlane();
			float distance = portalPlane ^ cameraPosition;
			if ((!(distance < 0.0F)) && ((portalPlane ^ camera->GetWorldTransform()[2]) < camera->GetSineHalfField()))
			{
				Point3D				tempVertex[2][kMaxPortalVertexCount];
				MapReservation		reservation;

				const PortalObject *portalObject = portal->GetObject();
				int32 vertexCount = portalObject->GetVertexCount();
				const Point3D *vertex = portal->GetWorldVertexArray();

				if ((distance > kMinPortalClipDistance) || (Math::PointInConvexPolygon(cameraPosition, vertexCount, vertex, portalPlane.GetAntivector3D()) != kPolygonInterior))
				{
					int32 planeCount = rootRegion->GetPlaneCount() - rootRegion->GetPortalExcludePlaneCount();
					int32 count = Min(planeCount, kMaxPortalVertexCount - vertexCount);

					const Antivector4D *plane = rootRegion->GetPlaneArray();
					for (machine a = 0; a < count; a++)
					{
						int8	location[kMaxPortalVertexCount];

						Point3D *result = tempVertex[a & 1];
						vertexCount = Math::ClipPolygonAgainstPlane(vertexCount, vertex, plane[a], location, result);
						if (vertexCount == 0)
						{
							return;
						}

						vertex = result;
					}

					const Region *region = worldContext->occlusionList.First();
					while (region)
					{
						if (region->PolygonOccluded(vertexCount, vertex))
						{
							return;
						}

						region = region->Next();
					}
				}
				else
				{
					vertexCount = 4;
					vertex = camera->GetFrustumVertexArray();
				}

				PortalType portalType = portal->GetPortalType();
				if (portalType == kPortalDirect)
				{
					Zone *connectedZone = portal->GetConnectedZone();
					if ((connectedZone) && (connectedZone->GetTraversalExclusionMask() == 0))
					{
						CameraRegion *newRegion = new CameraRegion(camera, connectedZone);
						newRegion->SetFrustumPortalPlanes(vertexCount, vertex, portalPlane);
						rootRegion->AddSubnode(newRegion);

						const FogSpace **fsp = worldContext->fogSpacePtr;
						if (portalObject->GetPortalFlags() & kPortalFogInhibit)
						{
							worldContext->fogSpacePtr = nullptr;
						}

						ProcessCameraRegion(worldContext, newRegion);
						worldContext->fogSpacePtr = fsp;

						worldCounter[kWorldCounterDirectPortal]++;
					}
				}
				else if (portalType == kPortalRemote)
				{
					if (remoteRecursionCount < kMaxRemoteRecursionCount)
					{
						PortalBuffer buffer = static_cast<const RemotePortalObject *>(portalObject)->GetPortalBuffer();
						if (buffer == kPortalBufferReflection)
						{
							Map<PortalData> *portalMap = &worldContext->portalGroup[kPortalGroupReflection];
							if (portalMap->Reserve(portal, &reservation))
							{
								portalMap->Insert(new PortalData(portal, rootRegion->GetZone(), vertexCount, vertex), &reservation);
							}
						}
						else if (buffer == kPortalBufferRefraction)
						{
							Map<PortalData> *portalMap = &worldContext->portalGroup[kPortalGroupRefraction];
							if (portalMap->Reserve(portal, &reservation))
							{
								portalMap->Insert(new PortalData(portal, rootRegion->GetZone(), vertexCount, vertex), &reservation);
							}
						}
						else
						{
							Map<PortalData> *portalMap = &worldContext->portalGroup[kPortalGroupRemote];
							if (portalMap->Reserve(portal, &reservation))
							{
								portalMap->Insert(new PortalData(portal, rootRegion->GetZone(), vertexCount, vertex), &reservation);
							}
						}
					}
				}
				else if (portalType == kPortalCamera)
				{
					if (cameraRecursionCount < kMaxCameraRecursionCount)
					{
						Map<PortalData> *portalMap = &worldContext->portalGroup[kPortalGroupCamera];
						if (portalMap->Reserve(portal, &reservation))
						{
							portalMap->Insert(new PortalData(portal, rootRegion->GetZone(), vertexCount, vertex), &reservation);
						}
					}
				}
			}
		}
	}
}

void World::ProcessCameraRegion(WorldContext *worldContext, CameraRegion *rootRegion)
{
	Zone *zone = rootRegion->GetZone();
	zone->AddCameraRegion(rootRegion);
	zone->SetTraversalExclusionMask(zone->GetTraversalExclusionMask() | kZoneTraversalLocal);

	if ((worldContext->fogSpacePtr) && (!*worldContext->fogSpacePtr))
	{
		const FogSpace *fogSpace = zone->GetFirstFogSpace();
		while (fogSpace)
		{
			if ((fogSpace->Enabled()) && (ProcessFogSpace(worldContext, fogSpace, rootRegion)))
			{
				break;
			}

			fogSpace = fogSpace->Next();
		}

		if (!fogSpace)
		{
			fogSpace = zone->GetConnectedFogSpace();
			if ((fogSpace) && (fogSpace->Enabled()))
			{
				ProcessFogSpace(worldContext, fogSpace, rootRegion);
			}
		}
	}

	const OcclusionPortal *occlusionPortal = static_cast<OcclusionPortal *>(zone->GetFirstOcclusionPortal());
	while (occlusionPortal)
	{
		if ((occlusionPortal->Enabled()) && ((occlusionPortal->GetPerspectiveExclusionMask() & worldContext->perspectiveFlags) == 0))
		{
			const BoundingSphere *sphere = occlusionPortal->GetBoundingSphere();
			if (rootRegion->SphereVisible(sphere->GetCenter(), sphere->GetRadius()))
			{
				CameraRegion *newRegion = occlusionPortal->CalculateFrustumOcclusionRegion(worldContext->renderCamera, zone);
				if (newRegion)
				{
					worldContext->occlusionList.Append(newRegion);
					worldCounter[kWorldCounterOcclusionRegion]++;
				}
			}
		}

		occlusionPortal = static_cast<OcclusionPortal *>(occlusionPortal->Next());
	}

	const OcclusionSpace *occlusionSpace = zone->GetFirstOcclusionSpace();
	while (occlusionSpace)
	{
		if ((occlusionSpace->Enabled()) && (occlusionSpace->Visible(rootRegion, &worldContext->occlusionList)))
		{
			CameraRegion *newRegion = occlusionSpace->CalculateFrustumOcclusionRegion(worldContext->renderCamera, zone);
			if (newRegion)
			{
				worldContext->occlusionList.Append(newRegion);
				worldCounter[kWorldCounterOcclusionRegion]++;
			}
		}

		occlusionSpace = occlusionSpace->Next();
	}

	Portal *portal = zone->GetFirstPortal();
	while (portal)
	{
		ProcessPortal(worldContext, portal, rootRegion);
		portal = portal->Next();
	}

	zone->SetTraversalExclusionMask(zone->GetTraversalExclusionMask() & ~kZoneTraversalLocal);
}

void World::RenderIndirectPortals(const WorldContext *worldContext)
{
	const PortalData *portalData = worldContext->portalGroup[kPortalGroupCamera].First();
	while (portalData)
	{
		const CameraPortal *cameraPortal = static_cast<const CameraPortal *>(portalData->GetPortal());
		FrustumCamera *targetCamera = cameraPortal->GetTargetCamera();
		if (targetCamera)
		{
			const CameraPortalObject *portalObject = cameraPortal->GetObject();
			int32 width = portalObject->GetViewportWidth();
			int32 height = portalObject->GetViewportHeight();

			if (width > renderWidth)
			{
				height = Max(renderWidth * height / width, 32);
				width = renderWidth;
			}

			if (height > renderHeight)
			{
				width = Max(renderHeight * width / height, 32);
				height = renderHeight;
			}

			int32 prevRenderWidth = renderWidth;
			int32 prevRenderHeight = renderHeight;
			renderWidth = width;
			renderHeight = height;

			int32 displayHeight = TheDisplayMgr->GetDisplayHeight();
			Rect viewportRect(0, displayHeight - height, width, displayHeight);
			cameraPortal->CallRenderSizeProc(width, height);

			FrustumCameraObject *cameraObject = targetCamera->GetObject();
			cameraObject->SetAspectRatio((float) height / (float) width);
			cameraObject->SetViewRect(viewportRect);
			SetCameraClearParams(cameraObject);

			WorldContext portalContext(targetCamera);
			portalContext.skyboxNode = worldContext->skyboxNode;
			portalContext.perspectiveFlags = kPerspectivePrimary << kPerspectiveCameraWidgetShift;
			portalContext.cameraMinDetailLevel = Max(worldContext->cameraMinDetailLevel, portalObject->GetMinDetailLevel());
			portalContext.cameraDetailBias = worldContext->cameraDetailBias + portalObject->GetDetailLevelBias();

			targetCamera->UpdateRootRegions(targetCamera->GetOwningZone());

			RootCameraRegion *cameraRegion = targetCamera->GetFirstRootRegion();
			portalContext.shadowReceiveRegion = cameraRegion;

			while (cameraRegion)
			{
				targetCamera->CalculateFrustumCameraRegion(cameraRegion);
				cameraRegion = cameraRegion->Next();
			}

			cameraRecursionCount++;

			RenderCamera(&portalContext, targetCamera, kRenderTargetPrimary);
			TheGraphicsMgr->CopyRenderTarget(cameraPortal->GetCameraTexture(), viewportRect);

			cameraRecursionCount--;

			renderWidth = prevRenderWidth;
			renderHeight = prevRenderHeight;
		}

		portalData = portalData->Next();
	}

	unsigned_int32 shift = 0;
	unsigned_int32 perspectiveFlags = worldContext->perspectiveFlags;
	if (perspectiveFlags & kPerspectiveRemotePortalMask)
	{
		shift = kPerspectiveRemotePortalShift;
	}
	else if (perspectiveFlags & kPerspectiveCameraWidgetMask)
	{
		shift = kPerspectiveCameraWidgetShift;
	}

	portalData = worldContext->portalGroup[kPortalGroupReflection].First();
	while (portalData)
	{
		RemotePortal *remotePortal = static_cast<RemotePortal *>(portalData->GetPortal());
		RenderRemoteCamera(worldContext, remotePortal, kRenderTargetReflection, kPerspectiveReflection << shift, portalData);
		portalData = portalData->Next();
	}

	portalData = worldContext->portalGroup[kPortalGroupRefraction].First();
	while (portalData)
	{
		RemotePortal *remotePortal = static_cast<RemotePortal *>(portalData->GetPortal());
		RenderRemoteCamera(worldContext, remotePortal, kRenderTargetRefraction, kPerspectiveRefraction << shift, portalData);
		portalData = portalData->Next();
	}

	portalData = worldContext->portalGroup[kPortalGroupRemote].First();
	while (portalData)
	{
		RemotePortal *remotePortal = static_cast<RemotePortal *>(portalData->GetPortal());
		RenderRemoteCamera(worldContext, remotePortal, kRenderTargetPrimary, kPerspectivePrimary << kPerspectiveRemotePortalShift, portalData);
		portalData = portalData->Next();
	}
}

void World::RenderRemoteCamera(const WorldContext *worldContext, RemotePortal *remotePortal, RenderTargetType target, unsigned_int32 perspectiveFlags, const PortalData *portalData)
{
	Zone *remoteZone = remotePortal->GetConnectedZone();
	if (!remoteZone)
	{
		remoteZone = portalData->GetOriginZone();
	}

	int32 vertexCount = portalData->GetVertexCount();
	const Point3D *vertex = portalData->GetVertexArray();

	Transform4D remoteTransform = remotePortal->CalculateRemoteTransform();

	const FrustumCamera *camera = worldContext->renderCamera;
	const FrustumCameraObject *cameraObject = camera->GetObject();

	const RemotePortalObject *portalObject = remotePortal->GetObject();
	const Antivector4D& plane = remotePortal->GetWorldPlane();

	RemoteCamera remoteCamera(cameraObject->GetFocalLength() * portalObject->GetFocalLengthMultiplier(), cameraObject->GetAspectRatio(), remoteTransform, Antivector4D(plane.GetAntivector3D(), plane.w - portalObject->GetPortalPlaneOffset()));
	RemoteCameraObject *remoteCameraObject = static_cast<RemoteCameraObject *>(remoteCamera.GetObject());

	unsigned_int32 portalFlags = portalObject->GetPortalFlags();
	if (portalFlags & kPortalObliqueFrustum)
	{
		remoteCameraObject->SetFrustumFlags(kFrustumInfinite | kFrustumOblique);
	}
	else
	{
		remoteCameraObject->SetFrustumFlags(kFrustumInfinite);
	}

	SetCameraClearParams(remoteCameraObject);

	if (portalFlags & kPortalOverrideClearColor)
	{
		remoteCameraObject->SetClearColor(portalObject->GetPortalClearColor());
	}

	remoteCameraObject->SetProjectionOffset(cameraObject->GetProjectionOffset());
	remoteCameraObject->SetNearDepth(cameraObject->GetNearDepth());
	remoteCameraObject->SetFarDepth(cameraObject->GetFarDepth());
	remoteCameraObject->SetViewRect(cameraObject->GetViewRect());

	remoteCamera.SetNodeTransform(camera->GetWorldTransform());
	remoteCamera.Invalidate();
	remoteCamera.Update();

	const Transform4D& previousCameraWorldTransform = remotePortal->GetPreviousCameraWorldTransform();
	if (previousCameraWorldTransform(3,3) != 0.0F)
	{
		remoteCamera.SetPreviousWorldTransform(previousCameraWorldTransform);
	}

	remotePortal->SetPreviousCameraWorldTransform(remoteCamera.GetWorldTransform());

	WorldContext portalContext(&remoteCamera);
	portalContext.skyboxNode = (!(portalFlags & kPortalSkyboxInhibit)) ? worldContext->skyboxNode : nullptr;
	portalContext.perspectiveFlags = perspectiveFlags;
	portalContext.cameraMinDetailLevel = Max(worldContext->cameraMinDetailLevel, portalObject->GetMinDetailLevel());
	portalContext.cameraDetailBias = worldContext->cameraDetailBias + portalObject->GetDetailLevelBias();

	remoteRecursionCount++;
	unsigned_int32 nodeFlags = remotePortal->GetNodeFlags();
	if (!(portalFlags & kPortalRecursive))
	{
		remotePortal->SetNodeFlags(nodeFlags | kNodeDisabled);
	}

	remoteCamera.UpdateRootRegions(GetRootNode());
	remoteCamera.AddRootRegion(new RootCameraRegion(&remoteCamera, remoteZone));

	RootCameraRegion *cameraRegion = remoteCamera.GetFirstRootRegion();
	while (cameraRegion)
	{
		remoteCamera.CalculateRemoteCameraRegion(vertexCount, vertex, cameraRegion);
		cameraRegion = cameraRegion->Next();
	}

	CameraRegion receiveRegion(&remoteCamera, remoteZone);

	bool keepFlag = ((perspectiveFlags & kPerspectiveRefractionMask) && (!(portalFlags & kPortalSeparateShadowMap)));
	if (keepFlag)
	{
		remoteCamera.CalculateFrustumCameraRegion(&receiveRegion);
		portalContext.shadowReceiveRegion = &receiveRegion;
	}
	else
	{
		portalContext.shadowReceiveRegion = remoteCamera.GetFirstRootRegion();
	}

	if (!(portalFlags & kPortalDistant))
	{
		RenderCamera(&portalContext, &remoteCamera, target);
	}
	else
	{
		RenderDistantCamera(&portalContext, &remoteCamera, target);
	}

	worldContext->shadowMapKeepFlag = keepFlag;

	remoteRecursionCount--;
	remotePortal->SetNodeFlags(nodeFlags);

	worldCounter[kWorldCounterRemotePortal]++;
}

void World::RenderCamera(WorldContext *worldContext, const FrustumCamera *camera, RenderTargetType target)
{
	currentWorldContext = worldContext;

	const FogSpace *cameraFogSpace = nullptr;
	worldContext->fogSpacePtr = &cameraFogSpace;

	RootCameraRegion *cameraRegion = camera->GetFirstRootRegion();
	do
	{
		ProcessCameraRegion(worldContext, cameraRegion);
		cameraRegion = cameraRegion->Next();
	} while (cameraRegion);

	RenderIndirectPortals(worldContext);

	ambientRenderStamp++;

	TheGraphicsMgr->SetRenderTarget(target);

	const FrustumCameraObject *cameraObject = camera->GetObject();
	TheGraphicsMgr->SetCamera(cameraObject, camera);

	if (cameraFogSpace)
	{
		TheGraphicsMgr->SetFogSpace(cameraFogSpace->GetObject(), cameraFogSpace);
	}

	const Vector3D& view = camera->GetWorldTransform()[2];
	worldContext->deepestBoxComponent[0] = (view.x < 0.0F) ? 0 : 3;
	worldContext->deepestBoxComponent[1] = (view.y < 0.0F) ? 1 : 4;
	worldContext->deepestBoxComponent[2] = (view.z < 0.0F) ? 2 : 5;
	worldContext->maxGeometryDepth = cameraObject->GetNearDepth() + 1.0F;

	cameraRegion = camera->GetFirstRootRegion();
	do
	{
		RenderAmbientRegion(worldContext, cameraRegion);
		cameraRegion = cameraRegion->Next();
	} while (cameraRegion);

	TerrainLevelGeometry *terrain = terrainList.First();
	while (terrain)
	{
		terrain->UpdateBorderState();
		terrain = terrain->Next();
	}

	ImpostorSystem *system = impostorSystemMap.First();
	while (system)
	{
		system->RenderSystem(&renderStageList[kRenderStageImpostor]);
		system = system->Next();
	}

	unsigned_int32 structureFlags = kStructureRenderVelocity | kStructureRenderDepth;
	if (worldFlags & kWorldClearColor)
	{
		structureFlags |= kStructureClearBuffer;
	}

	if (worldContext->skyboxFlag)
	{
		Skybox *skybox = worldContext->skyboxNode;
		if ((skybox) && (skybox->Render(camera, &renderStageList[kRenderStageDefault])))
		{
			structureFlags |= kStructureClearBuffer;
		}
	}

	FinishWorldBatch();

	bool structurePass = false;
	if ((worldContext->perspectiveFlags & (kPerspectivePrimary | (kPerspectivePrimary << kPerspectiveCameraWidgetShift))) != 0)
	{
		if (worldFlags & kWorldMotionBlurInhibit)
		{
			structureFlags &= ~kStructureRenderVelocity;
		}
		else if (worldFlags & kWorldZeroBackgroundVelocity)
		{
			structureFlags |= kStructureZeroBackgroundVelocity;
		}

		float velocityScale = velocityNormalizationTime / Fmax(TheTimeMgr->GetSystemFloatDeltaTime(), 1.0F);

		if (TheGraphicsMgr->BeginStructureRendering(camera->GetPreviousWorldTransform(), structureFlags, velocityScale))
		{
			TheGraphicsMgr->DrawStructureList(&renderStageList[kRenderStageCover]);

			TheGraphicsMgr->GroupAmbientRenderList(&renderStageList[kRenderStageDefault]);
			TheGraphicsMgr->DrawStructureList(&renderStageList[kRenderStageDefault]);

			TheGraphicsMgr->GroupAmbientRenderList(&renderStageList[kRenderStageAlphaTest]);
			TheGraphicsMgr->DrawStructureList(&renderStageList[kRenderStageAlphaTest]);

			TheGraphicsMgr->DrawStructureList(&renderStageList[kRenderStageImpostor]);
			TheGraphicsMgr->DrawStructureList(&renderStageList[kRenderStageEffectVelocity]);

			TheGraphicsMgr->EndStructureRendering();
			structurePass = true;
		}
	}

	if (structurePass)
	{
		TheGraphicsMgr->DrawRenderList(&renderStageList[kRenderStageCover]);
		TheGraphicsMgr->DrawRenderList(&renderStageList[kRenderStageDefault]);
		TheGraphicsMgr->DrawRenderList(&renderStageList[kRenderStageAlphaTest]);
		TheGraphicsMgr->DrawRenderList(&renderStageList[kRenderStageImpostor]);

		TheGraphicsMgr->GroupAmbientRenderList(&renderStageList[kRenderStageDecal]);
		TheGraphicsMgr->DrawRenderList(&renderStageList[kRenderStageDecal]);

		BlobParticleSystem::FinishBatches(&renderStageList[kRenderStageFirstEffect]);
		TheGraphicsMgr->DrawRenderList(&renderStageList[kRenderStageEffectLight]);
	}
	else
	{
		TheGraphicsMgr->DrawRenderList(&renderStageList[kRenderStageCover]);

		TheGraphicsMgr->GroupAmbientRenderList(&renderStageList[kRenderStageDefault]);
		TheGraphicsMgr->DrawRenderList(&renderStageList[kRenderStageDefault]);

		TheGraphicsMgr->GroupAmbientRenderList(&renderStageList[kRenderStageAlphaTest]);
		TheGraphicsMgr->DrawRenderList(&renderStageList[kRenderStageAlphaTest]);

		TheGraphicsMgr->DrawRenderList(&renderStageList[kRenderStageImpostor]);

		TheGraphicsMgr->GroupAmbientRenderList(&renderStageList[kRenderStageDecal]);
		TheGraphicsMgr->DrawRenderList(&renderStageList[kRenderStageDecal]);

		BlobParticleSystem::FinishBatches(&renderStageList[kRenderStageFirstEffect]);
		TheGraphicsMgr->DrawRenderList(&renderStageList[kRenderStageEffectLight]);
	}

	renderStageList[kRenderStageDefault].RemoveAll();
	renderStageList[kRenderStageAlphaTest].RemoveAll();
	renderStageList[kRenderStageImpostor].RemoveAll();
	renderStageList[kRenderStageCover].RemoveAll();
	renderStageList[kRenderStageDecal].RemoveAll();

	if (!(worldFlags & kWorldAmbientOnly))
	{
		TheGraphicsMgr->DrawRenderList(&renderStageList[kRenderStageEffectOcclusion]);

		cameraRegion = camera->GetFirstRootRegion();
		do
		{
			CollectLightRegions(worldContext, cameraRegion);
			cameraRegion->AddOppositePlane();

			cameraRegion = cameraRegion->Next();
		} while (cameraRegion);

		int32 lightDetailLevel = TheWorldMgr->GetLightDetailLevel();
		for (;;)
		{
			Reference<LightRegion> *lightRegionReference = lightRegionList.First();
			if (!lightRegionReference)
			{
				break;
			}

			LightRegion *lightRegion = lightRegionReference->GetTarget();
			Light *light = lightRegion->GetLight();

			if ((light->Enabled()) && (light->GetObject()->GetMinDetailLevel() <= lightDetailLevel))
			{
				lightRenderStamp++;

				Controller *controller = light->GetController();
				if ((controller) && (controller->GetControllerFlags() & kControllerUpdate))
				{
					controller->Update();
				}

				if (light->GetBaseLightType() == kLightInfinite)
				{
					InfiniteLight *infiniteLight = static_cast<InfiniteLight *>(light);
					RenderInfiniteLight(worldContext, infiniteLight);
				}
				else
				{
					PointLight *pointLight = static_cast<PointLight *>(light);
					RenderPointLight(worldContext, pointLight);
				}

				worldCounter[kWorldCounterLight]++;
			}
			else
			{
				do
				{
					Reference<LightRegion> *nextLightRegionReference = lightRegionReference->Next();

					if (lightRegionReference->GetTarget()->GetLight() == light)
					{
						delete lightRegionReference;
					}

					lightRegionReference = nextLightRegionReference;
				} while (lightRegionReference);
			}
		}

		TheGraphicsMgr->SetAmbient();

		TheGraphicsMgr->DrawRenderList(&renderStageList[kRenderStageEffectOpaque]);
		TheGraphicsMgr->DrawRenderList(&renderStageList[kRenderStageEffectVelocity]);
		TheGraphicsMgr->DrawRenderList(&renderStageList[kRenderStageEffectCover]);

		TheGraphicsMgr->SortRenderList(&renderStageList[kRenderStageEffectTransparent]);
		TheGraphicsMgr->DrawRenderList(&renderStageList[kRenderStageEffectTransparent]);
		TheGraphicsMgr->DrawRenderList(&renderStageList[kRenderStageEffectFrontmost]);

		TheGraphicsMgr->ProcessOcclusionQueries();
	}

	TheGraphicsMgr->SetFogSpace(nullptr, nullptr);

	if ((!renderStageList[kRenderStageEffectDistortion].Empty()) && (worldContext->perspectiveFlags == kPerspectivePrimary))
	{
		if (TheGraphicsMgr->BeginDistortionRendering())
		{
			TheGraphicsMgr->DrawDistortionList(&renderStageList[kRenderStageEffectDistortion]);
			TheGraphicsMgr->EndDistortionRendering();
		}
	}

	for (machine a = kRenderStageFirstEffect; a <= kRenderStageLastEffect; a++)
	{
		renderStageList[a].RemoveAll();
	}

	terrainList.RemoveAll();

	#if C4DIAGNOSTICS

		#if C4ORBIS || C4PS3 //[ 

			// -- PS3 code hidden --

		#endif //]

		if (!shadowRegionDiagnosticList.Empty())
		{
			TheGraphicsMgr->DrawRenderList(&shadowRegionDiagnosticList);
			TheGraphicsMgr->DrawWireframe(kWireframeTwoSided | kWireframeColor, &shadowRegionDiagnosticList);
		}

		if (diagnosticFlags & kDiagnosticSourcePaths)
		{
			cameraRegion = camera->GetFirstRootRegion();
			do
			{
				RenderSourcePaths(cameraRegion->GetZone(), TheSoundMgr->GetListenerTransformable()->GetWorldTransform());
				cameraRegion = cameraRegion->Next();
			} while (cameraRegion);
		}

		if (!rigidBodyDiagnosticList.Empty())
		{
			TheGraphicsMgr->DrawRenderList(&rigidBodyDiagnosticList);
		}

		if (!contactDiagnosticList.Empty())
		{
			TheGraphicsMgr->DrawRenderList(&contactDiagnosticList);
		}

		#if C4ORBIS || C4PS3 //[ 

			// -- PS3 code hidden --

		#endif //]

	#endif
}

void World::RenderDistantCamera(WorldContext *worldContext, const FrustumCamera *camera, RenderTargetType target)
{
	currentWorldContext = worldContext;

	TheGraphicsMgr->SetRenderTarget(target);
	TheGraphicsMgr->SetCamera(camera->GetObject(), camera);

	const RootCameraRegion *cameraRegion = camera->GetFirstRootRegion();
	while (cameraRegion)
	{
		const Zone *zone = cameraRegion->GetZone();
		if (zone->GetObject()->GetZoneFlags() & kZoneRenderSkybox)
		{
			Skybox *skybox = worldContext->skyboxNode;
			if (skybox)
			{
				const FogSpace *cameraFogSpace = nullptr;
				worldContext->fogSpacePtr = &cameraFogSpace;

				const FogSpace *fogSpace = zone->GetFirstFogSpace();
				while (fogSpace)
				{
					if ((fogSpace->Enabled()) && (ProcessFogSpace(worldContext, fogSpace, cameraRegion)))
					{
						break;
					}

					fogSpace = fogSpace->Next();
				}

				if (!fogSpace)
				{
					fogSpace = zone->GetConnectedFogSpace();
					if ((fogSpace) && (fogSpace->Enabled()))
					{
						ProcessFogSpace(worldContext, fogSpace, cameraRegion);
					}
				}

				if (cameraFogSpace)
				{
					TheGraphicsMgr->SetFogSpace(cameraFogSpace->GetObject(), cameraFogSpace);
				}

				skybox->Render(camera, &renderStageList[kRenderStageDefault]);
				TheGraphicsMgr->DrawRenderList(&renderStageList[kRenderStageDefault]);
				renderStageList[kRenderStageDefault].RemoveAll();

				TheGraphicsMgr->SetFogSpace(nullptr, nullptr);
				break;
			}
		}

		cameraRegion = cameraRegion->Next();
	}
}

void World::BeginRendering(void)
{
	if (worldFlags & kWorldPostColorMatrix)
	{
		TheGraphicsMgr->SetFinalColorTransform(finalColorScale[0], finalColorScale[1], finalColorScale[2], finalColorBias);
	}
	else
	{
		TheGraphicsMgr->SetFinalColorTransform(finalColorScale[0], finalColorBias);
	}

	TheGraphicsMgr->SetShaderTime(shaderTime, TheTimeMgr->GetFloatDeltaTime());

	#if C4ORBIS || C4PS3 //[ 

			// -- PS3 code hidden --

	#endif //]
}

void World::EndRendering(void)
{
	#if C4ORBIS || C4PS3 //[ 

			// -- PS3 code hidden --

	#endif //]

	TheGraphicsMgr->SetDisplayRenderTarget();

	shaderTime = PositiveFrac((shaderTime + TheTimeMgr->GetFloatDeltaTime()) * kInverseShaderTimePeriod) * kShaderTimePeriod;

	#if C4DIAGNOSTICS

		diagnosticFlags &= ~kDiagnosticShadowRegions;

	#endif
}

void World::SetFinalColorTransform(const ColorRGBA& scale, const ColorRGBA& bias)
{
	finalColorScale[0] = scale;
	finalColorBias = bias;
	worldFlags &= ~kWorldPostColorMatrix;
}

void World::SetFinalColorTransform(const ColorRGBA& red, const ColorRGBA& green, const ColorRGBA& blue, const ColorRGBA& bias)
{
	finalColorScale[0] = red;
	finalColorScale[1] = green;
	finalColorScale[2] = blue;
	finalColorBias = bias;
	worldFlags |= kWorldPostColorMatrix;
}

void World::Render(void)
{
	FrustumCamera *camera = currentCamera;
	if (camera)
	{
		ImpostorSystem *system = impostorSystemMap.First();
		while (system)
		{
			system->Build();
			system = system->Next();
		}

		remoteRecursionCount = 0;
		cameraRecursionCount = 0;

		WorldContext worldContext(camera);

		RootCameraRegion *cameraRegion = camera->GetFirstRootRegion();
		while (cameraRegion)
		{
			camera->CalculateFrustumCameraRegion(cameraRegion);
			cameraRegion = cameraRegion->Next();
		}

		Skybox *skybox = worldSkybox;
		worldContext.skyboxNode = ((skybox) && (skybox->Enabled())) ? skybox : nullptr;

		worldContext.perspectiveFlags = kPerspectivePrimary | worldPerspective;
		worldContext.cameraMinDetailLevel = 0;
		worldContext.cameraDetailBias = 0.0F;
		worldContext.shadowReceiveRegion = camera->GetFirstRootRegion();

		RenderCamera(&worldContext, camera, kRenderTargetPrimary);
	}
}


WorldMgr::WorldMgr(int) :
		objectConstructor(&ConstructObject),
		controllerStateSender(&SendControllerState, this),
		displayEventHandler(&HandleDisplayEvent, this),
		lightDetailLevelObserver(this, &WorldMgr::HandleLightDetailLevelEvent)
{
}

WorldMgr::~WorldMgr()
{
}

EngineResult WorldMgr::Construct(void)
{
	currentWorld = nullptr;

	worldConstructorProc = nullptr;
	Object::InstallConstructor(&objectConstructor);
	TheMessageMgr->InstallStateSender(&controllerStateSender);
	TheDisplayMgr->InstallDisplayEventHandler(&displayEventHandler);

	unsigned_int32 speed = TheGraphicsMgr->GetCapabilities()->hardwareSpeed;
	TheEngine->InitVariable("lightDetailLevel", (speed >= 2) ? "2" : ((speed >= 1) ? "1" : "0"), kVariablePermanent, &lightDetailLevelObserver);

	defaultVelocityNormalizationTime = 8.33333F;
	trackingOrientation.Set(0.0F, 0.0F, 0.0F, 1.0F);

	Controller::RegisterStandardControllers();
	Property::RegisterStandardProperties();
	Modifier::RegisterStandardModifiers();
	Mutator::RegisterStandardMutators();
	Process::RegisterStandardProcesses();
	Widget::RegisterStandardWidgets();
	Method::RegisterStandardMethods();
	Force::RegisterStandardForces();
	ParticleSystem::RegisterStandardParticleSystems();

	#if C4DIAGNOSTICS

		World::lightRegionRenderList.Append(&World::lightRegionRenderable);
		World::lightRegionRenderable.SetRenderableFlags(kRenderableFogInhibit);
		World::lightRegionRenderable.SetShaderFlags(kShaderAmbientEffect);
		World::lightRegionRenderable.SetVertexBuffer(kVertexBufferAttributeArray, &World::lightRegionVertexBuffer, sizeof(Point3D));
		World::lightRegionRenderable.SetVertexAttributeArray(kArrayPosition, 0, 3);
		World::lightRegionAttributeList.Append(&World::lightRegionDiffuseColor);
		World::lightRegionRenderable.SetMaterialAttributeList(&World::lightRegionAttributeList);

		World::sourcePathRenderList.Append(&World::sourcePathRenderable);
		World::sourcePathRenderable.SetRenderableFlags(kRenderableFogInhibit);
		World::sourcePathRenderable.SetShaderFlags(kShaderAmbientEffect);
		World::sourcePathRenderable.SetVertexCount(2);
		World::sourcePathRenderable.SetVertexBuffer(kVertexBufferAttributeArray, &World::sourcePathVertexBuffer, sizeof(Point3D));
		World::sourcePathRenderable.SetVertexAttributeArray(kArrayPosition, 0, 3);
		World::sourcePathAttributeList.Append(&World::sourcePathDiffuseColor);
		World::sourcePathRenderable.SetMaterialAttributeList(&World::sourcePathAttributeList);

	#endif

	new(loaderSignal) Signal(2);
	new(loaderThread) Thread(&LoaderThread, this, 0, loaderSignal);

	return (kEngineOkay);
}

void WorldMgr::Destruct(void)
{
	loaderThread->~Thread();
	loaderSignal->~Signal();

	#if C4DIAGNOSTICS

		World::sourcePathVertexBuffer.Establish(0);
		World::lightRegionVertexBuffer.Establish(0);

	#endif

	displayEventHandler.Detach();
	controllerStateSender.Detach();
	Object::RemoveConstructor(&objectConstructor);

	TheResourceMgr->FlushCache(AnimationResource::GetDescriptor());
}

Object *WorldMgr::ConstructObject(Unpacker& data, unsigned_int32 unpackFlags)
{
	switch (data.GetType())
	{
		case kObjectProperty:

			return (new PropertyObject);

		case kObjectMaterial:

		#if C4LEGACY

			case 'EMAT':

		#endif

			return (new MaterialObject);

		case kObjectScript:

			return (new ScriptObject);

		case kObjectTerrainBlock:

		#if C4LEGACY

			case 'BLCK':

		#endif

			return (new TerrainBlockObject);

		case kObjectWaterBlock:

			return (new WaterBlockObject);

		case kObjectGeometry:

			return (GeometryObject::Construct(++data, unpackFlags));

		case kObjectSource:

			return (SourceObject::Construct(++data, unpackFlags));

		case kObjectPortal:

			return (PortalObject::Construct(++data, unpackFlags));

		case kObjectZone:

			return (ZoneObject::Construct(++data, unpackFlags));

		case kObjectTrigger:

			return (TriggerObject::Construct(++data, unpackFlags));

		case kObjectEffect:

			return (EffectObject::Construct(++data, unpackFlags));

		case kObjectEmitter:

			return (EmitterObject::Construct(++data, unpackFlags));

		case kObjectShape:

			return (ShapeObject::Construct(++data, unpackFlags));

		case kObjectJoint:

			return (JointObject::Construct(++data, unpackFlags));

		case kObjectField:

			return (FieldObject::Construct(++data, unpackFlags));

		case kObjectBlocker:

			return (BlockerObject::Construct(++data, unpackFlags));
	}

	return (nullptr);
}

void WorldMgr::SendControllerState(Player *to, void *cookie)
{
	World *world = static_cast<WorldMgr *>(cookie)->GetWorld();
	if (world)
	{
		machine count = world->GetControllerArraySize();
		for (machine index = 0; index < count; index++)
		{
			const Controller *controller = world->GetController(index);
			if (controller)
			{
				controller->SendInitialStateMessages(to);
			}
		}
	}
}

void WorldMgr::HandleDisplayEvent(const DisplayEventData *eventData, void *cookie)
{
	if (eventData->eventType == kEventDisplayChange)
	{
		WorldMgr *worldMgr = static_cast<WorldMgr *>(cookie);

		World *world = worldMgr->currentWorld;
		if (world)
		{
			int32 width = TheDisplayMgr->GetDisplayWidth();
			int32 height = TheDisplayMgr->GetDisplayHeight();
			world->SetRenderSize(width, height);
		}
	}
}

void WorldMgr::HandleLightDetailLevelEvent(Variable *variable)
{
	lightDetailLevel = MaxZero(Min(variable->GetIntegerValue(), 2));
}

WorldResult WorldMgr::LoadWorld(const char *name)
{
	UnloadWorld();

	World *world = (worldConstructorProc) ? (*worldConstructorProc)(name, worldConstructorCookie) : new World(name);
	if (world->Preprocess() != kWorldOkay)
	{
		delete world;
		return (kWorldLoadFailed);
	}

	RunWorld(world);
	return (kWorldOkay);
}

void WorldMgr::UnloadWorld(void)
{
	if (currentWorld)
	{
		TheMessageMgr->SetControllerMessageProcs(nullptr, nullptr);

		delete currentWorld;
		currentWorld = nullptr;

		MaterialObject::ReleaseCache();
		ShaderProgram::ReleaseCache();
	}
}

void WorldMgr::RunWorld(World *world)
{
	currentWorld = world;

	TheMessageMgr->SetControllerMessageProcs(&World::ConstructControllerMessage, &World::ReceiveControllerMessage, world);
	TheTimeMgr->ResetTime();
}

void WorldMgr::SaveDeltaWorld(const char *name)
{
	const World *world = currentWorld;
	if (world)
	{
		File			file;
		ResourcePath	path;

		TheResourceMgr->GetSaveCatalog()->GetResourcePath(SaveResource::GetDescriptor(), name, &path);
		if ((FileMgr::CreateDirectoryPath(path) == kFileOkay) && (file.Open(path, kFileCreate) == kFileOkay))
		{
			world->GetRootNode()->PackDeltaTree(&file, world->GetWorldName());
		}
	}
}

WorldResult WorldMgr::RestoreDeltaWorld(const char *name)
{
	World *world = (worldConstructorProc) ? (*worldConstructorProc)(name, worldConstructorCookie) : new World(name);
	world->SetWorldFlags(world->GetWorldFlags() | kWorldRestore);
	world->previousWorld = currentWorld;
	currentWorld = nullptr;

	if (world->Preprocess() != kWorldOkay)
	{
		delete world;
		return (kWorldLoadFailed);
	}

	RunWorld(world);
	return (kWorldOkay);
}

void WorldMgr::LoaderThread(const Thread *thread, void *cookie)
{
	WorldMgr *worldMgr = static_cast<WorldMgr *>(cookie);

	for (;;)
	{
		int32 index = worldMgr->loaderSignal->Wait();
		if (index == 0)
		{
			break;
		}

		TheGraphicsMgr->SetSyncLoadFlag(true);
		(*worldMgr->loaderProc)(worldMgr->loaderCookie);
		TheGraphicsMgr->SetSyncLoadFlag(false);
	}
}

void WorldMgr::RunLoaderTask(void (*proc)(void *), void *cookie)
{
	loaderProc = proc;
	loaderCookie = cookie;
	loaderSignal->Trigger(1);
}

void WorldMgr::Move(void)
{
	World *world = currentWorld;
	if (world)
	{
		if (!(world->GetWorldFlags() & kWorldPaused))
		{
			#if C4OCULUS

				Oculus::ReadOrientation(&trackingOrientation);

			#endif

			world->Move();
			world->Update();
			world->Interact();
		}
		else
		{
			for (machine a = 0; a < kWorldCounterRenderCount; a++)
			{
				world->worldCounter[a] = 0;
			}

			world->Update();
		}

		world->GetRootNode()->Update();
		world->Listen();
	}
}

void WorldMgr::Render(void)
{
	TheGraphicsMgr->BeginRendering();

	#if C4OCULUS

		if (TheDisplayMgr->GetDisplayFlags() & kDisplayOculus)
		{
			float ipd = Oculus::GetInterpupillaryDistance();

			World *world = currentWorld;
			if (world)
			{
				world->BeginRendering();

				Camera *camera = world->GetCamera();
				camera->SetWorldPosition(camera->GetWorldPosition() - camera->GetWorldTransform()[0] * (ipd * 0.5F));

				world->Render();
				TheGraphicsMgr->SetDisplayRenderTarget();
			}

			TheInterfaceMgr->Render();
			TheGraphicsMgr->SetFullFrameRenderTarget();

			TheGraphicsMgr->SetCameraLensMultiplier(-1.0F);

			if (world)
			{
				Camera *camera = world->GetCamera();
				camera->SetWorldPosition(camera->GetWorldPosition() + camera->GetWorldTransform()[0] * ipd);

				world->Render();
				world->EndRendering();
			}

			TheInterfaceMgr->Render();
			TheGraphicsMgr->SetFullFrameRenderTarget();
		}
		else
		{
			World *world = currentWorld;
			if (world)
			{
				world->BeginRendering();
				world->Render();
				world->EndRendering();
			}

			TheInterfaceMgr->Render();
			TheGraphicsMgr->SetFullFrameRenderTarget();
		}

	#else

		World *world = currentWorld;
		if (world)
		{
			world->BeginRendering();
			world->Render();
			world->EndRendering();
		}

		TheInterfaceMgr->Render();
		TheGraphicsMgr->SetFullFrameRenderTarget();

	#endif

	TheMovieMgr->RecordTask();

	TheGraphicsMgr->EndRendering();
}

// ZYUTNLM
