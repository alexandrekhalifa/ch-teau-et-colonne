//=============================================================
//
// C4 Engine version 3.5.1
// Copyright 1999-2013, by Terathon Software LLC
//
// This copy is licensed to the following:
//
//     Registered user: Université de Sherbrooke
//     Maximum number of users: Site License
//     License #C4T0033897
//
// License is granted under terms of the license agreement
// entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#ifndef C4Render_h
#define C4Render_h


#include "C4Packing.h"


#if C4OPENGL

	#include "C4OpenGL.h"
	#include "C4Computation.h"

#elif C4ORBIS //[ 

			// -- Orbis code hidden --

#elif C4PS3 //[ 

			// -- PS3 code hidden --

#endif //]


namespace C4
{
	struct Render
	{
		enum
		{
			kMaxTextureUnitCount		= 16,
			kMaxVertexParamCount		= 62,
			kMaxFragmentParamCount		= 28,
			kMaxGeometryParamCount		= 4
		};


		enum
		{
			kTextureTarget2D,
			kTextureTarget3D,
			kTextureTargetRectangle,
			kTextureTargetCube,
			kTextureTargetArray2D,
			kTextureTargetCount
		};


		enum
		{
			kTextureRGBX8,
			kTextureRGBA8,
			kTextureBGRX8,
			kTextureBGRA8,
			kTextureXRGB8,
			kTextureARGB8,
			kTextureR8,
			kTextureRG8,
			kTextureL8,
			kTextureLA8,
			kTextureI8,
			kTextureDepth,
			kTextureBC1,
			kTextureBC3,
			kTextureRenderBufferR8,
			kTextureRenderBufferRGB8,
			kTextureRenderBufferRGBA8,
			kTextureRenderBufferRGBA16F,
			kTextureFormatCount
		};


		enum
		{
			kTextureEncodingLinear,
			kTextureEncodingSrgb,
			kTextureEncodingCount
		};


		enum
		{
			kTextureBorderTransparent,
			kTextureBorderBlack,
			kTextureBorderWhite
		};


		struct VertexBufferObject;


		typedef void Decompressor(const unsigned_int8 *, unsigned_int32, void *);


		struct TextureImageData
		{
			const void				*image;
			unsigned_int32			size;
			Decompressor			*decompressor;
		};

		struct TextureUploadData 
		{
			unsigned_int32			*memorySize;
 
			unsigned_int32			format;
			unsigned_int32			encoding; 
			unsigned_int32			width; 
			unsigned_int32			height;

			union
			{ 
				unsigned_int32		depth;
				unsigned_int32		rowLength;
			};

			int32					mipmapCount; 
			TextureImageData		imageData[66];
		};

		struct TextureAllocationData
		{
			unsigned_int32			*memorySize;

			unsigned_int32			format;
			unsigned_int32			encoding;
			unsigned_int32			width;
			unsigned_int32			height;

			bool					renderBuffer;
		};

		struct BufferUploadData
		{
			unsigned_int32			offset;
			unsigned_int32			size;
			const void				*data;
		};


		struct VertexArrayObject
		{

			VertexArrayObject();
			~VertexArrayObject();
		};


		static void Initialize(void);
		static void Terminate(void);


		#if C4OPENGL

			enum
			{
				kAlphaNever						= GL_NEVER,
				kAlphaLess						= GL_LESS,
				kAlphaEqual						= GL_EQUAL,
				kAlphaLessEqual					= GL_LEQUAL,
				kAlphaGreater					= GL_GREATER,
				kAlphaNotEqual					= GL_NOTEQUAL,
				kAlphaGreaterEqual				= GL_GEQUAL,
				kAlphaAlways					= GL_ALWAYS
			};


			enum
			{
				kDepthNever						= GL_NEVER,
				kDepthLess						= GL_LESS,
				kDepthEqual						= GL_EQUAL,
				kDepthLessEqual					= GL_LEQUAL,
				kDepthGreater					= GL_GREATER,
				kDepthNotEqual					= GL_NOTEQUAL,
				kDepthGreaterEqual				= GL_GEQUAL,
				kDepthAlways					= GL_ALWAYS
			};


			enum
			{
				kStencilNever					= GL_NEVER,
				kStencilLess					= GL_LESS,
				kStencilEqual					= GL_EQUAL,
				kStencilLessEqual				= GL_LEQUAL,
				kStencilGreater					= GL_GREATER,
				kStencilNotEqual				= GL_NOTEQUAL,
				kStencilGreaterEqual			= GL_GEQUAL,
				kStencilAlways					= GL_ALWAYS
			};


			enum
			{
				kStencilZero					= GL_ZERO,
				kStencilInvert					= GL_INVERT,
				kStencilKeep					= GL_KEEP,
				kStencilReplace					= GL_REPLACE,
				kStencilIncr					= GL_INCR,
				kStencilDecr					= GL_DECR,
				kStencilIncrWrap				= GL_INCR_WRAP,
				kStencilDecrWrap				= GL_DECR_WRAP
			};


			enum
			{
				kBlendZero						= GL_ZERO,
				kBlendOne						= GL_ONE,
				kBlendSrcColor					= GL_SRC_COLOR,
				kBlendInvSrcColor				= GL_ONE_MINUS_SRC_COLOR,
				kBlendSrcAlpha					= GL_SRC_ALPHA,
				kBlendInvSrcAlpha				= GL_ONE_MINUS_SRC_ALPHA,
				kBlendDstAlpha					= GL_DST_ALPHA,
				kBlendInvDstAlpha				= GL_ONE_MINUS_DST_ALPHA,
				kBlendDstColor					= GL_DST_COLOR,
				kBlendInvDstColor				= GL_ONE_MINUS_DST_COLOR,
				kBlendConstColor				= GL_CONSTANT_COLOR,
				kBlendInvConstColor				= GL_ONE_MINUS_CONSTANT_COLOR,
				kBlendConstAlpha				= GL_CONSTANT_ALPHA,
				kBlendInvConstAlpha				= GL_ONE_MINUS_CONSTANT_ALPHA
			};


			enum
			{
				kBlendEquationAdd				= GL_FUNC_ADD,
				kBlendEquationMin				= GL_MIN,
				kBlendEquationMax				= GL_MAX,
				kBlendEquationSubtract			= GL_FUNC_SUBTRACT,
				kBlendEquationReverseSubtract	= GL_FUNC_REVERSE_SUBTRACT
			};


			enum
			{
				kCullFront						= GL_FRONT,
				kCullBack						= GL_BACK,
				kCullFrontAndBack				= GL_FRONT_AND_BACK
			};


			enum
			{
				kFrontCW						= GL_CW,
				kFrontCCW						= GL_CCW
			};


			enum
			{
				kShadowNever					= GL_NEVER,
				kShadowLess						= GL_LESS,
				kShadowEqual					= GL_EQUAL,
				kShadowLessEqual				= GL_LEQUAL,
				kShadowGreater					= GL_GREATER,
				kShadowNotEqual					= GL_NOTEQUAL,
				kShadowGreaterEqual				= GL_GEQUAL,
				kShadowAlways					= GL_ALWAYS
			};


			enum
			{
				kTextureCompareNone				= GL_NONE,
				kTextureCompareReference		= GL_COMPARE_REF_TO_TEXTURE
			};


			enum
			{
				kPrimitivePoints				= GL_POINTS,
				kPrimitiveLines					= GL_LINES,
				kPrimitiveLineLoop				= GL_LINE_LOOP,
				kPrimitiveLineStrip				= GL_LINE_STRIP,
				kPrimitiveTriangles				= GL_TRIANGLES,
				kPrimitiveTriangleStrip			= GL_TRIANGLE_STRIP,
				kPrimitiveTriangleFan			= GL_TRIANGLE_FAN
			};


			enum
			{
				kVertexFloat					= GL_FLOAT,
				kVertexUnsignedByte				= GL_UNSIGNED_BYTE,
				kVertexSignedShort				= GL_SHORT
			};


			enum
			{
				kRenderBufferRGBA8				= GL_RGBA8,
				kRenderBufferRGBA16F			= GL_RGBA16F,
				kRenderBufferRGBA32F			= GL_RGBA32F,
				kRenderBufferDepth				= GL_DEPTH_COMPONENT24,
				kRenderBufferDepthStencil		= GL_DEPTH24_STENCIL8
			};


			enum
			{
				kWrapRepeat						= GL_REPEAT,
				kWrapMirrorRepeat				= GL_MIRRORED_REPEAT,
				kWrapClampToEdge				= GL_CLAMP_TO_EDGE,
				kWrapClampToBorder				= GL_CLAMP_TO_BORDER,
				kWrapClamp						= GL_CLAMP,
				kWrapMirrorClampToEdge			= GL_MIRROR_CLAMP_TO_EDGE,
				kWrapMirrorClampToBorder		= GL_MIRROR_CLAMP_TO_BORDER_EXT,
				kWrapMirrorClamp				= GL_MIRROR_CLAMP_EXT
			};


			enum
			{
				kFilterNearest					= GL_NEAREST,
				kFilterLinear					= GL_LINEAR,
				kFilterNearestMipmapNearest		= GL_NEAREST_MIPMAP_NEAREST,
				kFilterLinearMipmapNearest		= GL_LINEAR_MIPMAP_NEAREST,
				kFilterNearestMipmapLinear		= GL_NEAREST_MIPMAP_LINEAR,
				kFilterLinearMipmapLinear		= GL_LINEAR_MIPMAP_LINEAR
			};


			enum
			{
				kBlitFilterPoint				= GL_NEAREST,
				kBlitFilterBilinear				= GL_LINEAR
			};


			enum
			{
				kVertexBufferTargetAttribute	= GL_ARRAY_BUFFER,
				kVertexBufferTargetIndex		= GL_ELEMENT_ARRAY_BUFFER
			};


			enum
			{
				kVertexBufferUsageStatic		= GL_STATIC_DRAW,
				kVertexBufferUsageDynamic		= GL_DYNAMIC_DRAW
			};


			struct TextureObject
			{
				private:

					GLuint				textureIdentifier;

					unsigned_int16		targetIndex;
					unsigned_int16		openglTarget;

					unsigned_int32		formatIndex;

				public:

					void Construct(unsigned_int32 index);
					void Destruct(void);

					unsigned_int32 GetTextureIdentifier(void) const
					{
						return (textureIdentifier);
					}

					unsigned_int32 GetTextureTargetIndex(void) const
					{
						return (targetIndex);
					}

					unsigned_int32 GetOpenGLTextureTarget(void) const
					{
						return (openglTarget);
					}

					void InvalidateImage(void)
					{
						glInvalidateTexImage(textureIdentifier, 0);
					}

					void SetSWrapMode(unsigned_int32 mode)
					{
						glTextureParameteriEXT(textureIdentifier, openglTarget, GL_TEXTURE_WRAP_S, mode);
					}

					void SetTWrapMode(unsigned_int32 mode)
					{
						glTextureParameteriEXT(textureIdentifier, openglTarget, GL_TEXTURE_WRAP_T, mode);
					}

					void SetRWrapMode(unsigned_int32 mode)
					{
						glTextureParameteriEXT(textureIdentifier, openglTarget, GL_TEXTURE_WRAP_R, mode);
					}

					void SetCompareFunc(unsigned_int32 func)
					{
						glTextureParameteriEXT(textureIdentifier, openglTarget, GL_TEXTURE_COMPARE_FUNC, func);
					}

					void SetCompareMode(unsigned_int32 mode)
					{
						glTextureParameteriEXT(textureIdentifier, openglTarget, GL_TEXTURE_COMPARE_MODE, mode);
					}

					void SetMinLod(unsigned_int32 lod)
					{
						glTextureParameteriEXT(textureIdentifier, openglTarget, GL_TEXTURE_BASE_LEVEL, lod);
					}

					void SetMaxLod(unsigned_int32 lod)
					{
						glTextureParameteriEXT(textureIdentifier, openglTarget, GL_TEXTURE_MAX_LEVEL, lod);
					}

					void SetLodBias(float bias)
					{
						glTextureParameterfEXT(textureIdentifier, openglTarget, GL_TEXTURE_LOD_BIAS, bias);
					}

					void SetMinFilterMode(unsigned_int32 mode)
					{
						glTextureParameteriEXT(textureIdentifier, openglTarget, GL_TEXTURE_MIN_FILTER, mode);
					}

					void SetMagFilterMode(unsigned_int32 mode)
					{
						glTextureParameteriEXT(textureIdentifier, openglTarget, GL_TEXTURE_MAG_FILTER, mode);
					}

					void SetMaxAnisotropy(float anisotropy)
					{
						glTextureParameterfEXT(textureIdentifier, openglTarget, GL_TEXTURE_MAX_ANISOTROPY_EXT, anisotropy);
					}

					void SetBorderColor(unsigned_int32 color);

					void Bind(unsigned_int32 unit) const;
					void Unbind(unsigned_int32 unit) const;
					void UnbindAll(void) const;

					void SetImage2D(const TextureUploadData *uploadData);
					void SetImage3D(const TextureUploadData *uploadData);
					void SetImageCube(const TextureUploadData *uploadData);
					void SetImageRect(const TextureUploadData *uploadData);
					void SetImageArray2D(const TextureUploadData *uploadData);
					void SetCompressedImage2D(const TextureUploadData *uploadData);
					void SetCompressedImageCube(const TextureUploadData *uploadData);
					void SetCompressedImageArray2D(const TextureUploadData *uploadData);

					void AllocateStorage2D(const TextureAllocationData *allocationData);
					void AllocateStorageRect(const TextureAllocationData *allocationData);

					void UpdateImage2D(unsigned_int32 width, unsigned_int32 height, int32 count, const void *image) const;
					void UpdateImage2D(unsigned_int32 x, unsigned_int32 y, unsigned_int32 width, unsigned_int32 height, unsigned_int32 rowLength, const void *image) const;
					void UpdateImage3D(unsigned_int32 x, unsigned_int32 y, unsigned_int32 z, unsigned_int32 width, unsigned_int32 height, unsigned_int32 depth, const void *image) const;
					void UpdateImageRect(unsigned_int32 x, unsigned_int32 y, unsigned_int32 width, unsigned_int32 height, unsigned_int32 rowLength, const void *image) const;

					void BlitImageRect(unsigned_int32 srcX, unsigned_int32 srcY, unsigned_int32 dstX, unsigned_int32 dstY, unsigned_int32 width, unsigned_int32 height) const
					{
						glCopyTextureSubImage2DEXT(textureIdentifier, GL_TEXTURE_RECTANGLE, 0, dstX, dstY, srcX, srcY, width, height);
					}
			};


			struct RenderBufferObject
			{
				private:

					GLuint				renderBufferIdentifier;

				public:

					void Construct(void);
					void Destruct(void);

					unsigned_int32 GetRenderBufferIdentifier(void) const
					{
						return (renderBufferIdentifier);
					}

					void AllocateStorage(unsigned_int32 width, unsigned_int32 height, unsigned_int32 format);
					void AllocateMultisampleStorage(unsigned_int32 width, unsigned_int32 height, unsigned_int32 sampleCount, unsigned_int32 format);
			};


			struct FrameBufferObject
			{
				friend struct Render;

				private:

					GLuint					frameBufferIdentifier;

					const TextureObject		*currentColorTexture;
					const TextureObject		*currentDepthTexture;

					void Bind(void) const
					{
						if (renderState.drawFrameBuffer != this)
						{
							renderState.drawFrameBuffer = this;
							renderState.readFrameBuffer = this;
							glBindFramebuffer(GL_FRAMEBUFFER, frameBufferIdentifier);
						}
					}

				public:

					void Construct(void);
					void Destruct(void);

					unsigned_int32 GetFrameBufferIdentifier(void) const
					{
						return (frameBufferIdentifier);
					}

					void SetColorRenderBuffer(const RenderBufferObject *renderBuffer);
					void SetDepthStencilRenderBuffer(const RenderBufferObject *renderBuffer);

					void SetColorRenderTexture(const TextureObject *renderTexture);
					void ResetColorRenderTexture(void);

					void SetDepthRenderTexture(const TextureObject *renderTexture);
					void ResetDepthRenderTexture(void);
			};


			struct VertexBufferObject
			{
				friend struct Render;

				private:

					GLuint				vertexBufferIdentifier;

					unsigned_int32		bufferTarget;
					unsigned_int32		bufferUsage;

					unsigned_int32		bufferSize;
					char				*staticBuffer;

				protected:

					void SetVertexBufferSize(unsigned_int32 size)
					{
						bufferSize = size;
					}

					void ReadBuffer(unsigned_int32 offset, unsigned_int32 size, void *data) const;

				public:

					VertexBufferObject(unsigned_int32 target, unsigned_int32 usage)
					{
						vertexBufferIdentifier = 0;
						bufferTarget = target;
						bufferUsage = usage;
						bufferSize = 0;
					}

					void Construct(void);
					void Destruct(void);

					unsigned_int32 GetVertexBufferIdentifier(void) const
					{
						return (vertexBufferIdentifier);
					}

					unsigned_int32 GetVertexBufferSize(void) const
					{
						return (bufferSize);
					}

					void AllocateStorage(const void *data = nullptr);

					C4API volatile void *BeginUpdate(void);
					C4API void EndUpdate(void);
					C4API void UpdateBuffer(unsigned_int32 offset, unsigned_int32 size, const void *data);

					void BeginUpdateSync(volatile void **const *ptr);
					void EndUpdateSync(const void *);
					void UpdateBufferSync(const BufferUploadData *uploadData);

					template <typename type> volatile type *BeginUpdate(void)
					{
						return (static_cast<volatile type *>(BeginUpdate()));
					}
			};


			struct QueryObject
			{
				friend struct Render;

				private:

					unsigned_int32		queryIndex;
					unsigned_int32		queryFrame[4];
					GLuint				queryIdentifier[4];

				public:

					void Construct(void);
					void Destruct(void);
			};


			struct ShaderObject
			{
				friend class ShaderProgramObject;

				private:

					bool		compiledFlag;

				protected:

					GLuint		shaderIdentifier;

					ShaderObject();
					~ShaderObject();

				public:

					unsigned_int32 GetShaderIdentifier(void) const
					{
						return (shaderIdentifier);
					}

					void Compile(void);

					#if C4DEBUG

						bool GetShaderStatus(const char **error, const char **source) const;
						void ReleaseShaderStatus(const char *error, const char *source) const;

					#endif
			};


			struct VertexShaderObject : ShaderObject
			{
				public:

					VertexShaderObject(const char *source, unsigned_int32 size);
			};


			struct FragmentShaderObject : ShaderObject
			{
				public:

					FragmentShaderObject(const char *source, unsigned_int32 size);
			};


			struct GeometryShaderObject : ShaderObject
			{
				public:

					GeometryShaderObject(const char *source, unsigned_int32 size);
			};


			struct ShaderProgramObject
			{
				private:

					VertexShaderObject		*vertexShader;
					FragmentShaderObject	*fragmentShader;
					GeometryShaderObject	*geometryShader;

					GLuint					programIdentifier;
					unsigned_int32			programUpdateFlags;

					int32					vertexUniformLocation;
					int32					fragmentUniformLocation;
					int32					geometryUniformLocation;

				public:

					ShaderProgramObject();
					~ShaderProgramObject();

					GLuint GetProgramIdentifier(void) const
					{
						return (programIdentifier);
					}

					unsigned_int32 GetProgramUpdateFlags(void) const
					{
						return (programUpdateFlags);
					}

					int32 QueryUniformLocation(const char *name) const
					{
						return (glGetUniformLocation(programIdentifier, name));
					}

					int32 GetVertexUniformLocation(void) const
					{
						return (vertexUniformLocation);
					}

					int32 GetFragmentUniformLocation(void) const
					{
						return (fragmentUniformLocation);
					}

					int32 GetGeometryUniformLocation(void) const
					{
						return (geometryUniformLocation);
					}

					void SetVertexShader(VertexShaderObject *shader);
					void SetFragmentShader(FragmentShaderObject *shader);
					void SetGeometryShader(GeometryShaderObject *shader);

					void Activate(void);
					bool SetProgramBinary(unsigned_int32 format, const void *data, unsigned_int32 size);

					void Preprocess(void);

					#if C4DEBUG

						bool GetProgramStatus(const char **error, const char **source) const;
						void ReleaseProgramStatus(const char *error, const char *source) const;

					#endif
			};


			struct PixelBufferObject
			{
				private:

					GLuint				pixelBufferIdentifier;
					unsigned_int32		bufferSize;

				public:

					void Construct(void);
					void Destruct(void);

					void Bind(void)
					{
						glBindBuffer(GL_PIXEL_PACK_BUFFER, pixelBufferIdentifier);
					}

					void Unbind(void)
					{
						glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
					}

					const void *BeginRead(void)
					{
						return (glMapNamedBufferRangeEXT(pixelBufferIdentifier, 0, bufferSize, GL_MAP_READ_BIT));
					}

					void EndRead(void)
					{
						glUnmapNamedBufferEXT(pixelBufferIdentifier);
					}

					void AllocateStorage(unsigned_int32 size);
			};


			struct RenderState
			{
				unsigned_int32					imageUnit;
				GLuint							texture[kMaxTextureUnitCount][kTextureTargetCount];

				const FrameBufferObject			*drawFrameBuffer;
				const FrameBufferObject			*readFrameBuffer;

				GLuint							vertexArrayObject;
				GLuint							attributeVertexBuffer;
				GLuint							indexVertexBuffer;

				unsigned_int32					frameCount;
				unsigned_int32					updateFlags;

				const ShaderProgramObject		*shaderProgram;

				Vector4D						vertexShaderParam[kMaxVertexParamCount];
				Vector4D						fragmentShaderParam[kMaxFragmentParamCount];
				Vector4D						geometryShaderParam[kMaxGeometryParamCount];
			};


			private:

				enum
				{
					kMaxQuadPrimitiveCount = 16383
				};

				static RenderState				renderState;

				static VertexBufferObject		quadIndexBuffer;
				static const Triangle			quadTriangle[kMaxQuadPrimitiveCount * 2];

				static void Update(void);

				static void BindTextureUnit0(GLuint texture, GLenum target);

			public:

				static void InitializeCoreOpenGL();
				static void TerminateCoreOpenGL();

				static void BeginRendering(void)
				{
					renderState.frameCount++;
				}

				static void EndRendering(void)
				{
					glFlush();
				}

				static void SetColorMask(bool r, bool g, bool b, bool a)
				{
					glColorMask(r, g, b, a);
				}

				static void EnableAlphaCoverage(void)
				{
					glEnable(GL_SAMPLE_ALPHA_TO_COVERAGE);
				}

				static void DisableAlphaCoverage(void)
				{
					glDisable(GL_SAMPLE_ALPHA_TO_COVERAGE);
				}

				static void EnableSampleShading(void)
				{
					glEnable(GL_SAMPLE_SHADING);
				}

				static void DisableSampleShading(void)
				{
					glDisable(GL_SAMPLE_SHADING);
				}

				static void SetMinSampleShading(float value)
				{
					glMinSampleShading(value);
				}

				static void EnableDepthTest(void)
				{
					glEnable(GL_DEPTH_TEST);
				}

				static void DisableDepthTest(void)
				{
					glDisable(GL_DEPTH_TEST);
				}

				static void SetDepthFunc(unsigned_int32 func)
				{
					glDepthFunc(func);
				}

				static void SetDepthMask(bool mask)
				{
					glDepthMask(mask);
				}

				static void EnableDepthClamp(void)
				{
					glEnable(GL_DEPTH_CLAMP);
				}

				static void DisableDepthClamp(void)
				{
					glDisable(GL_DEPTH_CLAMP);
				}

				static void EnableDepthBoundsTest(void)
				{
					glEnable(GL_DEPTH_BOUNDS_TEST_EXT);
				}

				static void DisableDepthBoundsTest(void)
				{
					glDisable(GL_DEPTH_BOUNDS_TEST_EXT);
				}

				static void SetDepthBounds(float zmin, float zmax)
				{
					glDepthBoundsEXT(zmin, zmax);
				}

				static void EnableStencilTest(void)
				{
					glEnable(GL_STENCIL_TEST);
				}

				static void DisableStencilTest(void)
				{
					glDisable(GL_STENCIL_TEST);
				}

				static void SetStencilFunc(unsigned_int32 func, unsigned_int32 ref, unsigned_int32 mask)
				{
					glStencilFunc(func, ref, mask);
				}

				static void SetStencilOp(unsigned_int32 fail, unsigned_int32 zfail, unsigned_int32 zpass)
				{
					glStencilOp(fail, zfail, zpass);
				}

				static void SetFrontStencilOp(unsigned_int32 fail, unsigned_int32 zfail, unsigned_int32 zpass)
				{
					glStencilOpSeparate(GL_FRONT, fail, zfail, zpass);
				}

				static void SetBackStencilOp(unsigned_int32 fail, unsigned_int32 zfail, unsigned_int32 zpass)
				{
					glStencilOpSeparate(GL_BACK, fail, zfail, zpass);
				}

				static void SetStencilMask(unsigned_int32 mask)
				{
					glStencilMask(mask);
				}

				static void EnableBlend(void)
				{
					glEnable(GL_BLEND);
				}

				static void DisableBlend(void)
				{
					glDisable(GL_BLEND);
				}

				static void SetBlendFunc(unsigned_int32 srcFunc, unsigned_int32 dstFunc)
				{
					glBlendFunc(srcFunc, dstFunc);
				}

				static void SetBlendFunc(unsigned_int32 srcRgbFunc, unsigned_int32 dstRgbFunc, unsigned_int32 srcAlphaFunc, unsigned_int32 dstAlphaFunc)
				{
					glBlendFuncSeparate(srcRgbFunc, dstRgbFunc, srcAlphaFunc, dstAlphaFunc);
				}

				static void SetBlendEquation(unsigned_int32 equation)
				{
					glBlendEquation(equation);
				}

				static void SetBlendColor(float r, float g, float b, float a)
				{
					glBlendColor(r, g, b, a);
				}

				static void EnableCullFace(void)
				{
					glEnable(GL_CULL_FACE);
				}

				static void DisableCullFace(void)
				{
					glDisable(GL_CULL_FACE);
				}

				static void SetCullFace(unsigned_int32 face)
				{
					glCullFace(face);
				}

				static void SetFrontFace(unsigned_int32 front)
				{
					glFrontFace(front);
				}

				static void SetFillPolygonMode(void)
				{
					glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
				}

				static void SetLinePolygonMode(void)
				{
					glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
				}

				static void SetPointSize(float size)
				{
					glPointSize(size);
				}

				static void EnablePointSprite(void)
				{
					glEnable(GL_PROGRAM_POINT_SIZE);
				}

				static void DisablePointSprite(void)
				{
					glDisable(GL_PROGRAM_POINT_SIZE);
				}

				static void EnableLineSmooth(void)
				{
					glEnable(GL_LINE_SMOOTH);
				}

				static void DisableLineSmooth(void)
				{
					glDisable(GL_LINE_SMOOTH);
				}

				static void EnablePolygonLineOffset(void)
				{
					glEnable(GL_POLYGON_OFFSET_LINE);
				}

				static void DisablePolygonLineOffset(void)
				{
					glDisable(GL_POLYGON_OFFSET_LINE);
				}

				static void EnablePolygonFillOffset(void)
				{
					glEnable(GL_POLYGON_OFFSET_FILL);
				}

				static void DisablePolygonFillOffset(void)
				{
					glDisable(GL_POLYGON_OFFSET_FILL);
				}

				static void SetPolygonOffset(float slope, float bias)
				{
					glPolygonOffset(slope, bias);
				}

				static void EnableFrameBufferSRGB(void)
				{
					glEnable(GL_FRAMEBUFFER_SRGB);
				}

				static void DisableFrameBufferSRGB(void)
				{
					glDisable(GL_FRAMEBUFFER_SRGB);
				}

				static void ClearColorBuffer(float r, float g, float b, float a)
				{
					glClearColor(r, g, b, a);
					glClear(GL_COLOR_BUFFER_BIT);
				}

				static void ClearDepthBuffer(void)
				{
					glClear(GL_DEPTH_BUFFER_BIT);
				}

				static void ClearStencilBuffer(void)
				{
					glClear(GL_STENCIL_BUFFER_BIT);
				}

				static void ClearDepthStencilBuffers(void)
				{
					glClear(GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
				}

				static void ClearColorDepthStencilBuffers(float r, float g, float b, float a)
				{
					glClearColor(r, g, b, a);
					glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
				}

				static void EnableVertexAttribArray(unsigned_int32 index)
				{
					glEnableVertexAttribArray(index);
				}

				static void DisableVertexAttribArray(unsigned_int32 index)
				{
					glDisableVertexAttribArray(index);
				}

				static void SetViewport(int32 x, int32 y, int32 width, int32 height)
				{
					glViewport(x, y, width, height);
				}

				static void SetScissor(int32 x, int32 y, int32 width, int32 height)
				{
					glScissor(x, y, width, height);
				}

				static void BindTexture(unsigned_int32 unit, const TextureObject *texture)
				{
					texture->Bind(unit);
				}

				static void ResetFrameBuffer(void)
				{
					renderState.drawFrameBuffer = nullptr;
					renderState.readFrameBuffer = nullptr;
					glBindFramebuffer(GL_FRAMEBUFFER, 0);
				}

				static void SetFrameBuffer(const FrameBufferObject *frameBuffer)
				{
					frameBuffer->Bind();
				}

				static void SetDrawFrameBuffer(const FrameBufferObject *frameBuffer)
				{
					if (renderState.drawFrameBuffer != frameBuffer)
					{
						renderState.drawFrameBuffer = frameBuffer;
						glBindFramebuffer(GL_DRAW_FRAMEBUFFER, frameBuffer->GetFrameBufferIdentifier());
					}
				}

				static void SetReadFrameBuffer(const FrameBufferObject *frameBuffer)
				{
					if (renderState.readFrameBuffer != frameBuffer)
					{
						renderState.readFrameBuffer = frameBuffer;
						glBindFramebuffer(GL_READ_FRAMEBUFFER, frameBuffer->GetFrameBufferIdentifier());
					}
				}

				static void InvalidateReadFrameBuffer(void)
				{
					static const GLenum attachment = GL_COLOR_ATTACHMENT0;
					glInvalidateFramebuffer(GL_READ_FRAMEBUFFER, 1, &attachment);
				}

				static void ResolveMultisampleFrameBuffer(unsigned_int32 left, unsigned_int32 bottom, unsigned_int32 right, unsigned_int32 top)
				{
					glBlitFramebuffer(left, bottom, right, top, left, bottom, right, top, GL_COLOR_BUFFER_BIT, GL_LINEAR);
				}

				static void CopyFrameBuffer(unsigned_int32 srcX1, unsigned_int32 srcY1, unsigned_int32 srcX2, unsigned_int32 srcY2, unsigned_int32 dstX1, unsigned_int32 dstY1, unsigned_int32 dstX2, unsigned_int32 dstY2, unsigned_int32 filter)
				{
					glBlitFramebuffer(srcX1, srcY1, srcX2, srcY2, dstX1, dstY1, dstX2, dstY2, GL_COLOR_BUFFER_BIT, filter);
				}

				static void QueryTimeStamp(QueryObject *query);
				static void BeginOcclusionQuery(QueryObject *query);
				static void BeginConditionalRender(const QueryObject *query);
				static unsigned_int64 GetQueryTimeStamp(const QueryObject *query);
				static unsigned_int32 GetQuerySamplesPassed(const QueryObject *query);

				static void EndOcclusionQuery(QueryObject *query)
				{
					glEndQuery(GL_SAMPLES_PASSED);
				}

				static void EndConditionalRender(void)
				{
					glEndConditionalRender();
				}

				static void SetShaderProgram(const ShaderProgramObject *shaderProgram);
				static void SetVertexShaderParameter(unsigned_int32 index, float x, float y, float z, float w);
				static void SetVertexShaderParameter(unsigned_int32 index, const float *v);
				static void SetFragmentShaderParameter(unsigned_int32 index, float x, float y, float z, float w);
				static void SetFragmentShaderParameter(unsigned_int32 index, const float *v);
				static void SetGeometryShaderParameter(unsigned_int32 index, float x, float y, float z, float w);
				static void SetGeometryShaderParameter(unsigned_int32 index, const float *v);

				static void SetShaderTextureUnit(const ShaderProgramObject *shaderProgram, const char *name, int32 unit)
				{
					glUniform1i(shaderProgram->QueryUniformLocation(name), unit);
				}

				static void SetVertexAttribArray(unsigned_int32 index, int32 size, int32 type, int32 stride, const VertexBufferObject *buffer, unsigned_int32 offset)
				{
					glVertexArrayVertexAttribOffsetEXT(renderState.vertexArrayObject, buffer->vertexBufferIdentifier, index, size, type, false, stride, offset);
				}

				static void SetVertexAttribArray(unsigned_int32 index, int32 size, int32 type, bool normalized, int32 stride, const VertexBufferObject *buffer, unsigned_int32 offset)
				{
					glVertexArrayVertexAttribOffsetEXT(renderState.vertexArrayObject, buffer->vertexBufferIdentifier, index, size, type, normalized, stride, offset);
				}

				static void DrawPrimitives(unsigned_int32 prim, unsigned_int32 start, unsigned_int32 count)
				{
					Update();
					glDrawArrays(prim, start, count);
				}

				static void MultiDrawPrimitives(unsigned_int32 prim, const unsigned_int32 *startArray, const unsigned_int32 *countArray, unsigned_int32 size)
				{
					Update();
					glMultiDrawArrays(prim, reinterpret_cast<const GLint *>(startArray), reinterpret_cast<const GLsizei *>(countArray), size);
				}

				static void DrawIndexedPrimitives(unsigned_int32 prim, unsigned_int32 range, unsigned_int32 count, const VertexBufferObject *buffer, unsigned_int32 offset);
				static void MultiDrawIndexedPrimitives(unsigned_int32 prim, const unsigned_int32 *countArray, const VertexBufferObject *buffer, const machine_address *offsetArray, unsigned_int32 size);
				static void DrawQuads(unsigned_int32 start, unsigned_int32 count);

				// If GL_EXT_direct_state_access is not available, then the DSA functions used by the engine
				// get remapped to the following functions in the Render namespace.

				#if C4WINDOWS

					#define OPENGLCALL APIENTRY

				#else

					#define OPENGLCALL

				#endif

				static void OPENGLCALL BindMultiTexture(GLenum texunit, GLenum target, GLuint texture);
				static void OPENGLCALL TextureParameteri(GLuint texture, GLenum target, GLenum pname, GLint param);
				static void OPENGLCALL TextureParameteriv(GLuint texture, GLenum target, GLenum pname, const GLint *param);
				static void OPENGLCALL TextureParameterf(GLuint texture, GLenum target, GLenum pname, GLfloat param);
				static void OPENGLCALL TextureParameterfv(GLuint texture, GLenum target, GLenum pname, const GLfloat *param);
				static void OPENGLCALL TextureImage2D(GLuint texture, GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const void *pixels);
				static void OPENGLCALL TextureSubImage2D(GLuint texture, GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const void *pixels);
				static void OPENGLCALL TextureImage3D(GLuint texture, GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLenum format, GLenum type, const void *pixels);
				static void OPENGLCALL TextureSubImage3D(GLuint texture, GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const void *pixels);
				static void OPENGLCALL CompressedTextureImage2D(GLuint texture, GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLint border, GLsizei imageSize, const void *data);
				static void OPENGLCALL CompressedTextureImage3D(GLuint texture, GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLsizei imageSize, const void *data);
				static void OPENGLCALL CopyTextureSubImage2D(GLuint texture, GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint x, GLint y, GLsizei width, GLsizei height);
				static void OPENGLCALL NamedBufferData(GLuint buffer, GLsizeiptr size, const void *data, GLenum usage);
				static void OPENGLCALL NamedBufferSubData(GLuint buffer, GLintptr offset, GLsizeiptr size, const void *data);
				static void OPENGLCALL GetNamedBufferSubData(GLuint buffer, GLintptr offset, GLsizeiptr size, void *data);
				static void *OPENGLCALL MapNamedBuffer(GLuint buffer, GLenum access);
				static void *OPENGLCALL MapNamedBufferRange(GLuint buffer, GLintptr offset, GLsizeiptr length, GLbitfield access);
				static GLboolean OPENGLCALL UnmapNamedBuffer(GLuint buffer);
				static void *OPENGLCALL MapBufferRange(GLenum target, GLintptr offset, GLsizeiptr length, GLbitfield access);
				static void OPENGLCALL ProgramUniform4fv(GLuint program, GLint location, GLsizei count, const GLfloat *param);
				static void OPENGLCALL VertexArrayVertexAttribOffset(GLuint vaobj, GLuint buffer, GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, GLintptr offset);
				static void OPENGLCALL InvalidateTexImage(GLuint texture, GLint level);
				static void OPENGLCALL InvalidateBufferData(GLuint buffer);
				static void OPENGLCALL InvalidateFramebuffer(GLenum target, GLsizei numAttachments, const GLenum *attachments);

		#elif C4ORBIS //[ 

			// -- Orbis code hidden --

		#elif C4PS3 //[ 

			// -- PS3 code hidden --

		#endif //]
	};
}


#endif

// ZYUTNLM
