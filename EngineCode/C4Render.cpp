//=============================================================
//
// C4 Engine version 3.5.1
// Copyright 1999-2013, by Terathon Software LLC
//
// This copy is licensed to the following:
//
//     Registered user: Université de Sherbrooke
//     Maximum number of users: Site License
//     License #C4T0033897
//
// License is granted under terms of the license agreement
// entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#include "C4Render.h"

#if C4GAMECONSOLE

	#include "C4Files.h"
	#include "C4Threads.h"

#endif


using namespace C4;


Render::RenderState Render::renderState;


#if C4OPENGL

	namespace
	{
		enum
		{
			kUpdateVertexShaderParameters		= 1 << 0,
			kUpdateFragmentShaderParameters		= 1 << 1,
			kUpdateGeometryShaderParameters		= 1 << 2
		};


		struct TextureFormatData
		{
			GLenum				internalFormat[Render::kTextureEncodingCount];
			GLint				textureSwizzle[4];
			GLenum				pixelFormat;
			GLenum				pixelType;
			unsigned_int32		pixelSize;
		};

		static const TextureFormatData textureFormatData[Render::kTextureFormatCount] =
		{
			{{GL_RGB8, GL_SRGB8},															{GL_RED, GL_GREEN, GL_BLUE, GL_ONE},		GL_RGBA,				GL_UNSIGNED_BYTE,				4},			// kTextureRGBX8
			{{GL_RGBA8, GL_SRGB8_ALPHA8},													{GL_RED, GL_GREEN, GL_BLUE, GL_ALPHA},		GL_RGBA,				GL_UNSIGNED_BYTE,				4},			// kTextureRGBA8
			{{GL_RGB8, GL_SRGB8},															{GL_RED, GL_GREEN, GL_BLUE, GL_ONE},		GL_BGRA,				GL_UNSIGNED_BYTE,				4},			// kTextureBGRX8
			{{GL_RGBA8, GL_SRGB8_ALPHA8},													{GL_RED, GL_GREEN, GL_BLUE, GL_ALPHA},		GL_BGRA,				GL_UNSIGNED_BYTE,				4},			// kTextureBGRA8
			{{GL_RGB8, GL_SRGB8},															{GL_RED, GL_GREEN, GL_BLUE, GL_ONE},		GL_RGBA,				GL_UNSIGNED_INT_8_8_8_8_REV,	4},			// kTextureXRGB8
			{{GL_RGBA8, GL_SRGB8_ALPHA8},													{GL_RED, GL_GREEN, GL_BLUE, GL_ALPHA},		GL_RGBA,				GL_UNSIGNED_INT_8_8_8_8_REV,	4},			// kTextureARGB8
			{{GL_R8, GL_R8},																{GL_RED, GL_ZERO, GL_ZERO, GL_ONE},			GL_RED,					GL_UNSIGNED_BYTE,				1},			// kTextureR8
			{{GL_RG8, GL_RG8},																{GL_RED, GL_GREEN, GL_ZERO, GL_ZERO},		GL_RG,					GL_UNSIGNED_BYTE,				2},			// kTextureRG8
			{{GL_R8, GL_R8},																{GL_RED, GL_RED, GL_RED, GL_ONE},			GL_RED,					GL_UNSIGNED_BYTE,				1},			// kTextureL8
			{{GL_RG8, GL_RG8},																{GL_RED, GL_RED, GL_RED, GL_GREEN},			GL_RG,					GL_UNSIGNED_BYTE,				2},			// kTextureLA8
			{{GL_R8, GL_R8},																{GL_RED, GL_RED, GL_RED, GL_RED},			GL_RED,					GL_UNSIGNED_BYTE,				1},			// kTextureI8
			{{GL_DEPTH_COMPONENT24, GL_DEPTH_COMPONENT24},									{GL_RED, GL_RED, GL_RED, GL_ONE},			GL_DEPTH_COMPONENT,		GL_UNSIGNED_INT,				4},			// kTextureDepth
			{{GL_COMPRESSED_RGB_S3TC_DXT1_EXT, GL_COMPRESSED_SRGB_S3TC_DXT1_EXT},			{GL_RED, GL_GREEN, GL_BLUE, GL_ALPHA},		GL_RGBA,				GL_UNSIGNED_BYTE,				8},			// kTextureBC1
			{{GL_COMPRESSED_RGBA_S3TC_DXT5_EXT, GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT5_EXT},	{GL_RED, GL_GREEN, GL_BLUE, GL_ALPHA},		GL_RGBA,				GL_UNSIGNED_BYTE,				16},		// kTextureBC3
			{{GL_R8, GL_R8},																{GL_RED, GL_RED, GL_RED, GL_RED},			GL_RED,					GL_UNSIGNED_BYTE,				1},			// kTextureRenderBufferR8
			{{GL_RGB8, GL_RGB8},															{GL_RED, GL_GREEN, GL_BLUE, GL_ONE},		GL_RGBA,				GL_UNSIGNED_BYTE,				4},			// kTextureRenderBufferRGB8
			{{GL_RGBA8, GL_RGBA8},															{GL_RED, GL_GREEN, GL_BLUE, GL_ALPHA},		GL_RGBA,				GL_UNSIGNED_BYTE,				4},			// kTextureRenderBufferRGBA8
			{{GL_RGBA16F, GL_RGBA16F},														{GL_RED, GL_GREEN, GL_BLUE, GL_ALPHA},		GL_RGBA,				GL_HALF_FLOAT,					8}			// kTextureRenderBufferRGBA16F
		};
	}


	Render::VertexBufferObject Render::quadIndexBuffer(Render::kVertexBufferTargetIndex, Render::kVertexBufferUsageStatic);


	void Render::TextureObject::Construct(unsigned_int32 index)
	{
		static const unsigned_int16 target[kTextureTargetCount] =
		{
			GL_TEXTURE_2D, GL_TEXTURE_3D, GL_TEXTURE_RECTANGLE, GL_TEXTURE_CUBE_MAP, GL_TEXTURE_2D_ARRAY
		};

		targetIndex = (unsigned_int16) index;
		openglTarget = target[index];

		glGenTextures(1, &textureIdentifier);
	}

	void Render::TextureObject::Destruct(void)
	{
		glDeleteTextures(1, &textureIdentifier);

		for (unsigned_machine unit = 0; unit < kMaxTextureUnitCount; unit++)
		{
			if (renderState.texture[unit][targetIndex] == textureIdentifier)
			{
				renderState.texture[unit][targetIndex] = 0;
			}
		}
	}

	void Render::TextureObject::SetBorderColor(unsigned_int32 color)
	{
		static const ConstColorRGBA colorTable[3] =
		{
			{0.0F, 0.0F, 0.0F, 0.0F}, {0.0F, 0.0F, 0.0F, 1.0F}, {1.0F, 1.0F, 1.0F, 1.0F}
		};

		glTextureParameterfvEXT(textureIdentifier, openglTarget, GL_TEXTURE_BORDER_COLOR, &colorTable[color].red);
	}

	void Render::TextureObject::Bind(unsigned_int32 unit) const 
	{
		GLuint *object = &renderState.texture[unit][targetIndex];
		if (*object != textureIdentifier) 
		{
			*object = textureIdentifier; 
			glBindMultiTextureEXT(GL_TEXTURE0 + unit, openglTarget, textureIdentifier); 
		}
	}

	void Render::TextureObject::Unbind(unsigned_int32 unit) const 
	{
		GLuint *object = &renderState.texture[unit][targetIndex];
		if (*object == textureIdentifier)
		{
			*object = 0; 
			glBindMultiTextureEXT(GL_TEXTURE0 + unit, openglTarget, 0);
		}
	}

	void Render::TextureObject::UnbindAll(void) const
	{
		for (machine a = 0; a < kMaxTextureUnitCount; a++)
		{
			GLuint *object = &renderState.texture[a][targetIndex];
			if (*object == textureIdentifier)
			{
				*object = 0;
				glBindMultiTextureEXT(GL_TEXTURE0 + a, openglTarget, 0);
			}
		}
	}

	void Render::TextureObject::SetImage2D(const TextureUploadData *uploadData)
	{
		glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);

		unsigned_int32 format = uploadData->format;
		const TextureFormatData *formatData = &textureFormatData[format];
		formatIndex = format;

		char *storage = nullptr;
		unsigned_int32 storageSize = 0;

		unsigned_int32 width = uploadData->width;
		unsigned_int32 height = uploadData->height;
		GLenum internal = formatData->internalFormat[uploadData->encoding];
		int32 count = uploadData->mipmapCount;
		const TextureImageData *imageData = uploadData->imageData;

		for (machine level = 0; level < count; level++)
		{
			unsigned_int32 pixelCount = width * height;
			unsigned_int32 size = pixelCount * formatData->pixelSize;
			storageSize += size;

			const void *image = imageData->image;
			if (imageData->decompressor)
			{
				if (!storage)
				{
					storage = new char[size];
				}

				(*imageData->decompressor)(static_cast<const unsigned_int8 *>(image), imageData->size, storage);
				image = storage;
			}

			glTextureImage2DEXT(textureIdentifier, GL_TEXTURE_2D, level, internal, width, height, 0, formatData->pixelFormat, formatData->pixelType, image);

			width = Max(width >> 1, 1);
			height = Max(height >> 1, 1);

			imageData++;
		}

		*uploadData->memorySize = storageSize;
		delete[] storage;

		#if !C4MACOS

			glTextureParameterivEXT(textureIdentifier, GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_RGBA, formatData->textureSwizzle);

		#endif
	}

	void Render::TextureObject::SetImage3D(const TextureUploadData *uploadData)
	{
		glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);

		unsigned_int32 format = uploadData->format;
		const TextureFormatData *formatData = &textureFormatData[format];
		formatIndex = format;

		char *storage = nullptr;
		unsigned_int32 storageSize = 0;

		unsigned_int32 width = uploadData->width;
		unsigned_int32 height = uploadData->height;
		unsigned_int32 depth = uploadData->depth;
		GLenum internal = formatData->internalFormat[uploadData->encoding];
		int32 count = uploadData->mipmapCount;
		const TextureImageData *imageData = uploadData->imageData;

		for (machine level = 0; level < count; level++)
		{
			unsigned_int32 pixelCount = width * height * depth;
			unsigned_int32 size = pixelCount * formatData->pixelSize;
			storageSize += size;

			const void *image = imageData->image;
			if (imageData->decompressor)
			{
				if (!storage)
				{
					storage = new char[size];
				}

				(*imageData->decompressor)(static_cast<const unsigned_int8 *>(image), imageData->size, storage);
				image = storage;
			}

			glTextureImage3DEXT(textureIdentifier, GL_TEXTURE_3D, level, internal, width, height, depth, 0, formatData->pixelFormat, formatData->pixelType, image);

			width = Max(width >> 1, 1);
			height = Max(height >> 1, 1);
			depth = Max(depth >> 1, 1);

			imageData++;
		}

		*uploadData->memorySize = storageSize;
		delete[] storage;

		#if !C4MACOS

			glTextureParameterivEXT(textureIdentifier, GL_TEXTURE_3D, GL_TEXTURE_SWIZZLE_RGBA, formatData->textureSwizzle);

		#endif
	}

	void Render::TextureObject::SetImageCube(const TextureUploadData *uploadData)
	{
		glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);

		unsigned_int32 format = uploadData->format;
		const TextureFormatData *formatData = &textureFormatData[format];
		formatIndex = format;

		char *storage = nullptr;
		unsigned_int32 storageSize = 0;

		unsigned_int32 width = uploadData->width;
		GLenum internal = formatData->internalFormat[uploadData->encoding];
		int32 count = uploadData->mipmapCount;
		const TextureImageData *imageData = uploadData->imageData;

		for (machine level = 0; level < count; level++)
		{
			for (machine component = 0; component < 6; component++)
			{
				unsigned_int32 pixelCount = width * width;
				unsigned_int32 size = pixelCount * formatData->pixelSize;
				storageSize += size;

				const void *image = imageData->image;
				if (imageData->decompressor)
				{
					if (!storage)
					{
						storage = new char[size];
					}

					(*imageData->decompressor)(static_cast<const unsigned_int8 *>(image), imageData->size, storage);
					image = storage;
				}

				glTextureImage2DEXT(textureIdentifier, GL_TEXTURE_CUBE_MAP_POSITIVE_X + component, level, internal, width, width, 0, formatData->pixelFormat, formatData->pixelType, image);
				imageData++;
			}

			width >>= 1;
		}

		*uploadData->memorySize = storageSize;
		delete[] storage;

		#if !C4MACOS

			glTextureParameterivEXT(textureIdentifier, GL_TEXTURE_CUBE_MAP, GL_TEXTURE_SWIZZLE_RGBA, formatData->textureSwizzle);

		#endif
	}

	void Render::TextureObject::SetImageRect(const TextureUploadData *uploadData)
	{
		unsigned_int32 format = uploadData->format;
		const TextureFormatData *formatData = &textureFormatData[format];
		formatIndex = format;

		char *storage = nullptr;

		unsigned_int32 width = uploadData->width;
		unsigned_int32 height = uploadData->height;
		GLenum internal = formatData->internalFormat[uploadData->encoding];
		const TextureImageData *imageData = uploadData->imageData;

		int32 pixelCount = width * height;
		unsigned_int32 storageSize = pixelCount * formatData->pixelSize;

		const void *image = imageData->image;
		if (imageData->decompressor)
		{
			if (!storage)
			{
				storage = new char[storageSize];
			}

			(*imageData->decompressor)(static_cast<const unsigned_int8 *>(image), imageData->size, storage);
			image = storage;
		}

		glPixelStorei(GL_UNPACK_ROW_LENGTH, uploadData->rowLength);
		glTextureImage2DEXT(textureIdentifier, GL_TEXTURE_RECTANGLE, 0, internal, width, height, 0, formatData->pixelFormat, formatData->pixelType, image);

		*uploadData->memorySize = storageSize;
		delete[] storage;

		#if !C4MACOS

			glTextureParameterivEXT(textureIdentifier, GL_TEXTURE_RECTANGLE, GL_TEXTURE_SWIZZLE_RGBA, formatData->textureSwizzle);

		#endif
	}

	void Render::TextureObject::SetImageArray2D(const TextureUploadData *uploadData)
	{
		glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);

		unsigned_int32 format = uploadData->format;
		const TextureFormatData *formatData = &textureFormatData[format];
		formatIndex = format;

		char *storage = nullptr;
		unsigned_int32 storageSize = 0;

		unsigned_int32 width = uploadData->width;
		unsigned_int32 height = uploadData->height;
		unsigned_int32 depth = uploadData->depth;
		GLenum internal = formatData->internalFormat[uploadData->encoding];
		int32 count = uploadData->mipmapCount;
		const TextureImageData *imageData = uploadData->imageData;

		for (machine level = 0; level < count; level++)
		{
			unsigned_int32 pixelCount = width * height * depth;
			unsigned_int32 size = pixelCount * formatData->pixelSize;
			storageSize += size;

			const void *image = imageData->image;
			if (imageData->decompressor)
			{
				if (!storage)
				{
					storage = new char[size];
				}

				(*imageData->decompressor)(static_cast<const unsigned_int8 *>(image), imageData->size, storage);
				image = storage;
			}

			glTextureImage3DEXT(textureIdentifier, GL_TEXTURE_2D_ARRAY, level, internal, width, height, depth, 0, formatData->pixelFormat, formatData->pixelType, image);

			width = Max(width >> 1, 1);
			height = Max(height >> 1, 1);

			imageData++;
		}

		*uploadData->memorySize = storageSize;
		delete[] storage;

		#if !C4MACOS

			glTextureParameterivEXT(textureIdentifier, GL_TEXTURE_2D_ARRAY, GL_TEXTURE_SWIZZLE_RGBA, formatData->textureSwizzle);

		#endif
	}

	void Render::TextureObject::SetCompressedImage2D(const TextureUploadData *uploadData)
	{
		unsigned_int32 format = uploadData->format;
		const TextureFormatData *formatData = &textureFormatData[format];
		formatIndex = format;

		char *storage = nullptr;
		unsigned_int32 storageSize = 0;

		unsigned_int32 width = uploadData->width;
		unsigned_int32 height = uploadData->height;
		GLenum internal = formatData->internalFormat[uploadData->encoding];
		int32 count = uploadData->mipmapCount;
		const TextureImageData *imageData = uploadData->imageData;

		for (machine level = 0; level < count; level++)
		{
			const void *image = imageData->image;
			unsigned_int32 size = imageData->size;

			if (imageData->decompressor)
			{
				unsigned_int32 blockCount = ((width + 3) / 4) * ((height + 3) / 4);
				size = blockCount * formatData->pixelSize;
				if (!storage)
				{
					storage = new char[size];
				}

				(*imageData->decompressor)(static_cast<const unsigned_int8 *>(image), imageData->size, storage);
				image = storage;
			}

			storageSize += size;
			glCompressedTextureImage2DEXT(textureIdentifier, GL_TEXTURE_2D, level, internal, width, height, 0, size, image);

			width = Max(width >> 1, 1);
			height = Max(height >> 1, 1);

			imageData++;
		}

		*uploadData->memorySize = storageSize;
		delete[] storage;
	}

	void Render::TextureObject::SetCompressedImageCube(const TextureUploadData *uploadData)
	{
		unsigned_int32 format = uploadData->format;
		const TextureFormatData *formatData = &textureFormatData[format];
		formatIndex = format;

		char *storage = nullptr;
		unsigned_int32 storageSize = 0;

		unsigned_int32 width = uploadData->width;
		GLenum internal = formatData->internalFormat[uploadData->encoding];
		int32 count = uploadData->mipmapCount;
		const TextureImageData *imageData = uploadData->imageData;

		for (machine level = 0; level < count; level++)
		{
			for (machine component = 0; component < 6; component++)
			{
				const void *image = imageData->image;
				unsigned_int32 size = imageData->size;

				if (imageData->decompressor)
				{
					unsigned_int32 w = (width + 3) / 4;
					unsigned_int32 blockCount = w * w;
					size = blockCount * formatData->pixelSize;
					if (!storage)
					{
						storage = new char[size];
					}

					(*imageData->decompressor)(static_cast<const unsigned_int8 *>(image), imageData->size, storage);
					image = storage;
				}

				storageSize += size;
				glCompressedTextureImage2DEXT(textureIdentifier, GL_TEXTURE_CUBE_MAP_POSITIVE_X + component, level, internal, width, width, 0, size, image);

				imageData++;
			}

			width >>= 1;
		}

		*uploadData->memorySize = storageSize;
		delete[] storage;
	}

	void Render::TextureObject::SetCompressedImageArray2D(const TextureUploadData *uploadData)
	{
		unsigned_int32 format = uploadData->format;
		const TextureFormatData *formatData = &textureFormatData[format];
		formatIndex = format;

		char *storage = nullptr;
		unsigned_int32 storageSize = 0;

		unsigned_int32 width = uploadData->width;
		unsigned_int32 height = uploadData->height;
		unsigned_int32 depth = uploadData->depth;
		GLenum internal = formatData->internalFormat[uploadData->encoding];
		int32 count = uploadData->mipmapCount;
		const TextureImageData *imageData = uploadData->imageData;

		for (machine level = 0; level < count; level++)
		{
			const void *image = imageData->image;
			unsigned_int32 size = imageData->size;

			if (imageData->decompressor)
			{
				unsigned_int32 blockCount = ((width + 3) / 4) * ((height + 3) / 4) * depth;
				size = blockCount * formatData->pixelSize;
				if (!storage)
				{
					storage = new char[size];
				}

				(*imageData->decompressor)(static_cast<const unsigned_int8 *>(image), imageData->size, storage);
				image = storage;
			}

			storageSize += size;
			glCompressedTextureImage3DEXT(textureIdentifier, GL_TEXTURE_2D_ARRAY, level, internal, width, height, depth, 0, size, image);

			width = Max(width >> 1, 1);
			height = Max(height >> 1, 1);

			imageData++;
		}

		*uploadData->memorySize = storageSize;
		delete[] storage;
	}

	void Render::TextureObject::AllocateStorage2D(const TextureAllocationData *allocationData)
	{
		unsigned_int32 format = allocationData->format;
		const TextureFormatData *formatData = &textureFormatData[format];
		formatIndex = format;

		SetMinFilterMode(Render::kFilterLinear);
		SetMagFilterMode(Render::kFilterLinear);

		unsigned_int32 width = allocationData->width;
		unsigned_int32 height = allocationData->height;
		GLenum internal = formatData->internalFormat[allocationData->encoding];

		glTextureImage2DEXT(textureIdentifier, GL_TEXTURE_2D, 0, internal, width, height, 0, formatData->pixelFormat, formatData->pixelType, nullptr);

		#if !C4MACOS

			glTextureParameterivEXT(textureIdentifier, GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_RGBA, formatData->textureSwizzle);

		#endif

		Unbind(0);

		unsigned_int32 *memorySize = allocationData->memorySize;
		if (memorySize)
		{
			*memorySize = width * height * formatData->pixelSize;
		}
	}

	void Render::TextureObject::AllocateStorageRect(const TextureAllocationData *allocationData)
	{
		unsigned_int32 format = allocationData->format;
		const TextureFormatData *formatData = &textureFormatData[format];
		formatIndex = format;

		SetMinFilterMode(Render::kFilterLinear);
		SetMagFilterMode(Render::kFilterLinear);

		unsigned_int32 width = allocationData->width;
		unsigned_int32 height = allocationData->height;
		GLenum internal = formatData->internalFormat[allocationData->encoding];

		glTextureImage2DEXT(textureIdentifier, GL_TEXTURE_RECTANGLE, 0, internal, width, height, 0, formatData->pixelFormat, formatData->pixelType, nullptr);

		#if !C4MACOS

			glTextureParameterivEXT(textureIdentifier, GL_TEXTURE_RECTANGLE, GL_TEXTURE_SWIZZLE_RGBA, formatData->textureSwizzle);

		#endif

		Unbind(0);

		unsigned_int32 *memorySize = allocationData->memorySize;
		if (memorySize)
		{
			*memorySize = width * height * formatData->pixelSize;
		}
	}

	void Render::TextureObject::UpdateImage2D(unsigned_int32 width, unsigned_int32 height, int32 count, const void *image) const
	{
		glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);

		const TextureFormatData *formatData = &textureFormatData[formatIndex];

		for (machine level = 0; level < count; level++)
		{
			glTextureSubImage2DEXT(textureIdentifier, GL_TEXTURE_2D, level, 0, 0, width, height, formatData->pixelFormat, formatData->pixelType, image);

			image = static_cast<const char *>(image) + width * height * formatData->pixelSize;

			width = Max(width >> 1, 1);
			height = Max(height >> 1, 1);
		}
	}

	void Render::TextureObject::UpdateImage2D(unsigned_int32 x, unsigned_int32 y, unsigned_int32 width, unsigned_int32 height, unsigned_int32 rowLength, const void *image) const
	{
		glPixelStorei(GL_UNPACK_ROW_LENGTH, rowLength);

		const TextureFormatData *formatData = &textureFormatData[formatIndex];
		image = static_cast<const char *>(image) + (rowLength * y + x) * formatData->pixelSize;
		glTextureSubImage2DEXT(textureIdentifier, GL_TEXTURE_2D, 0, x, y, width, height, formatData->pixelFormat, formatData->pixelType, image);
	}

	void Render::TextureObject::UpdateImage3D(unsigned_int32 x, unsigned_int32 y, unsigned_int32 z, unsigned_int32 width, unsigned_int32 height, unsigned_int32 depth, const void *image) const
	{
		glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);

		const TextureFormatData *formatData = &textureFormatData[formatIndex];
		image = static_cast<const char *>(image) + (width * (height * z + y) + x) * formatData->pixelSize;
		glTextureSubImage3DEXT(textureIdentifier, GL_TEXTURE_3D, 0, x, y, z, width, height, depth, formatData->pixelFormat, formatData->pixelType, image);
	}

	void Render::TextureObject::UpdateImageRect(unsigned_int32 x, unsigned_int32 y, unsigned_int32 width, unsigned_int32 height, unsigned_int32 rowLength, const void *image) const
	{
		glPixelStorei(GL_UNPACK_ROW_LENGTH, rowLength);

		const TextureFormatData *formatData = &textureFormatData[formatIndex];
		image = static_cast<const char *>(image) + (rowLength * y + x) * formatData->pixelSize;
		glTextureSubImage2DEXT(textureIdentifier, GL_TEXTURE_RECTANGLE, 0, x, y, width, height, formatData->pixelFormat, formatData->pixelType, image);
	}


	void Render::RenderBufferObject::Construct(void)
	{
		glGenRenderbuffers(1, &renderBufferIdentifier);
	}

	void Render::RenderBufferObject::Destruct(void)
	{
		glDeleteRenderbuffers(1, &renderBufferIdentifier);
	}

	void Render::RenderBufferObject::AllocateStorage(unsigned_int32 width, unsigned_int32 height, unsigned_int32 format)
	{
		glBindRenderbuffer(GL_RENDERBUFFER, renderBufferIdentifier);
		glRenderbufferStorage(GL_RENDERBUFFER, format, width, height);
		glBindRenderbuffer(GL_RENDERBUFFER, 0);
	}

	void Render::RenderBufferObject::AllocateMultisampleStorage(unsigned_int32 width, unsigned_int32 height, unsigned_int32 sampleCount, unsigned_int32 format)
	{
		glBindRenderbuffer(GL_RENDERBUFFER, renderBufferIdentifier);
		glRenderbufferStorageMultisample(GL_RENDERBUFFER, sampleCount, format, width, height);
		glBindRenderbuffer(GL_RENDERBUFFER, 0);
	}


	void Render::FrameBufferObject::Construct(void)
	{
		glGenFramebuffers(1, &frameBufferIdentifier);

		currentColorTexture = nullptr;
		currentDepthTexture = nullptr;
	}

	void Render::FrameBufferObject::Destruct(void)
	{
		glDeleteFramebuffers(1, &frameBufferIdentifier);
	}

	void Render::FrameBufferObject::SetColorRenderBuffer(const RenderBufferObject *renderBuffer)
	{
		Bind();
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, renderBuffer->GetRenderBufferIdentifier());
	}

	void Render::FrameBufferObject::SetDepthStencilRenderBuffer(const RenderBufferObject *renderBuffer)
	{
		Bind();
		GLuint renderBufferIdentifier = renderBuffer->GetRenderBufferIdentifier();
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, renderBufferIdentifier);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, renderBufferIdentifier);
	}

	void Render::FrameBufferObject::SetColorRenderTexture(const TextureObject *renderTexture)
	{
		if (currentColorTexture != renderTexture)
		{
			currentColorTexture = renderTexture;

			Bind();
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_RECTANGLE, renderTexture->GetTextureIdentifier(), 0);
		}
	}

	void Render::FrameBufferObject::ResetColorRenderTexture(void)
	{
		if (currentColorTexture)
		{
			currentColorTexture = nullptr;

			Bind();
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_RECTANGLE, 0, 0);
		}
	}

	void Render::FrameBufferObject::SetDepthRenderTexture(const TextureObject *renderTexture)
	{
		if (currentDepthTexture != renderTexture)
		{
			currentDepthTexture = renderTexture;

			Bind();
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, renderTexture->GetTextureIdentifier(), 0);

			glDrawBuffer(GL_NONE);
			glReadBuffer(GL_NONE);
		}
	}

	void Render::FrameBufferObject::ResetDepthRenderTexture(void)
	{
		if (currentDepthTexture)
		{
			currentDepthTexture = nullptr;

			Bind();
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, 0, 0);
		}
	}


	void Render::VertexBufferObject::Construct(void)
	{
		glGenBuffers(1, &vertexBufferIdentifier);

		staticBuffer = nullptr;
	}

	void Render::VertexBufferObject::Destruct(void)
	{
		delete[] staticBuffer;
		staticBuffer = nullptr;

		glDeleteBuffers(1, &vertexBufferIdentifier);

		if (renderState.attributeVertexBuffer == vertexBufferIdentifier)
		{
			renderState.attributeVertexBuffer = 0;
		}
		else if (renderState.indexVertexBuffer == vertexBufferIdentifier)
		{
			renderState.indexVertexBuffer = 0;
		}

		vertexBufferIdentifier = 0;
	}

	void Render::VertexBufferObject::AllocateStorage(const void *data)
	{
		if (vertexBufferIdentifier == 0)
		{
			Construct();
		}

		if ((bufferUsage != kVertexBufferUsageStatic) || (data))
		{
			#if C4DEBUG

				glGetError();

			#endif

			glNamedBufferDataEXT(vertexBufferIdentifier, bufferSize, data, bufferUsage);

			#if C4DEBUG

				Assert((glGetError() == GL_NO_ERROR), "Vertex memory allocation failed.\n");

			#endif
		}
		else
		{
			staticBuffer = new char[bufferSize];
		}
	}

	volatile void *Render::VertexBufferObject::BeginUpdate(void)
	{
		if (!staticBuffer)
		{
			return (glMapNamedBufferRangeEXT(vertexBufferIdentifier, 0, bufferSize, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT));
		}

		return (staticBuffer);
	}

	void Render::VertexBufferObject::EndUpdate(void)
	{
		if (!staticBuffer)
		{
			glUnmapNamedBufferEXT(vertexBufferIdentifier);
		}
		else
		{
			glNamedBufferDataEXT(vertexBufferIdentifier, bufferSize, staticBuffer, bufferUsage);

			delete[] staticBuffer;
			staticBuffer = nullptr;
		}
	}

	void Render::VertexBufferObject::UpdateBuffer(unsigned_int32 offset, unsigned_int32 size, const void *data)
	{
		Assert(!staticBuffer, "UpdateBuffer() cannot be called for static vertex buffers.\n");

		glNamedBufferSubDataEXT(vertexBufferIdentifier, offset, size, data);
	}

	void Render::VertexBufferObject::ReadBuffer(unsigned_int32 offset, unsigned_int32 size, void *data) const
	{
		glGetNamedBufferSubDataEXT(vertexBufferIdentifier, offset, size, data);
	}

	void Render::VertexBufferObject::BeginUpdateSync(volatile void **const *ptr)
	{
		if (!staticBuffer)
		{
			**ptr = glMapNamedBufferRangeEXT(vertexBufferIdentifier, 0, bufferSize, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
		}
		else
		{
			**ptr = staticBuffer;
		}
	}

	void Render::VertexBufferObject::EndUpdateSync(const void *)
	{
		if (!staticBuffer)
		{
			glUnmapNamedBufferEXT(vertexBufferIdentifier);
		}
		else
		{
			glNamedBufferDataEXT(vertexBufferIdentifier, bufferSize, staticBuffer, bufferUsage);

			delete[] staticBuffer;
			staticBuffer = nullptr;
		}
	}

	void Render::VertexBufferObject::UpdateBufferSync(const BufferUploadData *uploadData)
	{
		Assert(!staticBuffer, "UpdateBufferSync() cannot be called for static vertex buffers.\n");

		glNamedBufferSubDataEXT(vertexBufferIdentifier, uploadData->offset, uploadData->size, uploadData->data);
	}


	void Render::QueryObject::Construct(void)
	{
		queryIndex = 0;

		unsigned_int32 frame = renderState.frameCount - 4;
		queryFrame[0] = frame;
		queryFrame[1] = frame;
		queryFrame[2] = frame;
		queryFrame[3] = frame;

		glGenQueries(4, queryIdentifier);
	}

	void Render::QueryObject::Destruct(void)
	{
		glDeleteQueries(4, queryIdentifier);
	}


	Render::ShaderObject::ShaderObject()
	{
		compiledFlag = false;
	}

	Render::ShaderObject::~ShaderObject()
	{
		glDeleteShader(shaderIdentifier);
	}

	void Render::ShaderObject::Compile(void)
	{
		if (!compiledFlag)
		{
			compiledFlag = true;
			glCompileShader(shaderIdentifier);
		}
	}

	#if C4DEBUG

		bool Render::ShaderObject::GetShaderStatus(const char **error, const char **source) const
		{
			GLint	status;

			glGetShaderiv(shaderIdentifier, GL_COMPILE_STATUS, &status);
			if (!status)
			{
				GLint	size;

				glGetShaderiv(shaderIdentifier, GL_INFO_LOG_LENGTH, &size);
				char *string = new char[size];
				glGetShaderInfoLog(shaderIdentifier, size, nullptr, string);
				*error = string;

				glGetShaderiv(shaderIdentifier, GL_SHADER_SOURCE_LENGTH, &size);
				string = new char[size];
				glGetShaderSource(shaderIdentifier, size, nullptr, string);
				*source = string;

				return (false);
			}

			return (true);
		}

		void Render::ShaderObject::ReleaseShaderStatus(const char *error, const char *source) const
		{
			delete[] source;
			delete[] error;
		}

	#endif


	Render::VertexShaderObject::VertexShaderObject(const char *source, unsigned_int32 size)
	{
		shaderIdentifier = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(shaderIdentifier, 1, &source, reinterpret_cast<GLint *>(&size));
	}


	Render::FragmentShaderObject::FragmentShaderObject(const char *source, unsigned_int32 size)
	{
		shaderIdentifier = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(shaderIdentifier, 1, &source, reinterpret_cast<GLint *>(&size));
	}


	Render::GeometryShaderObject::GeometryShaderObject(const char *source, unsigned_int32 size)
	{
		shaderIdentifier = glCreateShader(GL_GEOMETRY_SHADER);
		glShaderSource(shaderIdentifier, 1, &source, reinterpret_cast<GLint *>(&size));
	}


	Render::ShaderProgramObject::ShaderProgramObject()
	{
		programIdentifier = glCreateProgram();
		programUpdateFlags = 0;

		geometryShader = nullptr;
	}

	Render::ShaderProgramObject::~ShaderProgramObject()
	{
		if (renderState.shaderProgram == this)
		{
			renderState.shaderProgram = nullptr;
			glUseProgram(0);
		}

		glDeleteProgram(programIdentifier);
	}

	void Render::ShaderProgramObject::SetVertexShader(VertexShaderObject *shader)
	{
		vertexShader = shader;
		programUpdateFlags |= kUpdateVertexShaderParameters;
		glAttachShader(programIdentifier, shader->GetShaderIdentifier());
	}

	void Render::ShaderProgramObject::SetFragmentShader(FragmentShaderObject *shader)
	{
		fragmentShader = shader;
		programUpdateFlags |= kUpdateFragmentShaderParameters;
		glAttachShader(programIdentifier, shader->GetShaderIdentifier());
	}

	void Render::ShaderProgramObject::SetGeometryShader(GeometryShaderObject *shader)
	{
		geometryShader = shader;
		programUpdateFlags |= kUpdateGeometryShaderParameters;
		glAttachShader(programIdentifier, shader->GetShaderIdentifier());
	}

	void Render::ShaderProgramObject::Activate(void)
	{
		vertexShader->Compile();
		fragmentShader->Compile();

		if (geometryShader)
		{
			geometryShader->Compile();
		}

		glBindAttribLocation(programIdentifier, 0, "attrib");
		glBindFragDataLocation(programIdentifier, 0, "fcolor");

		glLinkProgram(programIdentifier);
	}

	bool Render::ShaderProgramObject::SetProgramBinary(unsigned_int32 format, const void *data, unsigned_int32 size)
	{
		GLint	status;

		glProgramBinary(programIdentifier, format, data, size);
		glGetProgramiv(programIdentifier, GL_LINK_STATUS, &status);
		return (status != 0);
	}

	void Render::ShaderProgramObject::Preprocess(void)
	{
		vertexUniformLocation = glGetUniformLocation(programIdentifier, "vparam");
		fragmentUniformLocation = glGetUniformLocation(programIdentifier, "fparam");
		geometryUniformLocation = glGetUniformLocation(programIdentifier, "gparam");
	}

	#if C4DEBUG

		bool Render::ShaderProgramObject::GetProgramStatus(const char **error, const char **source) const
		{
			GLint	status;

			if (!vertexShader->GetShaderStatus(error, source))
			{
				return (false);
			}

			if (!fragmentShader->GetShaderStatus(error, source))
			{
				return (false);
			}

			if ((geometryShader) && (!geometryShader->GetShaderStatus(error, source)))
			{
				return (false);
			}

			glGetProgramiv(programIdentifier, GL_LINK_STATUS, &status);
			if (!status)
			{
				GLint	size;

				glGetProgramiv(programIdentifier, GL_INFO_LOG_LENGTH, &size);
				char *string = new char[size];
				glGetProgramInfoLog(programIdentifier, size, nullptr, string);
				*error = string;

				*source = nullptr;
				return (false);
			}

			return (true);
		}

		void Render::ShaderProgramObject::ReleaseProgramStatus(const char *error, const char *source) const
		{
			delete[] source;
			delete[] error;
		}

	#endif


	void Render::PixelBufferObject::Construct(void)
	{
		glGenBuffers(1, &pixelBufferIdentifier);
	}

	void Render::PixelBufferObject::Destruct(void)
	{
		glDeleteBuffers(1, &pixelBufferIdentifier);
	}

	void Render::PixelBufferObject::AllocateStorage(unsigned_int32 size)
	{
		bufferSize = size;
		glNamedBufferDataEXT(pixelBufferIdentifier, size, nullptr, GL_STREAM_READ);
	}


	void Render::Initialize(void)
	{
		renderState.imageUnit = 0;
		for (machine unit = 0; unit < kMaxTextureUnitCount; unit++)
		{
			for (machine target = 0; target < kTextureTargetCount; target++)
			{
				renderState.texture[unit][target] = 0;
			}
		}

		renderState.drawFrameBuffer = nullptr;
		renderState.readFrameBuffer = nullptr;

		renderState.vertexArrayObject = 0;
		renderState.attributeVertexBuffer = 0;
		renderState.indexVertexBuffer = 0;

		renderState.frameCount = 0;
		renderState.updateFlags = 0;
		renderState.shaderProgram = nullptr;

		for (machine a = 0; a < kMaxVertexParamCount; a++)
		{
			renderState.vertexShaderParam[a].Set(0.0F, 0.0F, 0.0F, 0.0F);
		}

		for (machine a = 0; a < kMaxFragmentParamCount; a++)
		{
			renderState.fragmentShaderParam[a].Set(0.0F, 0.0F, 0.0F, 0.0F);
		}

		for (machine a = 0; a < kMaxGeometryParamCount; a++)
		{
			renderState.geometryShaderParam[a].Set(0.0F, 0.0F, 0.0F, 0.0F);
		}
	}

	void Render::Terminate(void)
	{
	}

	void Render::InitializeCoreOpenGL(void)
	{
		glGenVertexArrays(1, &renderState.vertexArrayObject);
		glBindVertexArray(renderState.vertexArrayObject);

		quadIndexBuffer.SetVertexBufferSize(kMaxQuadPrimitiveCount * 2 * sizeof(Triangle));
		quadIndexBuffer.AllocateStorage(quadTriangle);
	}

	void Render::TerminateCoreOpenGL(void)
	{
		quadIndexBuffer.Destruct();

		glDeleteVertexArrays(1, &renderState.vertexArrayObject);
	}

	void Render::QueryTimeStamp(QueryObject *query)
	{
		unsigned_int32 index = query->queryIndex;
		query->queryIndex = (index + 1) & 3;
		query->queryFrame[index] = renderState.frameCount;

		glQueryCounter(query->queryIdentifier[index], GL_TIMESTAMP);
	}

	void Render::BeginOcclusionQuery(QueryObject *query)
	{
		unsigned_int32 index = query->queryIndex;
		query->queryIndex = (index + 1) & 3;
		query->queryFrame[index] = renderState.frameCount;

		glBeginQuery(GL_SAMPLES_PASSED, query->queryIdentifier[index]);
	}

	void Render::BeginConditionalRender(const QueryObject *query)
	{
		unsigned_int32 index = (query->queryIndex - 1) & 3;
		glBeginConditionalRender(query->queryIdentifier[index], GL_QUERY_BY_REGION_NO_WAIT);
	}

	unsigned_int64 Render::GetQueryTimeStamp(const QueryObject *query)
	{
		unsigned_int32 index = (query->queryIndex - 3) & 3;
		unsigned_int32 frame = renderState.frameCount;
		if ((unsigned_int32) (frame - query->queryFrame[index]) < 4U)
		{
			GLuint64	result;

			glGetQueryObjectui64v(query->queryIdentifier[index], GL_QUERY_RESULT, &result);
			return (result);
		}

		return (0);
	}

	unsigned_int32 Render::GetQuerySamplesPassed(const QueryObject *query)
	{
		unsigned_int32 index = (query->queryIndex - 3) & 3;
		unsigned_int32 frame = renderState.frameCount;
		if ((unsigned_int32) (frame - query->queryFrame[index]) < 4U)
		{
			GLuint		result;

			glGetQueryObjectuiv(query->queryIdentifier[index], GL_QUERY_RESULT, &result);
			return (result);
		}

		return (0);
	}

	void Render::SetShaderProgram(const ShaderProgramObject *shaderProgram)
	{
		if (renderState.shaderProgram != shaderProgram)
		{
			renderState.shaderProgram = shaderProgram;
			glUseProgram(shaderProgram->GetProgramIdentifier());

			renderState.updateFlags |= shaderProgram->GetProgramUpdateFlags();
		}
	}

	void Render::SetVertexShaderParameter(unsigned_int32 index, float x, float y, float z, float w)
	{
		renderState.vertexShaderParam[index].Set(x, y, z, w);
		renderState.updateFlags |= kUpdateVertexShaderParameters;
	}

	void Render::SetVertexShaderParameter(unsigned_int32 index, const float *v)
	{
		renderState.vertexShaderParam[index].Set(v[0], v[1], v[2], v[3]);
		renderState.updateFlags |= kUpdateVertexShaderParameters;
	}

	void Render::SetFragmentShaderParameter(unsigned_int32 index, float x, float y, float z, float w)
	{
		renderState.fragmentShaderParam[index].Set(x, y, z, w);
		renderState.updateFlags |= kUpdateFragmentShaderParameters;
	}

	void Render::SetFragmentShaderParameter(unsigned_int32 index, const float *v)
	{
		renderState.fragmentShaderParam[index].Set(v[0], v[1], v[2], v[3]);
		renderState.updateFlags |= kUpdateFragmentShaderParameters;
	}

	void Render::SetGeometryShaderParameter(unsigned_int32 index, float x, float y, float z, float w)
	{
		renderState.geometryShaderParam[index].Set(x, y, z, w);
		renderState.updateFlags |= kUpdateGeometryShaderParameters;
	}

	void Render::SetGeometryShaderParameter(unsigned_int32 index, const float *v)
	{
		renderState.geometryShaderParam[index].Set(v[0], v[1], v[2], v[3]);
		renderState.updateFlags |= kUpdateGeometryShaderParameters;
	}

	void Render::DrawIndexedPrimitives(unsigned_int32 prim, unsigned_int32 range, unsigned_int32 count, const VertexBufferObject *buffer, unsigned_int32 offset)
	{
		Update();

		GLuint identifier = buffer->vertexBufferIdentifier;
		if (renderState.indexVertexBuffer != identifier)
		{
			renderState.indexVertexBuffer = identifier;
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, identifier);
		}

		glDrawRangeElements(prim, 0, range - 1, count, GL_UNSIGNED_SHORT, (char *) nullptr + offset);
	}

	void Render::MultiDrawIndexedPrimitives(unsigned_int32 prim, const unsigned_int32 *countArray, const VertexBufferObject *buffer, const machine_address *offsetArray, unsigned_int32 size)
	{
		Update();

		GLuint identifier = buffer->vertexBufferIdentifier;
		if (renderState.indexVertexBuffer != identifier)
		{
			renderState.indexVertexBuffer = identifier;
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, identifier);
		}

		glMultiDrawElements(prim, reinterpret_cast<const GLsizei *>(countArray), GL_UNSIGNED_SHORT, reinterpret_cast<const void *const *>(offsetArray), size);
	}

	void Render::DrawQuads(unsigned_int32 start, unsigned_int32 count)
	{
		Update();

		GLuint identifier = quadIndexBuffer.vertexBufferIdentifier;
		if (renderState.indexVertexBuffer != identifier)
		{
			renderState.indexVertexBuffer = identifier;
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, identifier);
		}

		glDrawRangeElements(GL_TRIANGLES, 0, kMaxQuadPrimitiveCount * 4 - 1, (count >> 2) * 6, GL_UNSIGNED_SHORT, (char *) nullptr + (start >> 2) * 12);
	}

	void Render::Update(void)
	{
		unsigned_int32 flags = renderState.updateFlags;
		renderState.updateFlags = 0;

		const ShaderProgramObject *program = renderState.shaderProgram;
		if (program)
		{
			GLuint identifier = program->GetProgramIdentifier();

			if (flags & kUpdateVertexShaderParameters)
			{
				glProgramUniform4fvEXT(identifier, program->GetVertexUniformLocation(), kMaxVertexParamCount, &renderState.vertexShaderParam[0].x);
			}

			if (flags & kUpdateFragmentShaderParameters)
			{
				glProgramUniform4fvEXT(identifier, program->GetFragmentUniformLocation(), kMaxFragmentParamCount, &renderState.fragmentShaderParam[0].x);
			}

			if (flags & kUpdateGeometryShaderParameters)
			{
				glProgramUniform4fvEXT(identifier, program->GetGeometryUniformLocation(), kMaxGeometryParamCount, &renderState.geometryShaderParam[0].x);
			}
		}
	}

	void Render::BindTextureUnit0(GLuint texture, GLenum target)
	{
		if (renderState.imageUnit != 0)
		{
			renderState.imageUnit = 0;
			glActiveTexture(GL_TEXTURE0);
		}

		machine targetIndex = 0;
		targetIndex += ((int32) (GL_TEXTURE_2D - target) >> 31) & 1;
		targetIndex += ((int32) (GL_TEXTURE_3D - target) >> 31) & 1;
		targetIndex += ((int32) (GL_TEXTURE_RECTANGLE - target) >> 31) & 1;
		targetIndex += ((int32) (GL_TEXTURE_CUBE_MAP - target) >> 31) & 1;		// kTextureTargetCount - 1

		GLuint *object = &renderState.texture[0][targetIndex];
		if (*object != texture)
		{
			*object = texture;
			glBindTexture(target, texture);
		}
	}

	void Render::BindMultiTexture(GLenum texunit, GLenum target, GLuint texture)
	{
		int32 unitIndex = texunit - GL_TEXTURE0;
		if (renderState.imageUnit != unitIndex)
		{
			renderState.imageUnit = unitIndex;
			glActiveTexture(texunit);
		}

		glBindTexture(target, texture);
	}

	void Render::TextureParameteri(GLuint texture, GLenum target, GLenum pname, GLint param)
	{
		BindTextureUnit0(texture, target);
		glTexParameteri(target, pname, param);
	}

	void Render::TextureParameteriv(GLuint texture, GLenum target, GLenum pname, const GLint *param)
	{
		BindTextureUnit0(texture, target);
		glTexParameteriv(target, pname, param);
	}

	void Render::TextureParameterf(GLuint texture, GLenum target, GLenum pname, GLfloat param)
	{
		BindTextureUnit0(texture, target);
		glTexParameterf(target, pname, param);
	}

	void Render::TextureParameterfv(GLuint texture, GLenum target, GLenum pname, const GLfloat *param)
	{
		BindTextureUnit0(texture, target);
		glTexParameterfv(target, pname, param);
	}

	void Render::TextureImage2D(GLuint texture, GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const void *pixels)
	{
		// Taking the minimum with GL_TEXTURE_CUBE_MAP remaps all of the pseudo-targets for the
		// individual cube faces to GL_TEXTURE_CUBE_MAP since their enums have greater values.

		BindTextureUnit0(texture, Min(target, GL_TEXTURE_CUBE_MAP));
		glTexImage2D(target, level, internalformat, width, height, border, format, type, pixels);
	}

	void Render::TextureSubImage2D(GLuint texture, GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const void *pixels)
	{
		BindTextureUnit0(texture, Min(target, GL_TEXTURE_CUBE_MAP));
		glTexSubImage2D(target, level, xoffset, yoffset, width, height, format, type, pixels);
	}

	void Render::TextureImage3D(GLuint texture, GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLenum format, GLenum type, const void *pixels)
	{
		BindTextureUnit0(texture, target);
		glTexImage3D(target, level, internalformat, width, height, depth, border, format, type, pixels);
	}

	void Render::TextureSubImage3D(GLuint texture, GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const void *pixels)
	{
		BindTextureUnit0(texture, Min(target, GL_TEXTURE_CUBE_MAP));
		glTexSubImage3D(target, level, xoffset, yoffset, zoffset, width, height, depth, format, type, pixels);
	}

	void Render::CompressedTextureImage2D(GLuint texture, GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLint border, GLsizei imageSize, const void *data)
	{
		BindTextureUnit0(texture, Min(target, GL_TEXTURE_CUBE_MAP));
		glCompressedTexImage2D(target, level, internalformat, width, height, border, imageSize, data);
	}

	void Render::CompressedTextureImage3D(GLuint texture, GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLsizei imageSize, const void *data)
	{
		BindTextureUnit0(texture, target);
		glCompressedTexImage3D(target, level, internalformat, width, height, depth, border, imageSize, data);
	}

	void Render::CopyTextureSubImage2D(GLuint texture, GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint x, GLint y, GLsizei width, GLsizei height)
	{
		BindTextureUnit0(texture, Min(target, GL_TEXTURE_CUBE_MAP));
		glCopyTexSubImage2D(target, level, xoffset, yoffset, x, y, width, height);
	}

	void Render::NamedBufferData(GLuint buffer, GLsizeiptr size, const void *data, GLenum usage)
	{
		if (renderState.attributeVertexBuffer != buffer)
		{
			renderState.attributeVertexBuffer = buffer;
			glBindBuffer(GL_ARRAY_BUFFER, buffer);
		}

		glBufferData(GL_ARRAY_BUFFER, size, data, usage);
	}

	void Render::NamedBufferSubData(GLuint buffer, GLintptr offset, GLsizeiptr size, const void *data)
	{
		GLuint boundBuffer = renderState.attributeVertexBuffer;
		renderState.attributeVertexBuffer = 0;

		if (boundBuffer != buffer)
		{
			glBindBuffer(GL_ARRAY_BUFFER, buffer);
		}

		glBufferSubData(GL_ARRAY_BUFFER, offset, size, data);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	void Render::GetNamedBufferSubData(GLuint buffer, GLintptr offset, GLsizeiptr size, void *data)
	{
		GLuint boundBuffer = renderState.attributeVertexBuffer;
		renderState.attributeVertexBuffer = 0;

		if (boundBuffer != buffer)
		{
			glBindBuffer(GL_ARRAY_BUFFER, buffer);
		}

		glGetBufferSubData(GL_ARRAY_BUFFER, offset, size, data);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	void *Render::MapNamedBuffer(GLuint buffer, GLenum access)
	{
		if (renderState.attributeVertexBuffer != buffer)
		{
			renderState.attributeVertexBuffer = buffer;
			glBindBuffer(GL_ARRAY_BUFFER, buffer);
		}

		return (glMapBuffer(GL_ARRAY_BUFFER, access));
	}

	void *Render::MapNamedBufferRange(GLuint buffer, GLintptr offset, GLsizeiptr length, GLbitfield access)
	{
		if (renderState.attributeVertexBuffer != buffer)
		{
			renderState.attributeVertexBuffer = buffer;
			glBindBuffer(GL_ARRAY_BUFFER, buffer);
		}

		return (glMapBufferRange(GL_ARRAY_BUFFER, offset, length, access));
	}

	GLboolean Render::UnmapNamedBuffer(GLuint buffer)
	{
		GLuint boundBuffer = renderState.attributeVertexBuffer;
		renderState.attributeVertexBuffer = 0;

		if (boundBuffer != buffer)
		{
			glBindBuffer(GL_ARRAY_BUFFER, buffer);
		}

		GLboolean result = glUnmapBuffer(GL_ARRAY_BUFFER);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		return (result);
	}

	void *Render::MapBufferRange(GLenum target, GLintptr offset, GLsizeiptr length, GLbitfield access)
	{
		return (glMapBuffer(target, GL_WRITE_ONLY));
	}

	void Render::ProgramUniform4fv(GLuint program, GLint location, GLsizei count, const GLfloat *param)
	{
		if ((renderState.shaderProgram) && (renderState.shaderProgram->GetProgramIdentifier() != program))
		{
			renderState.shaderProgram = nullptr;
			glUseProgram(program);
		}

		glUniform4fv(location, count, param);
	}

	void Render::VertexArrayVertexAttribOffset(GLuint vaobj, GLuint buffer, GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, GLintptr offset)
	{
		// Ignoring vertex array object until more than one is used.

		if (renderState.attributeVertexBuffer != buffer)
		{
			renderState.attributeVertexBuffer = buffer;
			glBindBuffer(GL_ARRAY_BUFFER, buffer);
		}

		glVertexAttribPointer(index, size, type, normalized, stride, (char *) nullptr + offset);
	}

	void Render::InvalidateTexImage(GLuint texture, GLint level)
	{
	}

	void Render::InvalidateBufferData(GLuint buffer)
	{
	}

	void Render::InvalidateFramebuffer(GLenum target, GLsizei numAttachments, const GLenum *attachments)
	{
	}

#elif C4ORBIS //[ 

			// -- Orbis code hidden --

#elif C4PS3 //[ 

			// -- PS3 code hidden --

#endif //]

// ZYUTNLM
