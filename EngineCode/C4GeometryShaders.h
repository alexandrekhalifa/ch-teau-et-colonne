//=============================================================
//
// C4 Engine version 3.5.1
// Copyright 1999-2013, by Terathon Software LLC
//
// This copy is licensed to the following:
//
//     Registered user: Université de Sherbrooke
//     Maximum number of users: Site License
//     License #C4T0033897
//
// License is granted under terms of the license agreement
// entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#ifndef C4GeometryShaders_h
#define C4GeometryShaders_h


#include "C4Programs.h"


namespace C4
{
	enum
	{
		kGeometryParamMatrixMVP					= 0		// Model-view-projection matrix

		// Render::kMaxGeometryParamCount
	};


	#define GEOMETRY_PARAM_MATRIX_MVP0				"0"
	#define GEOMETRY_PARAM_MATRIX_MVP1				"1"
	#define GEOMETRY_PARAM_MATRIX_MVP2				"2"
	#define GEOMETRY_PARAM_MATRIX_MVP3				"3"

	#define GEOMETRY_PARAM_COUNT					"4"


	class GeometryShader : public Render::GeometryShaderObject, public Shared, public HashTableElement<GeometryShader>
	{
		public:

			typedef ShaderSignature KeyType;

		private:

			static Storage<HashTable<GeometryShader>>	hashTable;

			unsigned_int32		shaderSignature[1];

			GeometryShader(const char *source, unsigned_int32 size, const unsigned_int32 *signature);
			~GeometryShader();

		public:

			static const char		extrudeNormalLine[];

			KeyType GetKey(void) const
			{
				return (ShaderSignature(shaderSignature));
			}

			static void Initialize(void);
			static void Terminate(void);

			static unsigned_int32 Hash(const KeyType& key);

			static GeometryShader *Find(const unsigned_int32 *signature);
			static GeometryShader *New(const char *source, unsigned_int32 size, const unsigned_int32 *signature);

			static void ReleaseCache(void);
	};
}


#endif

// ZYUTNLM
