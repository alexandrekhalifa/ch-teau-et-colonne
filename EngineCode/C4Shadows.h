//=============================================================
//
// C4 Engine version 3.5.1
// Copyright 1999-2013, by Terathon Software LLC
//
// This copy is licensed to the following:
//
//     Registered user: Université de Sherbrooke
//     Maximum number of users: Site License
//     License #C4T0033897
//
// License is granted under terms of the license agreement
// entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#ifndef C4Shadows_h
#define C4Shadows_h


#include "C4Renderable.h"


namespace C4
{
	class Light;
	class InfiniteLight;
	class PointLight;
	class Geometry;


	class StencilData : public StencilShadow
	{
		private:

			Geometry		*shadowGeometry;

		public:

			StencilData(Geometry *geometry);

			Geometry *GetGeometry(void) const
			{
				return (shadowGeometry);
			}

			void CalculateInfiniteShadowBounds(const InfiniteLight *light);
			void CalculatePointShadowBounds(const PointLight *light);
	};


	class StencilVolume : public StencilData, public ListElement<StencilVolume>, public LinkTarget<StencilVolume>
	{
		private:

			Light		*targetLight;

			int32		extrusionDetailLevel;
			int32		endcapDetailLevel;

		public:

			StencilVolume(Geometry *geometry, Light *light, Link<StencilVolume> *link);
			~StencilVolume();

			Light *GetLight(void) const
			{
				return (targetLight);
			}

			int32 GetExtrusionDetailLevel(void) const
			{
				return (extrusionDetailLevel);
			}

			void SetExtrusionDetailLevel(int32 level)
			{
				extrusionDetailLevel = level;
			}

			int32 GetEndcapDetailLevel(void) const
			{
				return (endcapDetailLevel);
			}

			void SetEndcapDetailLevel(int32 level)
			{
				endcapDetailLevel = level;
			}
	};
}


#endif

// ZYUTNLM
