//=============================================================
//
// C4 Engine version 3.5.1
// Copyright 1999-2013, by Terathon Software LLC
//
// This copy is licensed to the following:
//
//     Registered user: Université de Sherbrooke
//     Maximum number of users: Site License
//     License #C4T0033897
//
// License is granted under terms of the license agreement
// entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#include "C4Adjusters.h"
#include "C4Random.h"
#include "C4Time.h"


using namespace C4;


Adjuster::Adjuster(AdjusterType type)
{
	adjusterType = type;

	targetCamera = nullptr;
}

Adjuster::Adjuster(const Adjuster& adjuster)
{
	adjusterType = adjuster.adjusterType;

	targetCamera = nullptr;
}

Adjuster::~Adjuster()
{
}


ShakeAdjuster::ShakeAdjuster() : Adjuster(kAdjusterShake)
{
}

ShakeAdjuster::ShakeAdjuster(float maxIntensity) : Adjuster(kAdjusterShake)
{
	maxShakeIntensity = maxIntensity;

	shakeIntensity = 0.0F;
	shakeDuration = 1.0F;
	shakeTime = 1.0F;
}

ShakeAdjuster::ShakeAdjuster(const ShakeAdjuster& shakeAdjuster) : Adjuster(shakeAdjuster)
{
	maxShakeIntensity = shakeAdjuster.maxShakeIntensity;

	shakeIntensity = 0.0F;
	shakeDuration = 1.0F;
	shakeTime = 1.0F;
}

ShakeAdjuster::~ShakeAdjuster()
{
}

Adjuster *ShakeAdjuster::Replicate(void) const
{
	return (new ShakeAdjuster(*this));
}

void ShakeAdjuster::CalculateAdjustmentTransform(Transform4D *transform)
{
	float dt = TheTimeMgr->GetFloatDeltaTime();

	float time = shakeTime + dt;
	if (time < shakeDuration)
	{
		shakeTime = time;

		float phase = shakePhase + shakeFrequency * dt;
		if (phase < K::tau)
		{
			shakePhase = phase;
		}
		else
		{
			NewShakeAxis();
			phase = Fmin(shakeFrequency * dt, K::tau_over_2);
		}

		float angle = Sin(phase) * shakeIntensity * (1.0F - time / shakeDuration);
		transform->SetRotationAboutAxis(angle, shakeAxis);
	}
	else
	{
		shakeIntensity = 0.0F;
		shakeDuration = 1.0F;
		shakeTime = 1.0F;

		transform->SetIdentity();
		CallCompletionProc();
	}
}

void ShakeAdjuster::NewShakeAxis(void)
{
	shakeAxis = Math::RandomUnitVector3D();
	shakeFrequency = (Math::RandomFloat(0.01F) + 0.01F) * K::tau;
}

void ShakeAdjuster::Shake(float intensity, int32 duration)
{
	intensity = Fmin(intensity, maxShakeIntensity); 
	if (intensity > shakeIntensity * (1.0F - shakeTime / shakeDuration))
	{
		shakeIntensity = intensity; 
		shakeDuration = (float) duration;
		shakeTime = 0.0F; 
		shakePhase = 0.0F; 

		NewShakeAxis();
	}
} 

void ShakeAdjuster::Reset(void)
{
	shakeIntensity = 0.0F;
	shakeDuration = 1.0F; 
	shakeTime = 1.0F;
}

// ZYUTNLM
