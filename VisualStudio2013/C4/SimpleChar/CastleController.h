#pragma once

#include "../../../GameCode/SimpleChar.h"

namespace C4
{
	class CastleController : public Controller
	{
	private:
		float heightMax_;

		CastleController(const CastleController& castleController);

		Controller *Replicate(void) const override;

	public:

		CastleController();
		~CastleController();

		static bool ValidNode(const Node *node);

		void Preprocess(void) override;

		void Activate(Node *initiator, Node *trigger) override;
	};
}