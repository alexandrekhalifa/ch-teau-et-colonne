#pragma once

namespace C4
{
	class AIController;
	class State;
	class Machine
	{
	private:
		State *current_;
		AIController *controller_;
	public:
		Machine(AIController *controller);
		void setCurrent(State *s);
		void virtual execute();
		State* getCurrentState();
		AIController* getController();
		~Machine();
	};

	class State
	{
	public:
		virtual void transfert(Machine *m, State *s)
		{
			m->setCurrent(s);
			delete this;
		}
		virtual void testTransfert(Machine *m) = 0;
		virtual void execute(Machine *m) = 0;
	};

	class Idle
		: public State
	{
		void testTransfert(Machine *m);
		void execute(Machine *m);
	};

	class Orientation
		: public State
	{
		void testTransfert(Machine *m);
		void execute(Machine *m);
	};

	class Move
		: public State
	{
		void testTransfert(Machine *m);
		void execute(Machine *m);
	};

	class Shoot
		: public State
	{
		void testTransfert(Machine *m);
		void execute();
	};

}