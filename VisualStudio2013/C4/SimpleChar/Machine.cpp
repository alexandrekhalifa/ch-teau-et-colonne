#include "Machine.h"
#include "AIController.h"
#include "TriggerController.h"
#include "CastleController.h"
#include <assert.h>
#include <math.h>

using namespace C4;

Machine::Machine(AIController *controller) : current_(new Move()), controller_(controller)
{
}

void Machine::setCurrent(State *s)
{
	current_ = s;
}

void Machine::execute()
{
	current_->testTransfert(this);
	current_->execute(this);
}

State* Machine::getCurrentState()
{
	return current_;
}

AIController* Machine::getController()
{
	return controller_;
}

Machine::~Machine()
{
	delete current_;
	delete controller_;
}


// GESTION ETAT IDLE

void Idle::testTransfert(Machine *m)
{
	/*Node* currentAiNode = m->getController()->GetTargetNode();
	Node* targetNode = currentAiNode->GetConnectedNode("targetPlayer");
	if (sqrt(pow((currentAiNode->GetNodePosition() - targetNode->GetNodePosition()).x,2) +
		pow((currentAiNode->GetNodePosition() - targetNode->GetNodePosition()).y,2) +
		pow((currentAiNode->GetNodePosition() - targetNode->GetNodePosition()).z,2)) > 4)
	{*/
		transfert(m, new Orientation());
	
}

void Idle::execute(Machine *m)
{
 // Do nothing
}

// GESTION ETAT ORIENTATION

void Orientation::testTransfert(Machine *m)
{
	/*Node* currentAiNode = m->getController()->GetTargetNode();
	Node* targetNode = currentAiNode->GetConnectedNode("targetPlayer");
	Vector3D wantedOrientation = currentAiNode->GetNodePosition() - targetNode->GetNodePosition();
	Vector3D currentOrientation = currentAiNode->get*/
	transfert(m, new Move());
}

void Orientation::execute(Machine *m)
{
	//Do nothing
}

// GESTION ETAT MOVE

void Move::testTransfert(Machine *m)
{

}

void Move::execute(Machine *m)
{
	
	
	//Node* currentAiNode = m->getController()->GetTargetNode();
	//Transform4D currentTransform = currentAiNode->GetWorldTransform();
	//
	//Vector3D direction = Vector3D(1, 0, 0) * currentTransform;

	//currentAiNode->SetNodePosition(currentAiNode->GetNodePosition() + direction/1000);

	////Vector3D direction2 = Vector3D(0, 0, 1) * currentTransform;
	//
	//////m->getController()->SetExternalForce(direction);
	////m->getController()->SetAngularVelocity(direction2 / 5);
	//////m->getController()->SetRigidBodyPosition(m->getController()->GetFinalNodePosition() + direction/100);



	//m->getController()->Invalidate();
	//m->getController()->GetTargetNode()->Invalidate();
}

// GESTION ETAT SHOOT

void Shoot::testTransfert(Machine *m)
{

}

void Shoot::execute()
{

}