//=============================================================
//
// C4 Engine version 3.5.1
// Copyright 1999-2013, by Terathon Software LLC
//
// This file is part of the C4 Engine and is provided under the
// terms of the license agreement entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#include "MGGame.h"


using namespace C4;


const char C4::kConnectorKeyTeleporter[] = "Teleporter";
const char C4::kConnectorKeyJump[] = "Jump";


C4::Application *ConstructApplication(void)
{
	return (new Game);
}

// ZYUTNLM
