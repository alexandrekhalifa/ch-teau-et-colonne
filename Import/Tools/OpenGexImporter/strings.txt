Entry (id = 'ERRR') {string {"OpenGEX Import Error"}}
Entry (id = 'EMES') {string {"The scene import operation failed because the following error occurred:"}}
Entry (id = 'LINE') {string {"Line "}}
Entry (id = 'OKAY') {string {"OK"}}

Entry (id = 'OGEX')
{
	Entry (id = 'ivud') {string {"(OpenGEX) Invalid up direction"}}
	Entry (id = 'ivtk') {string {"(OpenGEX) Invalid translation kind"}}
	Entry (id = 'ivrk') {string {"(OpenGEX) Invalid rotation kind"}}
	Entry (id = 'ivsk') {string {"(OpenGEX) Invalid scale kind"}}
	Entry (id = 'dlod') {string {"(OpenGEX) Duplicate level of detail"}}
	Entry (id = 'mlsk') {string {"(OpenGEX) Inconsistent use of skin across levels of detail"}}
	Entry (id = 'udlt') {string {"(OpenGEX) Undefined light type"}}
	Entry (id = 'udab') {string {"(OpenGEX) Undefined attribute"}}
	Entry (id = 'udcv') {string {"(OpenGEX) Undefined curve type"}}
	Entry (id = 'udan') {string {"(OpenGEX) Undefined attenuation kind"}}
	Entry (id = 'dpva') {string {"(OpenGEX) Duplicate vertex array"}}
	Entry (id = 'parq') {string {"(OpenGEX) Vertex array with \"position\" attribute required"}}
	Entry (id = 'vcus') {string {"(OpenGEX) Unsupported vertex count (must be less than 65536)"}}
	Entry (id = 'ivus') {string {"(OpenGEX) Unsupported index value (must be less than 65536)"}}
	Entry (id = 'iarq') {string {"(OpenGEX) Index array required"}}
	Entry (id = 'vcmm') {string {"(OpenGEX) Vertex count mismatch"}}
	Entry (id = 'bcmm') {string {"(OpenGEX) Bone count mismatch"}}
	Entry (id = 'bwcm') {string {"(OpenGEX) Bone weight count mismatch"}}
	Entry (id = 'ivbr') {string {"(OpenGEX) Invalid bone reference"}}
	Entry (id = 'ivor') {string {"(OpenGEX) Invalid object reference"}}
	Entry (id = 'ivmr') {string {"(OpenGEX) Invalid material reference"}}
	Entry (id = 'mius') {string {"(OpenGEX) Unsupported material index (must be less than 256)"}}
	Entry (id = 'dprf') {string {"(OpenGEX) Duplicate material reference"}}
	Entry (id = 'msrf') {string {"(OpenGEX) Missing material reference"}}
	Entry (id = 'trnl') {string {"(OpenGEX) Target reference not local"}}
	Entry (id = 'ivst') {string {"(OpenGEX) Invalid target structure"}}
	Entry (id = 'ivkk') {string {"(OpenGEX) Invalid key kind for curve"}}
	Entry (id = 'ivct') {string {"(OpenGEX) Invalid curve type"}}
	Entry (id = 'kycm') {string {"(OpenGEX) Key count mismatch"}}
	Entry (id = 'emky') {string {"(OpenGEX) Empty key structure"}}
}
