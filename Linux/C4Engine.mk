#--------------------------------------------------------
#
# C4Engine
#
#--------------------------------------------------------

TARGETNAME			:= C4Engine

include Include.mk

INCDIR				:= -I../EngineCode $(COMMON_INCS)
CFLAGS				:= $(INCDIR) $(COMMON_DEFINES) -DC4ENGINEMODULE $(COMMON_CFLAGS) $(WARNINGS)
LFLAGS				:= $(COMMON_LFLAGS) -rdynamic 
LIBS				:= $(COMMON_LIBS) -lGL -lX11 -lXrandr -lasound

#--------------------------------------------------------

BASE				:= C4Base.cpp C4Memory.cpp
CONTROLLERS			:= C4Controller.cpp C4Expressions.cpp C4Methods.cpp C4Scripts.cpp C4Values.cpp
EFFECTS				:= C4Effects.cpp C4Emitters.cpp C4Markings.cpp C4Particles.cpp C4Shafts.cpp
GEOMETRY			:= C4Geometries.cpp C4GeometryObjects.cpp C4Mesh.cpp C4Primitives.cpp C4Shadows.cpp \
					   C4Terrain.cpp C4Topology.cpp
GRAPHICS			:= C4Attributes.cpp C4Bounding.cpp C4CameraObjects.cpp C4FragmentShaders.cpp C4GeometryShaders.cpp \
					   C4Graphics.cpp C4Horizon.cpp C4LightObjects.cpp C4MaterialObjects.cpp C4OpenGL.cpp C4Processes.cpp \
					   C4Programs.cpp C4Quads.cpp C4Render.cpp C4Renderable.cpp C4Shaders.cpp C4SpaceObjects.cpp \
					   C4Textures.cpp C4VertexShaders.cpp C4Voxels.cpp
INTERFACE			:= C4ColorPicker.cpp C4Configurable.cpp C4Configuration.cpp C4Dialog.cpp C4FilePicker.cpp \
					   C4Fonts.cpp C4Interface.cpp C4Logo.cpp C4Menus.cpp C4Movies.cpp C4Mutators.cpp C4Paint.cpp \
					   C4Panels.cpp C4ToolWindows.cpp C4Viewports.cpp C4Widgets.cpp
MATH				:= C4ColorRGBA.cpp C4Computation.cpp C4Constants.cpp C4Hull.cpp C4Math.cpp C4Matrix3D.cpp \
					   C4Matrix4D.cpp C4Quaternion.cpp C4Random.cpp C4Vector2D.cpp C4Vector3D.cpp C4Vector4D.cpp
OBJECTS				:= C4Objects.cpp C4Volumes.cpp
PHYSICS				:= C4Blockers.cpp C4Character.cpp C4Contacts.cpp C4Deformable.cpp C4Fields.cpp C4Forces.cpp C4Inertia.cpp \
					   C4Movement.cpp C4Physics.cpp C4Shapes.cpp C4Water.cpp
PLUGINS				:= C4Formats.cpp C4Plugins.cpp
SYSTEM				:= C4Application.cpp C4AudioCapture.cpp C4Commands.cpp C4Compression.cpp C4ConfigData.cpp C4Display.cpp \
					   C4Engine.cpp C4Files.cpp C4Image.cpp C4Input.cpp C4Main.cpp C4Messages.cpp C4Network.cpp C4OpenDDL.cpp \
					   C4Packing.cpp C4Resources.cpp C4Sound.cpp C4StringTable.cpp C4Threads.cpp C4Time.cpp C4Types.cpp \
					   C4Variables.cpp
UTILITIES			:= C4Graph.cpp C4Hash.cpp C4List.cpp C4Map.cpp C4String.cpp C4Tree.cpp
WORLD				:= C4Adjusters.cpp C4Animation.cpp C4Cameras.cpp C4Cell.cpp C4Connector.cpp C4Impostors.cpp C4Instances.cpp \
					   C4Lights.cpp C4Manipulator.cpp C4Markers.cpp C4Models.cpp C4Modifiers.cpp C4Node.cpp C4Paths.cpp \
					   C4Portals.cpp C4Properties.cpp C4Regions.cpp C4Skybox.cpp C4Sources.cpp C4Spaces.cpp C4Triggers.cpp \
					   C4World.cpp C4Zones.cpp

#--------------------------------------------------------

SRCS				:= $(addprefix EngineCode/,$(BASE) $(CONTROLLERS) $(EFFECTS) $(GEOMETRY) $(GRAPHICS) $(INTERFACE) $(MATH) $(OBJECTS) $(PHYSICS) $(PLUGINS) $(SYSTEM) $(UTILITIES) $(WORLD))
DEBUG_OBJS			:= $(patsubst %.cpp,Debug/%.o,$(SRCS))
OPTIMIZED_OBJS		:= $(patsubst %.cpp,Optimized/%.o,$(SRCS))

#--------------------------------------------------------

debug: debug_dir debug_pch Debug/$(TARGETNAME)
	cp Debug/$(TARGETNAME) ../$(TARGETNAME)

optimized: optimized_dir optimized_pch Optimized/$(TARGETNAME)
	cp Optimized/$(TARGETNAME) ../$(TARGETNAME)

debug_dir:
	mkdir -p Debug/EngineCode

optimized_dir:
	mkdir -p Optimized/EngineCode

debug_pch:
	gcc $(CFLAGS) $(DEBUG_CFLAGS) -x c++-header -c ../EngineCode/C4PrefixLinux.h -o Debug/C4PrefixLinux.h.gch

optimized_pch:
	gcc $(CFLAGS) $(OPTIMIZED_CFLAGS) -x c++-header -c ../EngineCode/C4PrefixLinux.h -o Optimized/C4PrefixLinux.h.gch

Debug/$(TARGETNAME): $(DEBUG_OBJS)
	gcc $(LFLAGS) -o $@ $(DEBUG_OBJS) $(LIBS)

Optimized/$(TARGETNAME): $(OPTIMIZED_OBJS)
	gcc $(LFLAGS) -o $@ $(OPTIMIZED_OBJS) $(LIBS)

Debug/%.o: ../%.cpp
	gcc -IDebug $(CFLAGS) $(DEBUG_CFLAGS) -c $< -o $@

Optimized/%.o: ../%.cpp
	gcc -IOptimized $(CFLAGS) $(OPTIMIZED_CFLAGS) -c $< -o $@
